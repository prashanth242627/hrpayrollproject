<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (isset($_GET['bid'])) {
  $getParam="bid=".$_GET['bid'];
}
else if (isset($_GET['ename'])) {
  $getParam="ename=".ltrim($_GET['ename']);
}
else if (strpos($_SERVER['REQUEST_URI'], '/applications/hrpayroll/paymentModes/All') !== false && !isset($_GET['payment_mode'])) {
  $getParam="type=All";
}
else if (isset($_GET['payment_mode']) && isset($_GET['type'])) {
  $getParam="type=".$_GET['type']."&payment_mode=".$_GET['payment_mode'];
}
elseif (isset($_GET['payment_mode'])) {
  $getParam="payment_mode=".$_GET['payment_mode'];
}
elseif (isset($_GET['type'])) {
  $getParam="type=".$_GET['type'];
}
?>

      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->

                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Employee Salary Payment Mode </div>
                     <div class="panel-body">
                     <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-3 bgc-2">
                              <p class="text-center"><b>Department</b></p>
                              <select name="account" class="form-control m-b" id="line_of_activity_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], '/applications/hrpayroll/paymentModes/All') !== false) { ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php echo $activity["dept_name"]; ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                           <div class="form-group col-md-3 bgc-3">
                              <p class="text-center"><b>Payment Mode</b></p>
                              <select name="account" class="form-control m-b" id="payment_mode_id" >
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], '/applications/hrpayroll/paymentModes/All') !== false) { ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($payment_modes as $payment_mode) { ?>
                                       <option value="<?php echo $payment_mode["payment_type"]; ?>" <?php if (isset($_GET['payment_mode']) && $_GET['payment_mode']==$payment_mode['payment_type']) { ?> selected <?php } ?> > <?php echo $payment_mode['payment_type'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                            <div class="form-group col-md-3 bgc-4">
                              <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.B ID</b></p>
                              <select name="account" class="form-control m-b" id="emp_bid_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], '/applications/hrpayroll/paymentModes/All') !== false) { ?> selected <?php } ?> >All</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo $bid['emp_bid']; ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']){  ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                            </div>
                            <div class="form-group col-md-3 bgc-5">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.Name</b></p>

                              <select name="account" class="form-control m-b" id="emp_name"> 
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], '/applications/hrpayroll/paymentModes/All') !== false) { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo $name['emp_name']; ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) {  ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>
                   
                           </div>
                        </form>
                        <div class="row input-padding">
                          <div class="form-group col-md-4">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Selection Criteria: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" value="<?php if(strpos($_SERVER['REQUEST_URI'], '/applications/hrpayroll/paymentModes/All') !== false){ echo "All"; } else if(isset($_GET['type'])){  echo $_GET['type']; } else if(isset($_GET['payment_mode'])){ echo $_GET['payment_mode']; } 
                                 else if(isset($_GET['bid'])){echo  $_GET['bid']; } 
                                 else if(isset($_GET['ename'])){ echo $_GET['ename']; } ?>" placeholder="Designation Name" readonly class="form-control p-0">
                              </div>
                           </div>
                           <div class="form-group col-md-4">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Employee Count: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" id="employee_count" value="" placeholder="Designation Name" readonly class="form-control p-0">
                              </div>
                           </div>
                         </div>
                      

                        <!-- START table-responsive-->
                     <div class="table-responsive fixtable"">
                       <table class="table  table-bordered table-hover" id="fixTable">
                        <thead>
                          <tr>
                            <td>S.No</td>
                            <td>E ID</td>
                            <td>E BID</td>
                            <td>E Name</td>
                            <td>D.O.J</td>
                            <td>Designation</td>
                            <td>Department</td>
                            <td>Bank A/c Number</td>
                            <td>Payment Mode</td>
                            <td>Status</td>
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                         
                         <?php if (isset($employees)) {
                           $i=1;
                           foreach ($employees as $employee) { 
                           ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $employee['emp_id'] ?></td>
                           <td><?php echo $employee['emp_bid'] ?></td>
                           <td><?php echo $employee['emp_name'] ?></td>
                           <td><?php echo $employee['date_of_join'] ?></td>
                           <td><?php echo $employee['designation'] ?></td>
                           <td><?php echo $employee['dept_name'] ?></td>
                           <td><?php echo $employee['acc_no'] ?></td>
                           <td><?php echo $employee['payment_type'] ?></td>
                           <?php if (isset($employee['flag'])) { ?>
                              <td>InActive</td>
                           <?php }else{ ?>
                           <td>Active</td> <?php } ?>
                         </tr>
                        <?php }} else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                           <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>

   <script type="text/javascript" >
      var base_url="<?php echo base_url(); ?>";
      $("#employee_count").val(<?php echo $i; ?>);
     $("#emp_bid_id").on("change",function(){
        $("#payment_mode_id").val("");
        $("#line_of_activity_id").val("");
        $("#emp_name").val("");
      });
      $("#line_of_activity_id").on("change",function(){
        $("#payment_mode_id").val("");
        $("#emp_bid_id").val("");
        $("#emp_name").val("");
      });
      $("#payment_mode_id").on("change",function(){
        $("#emp_bid_id").val("");
        $("#emp_name").val("");
      });
      $("#emp_name").on("change",function(){
        $("#payment_mode_id").val("");
        $("#line_of_activity_id").val("");
        $("#emp_bid_id").val("");
      });
      
        $("select[name=account]").on("change",function(){
          var line_of_activity= $("#line_of_activity_id").val();
        var payment_mode= $("#payment_mode_id").val();
        var emp_bid= $("#emp_bid_id").val(); 
        var emp_name=$('#emp_name').val();
          if (emp_bid!="") {
            window.location.replace(base_url+"paymentModes/activity?bid="+emp_bid);
          }
          else if (emp_name!="" && emp_name!=null) {
            window.location.replace(base_url+"paymentModes/activity?ename="+emp_name);
          }
          else if (line_of_activity!="" && payment_mode=="All") {
            window.location.replace(base_url+"paymentModes/activity?type="+line_of_activity);
          }
          else if (line_of_activity=="All" && payment_mode!="") {
            window.location.href=base_url+"paymentModes/activity?payment_mode="+payment_mode;
          }
          else if (line_of_activity!="" && payment_mode!="") {
            window.location.replace(base_url+"paymentModes/activity?type="+line_of_activity+"&payment_mode="+payment_mode);
          }
          else if (line_of_activity=="All") {
            window.location.replace(base_url+"paymentModes/All");
          }
          else if (payment_mode=="All") {
            window.location.replace(base_url+"paymentModes/All");
          }
          else if (line_of_activity!="" && payment_mode=="") {
            window.location.replace(base_url+"paymentModes/activity?type="+line_of_activity);
          }
          else if (payment_mode!="") {
           window.location.replace(base_url+"paymentModes/activity?payment_mode="+payment_mode);
          }
        });
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/paymentModes?"+"<?php echo $getParam; ?>";
      }
      $("#reports_ul").addClass("nav collapse in");
      $("#pm_li").addClass("active");
   </script>
</body>


</html>