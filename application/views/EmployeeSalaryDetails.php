<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (isset($_GET['to']) && !isset($_GET['from'])) {
   $getParam="to=".$_GET['to'];
}
else if (isset($_GET['from']) && isset($_GET['to'])) {
   $getParam="to=".$_GET['to']."&from=".$_GET['from'];
}
if (isset($_GET['type']) && !isset($_GET['payment_mode'])) {
   $getParam="type=".$_GET['type'];
}
else if (isset($_GET['type']) && isset($_GET['payment_mode'])) {
   $getParam="type=".$_GET['type']."&payment_mode=".$_GET['payment_mode'];
}

?>

      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->

                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Employee CTS Details  </div>
                     <div class="panel-body" style="overflow-x: scroll;">
                     <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-3 bgc-2">
                              <p class="text-center"><b>Department</b></p>
                              <select name="account" class="form-control m-b" id="line_of_activity_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/Salaries/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php echo $activity["dept_name"]; ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { $getParam="type=".$_GET['type']; ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                           <div class="form-group col-md-3 bgc-3">
                              <p class="text-center"><b>Payment Mode</b></p>
                              <select name="account" class="form-control m-b" id="payment_mode_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/Salaries/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($payment_modes as $payment_mode) { ?>
                                       <option value="<?php echo $payment_mode["payment_type"]; ?>" <?php if (isset($_GET['payment_mode']) && $_GET['payment_mode']==$payment_mode['payment_type']) { $getParam="payment_mode=".$_GET['payment_mode']; ?> selected <?php } ?> > <?php echo $payment_mode['payment_type'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                            <div class="form-group col-md-3 bgc-4">
                          <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.B ID</b></p>
                              <select name="account" class="form-control m-b" onchange="location = this.value;">
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('Salaries/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/Salaries/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo base_url('Salaries/Activity?bid='.$bid['emp_bid']); ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { $getParam="bid=".$_GET['bid']; ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                            </div>
                            <div class="form-group col-md-3 bgc-5">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.Name</b></p>

                              <select name="account" class="form-control m-b" onchange="location = this.value;"> 
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('Salaries/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/Salaries/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo base_url('Salaries/Activity?ename='.$name['emp_name']); ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { $getParam="ename=".$_GET['ename']; ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>


                           <div class="form-group col-md-4 bgc-3" style="display: none;">
                              <p style="margin: 0px;">
                              </p><div class="col-md-6 p-0">
                              <p class="text-center">Date From</p>
                              <input type="date" class="form-control" id="start_date" value="<?php if(isset($_GET['from'])){  echo $_GET['from']; } ?>">
                              </div>
                               <div class="col-md-6 p-0">
                              <p class="text-center">Date To</p>
                              <input type="date" class="form-control" id="end_date" value="<?php if(isset($_GET['to'])){ echo $_GET['to']; } ?>">
                              </div>
                              <p></p>
                           </div>
                           <!-- <div class="form-group col-md-2 bgc-2">
                              <p class="text-center">LINE OF ACTIVITY</p>
                              <select name="account" class="form-control m-b">
                                 <option> All</option>
                              </select>
                           </div> -->
                   
                           </div>
                        </form>
                      <div class="row input-padding">
                            <div class="form-group col-md-2">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Selection Criteria: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="Department Name" value="<?php if(isset($_GET['type'])){ echo $_GET['type']; }
                                 else if(isset($_GET['bid'])){ echo 'Emp Bid :'. $_GET['bid']; } else if(isset($_GET['ename'])){ echo 'Emp Name :'. $_GET['emp_name']; } ?>" readonly placeholder="Designation Name" class="form-control p-0">
                              </div>
                           </div>
                          <div class="form-group col-md-3 p-0 pl-6">
                              <label class="col-lg-4 control-label p-0 text-right pt-9">Payment Mode: </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" value="<?php if(isset($_GET['payment_mode'])){ echo $_GET['payment_mode']; } ?>" placeholder="Bank,Cheque" readonly class="form-control p-0">
                              </div>
                           </div>
                           <div class="form-group col-md-2 p-0 pl-6" >
                              <label class="col-lg-4 control-label p-0 text-right pt-9">EMP Count: </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" id="emp_count" value="" placeholder="ABBR" readonly class="form-control p-0">
                              </div>
                           </div>
                           <div class="form-group col-md-2 p-0 pl-6" >
                              <label class="col-lg-4 control-label p-0 text-right pt-9">Fixed Pay(Rs): </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" id="total_fixed" value="" readonly placeholder="ABBR" class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-2 p-0 pl-6" >
                              <label class="col-lg-4 control-label p-0 text-right pt-9">CTS(Rs): </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" id="total_ctc" value="" readonly placeholder="ABBR" class="form-control p-0">
                              </div>
                           </div>
                        </div>
                        <!-- START table-responsive-->
                     <div class="table-responsive fixtable">
                       <table class="table  table-bordered table-hover" id="fixTable">
                        <thead>
                          <tr>
                            <td>S.No</td>
                            <td>E ID</td>
                            <td>E BID</td>
                            <td>E Name</td>
                            <td>D.O.J</td>
                            <td>Designation</td>
                            <td>Department</td>
                            <td>Bank A/c Number</td>
                            <td>Payment Mode</td>
                            <td>UAN</td>
                            <td>PF Number</td>
                            <td>ESI Number</td>
                            <td>Basic(Rs)</td>
                            <td>INCR(Rs)</td>
                            <td>D.A(Rs)</td>
                            <td>H.R.A(Rs)</td>
                            <td>C.C.A(Rs)</td>
                            <td>Other allowances(Rs)</td>
                            <td>Head/HOD/Coordinator allowances(Rs)</td>
                            <td>Special Allowances(Rs)</td>
                            <td>Fixed Pay(Rs)</td>
                            <td>Employer PF Contribution(Rs)</td>
                            <td>CTS(Rs)</td>
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                         
                         <?php if (isset($employees)) {
                           $i=1;
                           $total=0;
                           foreach ($employees as $employee) { 
                            $total+=round($employee['CTC']);
                            $gross+=round($employee['gross']);
                           ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $employee['temp_emp_id'] ?></td>
                           <td><?php echo $employee['emp_bid'] ?></td>
                           <td><?php echo $employee['emp_name'] ?></td>
                           <td><?php echo $employee['date_of_join'] ?></td>
                           <td><?php echo $employee['designation_name'] ?></td>
                           <td><?php echo $employee['dept_name'] ?></td>
                           <td><?php echo $employee['acc_no'] ?></td>
                           <td><?php echo $employee['payment_type'] ?></td>
                           <td><?php echo $employee['uan'] ?></td>
                            <td><?php echo $employee['pf_no'] ?></td>
                            <td><?php echo $employee['esi_no'] ?></td>
                            <td><?php echo round($employee['basic']) ?></td>
                            <td><?php echo $employee['incr'] ?></td>
                            <td><?php echo $employee['da'] ?></td>
                            <td><?php echo $employee['hra'] ?></td>
                            <td><?php echo $employee['cca'] ?></td>
                            <td><?php echo round($employee['other_allowance']) ?></td>
                            <td><?php echo $employee['co_allowance'] ?></td>
                            <td><?php echo round($employee['special_allowance'])?></td>
                            <td><?php echo round($employee['gross']) ?></td>
                            <td><?php echo round($employee['empr_pf_contribution']) ?></td>
                            <td><?php echo round($employee['CTC']) ?></td>
                         </tr>
                        <?php } ?> <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total</td>
                            <td><?php echo round($gross); ?></td>
                            <td></td>
                            <td><?php echo round($total); ?></td>
                         </tr> <?php } else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>
   <script type="text/javascript" >
      var base_url="<?php echo base_url(); ?>";
      $('#emp_count').val("<?php echo $i; ?>");
      $('#total_ctc').val("<?php echo $total; ?>");
      $('#total_fixed').val("<?php echo $gross; ?>");
      $("#payment_mode_id").on("change",function(){
      var line_of_activity= $("#line_of_activity_id").val();
      var payment_mode= $("#payment_mode_id").val();
      if (line_of_activity=="All" && payment_mode=="All") {
        window.location.replace(base_url+"Salaries/All");
      }
      else if (line_of_activity=="All" && payment_mode!="") {
        window.location.href=base_url+"Salaries/activity?payment_mode="+payment_mode;
      }
      else if (payment_mode=="All" && line_of_activity_id!="") {
        window.location.replace(base_url+"Salaries/All");
      }
      else if (line_of_activity!="") {
        window.location.replace(base_url+"Salaries/activity?type="+line_of_activity+"&payment_mode="+payment_mode);
      }
      else{
        window.location.href=base_url+"Salaries/activity?payment_mode="+payment_mode;
      }
         
      })
      $("#line_of_activity_id").on("change",function(){
        var line_of_activity= $("#line_of_activity_id").val();
        if (line_of_activity=="All") {
          window.location.replace(base_url+"Salaries/All");
        }
        else{
          window.location.href=base_url+"Salaries/activity?type="+line_of_activity;
        }       
      })
      var employee_data=<?php if( isset($employees) && count($employees)){ echo json_encode($employees); } else{echo "{}"; } ?>;
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/salaryDetails?"+"<?php echo $getParam; ?>";
         /*$.ajax({
         type: 'post',
           url: base_url+"Excel_export/action",
           data: {employees_data:employee_data},
           success: function (data) {
            console.log(data);
            },
          error:function(error){
              console.log(error);
              $("#login_error_id").html(error);
          }
       });*/
      }
      $("#reports_ul").addClass("nv collapse in");
    $("#sald_li").addClass("active");
   </script>
