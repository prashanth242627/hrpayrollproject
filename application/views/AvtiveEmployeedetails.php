<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$this->load->view('theme/header');
$this->load->view('theme/sidebar');
$getParam="";
//  print_r($data);exit;
if (isset($_GET['to']) && !isset($_GET['from'])) {
   $getParam="to=".$_GET['to'];
}
else if (isset($_GET['from']) && isset($_GET['to'])) {
   $getParam="to=".$_GET['to']."&from=".$_GET['from'];
}
if (isset($_GET['type']) && !isset($_GET['status'])) {
   $getParam="type=".$_GET['type'];
}
else if (isset($_GET['type']) && isset($_GET['status'])) {
   $getParam="type=".$_GET['type']."&status=".$_GET['status'];
}

?>
      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->

                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Employee Personal Details </div>
                     <div class="panel-body">
                     <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-3 bgc-5">
                              <p class="text-center">DEPARTMENT</p>
                              <select name="account" class="form-control m-b" id="line_of_activity_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeDetails/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php echo $activity["dept_name"]; ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { $getParam="type=".$_GET['type']; ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                           <div class="form-group col-md-3  bgc-3">
                              <p class="text-center">Status</p>
                              <select name="account" class="form-control m-b p-0" id="status_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeDetails/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <option value="Active" <?php if (isset($_GET['status']) && $_GET['status']=='Active') { $getParam="status=Active"; ?> selected <?php } ?>>Active</option>
                                 <option value="Inactive" <?php if (isset($_GET['status']) && $_GET['status']=='Inactive') { $getParam="status=Inactive"; ?> selected <?php } ?>>Inactive</option>
                                 <option value="New" <?php if (isset($_GET['status']) && $_GET['status']=='New') { $getParam="status=New"; ?> selected <?php } ?>>New</option>

                              </select>
                           </div>
                            <div class="form-group col-md-3 bgc-4">
                              <div class="col-md-12 p-0">
                              <p class="text-center">Search BY B ID</p>
                              <select name="account" class="form-control m-b" onchange="location = this.value;">
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('EmployeeDetails/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeDetails/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo base_url('EmployeeDetails/Activity?bid='.$bid['emp_bid']); ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { $getParam="bid=".$_GET['emp_bid']; ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                            </div>
                            <div class="form-group col-md-3 bgc-5">
                               <div class="col-md-12 p-0">
                              <p class="text-center">BY E.Name</p>
                              <select name="account" class="form-control m-b" onchange="location = this.value;"> 
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('EmployeeDetails/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeDetails/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo base_url('EmployeeDetails/Activity?ename='.$name['emp_name']); ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { $getParam="ename=".$_GET['ename']; ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>

                           
                           <div class="form-group col-md-4 bgc-3" style="display: none;">
                              <p style="margin: 0px;">
                              </p><div class="col-md-6 p-0">
                              <p class="text-center">Date From</p>
                              <input type="date" class="form-control" id="start_date" value="<?php if(isset($_GET['from'])){  echo $_GET['from']; } ?>">
                              </div>
                               <div class="col-md-6 p-0">
                              <p class="text-center">Date To</p>
                              <input type="date" class="form-control" id="end_date" value="<?php if(isset($_GET['to'])){ echo $_GET['to']; } ?>">
                              </div>
                              <p></p>
                           </div>
                           <!-- <div class="form-group col-md-2 bgc-2">
                              <p class="text-center">LINE OF ACTIVITY</p>
                              <select name="account" class="form-control m-b">
                                 <option> All</option>
                              </select>
                           </div> -->
                   
                           </div>
                        </form>

                        <!-- START table-responsive-->
                     <div class="table-responsive fixtable">
                       <table class="table  table-bordered table-hover" id="fixTable">
                        <tbody class="mytable"> 
                            <thead>
                         <tr>
                           <td>SL.NO</td>
                           <td>EID</td> 
                            <td>BID</td>
                            <td>E.Name</td>
                            <td>Gender</td>
                            <td>Email</td>
                            <td>Department</td>
                            <td>Designation</td>
                            <td>ReportTime</td>
                            <td>D.O.J</td>
                            <td>D.O.B</td>
                            <td>Experience</td>
                            <td>Education </td>
                            <td>Mobile</td>
                            <td>ALT.Mobile</td>
                            <td>Transport</td>
                            <td>Address</td>
                            <td>Emergency NO</td>
                            <td>Spouse Name</td>
                            <td>Spouse Phone</td>
                            <td>Spouse Occupancy</td>
                            <td>Status</td>
                            <td>Employee Type</td>
                         </tr>
                         </thead>
                         <?php
                          if (isset($employees)) {

                           $i=1;
                           foreach ($employees as $employee) { 
                           ?>
                        <tr>

                           <td><?php echo $i++; ?></td>
                           <td><?php echo $employee['temp_emp_id'] ?></td>
                           <td><?php echo $employee['emp_bid'] ?></td>
                           <td><?php echo $employee['emp_name'] ?></td>
                           <td><?php echo $employee['sex'] ?></td>
                           <td><?php echo $employee['email'] ?></td>
                           <td><?php echo $employee['dept_name'] ?></td>
                           <td><?php echo $employee['designation_name'] ?></td>
                           <td><?php echo $employee['from_time']." - ".$employee['to_time']; ?></td>
                           <td><?php echo $employee['date_of_join'] ?></td>
                           <td><?php echo $employee['date_of_birth'] ?></td>
                           <td><?php echo $employee['experience'] ?></td>
                           <td><?php echo $employee['education'] ?></td>
                           <td><?php echo $employee['mobile'] ?></td>
                           <td><?php echo $employee['alternate_no'] ?></td>
                           <td><?php echo $employee['transport_name'] ?></td>
                           <td><?php echo $employee['address'] ?></td>
                           <td><?php echo $employee['emergency_no'] ?></td>
                           <td><?php echo $employee['spouse_name'] ?></td>
                           <td><?php echo $employee['spouse_phoneno'] ?></td>
                           <td><?php echo $employee['spouse_occupancy'] ?></td>
                           <?php if ($employee['status']) { ?>
                              <td>InActive</td>
                           <?php }else{ ?>
                           <td>Active</td> <?php } ?>
                           <?php if ($employee['regularize_status']) { ?>
                              <td>Regular</td>
                           <?php }else{ ?>
                           <td>Probation Period</td> <?php } ?>
                         </tr>
                        <?php }} else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->


   <script type="text/javascript" >
      var base_url="<?php echo base_url(); ?>";
      $("#end_date").on("change",function(){
         var start_date= $("#start_date").val();
         var end_date= $("#end_date").val();
         if (start_date!="") {
            window.location.replace(base_url+"EmployeeDetails/activity?from="+start_date+"&to="+end_date);
         }
         else{
            window.location.href=base_url+"EmployeeDetails/activity?to="+end_date;
         }
         
      })
      var base_url="<?php echo base_url(); ?>";
    $("#status_id").on("change",function(){
      var line_of_activity= $("#line_of_activity_id").val();
      var status= $("#status_id").val();
      
      if (line_of_activity!="") {
        if (line_of_activity=='All') {
           window.location.href=base_url+"EmployeeDetails/activity?status="+status;
        }
        else{
          window.location.replace(base_url+"EmployeeDetails/activity?type="+line_of_activity+"&status="+status);
        }
        
      }
      else if (line_of_activity!="") {
        window.location.replace(base_url+"EmployeeDetails/activity?type="+line_of_activity+"&status="+status);
      }
      else if (line_of_activity=="All" || status=="All") {
        window.location.replace(base_url+"EmployeeDetails/All");
      }

         
      })
      $("#line_of_activity_id").on("change",function(){
        var line_of_activity= $("#line_of_activity_id").val();
        if (line_of_activity=="All") {
          window.location.replace(base_url+"EmployeeDetails/All");
        }
        else{
          window.location.href=base_url+"EmployeeDetails/activity?type="+line_of_activity;
        }       
      })
      var employee_data=<?php if( isset($employees) && count($employees)){ echo json_encode($employees); } else{echo "{}"; } ?>;
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/employeesData?"+"<?php echo $getParam; ?>";
         /*$.ajax({
         type: 'post',
           url: base_url+"Excel_export/action",
           data: {employees_data:employee_data},
           success: function (data) {
            console.log(data);
            },
          error:function(error){
              console.log(error);
              $("#login_error_id").html(error);
          }
       });*/
      }
      $("#reports_ul").addClass("nav collapse in");
      $("#ed_li").addClass("active");
   </script>
