<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (isset($_GET['month']) && isset($_GET['year'])) {
   if (isset($uploadstatus) && $uploadstatus) {
    $getParam="";
   }
   else if (isset($_GET['bid'])) {
      $getParam="bid=".$_GET['bid']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (isset($_GET['ename'])) {
      $getParam="ename=".ltrim($_GET['ename'])."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/EmployeeMonthlyLeaveStatus/All') !== false ) {
      $getParam="type=All&month=".$_GET['month']."&year=".$_GET['year'];
    }
    elseif (isset($_GET['type'])) {
      $getParam="type=".$_GET['type']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
  }
?>
      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->

            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                    
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading" >Employee Monthly Leaves Status</div>
                     <div class="panel-body">
                      <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-3 bgc-2">
                              <p class="text-center"><b>Department</b></p>
                              <select name="account" class="form-control m-b" id="line_of_activity_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if(strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/EmployeeMonthlyLeaveStatus/All'))
                                 {?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php echo $activity["dept_name"]; ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                            <div class="form-group col-md-2 bgc-4">
                          <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.B ID</b></p>
                              <select name="account" class="form-control m-b" id="emp_bid_id">
                                 <option value="">Select</option><!-- 
                                 <option value="All" <?php if ($_SERVER['REQUEST_URI']=="applications/hrpayroll/EmployeeMonthlyLeaveStatus/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option> -->
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo $bid['emp_bid']; ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                            </div>
                            <div class="form-group col-md-2 bgc-5">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.Name</b></p>

                              <select name="account" class="form-control m-b" id="emp_name"> 
                                 <option value="">Select</option><!-- 
                                 <option value="All" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeMonthlyLeaveStatus/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option> -->
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo $name['emp_name']; ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>
                           <div class="form-group col-md-2 bgc-3">
                              <div class="col-md-12 p-0">
                              <p class="text-center"><b>Year</b></p>
                              <select name="account" class="form-control m-b" id="year_id">
                                 <option value="">Select</option>
                                 <option value="2019" <?php if (isset($_GET['year']) && $_GET['year']==2019) {  ?> selected <?php } ?>>2019</option>
                                 <option value="2020" <?php if (isset($_GET['year']) && $_GET['year']==2020) {  ?> selected <?php } ?>>2020</option>
                                 
                              </select>
                              </div>
                            </div>
                           <div class="form-group col-md-3 bgc-1">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>Month</b></p>

                              <select name="account" class="form-control m-b" id="month_id"> 
                                 <option value="">Select</option>
                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
                                 } ?>
                              </select>
                              </div>
                           </div>
                   
                           </div>
                        </form>
                        <div class="row input-padding">
                          <div class="form-group col-md-4">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Selection Criteria: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" readonly="" value="<?php 
                                 if(isset($_GET['type'])) { echo $_GET['type']; } 
                                 else if(strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/EmployeeMonthlyLeaveStatus/All')){
                                  echo "ALL";
                                 }
                                 else if(isset($_GET['bid'])){ echo "EMP BID =".$_GET['bid']; } 
                                 else if(isset($_GET['ename'])){ echo "EMP ENAME =".$_GET['ename']; } ?>" placeholder="Designation Name" class="form-control p-0">
                              </div>
                           </div>
                           <div class="form-group col-md-3 p-0 pl-6">
                              <label class="col-lg-4 control-label p-0 text-right pt-9">Month: </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" value="<?php if(isset($_GET['month'])){  echo $_GET['month']; } ?>" placeholder="Month" class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-3 p-0 pl-6">
                              <label class="col-lg-4 control-label p-0 text-right pt-9">Year: </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" value="<?php if(isset($_GET['year'])){  echo $_GET['year']; } ?>" placeholder="year" class="form-control p-0">
                              </div>
                           </div>
                        </div>
                        <!-- START table-responsive-->
                     <div class="table-responsive fixtable">
                       <table class="table  table-bordered table-hover" id="fixTable">
                        <thead>
                          <tr >
                            <th rowspan="2">SL.NO</th>
                            <th rowspan="2">EID</th> 
                            <th rowspan="2">BID</th>
                            <th rowspan="2">E.Name</th>
                            <th rowspan="2">Date OF Join</th>
                            <th rowspan="2">Department</th>
                            <th rowspan="2">Designation</th>
                            <th  colspan="3" class="custom_centered">CL</th>
                            <th  colspan="3" class="custom_centered">EL</th>
                            <th  colspan="3" class="custom_centered">SL</th>
                            <th rowspan="2">BIO LOP</th>
                            <th rowspan="2">Total LOP Days</th>
                            <th rowspan="2">Actual No.Of Days</th>
                            <th rowspan="2">No.Of Days Present</th>
                            <th rowspan="2">Details Of CL</th>
                            <th rowspan="2">Details Of LOP</th>
                         </tr>
                         <tr>
                            <th>OPB</th>
                            <th>Availed</th>
                            <th>Balance</th>
                            <th>OPB</th>
                            <th>Availed</th>
                            <th>Balance</th>
                            <th>OPB</th>
                            <th>Availed</th>
                            <th>Balance</th>

                         </tr>
                        </thead>
                        <tbody  class="mytable"> 
                         
                         <?php
                         if (isset($uploadstatus) && $uploadstatus) {
                          echo '<tr><td class="custom_centered" colspan="22" style="color:red">Please Upload BioMetric Attendance and Lop Calculation</td></tr>';
                          } else if (isset($employees)) {
                           $i=1;
                           foreach ($employees as $leaves1) { 
                           ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $leaves1['temp_emp_id'] ?></td>
                           <td><?php echo $leaves1['emp_bid'] ?></td>
                           <td><?php echo $leaves1['emp_name'] ?></td>
                           <td><?php echo $leaves1['date_of_join'] ?></td>
                           <td><?php echo $leaves1['dept_name'] ?></td>
                           <td><?php echo $leaves1['designation_name'] ?></td>
                           <td><?php echo $leaves1['cl_open_balance'] ?></td>
                           <td><?php echo $leaves1['cl_availed'] ?></td>
                           <td><?php echo $leaves1['cl_balance'] ?></td>
                           <td><?php echo $leaves1['el_open_balance'] ?></td>
                           <td><?php echo $leaves1['el_availed'] ?></td>
                           <td><?php echo $leaves1['el_balance'] ?></td>
                           <td><?php echo $leaves1['sl_open_balance'] ?></td>
                           <td><?php echo $leaves1['sl_availed'] ?></td>
                           <td><?php echo $leaves1['sl_balance'] ?></td>
                           <td><?php echo $leaves1['lop_count'] ?></td>
                            <td><?php echo $leaves1['lop_availed']+$leaves1['lop_count']; ?></td>
                            <td><?php echo $leaves1['no_of_days'] ?> </td>
                            <td><?php echo $leaves1['no_of_days']-($leaves1['lop_availed']+$leaves1['lop_count']) ?> </td>
                            <td><?php echo $leaves1['cl_leave_dates'] ?></td>
                            <td><?php echo $leaves1['lop_leave_dates'] ?></td>
                           	
                         </tr>
                        <?php }} else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>

   <script type="text/javascript">
      var base_url="<?php echo base_url(); ?>";
      $("#end_date").on("change",function(){
         var start_date= $("#start_date").val();
         var end_date= $("#end_date").val();
         if (start_date!="") {
            window.location.replace(base_url+"EmployeeLeaveStatus/activity?from="+start_date+"&to="+end_date);
         }
         else{
            window.location.href=base_url+"EmployeeLeaveStatus/activity?to="+end_date;
         }
         
      })
      $("#emp_bid_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#line_of_activity_id").val("");
        $("#emp_name").val("");
      });
      $("#line_of_activity_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#emp_bid_id").val("");
        $("#emp_name").val("");
      });
      $("#emp_name").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#line_of_activity_id").val("");
        $("#emp_bid_id").val("");
      });
      $("#year_id").on("change",function(){
        $("#month_id").val("");
      });
      $("#month_id").on("change",function(){
        var year=$("#year_id").val();
        var month=$("#month_id").val();
        var ename=$("#emp_name").val();
        var ebid=$("#emp_bid_id").val();
        var dept=$("#line_of_activity_id").val();

        if (year!="" && month!="") {
          if (dept=="All" || ename=="All" || ebid=="All") {
            window.location.replace(base_url+"EmployeeMonthlyLeaveStatus/All?month="+month+"&year="+year);
          }
          else if (dept!="") {
            window.location.replace(base_url+"EmployeeMonthlyLeaveStatus/activity?type="+dept+"&month="+month+"&year="+year);
          }
          else if (ename!="") {
            window.location.replace(base_url+"EmployeeMonthlyLeaveStatus/activity?ename="+ename+"&month="+month+"&year="+year);
          }
          else if (ebid!="") {
            window.location.replace(base_url+"EmployeeMonthlyLeaveStatus/activity?bid="+ebid+"&month="+month+"&year="+year);
          }
        }
      });
      
      var employee_data=<?php if( isset($employees) && count($employees)){ echo json_encode($employees); } else{echo "{}"; } ?>;
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/EmployeeMonthlyLeaveStatus?"+"<?php echo $getParam; ?>";
      }
      $("#reports_ul").addClass("nav collapse in");
      $("#emls_li").addClass("active");
</script> 
<style type="text/css">
  .table-responsive {

    max-width: 100%;
    float: left;
    overflow-x: scroll;

}
</style>
