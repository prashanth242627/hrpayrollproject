
 <?php if($this->session->userdata('create_status') == "success") { ?>
<script type="text/javascript">
  $(document).ready(function() {
    toastr.options.timeOut = 3000; // 1.5s
    toastr.success('<?php echo $this->session->userdata('record_name');  ?>');
  });
</script>
<?php } else if($this->session->userdata('create_status') == "failed") { ?>
 <script type="text/javascript">
  $(document).ready(function() {
    toastr.options.timeOut = 3000; // 1.5s
    toastr.error('<?php echo $this->session->userdata('record_name');  ?>');
  });
  </script>
<?php } 
$this->session->unset_userdata('record_name');
$this->session->unset_userdata('create_status');
?>
	