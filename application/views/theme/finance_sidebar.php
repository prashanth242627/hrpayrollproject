<nav class="navbar navbar-fixed-top" style="background-color: #354177">
  <div class="container-fluid">
  <div class="col-md-4">
    <div class="col-md-2 navbar-header" style="padding-top: 15px;font-size: 30px;">
      <a href="#" data-toggle-state="aside-collapsed" data-persists="true" class="hidden-xs">
             <em class="fa fa-navicon"></em>
           </a>
    </div>
    <ul class="col-md-6 nav navbar-nav">
      <li class="active"><img src="<?php echo base_url();?>images/logo.png" alt="App Logo" class="img-responsive" style="height: 77px;"></li>
    </ul>
    </div>

    <div class="col-md-4">
    <ul class="" style="list-style: none">
      <li class="active"><img src="<?php echo base_url();?>images/logo2.png" alt="App Logo" class="img-responsive" style="height: 68px;margin: 0px auto;padding-top: 16px;"></li>
    </ul>
    </div>

    <div class="col-md-4">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#" data-toggle="reset">
             <em class="fa fa-refresh"></em> Reload
           </a></li>
      <li><a href="<?php echo base_url(); ?>Login/logouts">Logout</a></li>
    </ul>
    </div>
  </div>
</nav>
  <!-- END Top Navbar-->
    <!-- START aside-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
          <nav class="sidebar">
            <ul class="nav" id="main_ul">
               <!-- START Menu-->
               <li class="" id="home_li">
                  <a href="<?php echo base_url();?>Dashboard" title="Home">
                  <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text">Home</span>
                  </a>
         
               </li>
               <!--************************************
                      REPORTS
              ******************************** -->
               <li class="" id="reports_li">
                  <a href="#" title="Pages" data-toggle="collapse-next" class="has-submenu">
                      <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text">Reports</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse " id="reports_ul">
                           
                     <!-- <li style="list-style-type: none;">
                        <a href="Listofabsentees.html" title="List of absentees" data-toggle="" class="no-submenu">
                           <span class="item-text"> --><b>FINANCE REPORTS</b><!-- </span>
                        </a>
                     </li> -->
                     <!-- <li class="" id="ss_li">
                        <a href="Empsalarystructure.html" title="Emp. salary structure" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. salary structure</span>
                        </a>
                     </li> -->
                     <li class="" id="sald_li">
                        <a href="<?php echo base_url()."Salaries"; ?>" title="Emp. CTS details" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. CTS details</span>
                        </a>
                     </li>
                     <li class="" id="msald_li">
                        <a href="<?php echo base_url()."MonthlySalaries"; ?>" title="Emp. Monthly salary details" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. Monthly Salary details</span>
                        </a>
                     </li>
                     <li class="" id="msalc_li">
                        <a href="<?php echo base_url()."SalariesCalculation"; ?>" title="Emp. Monthly salary details" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. Salary Calculation</span>
                        </a>
                     </li>
                     <li class="" id="pm_li">
                        <a href="<?php echo base_url()."PaymentModes"; ?>" title="Payment Modes" data-toggle="" class="no-submenu">
                           <span class="item-text">Payment Modes</span>
                        </a>
                     </li>
                     <li class="" id="deptsal_li">
                        <a href="<?php echo base_url()."DepartmentSalaries"; ?>" title="Department Salaries" data-toggle="" class="no-submenu">
                           <span class="item-text">Department Salaries</span>
                        </a>
                     </li>
                     <li class="" id="econ_li" style="display: none;">
                        <a href="Employercontribution.html" title="Employer contribution" data-toggle="" class="no-submenu">
                           <span class="item-text">Employer contribution</span>
                        </a>
                     </li>
                     <li class="" id="eps_li" ">
                        <a href="<?php echo base_url()."EmpPaySlips"; ?>" title="Emp.Payslip" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp.Payslip</span>
                        </a>
                     </li>
                     <!-- <li class="" id="atndsr_li">
                        <a href="DeptwiseSalarayPaymentsummary.html" title="Dept wise Salaray Payment summary" data-toggle="" class="no-submenu">
                           <span class="item-text">Dept wise Salaray Payment summary</span>
                        </a>
                     </li> -->
                     <li class="" id="esave_li">
                        <a href="<?php echo base_url()."EmployeeSavings"; ?>" title="Employee savings" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee TDS and Savings</span>
                        </a>
                     </li>
                     <li class="" id="eluts_li" style="display: none;">
                        <a href="EmployeesListunderTaxscope.html" title="Employees List under Tax scope" data-toggle="" class="no-submenu">
                           <span class="item-text">Employees List under Tax scope</span>
                        </a>
                     </li>
                     <li class="" id="eti_li" style="display: none;">
                        <a href="EmployeeTaxableincome.html" title="Employee Taxable income" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee Taxable income</span>
                        </a>
                     </li>
                     <!-- <li>
                        <a href="#," title="Employees List under Tax scope" data-toggle="" class="no-submenu">
                           <span class="item-text"> --><b>STATUTATORY </b><!-- </span>
                        </a>
                     </li> -->
                     <li class="" id="epfr_li" >
                        <a href="<?php echo base_url()."EmpPFContribution"; ?>" title="Stautatory PF Returns" data-toggle="" class="no-submenu">
                           <span class="item-text">Stautatory PF Returns</span>
                        </a>
                     </li>
                     <li class="" id="esir_li">
                        <a href="<?php echo base_url()."EmpESIreturns"; ?>" title="Stautatory ESI Returns" data-toggle="" class="no-submenu">
                           <span class="item-text">Stautatory ESI Returns</span>
                        </a>
                     </li>
                     <!-- <li>
                        <a href="#." title="Employees List under Tax scope" data-toggle="" class="no-submenu">
                           <span class="item-text"> <b>MANAGERIAL REPORTS</b>--><!-- </span>
                        </a>
                     </li> -->
                     <li class="" id="empr_li" style="display: none;">
                        <a href="EmployeesRating.html" title="Employees Rating" data-toggle="" class="no-submenu">
                           <span class="item-text">Employees Rating</span>
                        </a>
                     </li>
                     <li class="" id="ecfc_li" style="display: none;">
                        <a href="ListofEmployeescalls.html" title="List of Employees calls for councilling" data-toggle="" class="no-submenu">
                           <span class="item-text">List of Employees calls for councilling</span>
                        </a>
                     </li>
                  </ul>
                  <!-- END SubMenu item-->
               </li>
            </ul>
         </nav>
         <!-- END Sidebar (left)-->
      </aside>
      <!-- End aside-->