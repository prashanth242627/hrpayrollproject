
 <!-- START Page footer-->
<footer class="text-center">&copy; 2018 - jhpublicschool <span style="float: right;">Design&Developed By RD Enterprises</span></footer>
         <!-- END Page Footer-->
      </section>
      <!-- END Main section-->
   </div>
   <!-- END Main wrapper-->
   <!-- START Scripts-->
   <!-- Main vendor Scripts-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

   <?php $this->load->view('theme/notifications'); ?> 
   <!-- END Scripts-->
</body>


</html>
