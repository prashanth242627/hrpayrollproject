<?php 
$this->load->view('theme/header');
if ($this->session->userdata('role_id')==1) {
	$this->load->view('theme/sidebar');
}
else if ($this->session->userdata('role_id')==2) {
	$this->load->view('theme/hr_sidebar');
}
else if ($this->session->userdata('role_id')==3) {
	$this->load->view('theme/finance_sidebar');
}
else if ($this->session->userdata('role_id')==4) {
	$this->load->view('theme/am_sidebar');
}


$this->load->view($template);
$this->load->view('theme/footer');
$this->load->view('theme/notifications');