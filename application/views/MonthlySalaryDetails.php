<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (isset($_GET['month']) && isset($_GET['year'])) {
   
   if (isset($_GET['bid'])) {
      $getParam="bid=".$_GET['bid']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (isset($_GET['ename'])) {
      $getParam="ename=".ltrim($_GET['ename'])."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/MonthlySalaries/All') !== false && !isset($_GET['payment_mode'])) {
      $getParam="type=All&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (isset($_GET['payment_mode']) && isset($_GET['type'])) {
      $getParam="type=".$_GET['type']."&payment_mode=".$_GET['payment_mode']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    elseif (isset($_GET['payment_mode'])) {
      $getParam="payment_mode=".$_GET['payment_mode']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    elseif (isset($_GET['type'])) {
      $getParam="type=".$_GET['type']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
  }
?>

      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->

                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Employee Monthly Salary Details </div>
                     <div class="panel-body" style="overflow-x: scroll;">
                     <form>
                          <div >
                           <div class="form-group col-md-2 bgc-2">
                              <p class="text-center"><b>Department</b></p>
                              <select name="account" class="form-control m-b" id="line_of_activity_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/MonthlySalaries/All') !== false) { ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php echo $activity["dept_name"]; ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                           <div class="form-group col-md-2 bgc-3">
                              <p class="text-center"><b>Payment Mode</b></p>
                              <select name="account" class="form-control m-b" id="payment_mode_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/MonthlySalaries/All') !== false) { ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($payment_modes as $payment_mode) { ?>
                                       <option value="<?php echo $payment_mode["payment_type"]; ?>" <?php if (isset($_GET['payment_mode']) && $_GET['payment_mode']==$payment_mode['payment_type']) { ?> selected <?php } ?> > <?php echo $payment_mode['payment_type'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                            
                            <div class="form-group col-md-2 bgc-5">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>BY E.Name</b></p>

                              <select name="account" class="form-control m-b" id="emp_name"> 
                                 <option value="">Select</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo $name['emp_name']; ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) {?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>
                           <div class="form-group col-md-2 bgc-4">
                          <div class="col-md-12 p-0">
                              <p class="text-center"><b>Search BY B ID</b></p>
                              <select name="account" class="form-control m-b" id="emp_bid_id">
                                 <option value="">Select</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo $bid['emp_bid']; ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                            </div>
                           <div class="form-group col-md-2 bgc-3">
                              <div class="col-md-10 p-0">
                              <p class="text-center">Year</p>
                              <select name="account" class="form-control m-b" id="year_id">
                                 <option value="">Select</option>
                                 <option value="2019" <?php if (isset($_GET['year']) && $_GET['year']==2019) {  ?> selected <?php } ?>>2019</option>
                                 <option value="2020" <?php if (isset($_GET['year']) && $_GET['year']==2020) {  ?> selected <?php } ?>>2020</option>
                                 
                              </select>
                              </div>
                            </div>
                           <div class="form-group col-md-2 bgc-1">
                               <div class="col-md-10 p-0">
                              <p class="text-center"><b>Month</b></p>

                              <select name="account" class="form-control m-b" id="month_id"> 
                                 <option value="">Select</option>
                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
                                 } ?>
                              </select>
                              </div>
                           </div>
                   
                           </div>
                        </form>

                      <div class="row input-padding">
                          <div class="form-group col-md-2">
                              <label class="col-lg-5 control-label p-0">Selection Criteria: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" value="<?php if(isset($_GET['type'])){
                                  echo $_GET['type'];
                                 }else if(strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/MonthlySalaries/All') !== false){ echo "All"; } ?>" placeholder="Designation Name" class="form-control p-0">
                              </div>
                           </div>
                          <div class="form-group col-md-2 p-0 pl-6">
                              <label class="col-lg-4 control-label p-0 text-right">Payment Mode: </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" value="<?php if(isset($_GET['payment_mode'])){
                                  echo $_GET['payment_mode'];
                                 }else if(strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/MonthlySalaries/All') !== false){ echo "All"; }  ?>" placeholder="ABBR" class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-2 p-0 pl-6">
                              <label class="col-lg-4 control-label p-0 text-right pt-9">Month: </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" value="<?php if(isset($_GET['month'])){  echo $_GET['month']; } ?>" placeholder="Month" class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-2 p-0 pl-6">
                              <label class="col-lg-4 control-label p-0 text-right pt-9">Year: </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" value="<?php if(isset($_GET['year'])){  echo $_GET['year']; } ?>" placeholder="year" class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-2 p-0 pl-6">
                              <label class="col-lg-4 control-label p-0 text-right pt-9">Gross(Rs): </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" id="gross_id_val" value="" placeholder="ABBR" class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-2 p-0 pl-6">
                              <label class="col-lg-4 control-label p-0 text-right pt-9">Net(Rs): </label>
                              <div class="col-lg-8 p-0">
                                 <input type="text" name="desig_id" id="net_id_val" value="" placeholder="ABBR" class="form-control p-0">
                              </div>
                           </div>
                        </div>
                        <!-- START table-responsive-->
                     <div class="table-responsive fixtable">
                       <table class="table  table-bordered table-hover" id="fixTable">
                        <thead>
                          <tr>
                            <th rowspan="2">S.No</th>
                            <th rowspan="2">E ID</th>
                            <th rowspan="2">E BID</th>
                            <th rowspan="2">E Name</th>
                            <th rowspan="2">D.O.J</th>
                            <th rowspan="2">Designation</th>
                            <th rowspan="2">Department</th>
                            <th rowspan="2">Bank A/c Number</th>
                            <th rowspan="2">Payment Mode</th>
                            <!-- <th>UAN</th>
                            <th>PF Number</th>
                            <th>ESI Number</th> -->
                            <th rowspan="2">LOP days</th>
                            <th rowspan="2">NoOf PayDays</th>
                            <th class="text-center" colspan="9">Earnings</th>
                            <th class="text-center" colspan="6">Deductions</th>                          
                            <th rowspan="2">Net Pay(Rs)</th>
                            <th rowspan="2">Fixed Monthly CTC(Rs)</th>
                         </tr>
                         <tr>
                           <th>Basic(Rs)</th>
                            <th>INCR(Rs)</th>
                            <th>D.A(Rs)</th>
                            <th>H.R.A(Rs)</th>
                            <th>C.C.A(Rs)</th>
                            <th>Other allowances(Rs)</th>
                            <th>Head/HOD/Coordinator allowances(Rs)</th>
                            <th>Special Allowances(Rs)</th>
                            <th>Gross Pay(Rs)</th>

                            <th>PF Deducted(Rs)</th>
                            <th>Professional Tax(Rs)</th>
                            <th>ESI(Rs)</th>
                            <th>TDS(Rs)</th>
                            <th>Other Deductions</th>
                            <th>Remarks</th>
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                         
                         <?php
                         if ($message!="") {
                           echo "<tr style='color: red'><td colspan='29'>".$message."</td></tr>";
                         }
                         else{
                         if (isset($employees)) {
                           $i=1;
                           $gross=0;
                           $net=0;
                           foreach ($employees as $employee) { 
                            $gross+=$employee['Gross_pay'];
                            $net+=$employee['Net_pay'];
                           ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $employee['emp_id'] ?></td>
                           <td><?php echo $employee['emp_bid'] ?></td>
                           <td><?php echo $employee['emp_name'] ?></td>
                           <td><?php echo $employee['date_of_join'] ?></td>
                           <td><?php echo $employee['designation_name'] ?></td>
                           <td><?php echo $employee['dept_name'] ?></td>
                           <td><?php echo $employee['acc_no'] ?></td>
                           <td><?php echo $employee['payment_mode'] ?></td>
                           <!-- <td><?php //echo $employee['uan'] ?></td>
                            <td><?php //echo $employee['pf_no'] ?></td>
                            <td><?php //echo $employee['esi_no'] ?></td> -->
                            <td><?php if ($employee['Lop_days']==0.00) {
                              echo "0";
                            }else { echo $employee['Lop_days']; } ?></td>
                           <td><?php echo $employee['no_of_pay_days'] ?></td>
                            <td><?php echo round($employee['basic_earnings']) ?></td>
                            <td><?php echo round($employee['incr_earnings']) ?></td>
                            <td><?php echo round($employee['da_earnings']) ?></td>
                            <td><?php echo round($employee['hra_earnings']) ?></td>
                            <td><?php echo round($employee['cca_earnings']) ?></td>
                            <td><?php echo round($employee['other_allowance_earnings']) ?></td>
                            <td><?php echo round($employee['co_allowance_earnings']) ?></td>
                            <td><?php echo round($employee['special_allowance_earnings'])?></td>
                            <td><?php echo round($employee['Gross_pay']) ?></td>
                            <td><?php echo round($employee['pf_deducted']) ?></td>
                            <td><?php echo round($employee['professional_tax']) ?></td>
                            <td><?php echo round($employee['esi']) ?></td>
                            <td><?php echo round($employee['tds']) ?></td>
                            <td><?php echo round($employee['management_deductions']) ?></td>
                            <td><?php echo round($employee['remarks']) ?></td>
                            
                            <td><?php echo round($employee['Net_pay']) ?></td>
                            <td><?php echo round($employee['fixed_monthly_ctc']) ?></td>
                            
                         </tr>
                        <?php } ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total</td>
                            <td id="gross_id"><?php echo round($gross); ?></td>
                            <td id="net_id"><?php echo round($net); ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                    <?php  } else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>No Records</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                      <?php } } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
   <script type="text/javascript" >
    $("#gross_id_val").val($("#gross_id").html());
    $("#net_id_val").val($("#net_id").html());
     var base_url="<?php echo base_url(); ?>";
     $("#emp_bid_id").on("change",function(){
        $("#month_id").val("");
        $("#payment_mode_id").val("");
        $("#line_of_activity_id").val("");
        $("#year_id").val("");
        $("#emp_name").val("");
      });
      $("#line_of_activity_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#payment_mode_id").val("");
        $("#emp_bid_id").val("");
        $("#emp_name").val("");
      });
      $("#payment_mode_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#emp_bid_id").val("");
        $("#emp_name").val("");
      });
      $("#emp_name").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#payment_mode_id").val("");
        $("#line_of_activity_id").val("");
        $("#emp_bid_id").val("");
      });
      $("#year_id").on("change",function(){
        $("#month_id").val("");
      });
      $("#month_id").on("change",function(){
        var line_of_activity= $("#line_of_activity_id").val();
        var payment_mode= $("#payment_mode_id").val();
        var emp_bid= $("#emp_bid_id").val(); 
        var emp_name=$('#emp_name').val();
        var year=$("#year_id").val();
        var month=$("#month_id").val();
        if (year!="" && month!="") {
          if (emp_bid!="") {
            window.location.replace(base_url+"MonthlySalaries/activity?bid="+emp_bid+"&month="+month+"&year="+year);
          }
          else if (emp_name!="" && emp_name!=null) {
            window.location.replace(base_url+"MonthlySalaries/activity?ename="+emp_name+"&month="+month+"&year="+year);
          }
          
          else if (line_of_activity!="" && payment_mode=="All") {
            window.location.replace(base_url+"MonthlySalaries/activity?type="+line_of_activity+"&month="+month+"&year="+year);
          }
          else if (line_of_activity=="All" && payment_mode!="") {
            window.location.href=base_url+"MonthlySalaries/activity?payment_mode="+payment_mode+"&month="+month+"&year="+year;
          }
          else if (line_of_activity!="" && payment_mode!="") {
            window.location.replace(base_url+"MonthlySalaries/activity?type="+line_of_activity+"&payment_mode="+payment_mode+"&month="+month+"&year="+year);
          }
          else if ( line_of_activity=="All") {
            window.location.replace(base_url+"MonthlySalaries/All?month="+month+"&year="+year);
          }
          else if (payment_mode=="All") {
            window.location.replace(base_url+"MonthlySalaries/All?month="+month+"&year="+year);
          }
          else if (line_of_activity!="" && payment_mode=="") {
            window.location.replace(base_url+"MonthlySalaries/activity?type="+line_of_activity+"&month="+month+"&year="+year);
          }
          else if (payment_mode!="") {
            window.location.replace(base_url+"MonthlySalaries/activity?payment_mode="+payment_mode+"&month="+month+"&year="+year);
          }
        }
      })
      function exportToExcel() {
        
         window.location.href=base_url+"Excel_export/MonthlySalaries?"+"<?php echo $getParam; ?>";
      }
      $("#reports_ul").addClass("nav collapse in");
      $("#msald_li").addClass("active");
   </script>
