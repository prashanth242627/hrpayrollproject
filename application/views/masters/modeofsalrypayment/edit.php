 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE LINE OF ACTIVITY EDIT SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
						<div class="pull-right">
							<a href="<?php echo base_url()."Lineofactivity"; ?>" class="btn-form" value="List">List</a>
						</div>
                        <div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
                              <th>Dept</th>
                              <th>ABBR</th>
							  <th></th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <tr>
							<td><input type='text' value='<?php echo $get_lineofactivity[0]->dept_name; ?>' name='dept_name' placeholder='Dept Name' ><input type='hidden' value='<?php echo $get_lineofactivity[0]->activity_id; ?>' name='activity_id'></td>
							<td><input type='text' placeholder='Dept ID' value='<?php echo $get_lineofactivity[0]->dept_id; ?>' name='dept_id'></td>
							<td><input type='checkbox' name="status" <?php echo (($get_lineofactivity[0]->status==1)?"checked":""); ?>>&nbsp;Status</td>
						   </tr>
                        </tbody>
                  </table>
                  <div class="col-md-offset-3 btn-sec center">
                  <div class="btn-row">
						<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
						<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
                  </div>
                  </div>
                        </div>
                        <!-- END table-responsive-->
					
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
		 
<script>
	$("#master_ul").addClass("nav collapse in");
   $("#mospm").addClass("active");
</script>