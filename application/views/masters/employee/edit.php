 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE REPORTING TIME SLOT MASTER</div>
                     <div class="panel-body">
					 <div class="pull-right">
							<a href="<?php echo base_url()."Employee"; ?>" class="btn-form" value="List">List</a>
						</div>
                        <form class="form-horizontal" method="post">
                           <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Emp ID</label>
                              <div class="col-lg-8">
                                 <input type="text" name="emp_id" value="<?php echo $get_employee[0]->temp_emp_id; ?>" placeholder="Emp ID" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">D.O.B</label>
                              <div class="col-lg-8">
                                 <input type="date" name="dob" placeholder="D.O.B" value="<?php echo $get_employee[0]->dob; ?>" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Emp Name</label>
                              <div class="col-lg-8">
                                 <input type="text" name="emp_name" value="<?php echo $get_employee[0]->emp_name; ?>" placeholder="Number (20c)" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Education</label>
                              <div class="col-lg-8">
                                 <input type="text" name="education" value="<?php echo $get_employee[0]->education; ?>" placeholder="Number (20c)" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Emp BID</label>
                              <div class="col-lg-8">
                                 <input type="text" name="emp_bid" value="<?php echo $get_employee[0]->emp_bid; ?>"  placeholder="Number (20c)" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Experience-Yrs</label>
                              <div class="col-lg-8">
                                 <input type="text" name="experience" value="<?php echo $get_employee[0]->experience; ?>" placeholder="Number (20c)" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">D.O.J</label>
                              <div class="col-lg-8">
                                 <input type="date" name="doj" value="<?php echo $get_employee[0]->doj; ?>" placeholder="D.O.J" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Mobile No</label>
                              <div class="col-lg-8">
                                 <input type="text" name="mobile" value="<?php echo $get_employee[0]->mobile; ?>" placeholder="Number (20c)" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Line of Activity</label>
                              <div class="col-lg-8">
                                 <select name="line_of_activity" class="form-control">
								 <?php 
								 $line_of_activity="";
								 foreach($get_line_of_activity as $row)
								 {
									   $ts = (($get_employee[0]->activity_id == $row->activity_id)?'selected':'');
									 $line_of_activity .="<option value='".$row->activity_id."' ".$ts.">".$row->dept_name."</option>";
								 }
								 echo $line_of_activity;
								 ?>
								 </select>
                              </div>
                           </div>
                           
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Teach.Subject</label>
                              <div class="col-lg-8">
								<select name="subject_details" class="form-control">
								 <?php 
								 $subject_details="";
								 foreach($get_subject_details as $row)
								 {
									  $ts = (($get_employee[0]->subject_details_id == $row->subject_details_id)?'selected':'');
									 $subject_details .="<option value='".$row->subject_details_id."' ".$ts.">".$row->sub_name."</option>";
								 }
								 echo $subject_details;
								 ?>
								 </select>
                              </div>
                           </div>
						    <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">House Details</label>
                              <div class="col-lg-8">
                                 	<select name="house_details" class="form-control" id="house_details">
								 <?php 
								 $house_details="";
								 foreach($get_house_details as $row)
								 {
									 $ts = (($get_employee[0]->residence_details_id == $row->residence_details_id)?'selected':'');
									 
									 $house_details .="<option value='".$row->residence_details_id."' ".$ts.">".$row->transport_name."</option>";
								 }
								 echo $house_details;
								 ?>
								 </select>
                              </div>
                           </div>
						   <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Transport Details</label>
                              <div class="col-lg-8">
                                  	<select name="transport_details" class="form-control" id="transport_details">
								 <?php 
								 $transport_details="";
								 foreach($get_transport_details as $row)
								 {
									$ts = (($get_employee[0]->transport_details_id == $row->transport_details_id)?'selected':'');
									 $transport_details .="<option value='".$row->transport_details_id."' ".$ts.">".$row->transport_name."</option>";
								 }
								 echo $transport_details;
								 ?>
								 </select>
                              </div>
                           </div>
                  
						  <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Status</label>
                              <div class="col-lg-8">
                                 <input type='checkbox' name="status" checked>&nbsp;Status
                              </div>
                         </div>
						     <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label"></label>
                              <div class="col-lg-8">
                                 <input type='hidden' name="test" >&nbsp;
                              </div>
                         </div>
					 <div class="row">
						 <div class="text-center btn-sec">
							  <div class="btn-row">
									<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
									<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
							  </div>
						  </div>
					 </div>
    
                        </form>
          
                     </div>
                  </div>
                     <!-- END panel-->
        
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
		 <script type="text/javascript">
     $("#master_ul").addClass("nav collapse in");
   $("#empm").addClass("active");  
     </script>