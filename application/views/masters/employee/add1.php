 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 $dept_name = ($getPpaymentTypeEdit->dept_name)?$getPpaymentTypeEdit->dept_name:'';
 $dept_id = ($getPpaymentTypeEdit->dept_id)?$getPpaymentTypeEdit->dept_id:'';
 $from_time = ($getPpaymentTypeEdit->from_time)?$getPpaymentTypeEdit->from_time:'';
 $to_time = ($getPpaymentTypeEdit->to_time)?$getPpaymentTypeEdit->to_time:'';

 echo $from_time;
 echo $to_time;

 $line_of_activity="";
$i=1;
foreach($get_employee as $row)
{
	$line_of_activity .= "<tr><td>".$i++."</td><td>".$row->dept_name."</td><td>".$row->dept_id."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>".$row->from_time."</td><td>".$row->to_time."</td><td><p><a href='".base_url()."Lineofactivity/edit/".base64_encode($row->activity_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Lineofactivity/delete/".base64_encode($row->activity_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Lineofactivity/undo/".base64_encode($row->activity_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_employee_log as $row)
{
	$line_of_activity .= "<tr><td>".$i++."</td><td>".$row->dept_name."</td><td>".$row->dept_id."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>".$row->from_time."</td><td>".$row->to_time."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post" id="form_id">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE LINE OF ACTIVITY ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
					   <div class="row">
							<div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Dept Name</label>
                              <div class="col-lg-6">
                                 <input type="text" name="dept_name" value ="<?php echo $dept_name; ?>" placeholder="Dept Name" class="form-control">
                              </div>
                           </div>
						   	<div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">ABBR</label>
                              <div class="col-lg-6">
                                 <input type="text" name="dept_id" value ="<?php echo $dept_id; ?>" placeholder="Dept Name" class="form-control">
                              </div>
                           </div>
						   </div>
                     <div class="row">                
                         <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">From Time</label>
                              <div class="col-lg-6">
                                 <input type="time" class="form-control" id="from_time" name="from_time" value="<?php echo $from_time;?>">
                              </div>
                        </div>
                         <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">To Time</label>
                              <div class="col-lg-6">
                                 <input type="time"  class="form-control" id="to_time" name="to_time" value="<?php echo $to_time;?>">
                              </div>
                           </div>
                  
                     </div>
						   <div class="row">
						   <div class="form-group col-md-4">
						   &nbsp;
						   </div>
						   <div class="form-group col-md-6">
                              <div class="col-lg-6">
                                 <input type='checkbox' name="status" checked>&nbsp;Status
                              </div>
                           </div>
					   </div>
					   <div class="row">
						  <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($line_of_activity)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
                              <th>Dept Name</th>
							  <th>ABBR</th>
							  <th>Date</th>
                       <th>From Time</th>
                       <th>To Time</th>
							   <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $line_of_activity; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
		 
<script>
	$("#master_ul").addClass("nv class in");
   $("#loam").addClass("active");
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
