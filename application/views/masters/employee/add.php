 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 $emp_name = ($getEmployees->emp_name)?$getEmployees->emp_name:'';
 $emp_bid = ($getEmployees->emp_name)?$getEmployees->emp_bid:'';
 $dob = ($getEmployees->date_of_birth)?$getEmployees->date_of_birth:"";
 $temp_emp_id = ($getEmployees->temp_emp_id)?$getEmployees->temp_emp_id:'';
 $education = ($getEmployees->education)?$getEmployees->education:'';
 $experience = ($getEmployees->experience)?$getEmployees->experience:"";
$doj = ($getEmployees->date_of_join)?$getEmployees->date_of_join:'';
$date = ($getEmployees->date)?$getEmployees->date:"";
$mobile = ($getEmployees->mobile)?$getEmployees->mobile:"";
$activity_id = ($getEmployees->activity_id)?$getEmployees->activity_id:"";
$subject_details_id = ($getEmployees->subject_details_id)?$getEmployees->subject_details_id:"";
$residence_details_id = ($getEmployees->residence_details_id)?$getEmployees->residence_details_id:"";
$transport_details_id = ($getEmployees->transport_details_id)?$getEmployees->transport_details_id:"";
$email = ($getEmployees->email)?$getEmployees->email:'';
$sex = ($getEmployees->sex)?$getEmployees->sex:'';
$alternate_no = ($getEmployees->alternate_no)?$getEmployees->alternate_no:'';
$reportingslots_id = ($getEmployees->reportingslots_id)?$getEmployees->reportingslots_id:'';
$emergency_no = ($getEmployees->emergency_no)?$getEmployees->emergency_no:'';
$designation_no = ($getEmployees->designation)?$getEmployees->designation:'';
$address = ($getEmployees->address)?$getEmployees->address:'';
$spouse_name = ($getEmployees->spouse_name)?$getEmployees->spouse_name:'';
$spouse_phoneno = ($getEmployees->spouse_phoneno)?$getEmployees->spouse_phoneno:'';
$spouse_occupancy = ($getEmployees->spouse_occupancy)?$getEmployees->spouse_occupancy:'';
$holiday_group=($getEmployees->holiday_group_id)?$getEmployees->holiday_group_id:'';
$regularize_status=($getEmployees->regularize_status)?$getEmployees->regularize_status:0;
 $get_employees="";
$i=1;

foreach($get_employee as $row)
{
	$get_employees .= "<tr><td>".$i++."</td><td>".$row->temp_emp_id."</td><td>".$row->emp_name."</td><td>".$row->emp_bid."</td><td>".$row->dept_name."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Employee/edit/".base64_encode($row->emp_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Employee/delete/".base64_encode($row->emp_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Employee/undo/".base64_encode($row->emp_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_employee_log as $row)
{
	$get_employees .= "<tr><td>".$i++."</td><td>".$row->temp_emp_id."</td><td>".$row->emp_name."</td><td>".$row->emp_bid."</td><td>".$row->dept_name."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
    <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE  MASTER </div>
                     <div class="panel-body">
					   <div class="row">
					    <form class="form-horizontal" action="<?php echo base_url()."Employee/EmpBulkupload"; ?>" method="post" enctype="multipart/form-data">
						<?php if(empty($getEmployees)) { ?>
						<div class="form-group col-md-6"> </div>
						<div class="form-group col-md-6"> 
					         <label class="col-lg-4 control-label">Bulk Upload</label>
                              <div class="col-lg-8">
                               <span><input type="file" name="emp_bluk">
							   <input type="hidden" name="check" value="1" />
                                <input type="submit" name="employeeadd" id="go" value="go" class="btn-form btn-edit"></span>
							   </div>
						</div>
						<?php } ?>
						</form>
					   </div>
					   <br /><br />
                       <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                           <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Emp ID *</label>
                              <div class="col-lg-8">
                               <input type="text" name="emp_id" required placeholder="Emp ID" value ="<?php echo $temp_emp_id; ?>" class="form-control input-imp" <?php echo ($temp_emp_id)?"readonly":''; ?>>
							   
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">D.O.B*</label>
                              <div class="col-lg-8">
                                 <input type="date" name="dob" required value="<?php echo $dob; ?>" class="form-control input-imp" <?php echo ($dob)?"readonly":''; ?>>
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Emp Name*</label>
                              <div class="col-lg-8">
                                 <input type="text" name="emp_name" required placeholder="Number (20c)" value ="<?php echo $emp_name; ?>" class="form-control input-imp">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Education*</label>
                              <div class="col-lg-8">
                                 <input type="text" name="education" required placeholder="Number (20c)" value ="<?php echo $education; ?>" class="form-control input-imp">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Emp BID*</label>
                              <div class="col-lg-8">
                                 <input type="text" name="emp_bid" required  placeholder="Number (20c)" value ="<?php echo $emp_bid; ?>" class="form-control input-imp" <?php echo ($emp_bid)?"readonly":''; ?>>
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Experience-Yrs*</label>
                              <div class="col-lg-8">
                                 <input type="text" name="experience" required value="<?php echo $experience; ?>" placeholder="Number (20c)" class="form-control input-imp">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">D.O.J*</label>
                              <div class="col-lg-8">
                                 <input type="date" name="doj" required value="<?php echo $doj; ?>" class="form-control input-imp" <?php echo ($doj)?"readonly":''; ?>>
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Mobile No*</label>
                              <div class="col-lg-8">
                                 <input type="text" name="mobile" required value ="<?php echo $mobile; ?>" placeholder="Number (20c)" class="form-control input-imp">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Department*</label>
                              <div class="col-lg-8">
                                 <select name="line_of_activity" required class="form-control input-imp" id="line_of_activity">
								 <?php 
								 $line_of_activity="";
								 $cc ="";
								 foreach($get_line_of_activity as $row)
								 {
									 $cc = ($row->activity_id == $activity_id)?"selected":"";
									 $line_of_activity .="<option value='".$row->activity_id."' ".$cc.">".$row->dept_name."</option>";
								 }
								 echo $line_of_activity;
								 ?>
								 </select>
                              </div>
                           </div>
                           
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Teach.Subject*</label>
                              <div class="col-lg-8">
								<select name="subject_details" required class="form-control input-imp">
								 <?php 
								 $subject_details="";
								 $cc = "";
								 foreach($get_subject_details as $row)
								 {
									  $cc = ($row->subject_details_id == $subject_details_id)?"selected":"";
									 $subject_details .="<option value='".$row->subject_details_id."' ".$cc.">".$row->sub_name."</option>";
								 }
								 echo $subject_details;
								 ?>
								 </select>
                              </div>
                           </div>
						    <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">House Details*</label>
                              <div class="col-lg-8">
                                 	<select name="house_details" required class="form-control input-imp" id="house_details">
								 <?php 
								 $house_details="";
								 $cc ="";
								 foreach($get_house_details as $row)
								 {
									 $cc = ($row->transport_details_id == $house_details)?"selected":"";
									 $house_details .="<option value='".$row->transport_details_id."'>".$row->transport_name."</option>";
								 }
								 echo $house_details;
								 ?>
								 </select>
                              </div>
                           </div>
						   <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Transport Details*</label>
                              <div class="col-lg-8">
                                  	<select name="transport_details" class="form-control input-imp" id="transport_details">
								 <?php 
								 $transport_details="";
								 foreach($get_transport_details as $row)
								 {
									 $cc = ($row->transport_details_id == $transport_details)?"selected":"";
									 $transport_details .="<option value='".$row->transport_details_id."'>".$row->transport_name."</option>";
								 }
								 echo $transport_details;
								 ?>
								 </select>
                              </div>
                           </div>
                  
							<div class="form-group col-md-6">
								  <label class="col-lg-4 control-label">Email</label>
								  <div class="col-lg-8">
									 <input type="text" name="email" value="<?php echo $email; ?>" placeholder="email" class="form-control">
								  </div>
								</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Gender*</label>
						  <div class="col-lg-8">
							 	<select name="sex" class="form-control input-imp" id="sex" required>
									<option value="M">Male</option>
									<option value="F">Female</option>
								</select>
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Alternate No</label>
						  <div class="col-lg-8">
							 <input type="text" name="alternate_no" value="<?php echo $alternate_no; ?>" placeholder="alternate_no" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Reporting slots*</label>
						  <div class="col-lg-8">
						  	<input type="text" name="reportingslots_id" required class="form-control input-imp" id="reportingslots_id" readonly value="">
							   	<!-- <select name="reportingslots_id" class="form-control" id="reportingslots_id">
								 <?php 
								 $reportingslots_id="";
								 $cc = "";
								 foreach($get_reportingslots as $row)
								 {
									 $cc = ($row->reportingslots_id == $reportingslots_id)?"selected":"";
									 $reportingslots_id .="<option value='".$row->reportingslots_id."' ".$cc.">".$row->from_time." - ".$row->to_time."</option>";
								 }
								 echo $reportingslots_id;
								 ?>
								 </select> -->
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Emergency No</label>
						  <div class="col-lg-8">
							 <input type="text" name="emergency_no" value="<?php echo $emergency_no; ?>" placeholder="emergency_no" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Designation *</label>
						  <div class="col-lg-8">
							 <!-- <input type="text" value="<?php echo $designation; ?>" placeholder="designation" class="form-control"> -->
							 <select name="designation" id="designation" class="form-control input-imp " required="">
							 	 <?php 
								 $designation="";
								 $cc ="";
								 foreach($get_designation_details as $row)
								 {
									 $cc = ($row->designation_id == $designation_no)?"selected":"";
									 $designation .="<option value='".$row->designation_id."' ".$cc.">".$row->designation_name."</option>";
								 }
								 echo $designation;
								 ?>
							 </select>
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Address</label>
						  <div class="col-lg-8">
							 <input type="text" name="address" value="<?php echo $address; ?>" placeholder="address" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Spouse Name</label>
						  <div class="col-lg-8">
							 <input type="text" name="spouse_name" value="<?php echo $spouse_name; ?>" placeholder="spouse_name" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Spouse Phone No</label>
						  <div class="col-lg-8">
							 <input type="text" name="spouse_phoneno" value="<?php echo $spouse_phoneno; ?>" placeholder="spouse_phoneno" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Spouse Occupancy</label>
						  <div class="col-lg-8">
							 <input type="text" name="spouse_occupancy" value="<?php echo $spouse_occupancy; ?>" placeholder="spouse_occupancy" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Holiday Group</label>
						  <div class="col-lg-8">
							 <select name="holiday_group" id="holiday_group" class="form-control input-imp " required="">
							 	 <?php 
								 $h_group="";
								 $cc ="";
								 foreach($get_holiday_groups as $row)
								 {
									 $cc = ($row->id == $holiday_group)?"selected":"";
									 $h_group .="<option value='".$row->id."' ".$cc.">".$row->group_name."</option>";
								 }
								 echo $h_group;
								 ?>
							 </select>
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Employee Type</label>
						  <div class="col-lg-8">
							 <select name="regularize_status" id="regularize_status" class="form-control input-imp " required="">
							 	<option value="1" <?php if($regularize_status){ echo "selected"; } ?>>Regular</option>
							 	<option value="0" <?php  if($regularize_status==0){ echo "selected"; } ?>>Probation</option>
							 	 
							 </select>
						  </div>
						</div>
						
						
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Status</label>
						  <div class="col-lg-8">
							<input type='checkbox' name="status" checked>&nbsp;Status
						  </div>
						</div>
					 <div class="row">
						 <div class="text-center btn-sec">
							  <div class="btn-row">
									<input type="submit" name="employeeadd" id="employeeadd" class="btn-form btn-edit" value="SUBMIT"> 
									<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
							  </div>
						  </div>
					 </div>
    
                        </form>
          
                     </div>
                  </div>
                     <!-- END panel-->
        
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($get_employees)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
							  <th>Employee ID</th>
                              <th>Employee Name</th>
							  <th>Employee BID</th>
							  <th>Department</th>
							  <th>Date</th>
							  <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $get_employees; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
<script>
$(document).ready(function(){
	$("#residence_details").change(function(){
		$.ajax({
			url:'<?php echo base_url();?>Employee/get_housedetails',
			type:'POST',
			data: {store_id:store_id},
			success:function(data){
			}
		});
	});
	
	});
	
$("#master_ul").addClass("nav collapse in");
   $("#empm").addClass("active");
</script>
<script type="text/javascript">
	$(document).ready(function(){
	$("#line_of_activity").change(function(){
		var line_of_activity_id=$("#line_of_activity").val();
		$.ajax({
			url:'<?php echo base_url();?>Employee/getReportingSlots',
			type:'POST',
			data: {line_of_activity_id:line_of_activity_id},
			success:function(data){
				console.log(data);
				$("#reportingslots_id").val(data);
			},
			error: function (error) {
				console.log(error);
			}
		});
	});
	$("#line_of_activity").trigger("change");
});
</script>		 