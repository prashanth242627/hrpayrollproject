 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php

?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">

            <!-- START row-->
            <div class="row">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading"><?php echo $emp_bid; ?>Employee Salary Calculation</div>
                     <div class="panel-body bgc-1">
                        <!-- START table-responsive-->
                        <div class="row col-md-5 col-md-offset-2">
						   <div class="form-group">
	                              <label class="col-lg-4 control-label p-0">Year</label>
	                              <div class="col-lg-4 p-0 pl-6">
	                                 <input  class="form-control" style="line-height: 20px;" id="year_id"  type="number" min="2019" max="2099" step="1" value="2019" name="year"  >
	                              </div>
	                         </div>
                         </div>
                         <div class="row col-md-5 ">
						   <div class="form-group">
	                              <label class="col-lg-4 control-label p-0">Month</label>
	                              <div class="col-lg-4 p-0 pl-6">
	                                 <select name="month" class="form-control m-b" id="month_id" required> 
		                                 <option value="">Select</option>
		                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
		                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
		                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
		                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
		                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
		                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
		                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
		                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
		                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
		                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
		                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
		                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
		                                 } ?>
		                              </select>
	                              </div>
	                         </div>
                         </div>

					   <div class="row">

						  <div class="col-md-offset-5 btn-sec center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
            </div>
         <!-- END Page content-->
		 
<script>
$("#reports_ul").addClass("nav collapse in");
$("#msalc_li").addClass("active");
var base_url="<?php echo base_url(); ?>";
		$("#month_id1").on("change",function(){
         var year= $("#year_id1").val();
         var month= $("#month_id1").val();
         window.location.replace(base_url+"SalariesCalculation/activity?year="+year+"&month="+month);         
      });
</script>
<style type="text/css">
	#attendence_form .form-group {

    	margin-left: -10px;
    	margin-right: -10px;
	}
	tr.custom_centered th,td{
		text-align: center;
	}

</style>