 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 $designation_name = ($getPpaymentTypeEdit->designation_name)?$getPpaymentTypeEdit->designation_name:'';
 $desig_id = ($getPpaymentTypeEdit->desig_id)?$getPpaymentTypeEdit->desig_id:'';
 $designation="";
$i=1;
foreach($designations as $row)
{
	$designation .= "<tr><td>".$i++."</td><td>".$row->designation_name."</td><td>".$row->desig_id."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Designation/edit/".base64_encode($row->designation_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Designation/delete/".base64_encode($row->designation_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Designation/undo/".base64_encode($row->designation_id)."' class='btn-form btn-undo' style='display:none'> UNDO </a></p></td></tr>"; 
}
foreach($designations_log as $row)
{
	$designation .= "<tr><td>".$i++."</td><td>".$row->designation_name."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-6 col-md-offset-3">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE DESIGNATIONS ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
					   <div class="row">
							<div class="form-group col-md-7">
                              <label class="col-lg-5 control-label p-0">Designation Name</label>
                              <div class="col-lg-7">
                                 <input type="text" name="designation_name" value ="<?php echo $designation_name; ?>" placeholder="Designation Name" class="form-control p-0">
                              </div>
                           </div>
						   	<div class="form-group col-md-5 p-0 pl-6">
                              <label class="col-lg-4 control-label p-0">ABBR</label>
                              <div class="col-lg-8">
                                 <input type="text" name="desig_id" value ="<?php echo $desig_id; ?>" placeholder="ABBR" class="form-control p-0">
                              </div>
                           </div>
						   </div>
					   <div class="row">
						  <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			<?php if(count($designation)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
                              <th>Designation Name</th>
							  <th>ABBR</th>
							  <th>Date</th>
							   <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $designation; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
		 
<script>
	$("#master_ul").addClass("nav collapse in");
   $("#desim").addClass("active");
   
</script>