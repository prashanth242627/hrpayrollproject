 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-10 col-md-offset-1 ">
                  <form action="<?php echo base_url(); ?>StanderdDeductions/addForm" method="POST"  novalidate="" class="form-horizontal">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">STANDARD DEDUCTIONS ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
                        <div class="table-responsive">

                           <table class="table  table-bordered table-hover">
                        <tbody class="mytable">
                           <tr>
                              <td>EMP P.F</td>
                              <td colspan="2"><input type="text" name="pf" placeholder="<?php echo $pfdata[0]->deduct_value; ?>%" value="<?php echo $pfdata[0]->deduct_value; ?>%" readonly></td>
                           </tr>
                           <tr class="bg-yellow">
                              <td colspan="3">SPL DEDUCTIONS</td>
                           </tr>
                           <tr>
                              <td>10% S.D</td>
                              <td><input type="checkbox" name="sda"  value="10" <?php if($spdata[0]->deduct_value==10){ echo "checked"; } ?>> Auto</td>
                              <td><input type="text" name="sdo" placeholder="<?php if($spdata[0]->deduct_value!=10){ echo $spdata[0]->deduct_value; }else{ ?>Others:0<?php } ?>" readonly></td>
                           </tr> 

                            <tr class="bg-yellow">
                              <td colspan="3">EMP P.T</td>
                           </tr>
                            <tr>
                              <td>Min Amount</td>
                              <td>Limit</td>
                               <td>Deductions</td>
                           </tr>
                            <?php 
                            for($i=1;$i<=5; $i++){ 
                              ?>
                            <tr>

                              <td><input type="text" name="pt<?php echo $i; ?>minlimit"  placeholder="<?php echo $ptdata[$i-1]->min_limit; ?>" readonly> </td>
                              <td><input type="text" name="pt<?php echo $i; ?>limit"  placeholder="<?php echo $ptdata[$i-1]->max_limit; ?>" readonly></td>
                              <td><input type="text" name="pt<?php echo $i; ?>rate" placeholder="<?php echo $ptdata[$i-1]->deduct_value; ?>" readonly></td>
                           </tr> 
                        <?php } ?>
                           
                           <tr class="bg-yellow">
                              <td colspan="3">EMP E.S.I</td>
                           </tr>
                           <?php 
                            for($i=1;$i<=2; $i++){ 
                           ?> 
                           <tr>
                              <td><input type="text" name="esi<?php echo $i; ?>minlimit" placeholder="<?php echo $esidata[$i-1]->min_limit; ?>" readonly></td>
                              <td><input type="text" name="esi<?php echo $i; ?>limit" placeholder="<?php echo $esidata[$i-1]->max_limit; ?>" readonly></td>
                              <td><input type="text" name="esi<?php echo $i; ?>rate" placeholder="<?php echo $esidata[$i-1]->deduct_value; ?>%" readonly></td>
                           </tr>
                           <?php } ?> 
                           
                            <tr class="bg-yellow">
                              <td colspan="3">TDS</td>
                           </tr>
                           <?php 
                           for($i=1;$i<=9; $i++){ 
                           ?>  
                           <tr>
                              <td><input type="text" name="tds<?php echo $i; ?>minlimit" placeholder="<?php  echo $tdsdata[$i-1]->min_limit; ?>" readonly></td>
                              <td><input type="text" name="tds<?php echo $i; ?>limit" placeholder="<?php echo $tdsdata[$i-1]->max_limit; ?>" readonly></td>
                              <td><input type="text" name="tds<?php echo $i; ?>rate" placeholder="<?php echo $tdsdata[$i-1]->deduct_value; ?>%" readonly></td>
                           </tr>
                        <?php } ?>
                           
                        </tbody>
                  </table>
                  <div class="col-md-12 btn-sec">
                  <ul class="btn-row">
                     <input type="submit" class="btn-form btn-edit" value="Edit"> 
                     <input type="reset" class="btn-form btn-undo" value="Undo" id="undo">
                     
                     <!-- <li><a href="" class="btn-form btn-delete"> DELETE </a></li> -->
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                     
                  </div>
                        </div>
                        <!-- END table-responsive-->

                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
<script type="text/javascript">
$("#undo").click(function(){
   location.href = "StanderdDeductions/undo";
})   
$("#master_ul").addClass("nav collapse in");
$("#ddm").addClass("active");
</script>