 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-10 col-md-offset-1 ">
                  <form action="<?php echo base_url(); ?>StanderdDeductions/add" method="POST"  novalidate="" class="form-horizontal">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">STANDARD DEDUCTIONS ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
                        <div class="table-responsive">
                           <table class="table  table-bordered table-hover">
                        <tbody class="mytable">
                           <tr>
                              <td>EMP P.F</td>
                              <td colspan="2"><input type="text" name="pf" placeholder="12%"></td>
                           </tr>
                           <tr class="bg-yellow">
                              <td colspan="3">SPL DEDUCTIONS</td>
                           </tr>
                           <tr>
                              <td>10% S.D</td>
                              <td><input type="text" name="sda"  placeholder="Auto"></td>
                              <td><input type="text" name="sdo" placeholder="Others:0"></td>
                           </tr> 

                            <tr class="bg-yellow">
                              <td colspan="3">EMP P.T</td>
                           </tr>
                            <tr>
                              <td>Salary</td>
                              <td>Limit</td>
                               <td>Deductions</td>
                           </tr>
                            <tr>
                              <td>< </td>
                              <td><input type="text" name="pt1limit"  placeholder="15000"></td>
                              <td><input type="text" name="pt1rate" placeholder="0"></td>
                           </tr> 
                           <tr>
                              <td>= </td>
                              <td><input type="text" name="pt2limit"  placeholder="15000"></td>
                              <td><input type="text" name="pt2rate" placeholder="0"></td>
                           </tr> 
                           <tr>
                              <td>> </td>
                              <td><input type="text" name="pt3limit" placeholder="15000"></td>
                              <td><input type="text" name="pt3rate" placeholder="150"></td>
                           </tr> 
                           <tr>
                              <td>< </td>
                              <td><input type="text" name="pt4limit" placeholder="20000"></td>
                              <td><input type="text" name="pt4rate" placeholder="150"></td>
                           </tr>
                           <tr>
                              <td>> </td>
                              <td><input type="text" name="pt5limit" placeholder="20000"></td>
                              <td><input type="text" name="pt5rate" placeholder="200"></td>
                           </tr> 
                           <tr class="bg-yellow">
                              <td colspan="3">EMP E.S.I</td>
                           </tr> 
                           <tr>
                              <td>< </td>
                              <td><input type="text" name="esi1limit" placeholder="21000"></td>
                              <td><input type="text" name="esi1rate" placeholder="0%"></td>
                           </tr> 
                           <tr>
                              <td>= </td>
                              <td><input type="text" name="esi2limit" placeholder="21000"></td>
                              <td><input type="text" name="esi2rate" placeholder="0%"></td>
                           </tr> 
                            <tr class="bg-yellow">
                              <td colspan="3">TDS</td>
                           </tr> 
                           <tr>
                              <td>< </td>
                              <td><input type="text" name="tds1limit" placeholder="250000"></td>
                              <td><input type="text" name="tds1rate" placeholder="0%"></td>
                           </tr>
                           <tr>
                              <td>= </td>
                              <td><input type="text" name="tds2limit" placeholder="250000"></td>
                              <td><input type="text" name="tds2rate" placeholder="0%"></td>
                           </tr>
                           <tr>
                              <td>> </td>
                              <td><input type="text" name="tds3limit" placeholder="250000"></td>
                              <td><input type="text" name="tds3rate" placeholder="5%"></td>
                           </tr>
                           <tr>
                              <td>= </td>
                              <td><input type="text" name="tds4limit" placeholder="500000"></td>
                              <td><input type="text" name="tds4rate" placeholder="5%"></td>
                           </tr>
                           <tr>
                              <td>= </td>
                              <td><input type="text" name="tds5limit" placeholder="500000"></td>
                              <td><input type="text" name="tds5rate" placeholder="5%"></td>
                           </tr>
                           <tr>
                              <td>> </td>
                              <td><input type="text" name="tds6limit" placeholder="500000"></td>
                              <td><input type="text" name="tds6rate" placeholder="20%"></td>
                           </tr>
                           <tr>
                              <td>= </td>
                              <td><input type="text" name="tds7limit" placeholder="1000000"></td>
                              <td><input type="text" name="tds7rate" placeholder="20%"></td>
                           </tr>
                           <tr>
                              <td>< </td>
                              <td><input type="text"  name="tds8limit" placeholder="1000000"></td>
                              <td><input type="text" name="tds8rate" placeholder="20%"></td>
                           </tr>
                           <tr>
                              <td>> </td>
                              <td><input type="text" name="tds9limit" placeholder="1000000"></td>
                              <td><input type="text" name="tds9rate" placeholder="30%"></td>
                           </tr>
                        </tbody>
                  </table>
                  <div class="col-md-12 btn-sec">
                  <ul class="btn-row">
                     <input type="submit" class="btn-form" value="Save"> 
                     <input type="submit" class="btn-form btn-edit" value="Edit"> 
                     <input type="reset" class="btn-form btn-undo" value="Undo" id="undo">
                     
                     <!-- <li><a href="" class="btn-form btn-delete"> DELETE </a></li> -->
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                     
                  </div>
                        </div>
                        <!-- END table-responsive-->

                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
<script type="text/javascript">
$("#undo").click(function(){
   location.href = "undo";

})   
$("#master_ul").addClass("nav collapse in");
$("#ddm").addClass("active");
</script>