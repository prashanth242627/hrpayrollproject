 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 $dept_name = ($getPpaymentTypeEdit->dept_name)?$getPpaymentTypeEdit->dept_name:'';
 $dept_id = ($getPpaymentTypeEdit->dept_id)?$getPpaymentTypeEdit->dept_id:'';
 $from_time = ($getPpaymentTypeEdit->from_time)?$getPpaymentTypeEdit->from_time:'';
 $to_time = ($getPpaymentTypeEdit->to_time)?$getPpaymentTypeEdit->to_time:'';


$i=1;

?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                  <form action="#" novalidate=""  class="form-horizontal" method="post" id="form_id">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Other Deductions</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
					   <div class="row">
							<div class="form-group col-md-4">
                              <label class="col-lg-4 control-label p-0">Emp Bid</label>
                              <div class="col-lg-8 p-0 pl-6">
                                 <select name="emp_bid"  class="form-control m-b" id="emp_bid">
                                 <option value="">Select</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo $bid['emp_bid']; ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                           </div>
						   	<div class="form-group col-md-4">
                              <label class="col-lg-4 control-label p-0">Emp Name</label>
                              <div class="col-lg-8 p-0 pl-6">
                                 <input type="text" name="emp_name" id="emp_name" readonly="" value ="<?php echo $dept_id; ?>" placeholder="Emp Name" class="form-control">
                              </div>
                           </div>
                           <div class="form-group col-md-4">
                              <label class="col-lg-4 control-label p-0">Designation</label>
                              <div class="col-lg-8 p-0 pl-6">
                                 <input type="text" name="designation_id" id="designation_id" readonly="" value ="<?php echo $dept_id; ?>" placeholder="Designation Name" class="form-control">
                              </div>
                           </div>
						   </div>
                     <div class="row">                
                         <div class="form-group col-md-4">
                              <label class="col-lg-4 control-label p-0">Year</label>
                              <div class="col-lg-8 p-0 pl-6">
                                 <select name="year"  class="form-control m-b" id="year">
                                 <option value="">Select</option>
                                 <option value="2019" <?php if (isset($_GET['year']) && $_GET['year']==2019) {  ?> selected <?php } ?>>2019</option>
                                 <option value="2020" <?php if (isset($_GET['year']) && $_GET['year']==2020) {  ?> selected <?php } ?>>2020</option>
                                 
                              </select>
                              </div>
                           </div>
                           <div class="form-group col-md-4">
                              <label class="col-lg-4 control-label p-0">Month</label>
                              <div class="col-lg-8 p-0 pl-6">
                                 <select name="month"  class="form-control m-b" id="month"> 
                                 <option value="">Select</option>
                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
                                 } ?>
                              </select>
                              </div>
                           </div>
                         <div class="form-group col-md-4">
                              <label class="col-lg-4 control-label p-0">Amount</label>
                              <div class="col-lg-8 p-0 pl-6">
                                 <input type="text" name="amount" id="amount"  value ="" placeholder="Amount" class="form-control">
                              </div>
                           </div>
                     </div>
                     <div class="row">
                        <div class="form-group col-md-12">
                             <label class="col-lg-2 control-label p-0" for="comment">Remarks:</label>
                             <div class="col-lg-8 p-0 pl-10">
                                <textarea class="form-control" name="remarks" rows="3" id="comment"></textarea>
                             </div>
                        </div>
                     </div> 
                     <div class="row">
                       <div class="btn-sec text-center">
                       <div class="btn-row">
                           <input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
                           <input type="reset" class="btn-form btn-delete" value="CANCEL"> 
                       </div>
                       </div>
                     </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			
         <!-- END Page content-->
		 
<script>
	$("#master_ul").addClass("nv class in");
   $("#md").addClass("active");
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   var val = $("#emp_bid").val();
    $("#emp_bid").change(function(){ 
      $.ajax({
         url: '<?php echo base_url();?>ManagementDeductions/getEmployeeDetails',
         data: {emp_bid:$(this).val()},
         method:"POST",
         success:function(data){
            console.log(data);
            var obj=jQuery.parseJSON(data);
            $('#emp_name').val(obj.emp_name);
            $('#designation_id').val(obj.designation_name); 
         },
         error:function(error){
            console.log(error);
         }
      });
    });
   $("#emp_bid").trigger('change');
});
</script>
