 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">TRANSPORT DETAILS EDIT SCREEN</div>
                     <div class="panel-body">
					   <?php 
					  $transportType="";
					   foreach($getTransportTypeDetails as $row) {
						   $select = ($get_transport_details[0]->residence_details_id == $row->residence_details_id)?"selected":"";
						  $transportType .="<option value='".$row->residence_details_id."' ".$select.">".$row->residence_name."</option>" ;
					   }
					   ?>
                        <!-- START table-responsive-->
						<div class="pull-right">
							<a href="<?php echo base_url()."transport_details"; ?>" class="btn-form" value="List">List</a>
						</div>
                        <div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
                             <th>Transport Type</th>
                              <th>Transport Details</th>
							  <th></th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <tr>
						   <td><select name='residence_details_id' class='form-control'><?php echo $transportType; ?></select></td>
							<td><input type='text' value='<?php echo $get_transport_details[0]->transport_name; ?>' name='transport_name' placeholder='transport Name' ><input type='hidden' value='<?php echo $get_transport_details[0]->transport_details_id; ?>' name='transport_details_id'></td>
							<td><input type='checkbox' name="status" <?php echo (($get_transport_details[0]->status==1)?"checked":""); ?>>&nbsp;Status</td>
						   </tr>
                        </tbody>
                  </table>
                  <div class="col-md-offset-3 btn-sec center">
                  <div class="btn-row">
						<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
						<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
                  </div>
                  </div>
                        </div>
                        <!-- END table-responsive-->
					
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
		 
<script>
	$("#master_ul").addClass("nav collapse in");
      $("#tdm").addClass("active");
</script>