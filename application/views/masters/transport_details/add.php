 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 
 $residence_details_id = ($gethoureMasterEdit->residence_details_id)?$gethoureMasterEdit->residence_details_id:'';
 $transport_name = ($gethoureMasterEdit->transport_name)?$gethoureMasterEdit->transport_name:'';
  foreach($get_residence_name as $row) {  
   $getResidenceName[$row->residence_details_id] = $row->residence_name; 
}

 $transport_details="";
$i=1;
foreach($get_transport_details as $row)
{
   $transport_details .= "<tr><td>".$i++."</td><td>".$getResidenceName[$row->residence_details_id]."</td><td>".$row->transport_name."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Transport_details/edit/".base64_encode($row->transport_details_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Transport_details/delete/".base64_encode($row->transport_details_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Transport_details/undo/".base64_encode($row->transport_details_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_transport_details_log as $row)
{
   $transport_details .= "<tr><td>".$i++."</td><td>".$getResidenceName[$row->residence_details_id]."</td><td>".$row->transport_name."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">TRANSPORT DETAILS ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
                  <div class="row input-panel">
                     
                     
                     <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Employee Residence Details</label>
                              <div class="col-lg-6">
                                <select name="residence_details_id" class='form-control' id="residence_details_id">
                        <?php foreach($get_residence_name as $row) {  $getResidenceName[$row->residence_details_id] = $row->residence_name; ?>
                           
                         <option value='<?php echo $row->residence_details_id; ?>' <?php echo (($row->residence_details_id == $residence_details_id)?"selected":"");?>> <?php echo $row->residence_name; ?></option>
                        <?php } ?>
                        </select>
                              </div>
                           </div>
                
              
                     <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Transport Name</label>
                              <div class="col-lg-6">
                                 <input type="text" name="transport_name" value ="<?php echo $transport_name; ?>" placeholder="Transport Name" class="form-control">
                              </div>
                           </div>
                  </div>
                  <div class="row">
                    <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
                  </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         <?php if(!empty($transport_details)) {  ?>
         <div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
                       <th>S.No</th>
                       <th>Activity Id</th>
                       <th>Residence Name</th>
                       <th>Date</th>
                       <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $transport_details; ?>
                        </tbody>
                  </table>
            </div>
         <?php } ?>
         </div>
         <!-- END Page content-->
       
<script>
  $("#master_ul").addClass("nav collapse in");
      $("#tdm").addClass("active");
</script>