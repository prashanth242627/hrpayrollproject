 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 $from_time = ($getPpaymentTypeEdit->from_time)?$getPpaymentTypeEdit->from_time:'';
 $to_time = ($getPpaymentTypeEdit->to_time)?$getPpaymentTypeEdit->to_time:'';
 $reportingslots="";
$i=1;
foreach($get_reportingslots as $row)
{
   $reportingslots .= "<tr><td>".$i++."</td><td>".$row->from_time."</td><td>".$row->to_time."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Reportingslots/edit/".base64_encode($row->reportingslots_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Reportingslots/delete/".base64_encode($row->reportingslots_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Reportingslots/undo/".base64_encode($row->reportingslots_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_reportingslots_log as $row)
{
   $reportingslots .= "<tr><td>".$i++."</td><td>".$row->from_time."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE REPORTING TIME SLOT MASTER</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
                  <div class="row">
                     <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">From Time</label>
                              <div class="col-lg-6">
                                 <input type="time" name="from_time" value ="<?php echo $from_time; ?>" placeholder="From Time" class="form-control">
                              </div>
                           </div>
                        <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">ABBR</label>
                              <div class="col-lg-6">
                                 <input type="time" name="to_time" value ="<?php echo $to_time; ?>" placeholder="To Time" class="form-control">
                              </div>
                           </div>
                     </div>
                     <div class="row">
                     <div class="form-group col-md-4">
                     &nbsp;
                     </div>
                     <div class="form-group col-md-6">
                              <div class="col-lg-6">
                                 <input type='checkbox' name="status" checked>&nbsp;Status
                              </div>
                           </div>
                  </div>
                  <div class="row">
                    <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
                  </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         <?php if(!empty($reportingslots)) {  ?>
         <div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
                       <th>S.No</th>
                        <th>From Time</th>
                       <th>To Time</th>
                       <th>Date</th>
                        <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $reportingslots; ?>
                        </tbody>
                  </table>
            </div>
         <?php } ?>
         </div>
         <!-- END Page content-->
       
<script>
  $("#master_ul").addClass("nav collapse in");
  $("#rsm").addClass("active");
</script>