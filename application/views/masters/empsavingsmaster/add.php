 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 
 $emp_bid = ($getSavingsMasterEdit->emp_bid)?$getSavingsMasterEdit->emp_bid:'';
 $sec_80c = ($getSavingsMasterEdit->sec_80c)?$getSavingsMasterEdit->sec_80c:'';
 $sec_80d = ($getSavingsMasterEdit->sec_80d)?$getSavingsMasterEdit->sec_80d:'';
 $sec_80cc = ($getSavingsMasterEdit->sec_80cc)?$getSavingsMasterEdit->sec_80cc:'';
 $sec_80g = ($getSavingsMasterEdit->sec_80g)?$getSavingsMasterEdit->sec_80g:'';
 $empsavingsmasters="";
$i=1;
foreach($get_empsavingsmaster as $row)
{
	$empsavingsmasters .= "<tr><td>".$i++."</td><td>".$row->emp_bid."</td><td>".$row->sec_80c."</td><td>".$row->sec_80d."</td><td>".$row->sec_80cc."</td><td>".$row->sec_80g."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Empsavingsmaster/edit/".base64_encode($row->empsavings_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Empsavingsmaster/delete/".base64_encode($row->empsavings_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Empsavingsmaster/undo/".base64_encode($row->empsavings_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_empsavingsmaster_log as $row)
{
	$empsavingsmasters .= "<tr><td>".$i++."</td><td>".$row->emp_bid."</td><td>".$row->sec_80c."</td><td>".$row->sec_80d."</td><td>".$row->sec_80cc."</td><td>".$row->sec_80g."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-6 col-md-offset-3">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">SAVINGS ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
						<div class="row input-panel">
							<div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Employee BID</label>
                              <div class="col-lg-6">
                                <select name="emp_id" class='form-control' id="emp_bid">
								<?php foreach($get_employees as $row) {  ?>
								   
								 <option value='<?php echo $row->emp_bid; ?>' <?php echo (($row->emp_bid == $emp_bid)?"selected":"");?>> <?php echo $row->emp_bid; ?></option>
								<?php } ?>
								</select>
                              </div>
                           </div>
						   <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Employee Name</label>
                              <div class="col-lg-6">
                                 <input type='text' class="form-control" id="emp_name" readonly />
                              </div>
                           </div>
					   </div>
					   <div class="row input-panel">
							<div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Sec 80 C</label>
                              <div class="col-lg-6">
                                 <input type="text" name="sec_80c" value ="<?php echo $sec_80c; ?>" placeholder="Sec 80 C" class="form-control">
                              </div>
                           </div>
					
							<div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Sec 80 D</label>
                              <div class="col-lg-6">
                                 <input type="text" name="sec_80d" value ="<?php echo $sec_80d; ?>" placeholder="Sec 80 D" class="form-control">
                              </div>
                           </div>
					   </div>
					   <div class="row input-panel">
					     <div class="form-group col-md-6">
                        <label class="col-lg-6 control-label">Sec 80 CC</label>
                        <div class="col-lg-6">
                           <input type="text" name="sec_80cc" value ="<?php echo $sec_80cc; ?>" placeholder="Sec 80 CC" class="form-control">
                        </div>
                     </div>
                     <div class="form-group col-md-6">
                        <label class="col-lg-6 control-label">Sec 80 G</label>
                        <div class="col-lg-6">
                           <input type="text" name="sec_80g" value ="<?php echo $sec_80g; ?>" placeholder="Sec 80 G" class="form-control">
                        </div>
                     </div>
					   </div>
					    <input type='hidden' name="status" value='1'>
					   <div class="row">
						  <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($empsavingsmasters)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
                              <th>Employee BID</th>
							  <th>Sec 80 C</th>
							  <th>Sec 80 D</th>
							  <th>Sec 80 CC</th>
                       <th>Sec 80 G</th>
							  <th>Date</th>
							   <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $empsavingsmasters; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
		 
<script>
$(document).ready(function(){
	var val = $("#emp_bid").val();
	
    $("#emp_bid").change(function(){ 
		$.ajax({
			url: '<?php echo base_url();?>Empsavingsmaster/getEmployeeName',
			data: {emp_bid:$(this).val()},
			method:"POST",
			success:function(data){
				$('#emp_name').val(data);
			}
		});
    });
	$("#emp_bid").trigger('change');
});
$("#master_ul").addClass("nav collapse in");
   $("#savm").addClass("active");
</script>