 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 
 $emp_bid = ($getAttendanceMarkingEdit->emp_bid)?$getAttendanceMarkingEdit->emp_bid:'';
 $holiday_id = ($getAttendanceMarkingEdit->holiday_id)?$getAttendanceMarkingEdit->holiday_id:'';
  $leave_type_id = ($getAttendanceMarkingEdit->leave_type_id)?$getAttendanceMarkingEdit->leave_type_id:'';
 $leave_date = ($getAttendanceMarkingEdit->leave_date)?$getAttendanceMarkingEdit->leave_date:'';
 $no_leaves = ($getAttendanceMarkingEdit->no_leaves)?$getAttendanceMarkingEdit->no_leaves:'';
 
 $empattendance_markings="";
$i=1;
foreach($get_attendance_marking as $row)
{
	$empattendance_markings .= "<tr><td>".$i++."</td><td>".$row->emp_bid."</td><td>".$row->emp_name."</td><td>".$row->dept_name."</td><td>".$row->leave_type."</td><td>".$row->no_leaves."</td><td><p><a href='".base_url()."attendance_marking/edit/".base64_encode($row->leave_master_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."attendance_marking/delete/".base64_encode($row->leave_master_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."attendance_marking/undo/".base64_encode($row->leave_master_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_attendance_marking_log as $row)
{
	$empattendance_markings .= "<tr><td>".$i++."</td><td>".$row->emp_bid."</td><td>".$row->emp_name."</td><td>".$row->dept_name."</td><td>".$row->leave_type."</td><td>".$row->no_leaves."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">

            <!-- START row-->
            <div class="row">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading"><?php echo $emp_bid; ?>BIO METRIC ATTENDANCE MARKING</div>
                     <div class="panel-body bgc-1">
                     	<h4>Selection Pan</h4>
                        <!-- START table-responsive-->
						
						<div class="row" id="attendence_form">
							<div class="col-md-12" >
							<div class="form-group col-md-2" >
							   <div class="col-md-12 p-0">
		                            <p class="text-center">Attendance Date</p>
		                            <input  class="form-control" style="line-height: 20px;" id="attendan"  type="date" name="attendance_date" value="<?php echo date('Y-m-d') ?>" >
		                        </div>
                           </div>
							<div class="form-group col-md-3">
								<div class="col-md-12 p-0">
	                              <p class="text-center">Employee BID</p>
                                <select name="emp_bid" class='form-control' id="emp_bid">
                                	<option value="">Select</option>
								<?php foreach($get_employees as $row) {  ?>
								   
								 <option value='<?php echo $row['emp_bid']; ?>' <?php echo (($row['emp_bid'] == $emp_bid)?"selected":"");?>> <?php echo $row['emp_bid']; ?></option>
								<?php } ?>
								</select>
                              </div>
							</div>							  
							  <div class="form-group col-md-3">
							   <div class="col-md-12 p-0">
	                              <p class="text-center">Employee Name</p>
                                 <input type='text' class="form-control" name="emp_name" id="emp_name" readonly />
								 <input type='hidden' class="form-control" name="emp_id" id="emp_id" readonly />
                              </div>
                           </div>
						   <div class="form-group  col-md-3">
							   <div class="col-md-12 p-0">
	                              <p class="text-center">Line Of Activity</p>
                                 <input type='text' class="form-control" name="activity_id" id="activity_id" readonly />
                              </div>
                           </div>
						  <div class="form-group col-md-2" >
							   <div class="col-md-12 p-0">
	                              <p class="text-center">Teaching Subject</p>
                                 <input type='text' class="form-control" name="sub_name" id="sub_name" readonly />
                              </div>
                           </div>
                           
                       </div>
                       </div>
					   <div class="row">
						  <div class="col-md-offset-5 btn-sec center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
            </div>
         	</div>
         <!-- END Page content-->
		 
<script>
$(document).ready(function(){
	var val = $("#emp_bid").val();
    $("#emp_bid").change(function(){ 
		$.ajax({
			url: '<?php echo base_url();?>biometric_attendance_marking/getEmployeeDetails',
			data: {emp_bid:$(this).val()},
			method:"POST",
			success:function(data){
				console.log(data);
				var obj=jQuery.parseJSON(data);
				$('#emp_name').val(obj.emp_name);
				$('#activity_id').val(obj.dept_name); 
				$('#sub_name').val(obj.sub_name); 
				$('#leave_master_id').html(obj.get_emp_leavemaster);
				$('#emp_id').val(obj.emp_id); 
				$("#employee_leave_types_id").html("");
				$("#employee_leave_types_id").html(obj.employee_leave_types_id);
				$("#total_leaves_id").html("");
				$("#total_leaves_id").html(obj.total_leaves_id);
				$("#remaining_leaves_id").html("");
				$("#remaining_leaves_id").html(obj.remaining_leaves_id);
			},
			error:function(error){
				console.log(error);
			}
		});
    });
	$("#emp_bid").trigger('change');
	 $("#summary_date").change(function(){ 
		$.ajax({
			url: '<?php echo base_url();?>attendance_marking/SummaryDetails',
			data: {summary_date:$(this).val()},
			method:"POST",
			success:function(data){
				console.log(data);
				var obj=jQuery.parseJSON(data);
				$("#leaved_employees_data_id").html("");
				$("#leaved_employees_data_id").html(obj.get_current_date_leaves);
				$("#leaves_count_summary_id").html("");
				$("#leaves_count_summary_id").html(obj.get_current_date_leave_emps);

			}
		});
    });
});
$("#assin_ul").addClass("nav collapse in");
$("#bmatten_li").addClass("active");
</script>
<style type="text/css">
	#attendence_form .form-group {

    	margin-left: -10px;
    	margin-right: -10px;
	}
	tr.custom_centered th,td{
		text-align: center;
	}

</style>