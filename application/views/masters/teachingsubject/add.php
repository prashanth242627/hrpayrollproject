 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 $sub_name = ($getsubject_detailsEdit->sub_name)?$getsubject_detailsEdit->sub_name:'';
 $sub_id = ($getsubject_detailsEdit->sub_id)?$getsubject_detailsEdit->sub_id:'';
 $subject_details="";
$i=1;
foreach($get_subject_details as $row)
{
   $subject_details .= "<tr><td>".$i++."</td><td>".$row->sub_name."</td><td>".$row->sub_id."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Teachingsubject/edit/".base64_encode($row->subject_details_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Teachingsubject/delete/".base64_encode($row->subject_details_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Teachingsubject/undo/".base64_encode($row->subject_details_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_subject_details_log as $row)
{
   $subject_details .= "<tr><td>".$i++."</td><td>".$row->sub_name."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-6 col-md-offset-3">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">TRECHING SUBJECT DETAILS ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
                  <div class="row input-panel">
                     <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Subject Name</label>
                              <div class="col-lg-6">
                                 <input type="text" name="sub_name" value ="<?php echo $sub_name; ?>" placeholder="Subject Name" class="form-control">
                              </div>
                           </div>
                        <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">ABBR</label>
                              <div class="col-lg-6">
                                 <input type="text" name="sub_id" value ="<?php echo $sub_id; ?>" placeholder="Subject id" class="form-control">
                              </div>
                           </div>
                     </div>
                  <div class="row">
                    <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
                  </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         <?php if(!empty($subject_details)) {  ?>
         <div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
                       <th>S.No</th>
					  <th>Subject Name</th>
                       <th>ABBR</th>
                       <th>Date</th>
                        <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $subject_details; ?>
                        </tbody>
                  </table>
            </div>
         <?php } ?>
         </div>
         <!-- END Page content-->
       
<script>
   $("#master_ul").addClass("nav collapse in");
   $("#tsm").addClass("active");
</script>