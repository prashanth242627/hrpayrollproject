 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 
 $activity_id = ($gethoureMasterEdit->activity_id)?$gethoureMasterEdit->activity_id:'';
 $per_hour_payment = ($gethoureMasterEdit->per_hour_payment)?$gethoureMasterEdit->per_hour_payment:'';

 $empextrahoursmasters="";
$i=1;
foreach($get_empextrahoursmaster as $row)
{
	$empextrahoursmasters .= "<tr><td>".$i++."</td><td>".$row->activity_id."</td><td>".$row->per_hour_payment."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Empextrahoursmaster/edit/".base64_encode($row->empextrahours_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Empextrahoursmaster/delete/".base64_encode($row->empextrahours_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Empextrahoursmaster/undo/".base64_encode($row->empextrahours_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_empextrahoursmaster_log as $row)
{
	$empextrahoursmasters .= "<tr><td>".$i++."</td><td>".$row->activity_id."</td><td>".$row->per_hour_payment."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EXTRA HOUES PAYMENT MASTER FOR LINE OF ACTIVITY</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
						<div class="row">
							<div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Employee Line of Activity</label>
                              <div class="col-lg-6">
                                <select name="activity_id" class='form-control' id="activity_id">
								<?php foreach($get_empextrahoursmasters as $row) {  ?>
								   
								 <option value='<?php echo $row->activity_id; ?>' <?php echo (($row->activity_id == $activity_id)?"selected":"");?>> <?php echo $row->dept_name; ?></option>
								<?php } ?>
								</select>
                              </div>
                           </div>
					  
							<div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Per Hour Payment</label>
                              <div class="col-lg-6">
                                 <input type="text" name="per_hour_payment" value ="<?php echo $per_hour_payment; ?>" placeholder="Per Hour Payment" class="form-control">
                              </div>
                           </div>
					   </div>
					    <input type='hidden' name="status" value='1'>
					   <div class="row">
						  <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($empextrahoursmasters)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
                              <th>Activity Id</th>
							  <th>Per Hour Payment</th>
							  <th>Date</th>
							  <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $empextrahoursmasters; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
		 
<script>
  $("#master_ul").addClass("nav collapse in");
   $("#exhm").addClass("active");
</script>