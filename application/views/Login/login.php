<!DOCTYPE html>

<html lang="en" class="no-ie">
<head>
   <!-- Meta-->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <meta name="author" content="">
   <title>HR & Payroll || Login</title>

   <link rel="stylesheet" href="<?php echo base_url();?>app/css/bootstrap.css">
   <!-- Vendor CSS-->
   <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/fontawesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animo/animate%2banimo.css">
   <!-- App CSS-->
   <link rel="stylesheet" href="<?php echo base_url(); ?>app/css/app.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>app/css/common.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/hrpayroll.css">
   <!-- Modernizr JS Script-->
   <script src="<?php echo base_url();?>vendor/modernizr/modernizr.js" type="application/javascript"></script>
   <!-- FastClick for mobiles-->
   <script src="<?php echo base_url();?>vendor/fastclick/fastclick.js" type="application/javascript"></script>
     <script src="<?php echo base_url(); ?>assets/js/gen_validatorv4.js" ></script>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
</head>

<body>
   <!-- START wrapper-->
   <div class="row row-table page-wrapper">
      <div class="col-lg-3 col-md-6 col-sm-8 col-xs-12 align-middle">
         <!-- START panel-->
         <div data-toggle="play-animation" data-play="fadeIn" data-offset="0" class="panel panel-dark panel-flat">
            <div class="panel-heading text-center">
               <a href="#">
                  <img src="<?php echo base_url();?>images/logo.png" alt="Image" class="block-center img-rounded">
               </a>
               <p class="text-center mt-lg">
                  <strong>SIGN IN TO CONTINUE.</strong>
               </p>
            </div>
            <div class="panel-body">
               <form role="form" class="mb-lg" method="post" id="login" action="">
                  <div class="text-right mb-sm"><a href="#" class="text-muted">Need to Signup?</a>
                  </div>
                  <div class="form-group has-feedback">
                     <input id="exampleInputEmail1" type="text" placeholder="Enter user name" name="username" class="form-control">
                     <span class="fa fa-envelope form-control-feedback text-muted"></span>
					 <div id='login_username_errorloc' class="error_strings"></div>
                  </div>
                  <div class="form-group has-feedback">
                     <input id="exampleInputPassword1" type="password" placeholder="Password" class="form-control" name="password">
                     <span class="fa fa-lock form-control-feedback text-muted"></span>
					 <div id='login_password_errorloc' class="error_strings"></div>
                  </div>
                  <div class="clearfix">
                     <div class="checkbox c-checkbox pull-left mt0">
                        <label>
                           <input type="checkbox" value="">
                           <span class="fa fa-check"></span>Remember Me</label>
                     </div>
                     <div class="pull-right"><a href="#" class="text-muted">Forgot your password?</a>
                     </div>
                  </div>
                  <input type="submit" class="btn btn-block btn-success" value="Login">
               </form>
			   <script language="JavaScript" type="text/javascript"
				xml:space="preserve">//<![CDATA[
				//You should create the validator only after the definition of the HTML form
				  var frmvalidator  = new Validator("login");
					frmvalidator.EnableOnPageErrorDisplay();
					frmvalidator.EnableMsgsTogether();
					frmvalidator.addValidation("username","req","Please enter your User Name");
					frmvalidator.addValidation("password","req","Please enter your Password");
				//]]></script>
            </div>
         </div>
         <!-- END panel-->
      </div>
   </div>
   <!-- END wrapper-->
   <!-- START Scripts-->
   <!-- Main vendor Scripts-->

   <script src="<?php echo base_url();?>vendor/bootstrap/js/bootstrap.min.js"></script>
   <!-- Animo-->
   <script src="<?php echo base_url();?>vendor/animo/animo.min.js"></script>
   <!-- Custom script for pages-->
   <script src="<?php echo base_url();?>app/js/pages.js"></script>
   <!-- END Scripts-->
</body>

</html>
<?php $this->load->view('theme/notifications'); ?>
  