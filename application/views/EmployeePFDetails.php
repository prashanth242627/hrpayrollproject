<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (isset($_GET['month']) && isset($_GET['year'])) {
   
   if (isset($_GET['bid'])) {
      $getParam="bid=".$_GET['bid']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (isset($_GET['ename'])) {
      $getParam="ename=".ltrim($_GET['ename'])."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (strpos($_SERVER['REQUEST_URI'], '/hrpayroll/EmpPFContribution/All') !== false) {
      $getParam="type=All&month=".$_GET['month']."&year=".$_GET['year'];
    }
    elseif (isset($_GET['type'])) {
      $getParam="type=".$_GET['type']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else{
      $getParam="";
    }
  }

?>


      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->

                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Statutory PF Returns</div>
                     <div class="panel-body" style="overflow-x: scroll;">
                     <form>
                          <div >
                           <div class="form-group col-md-3 bgc-2">
                              <p class="text-center"><b>Department</b></p>
                              <select name="account" class="form-control m-b" id="line_of_activity_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/EmpPFContribution/All')) { ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php echo $activity["dept_name"]; ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>                            
                            <div class="form-group col-md-3 bgc-5">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>BY E.Name</b></p>

                              <select name="account" class="form-control m-b" id="emp_name"> 
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/EmpPFContribution/All')) { ?> selected <?php } ?> >All</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo $name['emp_name']; ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) {?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>
                           <div class="form-group col-md-2 bgc-4">
                          <div class="col-md-12 p-0">
                              <p class="text-center"><b>Search BY B ID</b></p>
                              <select name="account" class="form-control m-b" id="emp_bid_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/EmpPFContribution/All')) {  ?> selected <?php } ?> >All</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo $bid['emp_bid']; ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                            </div>
                           <div class="form-group col-md-2 bgc-3">
                              <div class="col-md-10 p-0">
                              <p class="text-center">Year</p>
                              <select name="account" class="form-control m-b" id="year_id">
                                 <option value="">Select</option>
                                 <option value="2019" <?php if (isset($_GET['year']) && $_GET['year']==2019) {  ?> selected <?php } ?>>2019</option>
                                 <option value="2020" <?php if (isset($_GET['year']) && $_GET['year']==2020) {  ?> selected <?php } ?>>2020</option>
                                 
                              </select>
                              </div>
                            </div>
                           <div class="form-group col-md-2 bgc-1">
                               <div class="col-md-10 p-0">
                              <p class="text-center"><b>Month</b></p>

                              <select name="account" class="form-control m-b" id="month_id"> 
                                 <option value="">Select</option>
                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
                                 } ?>
                              </select>
                              </div>
                           </div>
                   
                           </div>
                        </form>

                      <div class="row input-padding">
                          <div class="form-group col-md-3">
                              <label class="col-lg-6 control-label p-0">Selection Criteria: </label>
                              <div class="col-lg-6 p-0">
                                 <input type="text" name="designation_name" value="<?php 
                                 if(isset($_GET['type'])) { echo $_GET['type']; } 
                                 else if(isset($_GET['bid'])){ echo "EMP BID =".$_GET['bid']; } 
                                 else if(isset($_GET['ename'])){ echo "EMP ENAME =".$_GET['ename']; }
                                 else if(strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/EmpPFContribution/All')){ echo "All"; } ?>" placeholder="Department" class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-2">
                              <label class="col-lg-6 control-label p-0 text-right pt-9">Month: </label>
                              <div class="col-lg-6 p-0">
                                 <input type="text" name="desig_id" value="<?php if(isset($_GET['month'])){  echo $_GET['month']; } ?>" placeholder="Month" class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-2">
                              <label class="col-lg-6 control-label p-0 text-right pt-9">Year: </label>
                              <div class="col-lg-6 p-0">
                                 <input type="text" name="desig_id" value="<?php if(isset($_GET['year'])){  echo $_GET['year']; } ?>" placeholder="year" class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-3">
                              <label class="col-lg-6 control-label p-0 text-right pt-9">Gross Wages(Rs): </label>
                              <div class="col-lg-6 p-0">
                                 <input type="text" name="desig_id" id="gross_id_val" value="" placeholder="Total Contr.." class="form-control p-0">
                              </div>
                           </div>
                            <div class="form-group col-md-2">
                              <label class="col-lg-6 control-label p-0 text-right pt-9">Total(Rs): </label>
                              <div class="col-lg-6 p-0">
                                 <input type="text" name="desig_id" id="net_id_val" value="" placeholder="emp contri.." class="form-control p-0">
                              </div>
                           </div>
                        </div>

                        <!-- START table-responsive-->
                     <div class="table-responsive fixtable">
                       <table class="table  table-bordered table-hover" id="fixTable">
                        <thead>
                          <tr>
                            <td>S.No</td>
                            <td>E ID</td>
                            <td>E BID</td>
                            <td>E Name</td>
                            <td>Designation</td>
                            <td>Department</td>
                            <td>PF A/C No</td>
                            <td>UAN</td>
                            <td>Gross Wages</td>
                            <td>PF Wages</td>
                            <td>PF</td>
                            <td>EPS Contribution (A/C.10)</td>
                            <td>EPF Contribution (A/C.1)</td>
                            <td>Admin Charges (A/C.2)</td>
                            <td>INSP Charges (A/C.21)</td>
                            <td>EDLI Charges (A/C.22)</td>
                            <td>Total</td>
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                         
                         <?php if (isset($employees)) {
                           $i=1;
                           $gross_wages=0;
                           $pf_wages=0;
                           $PF=0;
                           $EPS_Contribution=0;
                           $EPF_Contribution=0;
                           $admin_charges_Contribution=0;
                           $insp_charges_Contribution=0;
                           $EDLI_Charges=0;
                           $Total=0;
                           
                           foreach ($employees as $employee) { 
                            $gross_wages+=round($employee['gross_wages']);
                           $pf_wages+=round($employee['pf_wages']);
                           $PF+=round($employee['PF']);
                           $EPS_Contribution+=round($employee['EPS_Contribution']);
                           $EPF_Contribution+=round($employee['EPF_Contribution']);
                           $admin_charges_Contribution+=round($employee['admin_charges_Contribution']);
                           $insp_charges_Contribution+=round($employee['insp_charges_Contribution']);
                           $EDLI_Charges+=round($employee['EDLI_Charges']);
                           $Total+=round($employee['Total']);

                           
                           ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $employee['emp_id'] ?></td>
                           <td><?php echo $employee['emp_bid'] ?></td>
                           <td><?php echo $employee['emp_name'] ?></td>
                           <td><?php echo $employee['designation_name'] ?></td>
                           <td><?php echo $employee['dept_name'] ?></td>
                           <td><?php echo $employee['pf_acc_no'] ?></td>
                           <td><?php echo $employee['uan'] ?></td>
                           <td><?php echo round($employee['gross_wages']) ?></td>
                           <td><?php echo round($employee['pf_wages']) ?></td>
                           <td><?php echo round($employee['PF']) ?></td>
                           <td><?php echo round($employee['EPS_Contribution']) ?></td>
                           <td><?php echo round($employee['EPF_Contribution']) ?></td>
                           <td><?php echo round($employee['admin_charges_Contribution']) ?></td>
                           <td><?php echo round($employee['insp_charges_Contribution']) ?></td>
                           <td><?php echo round($employee['EDLI_Charges']) ?></td>
                           <td><?php echo round($employee['Total']) ?></td>
                         </tr>
                        <?php } ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td> 
                            <td>Total</td>
                            <td id="gross_id"><?php echo $gross_wages; ?></td>
                            <td ><?php echo $pf_wages; ?></td>
                            <td><?php echo $PF; ?></td>
                            <td><?php echo $EPS_Contribution; ?></td>
                            <td ><?php echo $EPF_Contribution; ?></td>
                            <td><?php echo $admin_charges_Contribution; ?></td>
                            <td><?php echo $insp_charges_Contribution; ?></td>
                            <td><?php echo $EDLI_Charges; ?></td>
                            <td id="net_id"><?php echo $Total; ?></td>
                         </tr> <?php } else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>
                            <td>No Records</td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>
                            <td></td>
                             <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>
                            <td></td>
                             <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>

   <script type="text/javascript" >
      $("#gross_id_val").val($("#gross_id").html());
    $("#net_id_val").val($("#net_id").html());
     var base_url="<?php echo base_url(); ?>";
     $("#emp_bid_id").on("change",function(){
        $("#month_id").val("");
        $("#line_of_activity_id").val("");
        $("#year_id").val("");
        $("#emp_name").val("");
      });
      $("#line_of_activity_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#emp_bid_id").val("");
        $("#emp_name").val("");
      });
      $("#emp_name").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#line_of_activity_id").val("");
        $("#emp_bid_id").val("");
      });
      $("#year_id").on("change",function(){
        $("#month_id").val("");
      });
      $("#month_id").on("change",function(){
        var line_of_activity= $("#line_of_activity_id").val();
        var emp_bid= $("#emp_bid_id").val(); 
        var emp_name=$('#emp_name').val();
        var year=$("#year_id").val();
        var month=$("#month_id").val();
        if (year!="" && month!="") {
          if (emp_bid=="All" || emp_name=="All") {
            window.location.replace(base_url+"EmpPFContribution/All?month="+month+"&year="+year);
          }
          else if (emp_bid!="") {
            window.location.replace(base_url+"EmpPFContribution/activity?bid="+emp_bid+"&month="+month+"&year="+year);
          }
          else if (emp_name!="" && emp_name!=null) {
            window.location.replace(base_url+"EmpPFContribution/activity?ename="+emp_name+"&month="+month+"&year="+year);
          }
          else if ( line_of_activity=="All") {
            window.location.replace(base_url+"EmpPFContribution/All?month="+month+"&year="+year);
          }
          else if (line_of_activity!="") {
            window.location.replace(base_url+"EmpPFContribution/activity?type="+line_of_activity+"&month="+month+"&year="+year);
          }
        }
      })
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/EmpPFContribution?"+"<?php echo $getParam; ?>";
      }
      $("#reports_ul").addClass("nav collapse in");
      $("#epfr_li").addClass("active");
   </script>
