

<?php 

require_once BASEPATH . '/helpers/url_helper.php'; 



$getParam="";



  if (isset($_GET['year']) && isset($_GET['month'])) {

     $getParam="year=".$_GET['year']."&month=".$_GET['month'];

  }







?>

      <!-- START Main section-->

      <section>

         <!-- START Page content-->

         <div class="main-content">

            <!-- START row-->

            <div class="row">

               <div class="col-md-12">

                      <div class="panel panel-default" style="overflow: auto;">

                     <div class="panel-heading form-heading">Bank Statement</div>

                     <div class="panel-body ptb">

                     <form>

                      <div class="col-md-8 col-md-offset-2">

                            <div class="form-group  col-md-6 bgc-3">

                              <div class="col-md-10 p-0">

                              <p class="text-center"><b>Year</b></p>

                              <select name="account" class="form-control m-b" id="year_id">

                                 <option value="">Select</option>
                                 <option value="2019" <?php if (isset($_GET['year']) && $_GET['year']==2019) {  ?> selected <?php } ?>>2019</option>
                                 <option value="2020" <?php if (isset($_GET['year']) && $_GET['year']==2020) {  ?> selected <?php } ?>>2020</option>

                                 

                              </select>

                              </div>

                            </div>

                           <div class="form-group col-md-6 bgc-1">

                               <div class="col-md-10 p-0">

                              <p class="text-center"><b>Month</b></p>



                              <select name="account" class="form-control m-b" id="month_id"> 

                                 <option value="">Select</option>

                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>

                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>

                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>

                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>

                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>

                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>

                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>

                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>

                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>

                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>

                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>

                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>

                                 } ?>

                              </select>

                              </div>

                           </div>



                           <div class="form-group col-md-3 bgc-2" style="display: none;"><div class="col-md-6 p-0">

                              <p class="text-center">Date From</p>

                              <input type="date" class="form-control" id="start_date" value="<?php if(isset($_GET['from'])){  echo $_GET['from']; } ?>">

                              </div>

                               <div class="col-md-6 p-0">

                              <p class="text-center">Date To</p>

                              <input type="date" class="form-control" id="end_date" value="<?php if(isset($_GET['to'])){ echo $_GET['to']; } ?>">

                              </div>

                              <p></p>

                           </div>

                           <!-- <div class="form-group col-md-2 bgc-2">

                              <p class="text-center">LINE OF ACTIVITY</p>

                              <select name="account" class="form-control m-b">

                                 <option> All</option>

                              </select>

                           </div> -->

                   

                           </div>

                        </form>



                        <!-- START table-responsive-->

                    <div class="table-responsive fixtable" id="parent">

                      <table class="table  table-bordered table-hover" id="fixTable">

                        <thead>

                          <tr>

                            <th class="text-center">SL.NO</th>

                            <th class="text-center">Emp.Id</th>

                            <th class="text-center">Emp.Bid</th> 

                            <th class="text-center">Emp.Name</th>

                            

                            <th class="text-center">Account Number</th>

                            <th class="text-center">Net Salary</th>

                         </tr>

                       </thead> 

                         <?php if (isset($reports)) {

                           $i=1;

                           foreach ($reports as $report) {  ?>

                        <tr >

                           <td class="text-center"><?php echo $i++; ?></td>

                           <td class="text-center"><?php echo $report['temp_emp_id'] ?></td>

                           <td class="text-center"><?php echo $report['emp_bid'] ?></td>

                           <td class="text-center"><?php echo $report['emp_name'] ?></td>

                           

                           <td class="text-center"><?php echo $report['acc_no'] ?></td>

                           <td class="text-center"><?php echo round($report['net_income']-$report['management_deductions']) ?></td>

                           

                         </tr>

                        <?php } } else{ ?>

                         <tr>

                            <td></td>


                            <td></td>
                            <td></td>
                            <td>no Records Found</td>

                            <td></td>

                            <td></td>


                         </tr>



                         <tr>

                            <td></td>
                            <td></td>

                            <td></td>


                            <td></td>

                            <td></td>

                            <td></td>

                         </tr>

                      <?php } ?>

                        </tbody>

                  </table>

                        </div>

                        <!-- END table-responsive-->

                 

                     </div>

                  </div>

               </div>

            </div>

                   <div class="col-md-12 btn-sec text-center">

                  <ul class="btn-row">

                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->

                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>

                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->

                  </ul>

                  </div>

            <!-- END row-->

         </div>

   <!-- END Scripts-->

   <script type="text/javascript" >

     var base_url="<?php echo base_url(); ?>";

      $("#month_id").on("change",function(){

        var emp_bid= $("#emp_bid_id").val();

         var year= $("#year_id").val();

         var month= $("#month_id").val();

           if (year!="") {

              window.location.replace(base_url+"BankStatement/activity?year="+year+"&month="+month);

           }

          /* else{

              window.location.href=base_url+"BankStatement/activity?month="+month;

           }
*/
         

      })

      var employee_data=<?php if( isset($employees) && count($employees)){ echo json_encode($employees); } else{echo "{}"; } ?>;

      function exportToExcel() {

         window.location.href=base_url+"Excel_export/BankStatement?"+"<?php echo $getParam; ?>";

      }

      $("#reports_ul").addClass("nav collapse in");

      $("#mar_li").addClass("active");

   </script>







