<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (isset($_GET['month']) && isset($_GET['year'])) {
   
   if (strpos($_SERVER['REQUEST_URI'], "/applications/hrpayroll/DepartmentSalaries/All") !==false) {  
    $getParam="type=All&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (isset($_GET['type'])) {
      $getParam="type=".$_GET['type']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
}
?>
      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->

            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                    
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading" >Department Salaries</div>
                     <div class="panel-body" style="overflow-x: scroll;">
                      <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-4 bgc-2">
                              <p class="text-center"><b>Department</b></p>
                              <select name="account" class="form-control m-b" id="line_of_activity_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if (strpos($_SERVER['REQUEST_URI'], "/applications/hrpayroll/DepartmentSalaries/All") !==false) {?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php echo $activity["dept_name"]; ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) {?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                           <div class="form-group col-md-4 bgc-3">
                              <div class="col-md-12 p-0">
                              <p class="text-center"><b>Year</b></p>
                              <select name="account" class="form-control m-b" id="year_id">
                                 <option value="">Select</option>
                                 <option value="2019" <?php if (isset($_GET['year']) && $_GET['year']==2019) {  ?> selected <?php } ?>>2019</option>
                                 <option value="2020" <?php if (isset($_GET['year']) && $_GET['year']==2020) {  ?> selected <?php } ?>>2020</option>
                                 
                              </select>
                              </div>
                            </div>
                           <div class="form-group col-md-4 bgc-1">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>Month</b></p>

                              <select name="account" class="form-control m-b" id="month_id"> 
                                 <option value="">Select</option>
                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
                                 } ?>
                              </select>
                              </div>
                           </div>
                   
                           </div>
                        </form>
                        <div class="row input-padding">
                          <div class="col-md-12">
                          <div class="form-group col-md-3">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Selection Criteria:</label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" readonly="" value="<?php 
                                 if(isset($_GET['type'])) { echo $_GET['type']; } else if (strpos($_SERVER['REQUEST_URI'], "/applications/hrpayroll/DepartmentSalaries/All") !==false) {  echo 'ALL'; } ?>" placeholder="Designation Name" class="form-control p-0">
                              </div>
                           </div>
                          <div class="form-group col-md-3">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Month: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" readonly="" value="<?php 
                                 if(isset($_GET['month'])) { echo $_GET['month']; } 
                                ?>" placeholder="Month" class="form-control p-0">
                              </div>
                           </div>
                          <div class="form-group col-md-3">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Year: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" readonly="" value="<?php 
                                 if(isset($_GET['year'])) { echo $_GET['year']; } 
                                ?>" placeholder="Year" class="form-control p-0">
                              </div>
                           </div>
                         </div>
                        </div>
                        <!-- START table-responsive-->
                     <div class="table-responsive">
                       <table class="table  table-bordered table-hover">
                        <thead>
                          <tr >
                            <th >SL.NO</th>
                            <th >Department</th> 
                            <th >Employee Count</th>
                            <th >Gross Pay(Rs)</th>
                            <th >Deductions(Rs)</th>
                            <th >Net Pay(Rs)</th>
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                         
                         <?php if (isset($salaries)) {
                           $i=1;
                           foreach ($salaries as $salary) { 
                           ?>
                        <tr>
                           <td><?php echo $i; ?></td>
                           <td><?php echo $salary['dept_name'] ?></td>
                           <td><?php echo $salary['emp_count'] ?></td>
                           <td><?php echo round($salary['Gross_pay']) ?></td>
                           <td><?php echo round($salary['deductions']) ?></td>
                           <td><?php echo round($salary['Net_Pay']) ?></td>
                           	
                         </tr>
                        <?php 
                        $i++;
                      }} else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>

   <script type="text/javascript">
      var base_url="<?php echo base_url(); ?>";
      $("#employee_count").val(<?php echo $i-1; ?>);
      $("#line_of_activity_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
      });
      $("#year_id").on("change",function(){
        $("#month_id").val("");
      });
      $("#month_id").on("change",function(){
        var year=$("#year_id").val();
        var month=$("#month_id").val();
        var dept=$("#line_of_activity_id").val();

        if (year!="" && month!="") {
          if (dept=="All") {
            window.location.replace(base_url+"DepartmentSalaries/All?month="+month+"&year="+year);
          }
          else if (dept!="") {
            window.location.replace(base_url+"DepartmentSalaries/activity?type="+dept+"&month="+month+"&year="+year);
          }
        }
      });
      
      var employee_data=<?php if( isset($employees) && count($employees)){ echo json_encode($employees); } else{echo "{}"; } ?>;
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/DepartmentSalaries?"+"<?php echo $getParam; ?>";
      }
      $("#reports_ul").addClass("nav collapse in");
      $("#deptsal_li").addClass("active");
</script> 