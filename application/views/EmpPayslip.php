<!DOCTYPE html>
<html lang="en" class="no-ie">
<head>
   <!-- Meta-->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <meta name="author" content="">
   <title>HR & Payroll</title>

   <link rel="stylesheet" href="app/css/bootstrap.css">
   <!-- Vendor CSS-->
   <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="vendor/animo/animate%2banimo.css">
   <link rel="stylesheet" href="vendor/csspinner/csspinner.min.css">
   <!-- START Page Custom CSS-->
   <!-- END Page Custom CSS-->
   <!-- App CSS-->
   <link rel="stylesheet" href="app/css/app.css">
   <!-- Modernizr JS Script-->
   <script src="vendor/modernizr/modernizr.js" type="application/javascript"></script>
   <!-- FastClick for mobiles-->
   <script src="vendor/fastclick/fastclick.js" type="application/javascript"></script>
</head>

<body>
   <!-- START Main wrapper-->
   <div class="wrapper">
      <!-- START Top Navbar-->
     <nav role="navigation" class="navbar navbar-default navbar-top navbar-fixed-top">
         <!-- START navbar header-->
         <div class="navbar-header">
            <a href="dashboard.html" class="navbar-brand">
               <div class="brand-logo">
                  <img src="images/logo.png" alt="App Logo" class="img-responsive">
               </div>
               <div class="brand-logo-collapsed">
                  <img src="app/img/logo-single.png" alt="App Logo" class="img-responsive">
               </div>
            </a>
         </div>
         <!-- END navbar header-->
         <!-- START Nav wrapper-->
         <div class="nav-wrapper">
            <!-- START Left navbar-->
            <ul class="nav navbar-nav">
               <li>
                  <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                  <a href="#" data-toggle-state="aside-collapsed" data-persists="true" class="hidden-xs">
                     <em class="fa fa-navicon"></em>
                  </a>
                  <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                  <a href="#" data-toggle-state="aside-toggled" class="visible-xs">
                     <em class="fa fa-navicon"></em>
                  </a>
               </li>
               <!-- START Messages menu (dropdown-list)-->
        
               <!-- END Messages menu (dropdown-list)-->
               <li>
                  <!-- Button to clear all site options stored option from browser storage-->
                  <a href="#" data-toggle="reset">
                     <em class="fa fa-refresh"></em>
                  </a>
               </li>
            </ul>
            <!-- END Left navbar-->
            <!-- START Right Navbar-->
            <ul class="nav navbar-nav navbar-right">
               <!-- Search icon-->
               <li>
                  <a href="#" data-toggle="navbar-search">
                     <em class="fa fa-search"></em>
                  </a>
               </li>
               <!-- START Alert menu-->
               <li class="dropdown dropdown-list">
                  <a href="#" data-toggle="dropdown" data-play="fadeIn" class="dropdown-toggle">
                     <em class="fa fa-bell-o"></em>
                     <div class="label label-info">35</div>
                  </a>
                  <!-- START Dropdown menu-->
                  <ul class="dropdown-menu">
                     <li>
                        <!-- START list group-->
                        <div class="list-group">
                           <!-- list item-->
                           <a href="#" class="list-group-item">
                              <div class="media">
                                 <div class="pull-left">
                                    <em class="fa fa-envelope-o fa-2x text-success"></em>
                                 </div>
                                 <div class="media-body clearfix">
                                    <div class="media-heading">Unread messages</div>
                                    <p class="m0">
                                       <small>You have 10 unread messages</small>
                                    </p>
                                 </div>
                              </div>
                           </a>
                           <!-- list item-->
                           <a href="#" class="list-group-item">
                              <div class="media">
                                 <div class="pull-left">
                                    <em class="fa fa-cog fa-2x"></em>
                                 </div>
                                 <div class="media-body clearfix">
                                    <div class="media-heading">New settings</div>
                                    <p class="m0">
                                       <small>There are new settings available</small>
                                    </p>
                                 </div>
                              </div>
                           </a>
                           <!-- list item-->
                           <a href="#" class="list-group-item">
                              <div class="media">
                                 <div class="pull-left">
                                    <em class="fa fa-fire fa-2x"></em>
                                 </div>
                                 <div class="media-body clearfix">
                                    <div class="media-heading">Updates</div>
                                    <p class="m0">
                                       <small>There are
                                          <span class="text-primary">2</span>new updates available</small>
                                    </p>
                                 </div>
                              </div>
                           </a>
                           <!-- last list item -->
                           <a href="#" class="list-group-item">
                              <small>Unread notifications</small>
                              <span class="badge">14</span>
                           </a>
                        </div>
                        <!-- END list group-->
                     </li>
                  </ul>
                  <!-- END Dropdown menu-->
               </li>
               <!-- END Alert menu-->
               <!-- START User menu-->
               <li class="dropdown">
                  <a href="#" data-toggle="dropdown" data-play="fadeIn" class="dropdown-toggle">
                     Profile
                  </a>
                  <!-- START Dropdown menu-->
                  <ul class="dropdown-menu">
                     <li class="divider"></li>
                     <li><a href="#">Profile</a>
                     </li>
                     <li><a href="#">Settings</a>
                     </li>
                     <li><a href="#">Notifications<div class="label label-info pull-right">5</div></a>
                     </li>
                     <li><a href="#">Messages<div class="label label-danger pull-right">10</div></a>
                     </li>
                     <li><a href="#">Logout</a>
                     </li>
                  </ul>
                  <!-- END Dropdown menu-->
               </li>
            </ul>
            <!-- END Right Navbar-->
         </div>
         <!-- END Nav wrapper-->
         <!-- START Search form-->
         <form role="search" class="navbar-form">
            <div class="form-group has-feedback">
               <input type="text" placeholder="Type and hit Enter.." class="form-control">
               <div data-toggle="navbar-search-dismiss" class="fa fa-times form-control-feedback"></div>
            </div>
            <button type="submit" class="hidden btn btn-default">Submit</button>
         </form>
         <!-- END Search form-->
      </nav>
      <!-- END Top Navbar-->
      <!-- START aside-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
          <nav class="sidebar">
            <ul class="nav">
               <!-- START Menu-->
              <!-- START Menu-->
                 <li>
                  <a href="main.html" title="Home">
                  <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text">Home</span>
                  </a>
         
               </li>

               <li>
                  <a href="#" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-dot-circle-o"></em>
                     <div class="label label-primary pull-right">12</div>
                     <span class="item-text">Masters Screens</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse">
                     <li>
                        <a href="EmpLineofActivityMaster.html" title="Emp Line of Activity Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Line of Activity Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpTeachingsubjectMaster.html" title="Emp Teaching subject Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Teaching subject Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpResidenced.html" title="Emp Residenced" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Residences, Transport Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpReportingslotsMaster.html" title="Emp Reporting slots Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Reporting slots Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpMaster.html" title="Emp Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="ModeofsalryPaymentMaster.html" title="Mode of salry Payment Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Mode of salary Payment Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpPayscaleMaster.html" title="Emp Payscale Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Pay-scale Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpDeductionsMaster.html" title="Emp Deductions Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Deductions Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpsavingsMaster.html" title="Emp savings Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp savings Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpExtraHoursMaster.html" title="Emp Extra Hours Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Extra Hours Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpLeavetypeMaster.html" title="Emp Leave type Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Leave type Master</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpLeaveMaster.html" title="Emp Leave Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Leave Master</span>
                        </a>
                     </li>

                  </ul>
                  <!-- END MASTER SCRREN-->
               </li>
              <!--************************************
                        ASSIGNMETS /ENTRIES SCREENS
              ******************************** -->
               <li>
                  <a href="#" title="Assignments Entries" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text"> Assignments Entries </span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse">
                     <li>
                        <a href="AttendanceMarkingEntryscreen.html" title="Attendance Marking Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">Attendance Marking Entry screen</span>
                        </a>
                     </li>
                     <li>
                        <a href="BioMetricLeavesscreen.html" title="Bio Metric Leaves and LOP Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">Bio Metric Leaves and LOP Entry screen</span>
                        </a>
                     </li>
                  </ul>
                  <!-- END SubMenu item-->
               </li>
               <!--************************************
                      REPORTS
              ******************************** -->
               <li class="active">
                  <a href="#" title="Pages" data-toggle="collapse-next" class="has-submenu">
                      <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text">Reports</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse in">
                     <li>
                        <a href="#." title="HR REPORTS">
                           HR REPORTS
                        </a>
                     </li>
                     <li>
                        <a href="Attendancereport.html" title="Attendance report" data-toggle="" class="no-submenu">
                           <span class="item-text">Attendance report</span>
                        </a>
                     </li>
                     <li>
                        <a href="BioMetricattendancereport.html" title="Bio Metric attendance report" data-toggle="" class="no-submenu">
                           <span class="item-text">Bio Metric attendance report</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmployeevsBioMetric.html" title="Employee attendance vs Bio Metric reporting" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee attendance vs Bio Metric reporting</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmployeeLeaveavailreport.html" title="Employee Leave avail report (CL,EL,SL,ML)" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee Leave avail report (CL,EL,SL,ML)</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmployeeLeavestatus.html" title="Employee Leave status" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee Leave status</span>
                        </a>
                     </li>
                     <li>
                        <a href="Listofabsentees.html" title="List of absentees" data-toggle="" class="no-submenu">
                           <span class="item-text">List of absentees</span>
                        </a>
                     </li>
                     <li>
                        <a href="HolidaysCalender.html" title="Holidays Calender" data-toggle="" class="no-submenu">
                           <span class="item-text">Holidays Calender</span>
                        </a>
                     </li>
                     <li>
                        <a href="ListofEmployeesonOD.html" title="List of Employees on OD" data-toggle="" class="no-submenu">
                           <span class="item-text">List of Employees on OD</span>
                        </a>
                     </li>
                     <li>
                        <a href="AvtiveEmployeedetails.html" title="Avtive Employee details" data-toggle="" class="no-submenu">
                           <span class="item-text">AvtiveEmployeedetails</span>
                        </a>
                     </li>
                     <li>
                        <a href="NonActiveEmployees.html" title="Non Active Employees" data-toggle="" class="no-submenu">
                           <span class="item-text">Non Active Employees</span>
                        </a>
                     </li>
                     <li>
                        <a href="NewEmployeesList.html" title="New Employees List" data-toggle="" class="no-submenu">
                           <span class="item-text">New Employees List</span>
                        </a>
                     </li>


                     <li style="list-style-type: none;">
                        <a href="Listofabsentees.html" title="List of absentees" data-toggle="" class="no-submenu">
                           <span class="item-text"><b>FINANCE REPORTS</b></span>
                        </a>
                     </li>
                     <li>
                        <a href="Empsalarystructure.html" title="Emp. salary structure" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. salary structure</span>
                        </a>
                     </li>
                     <li>
                        <a href="Empsalarydetails.html" title="Emp. salary details" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. salary details</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpsalarytoBanksubmission.html" title="Emp. salary to Bank submission" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. salary to Bank submission</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpCHQPaymentList.html" title="Emp.CHQ Payment List" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp.CHQ Payment List</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpCashPaymentList.html" title="Emp.Cash Payment List" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp.Cash Payment List</span>
                        </a>
                     </li>
                     <li>
                        <a href="Employercontribution.html" title="Employer contribution" data-toggle="" class="no-submenu">
                           <span class="item-text">Employer contribution</span>
                        </a>
                     </li>
                     <li class="active">
                        <a href="EmpPayslip.html" title="Emp.Payslip" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp.Payslip</span>
                        </a>
                     </li>
                     <li>
                        <a href="DeptwiseSalarayPaymentsummary.html" title="Dept wise Salaray Payment summary" data-toggle="" class="no-submenu">
                           <span class="item-text">Dept wise Salary Payment summary</span>
                        </a>
                     </li>
                     <li>
                        <a href="Employeesavings.html" title="Employee savings" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee savings</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmployeesListunderTaxscope.html" title="Employees List under Tax scope" data-toggle="" class="no-submenu">
                           <span class="item-text">Employees List under Tax scope</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmployeeTaxableincome.html" title="Employee Taxable income" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee Taxable income</span>
                        </a>
                     </li>
                     <li>
                        <a href="#," title="Employees List under Tax scope" data-toggle="" class="no-submenu">
                           <span class="item-text"><b>STATUTATORY </b></span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpPFreturns.html" title="Emp.PF returns" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp.PF returns</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmpESIreturns.html" title="Emp.ESI returns" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp.ESI returns</span>
                        </a>
                     </li>
                     <li>
                        <a href="#." title="Employees List under Tax scope" data-toggle="" class="no-submenu">
                           <span class="item-text"><b>MANAGERIAL REPORTS</b></span>
                        </a>
                     </li>
                     <li>
                        <a href="EmployeesRating.html" title="Employees Rating" data-toggle="" class="no-submenu">
                           <span class="item-text">Employees Rating</span>
                        </a>
                     </li>
                     <li>
                        <a href="ListofEmployeescalls.html" title="List of Employees calls for councilling" data-toggle="" class="no-submenu">
                           <span class="item-text">List of Employees calls for counseling</span>
                        </a>
                     </li>
                     <li>
                        <a href="Attendancesummary.html" title="Attendance summary" data-toggle="" class="no-submenu">
                           <span class="item-text">Attendance summary</span>
                        </a>
                     </li>
                  </ul>
                  <!-- END SubMenu item-->
               </li>
            </ul>
         </nav>
         <!-- END Sidebar (left)-->
      </aside>
      <!-- End aside-->
      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->

                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Pay Slip</div>
                     <div class="panel-body">
                    <form role="form" class="form-horizontal">
                           <div class="col-md-12 col-md-offset-2">
                           <div class="form-group col-md-3 bgc-3">
                              <label>Search BY</label>
                              <p class="text-center">B ID</p>
                              <select name="account" class="form-control m-b">
                                 <option>Option 1</option>
                                 <option>Option 2</option>
                                 <option>Option 3</option>
                                 <option>Option 4</option>
                              </select>
                           </div>
                           <div class="form-group col-md-3 bgc-3">
                              <label> Search BY</label>
                              <p class="text-center">E.Name</p>
                              <select name="account" class="form-control m-b">
                                 <option>Option 1</option>
                                 <option>Option 2</option>
                                 <option>Option 3</option>
                                 <option>Option 4</option>
                              </select>
                           </div>
                           <div class="form-group col-md-3 bgc-3">
                              <label> <br></label>
                              <p class="text-center">Month</p>
                             <input type="date" class="form-control">
                           </div>
                           </div>

                          <!--  ============================ -->
                           <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Basic</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Other allowances</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">INCR</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Head/HOD/Coordinator allowances</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">D.A</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Special allowances</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">H.R.A</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Bonus</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">C.C.A</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Any other allowances</label>
                              <div class="col-lg-8">
                                 <input type="text" placeholder="" class="form-control">
                              </div>
                           </div>
                        </form>

                        <!-- START table-responsive-->
                     <div class="table-responsive">
                       <table class="table  table-bordered table-hover">
                        <tbody class="mytable"> 
                        <tr class="bg-yellow">
                              <td colspan="5">Deduction</td>
                           </tr>
                         <tr>
                            <td>ESI </td>
                            <td>Is Payment to hold</td>
                            <td>PF </td>
                            <td>TDS</td>
                            <td>Installments Paid</td>
                         </tr>

                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            

                         </tr>

                         <tr>
                             <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <li><a href="" class="btn-form"> Print </a></li>
                     <li><a href="" class="btn-form btn-undo"> Import </a></li>
                     <li><a href="" class="btn-form btn-exit"> EXIT </a></li>
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
         <!-- START Page footer-->
         <footer class="text-center">&copy; 2018 - HR Payroll</footer>
         <!-- END Page Footer-->
      </section>
      <!-- END Main section-->
   </div>
   <!-- END Main wrapper-->
   <!-- START Scripts-->
   <!-- Main vendor Scripts-->
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
   <!-- Plugins-->
   <script src="vendor/chosen/chosen.jquery.min.js"></script>
   <script src="vendor/slider/js/bootstrap-slider.js"></script>
   <script src="vendor/filestyle/bootstrap-filestyle.min.js"></script>
   <!-- Animo-->
   <script src="vendor/animo/animo.min.js"></script>
   <!-- Sparklines-->
   <script src="vendor/sparklines/jquery.sparkline.min.js"></script>
   <!-- Slimscroll-->
   <script src="vendor/slimscroll/jquery.slimscroll.min.js"></script>
   <!-- Store + JSON-->
   <script src="vendor/store/store%2bjson2.min.js"></script>
   <!-- Classyloader-->
   <script src="vendor/classyloader/js/jquery.classyloader.min.js"></script>
   <!-- START Page Custom Script-->
   <!-- Form Validation-->
   <script src="vendor/parsley/parsley.min.js"></script>
   <!-- END Page Custom Script-->
   <!-- App Main-->
   <script src="app/js/app.js"></script>
   <!-- END Scripts-->
</body>


</html>