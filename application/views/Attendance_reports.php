
<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (isset($_GET['month']) && isset($_GET['year'])) {
   
   if (isset($_GET['bid'])) {
      $getParam="bid=".$_GET['bid']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (isset($_GET['ename'])) {
      $getParam="ename=".$_GET['ename']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/AttendanceReports/All') !== false ) {
      $getParam="type=All&month=".$_GET['month']."&year=".$_GET['year'];
    }
}



?>
      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                     <div class="row">
                        <div class="form-group col-md-6">
                              <label class="col-lg-1 control-label pt-9">Select</label>
                              <div class="col-lg-8">
                              <select name="account" class="form-control m-b col-md-6 input-imp txt-select" onchange="location = this.value;">
                        <option value="">Select</option>
                        <option selected value="<?php echo base_url('AttendanceReports'); ?>">Attendance Manual Report</option>
                        <option value="<?php echo base_url('BioMetricReports'); ?>">Bio Metric Report</option>
                        <option  value="<?php echo base_url('ManualVSBioMetricReports'); ?>">Manual VS BioMetric Report</option>
                      </select>
                              </div>
                           </div>
                     </div>
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Attendance Manual Report </div>
                     <div class="panel-body">
                     <form>
                          <div class="col-md-12">
                            <div class="form-group col-md-3 bgc-4">
                              <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.B ID</b></p>
                              <select name="account" class="form-control m-b" id="emp_bid_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if(strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/AttendanceReports/All'))
                                 {?> selected <?php } ?> >All</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo $bid['emp_bid']; ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { ; ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                            </div>
                            <div class="form-group col-md-3 bgc-2">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.Name</b></p>

                              <select name="account" class="form-control m-b" id="emp_name"> 
                                 <option value="">Select</option>
                                 <option value="All" <?php if(strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/AttendanceReports/All'))
                                 {?> selected <?php } ?> >All</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo $name['emp_name'] ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                           </div>
                            <div class="form-group col-md-3 bgc-3">
                              <div class="col-md-12 p-0">
                              <p class="text-center"><b>Year</b></p>
                              <select name="account" class="form-control m-b" id="year_id">
                                 <option value="">Select</option>
                                 <option value="2019" <?php if (isset($_GET['year']) && $_GET['year']==2019) {  ?> selected <?php } ?>>2019</option>
                                 <option value="2020" <?php if (isset($_GET['year']) && $_GET['year']==2020) {  ?> selected <?php } ?>>2020</option>
                                 
                              </select>
                              </div>
                            </div>
                           <div class="form-group col-md-3 bgc-1">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>Month</b></p>

                              <select name="account" class="form-control m-b" id="month_id"> 
                                 <option value="">Select</option>
                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
                                 } ?>
                              </select>
                              </div>
                           </div>
                   
                           </div>
                      </form>

                        <!-- START table-responsive-->
                    <div class="table-responsive fixtable">
                      <table class="table  table-bordered table-hover" id="fixTable">
                        <thead>
                          <tr class="text-center">
                            <td>SL.NO</td>
                            <td>EID</td> 
                            <td>BID</td>
                            <td>E.Name</td>
                            <td>Department</td>
                            <td>Designation</td>
                            <td>Leave Type</td>
                            <td>Leave Date</td>
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                          
                         <?php if (isset($reports)) {
                           $i=1;
                           foreach ($reports as $report) { 
                           ?>
                        <tr class="text-center">
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $report['temp_emp_id'] ?></td>
                           <td><?php echo $report['emp_bid'] ?></td>
                           <td><?php echo $report['emp_name'] ?></td>
                           <td><?php echo $report['dept_name'] ?></td>
                           <td><?php echo $report['designation_name'] ?></td>
                           <td ><?php echo $report['leave_type'] ?></td>
                           <td><?php echo $report['leave_date'] ?></td>
                         </tr>
                        <?php } } else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>
    <script type="text/javascript" >
     var base_url="<?php echo base_url(); ?>";
     $("#emp_bid_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#emp_name").val("");
      });
      $("#line_of_activity_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#emp_bid_id").val("");
        $("#emp_name").val("");
      });
      $("#emp_name").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#emp_bid_id").val("");
      });
      $("#year_id").on("change",function(){
        $("#month_id").val("");
      });
      $("#month_id").on("change",function(){
        var emp_bid= $("#emp_bid_id").val(); 
        var emp_name=$('#emp_name').val();
        var year=$("#year_id").val();
        var month=$("#month_id").val();

        if (year!="" && month!="") {
          if (emp_bid!="") {
            if (emp_bid=="All") {
              window.location.replace(base_url+"AttendanceReports/All?month="+month+"&year="+year);
            }
            else{
              window.location.replace(base_url+"AttendanceReports/activity?bid="+emp_bid+"&month="+month+"&year="+year);
            }
            
          }
          else if (emp_name!="") {
            if (emp_name=="All") {
              window.location.replace(base_url+"AttendanceReports/All?month="+month+"&year="+year);
            }
            else{
              window.location.replace(base_url+"AttendanceReports/activity?ename="+emp_name+"&month="+month+"&year="+year);
            }
            
          }
        }
      })
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/AttendanceReports?"+"<?php echo $getParam; ?>";
      }
      $("#reports_ul").addClass("nav collapse in");
      $("#atndsr_li").addClass("active");
   </script>