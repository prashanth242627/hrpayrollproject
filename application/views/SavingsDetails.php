<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (strpos($_SERVER['REQUEST_URI'], '/applications/hrpayroll/EmployeeSavings/All') !== false ) {
  $getParam="type=All";
}

?>

      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Employee TDS Details </div>
                     <div class="panel-body" style="overflow-x: scroll;">
                     <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-4 bgc-5">
                              <p class="text-center"><b>Department</b></p>
                              <select name="account" class="form-control m-b" id="activity_id" onchange="location = this.value;">
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('EmployeeSavings/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeSavings/All") {?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php  echo base_url('EmployeeSavings/Activity?type='.$activity["dept_name"]); ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { $getParam="type=".$_GET['type']; ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                            <div class="form-group col-md-4 bgc-4">
                              <p style="margin: 0px;">
                              </p><div class="col-md-12 p-0">
                              <p class="text-center"><b>E.B ID</b></p>
                              <select name="account" class="form-control m-b" id="activity_id1" onchange="location = this.value;" >
                                 <option value="">Select</option>
                                 <option value="<?php echo  base_url('EmployeeSavings/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeSavings/All") {?> selected <?php } ?> >All</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo base_url('EmployeeSavings/Activity?bid='.$bid['emp_bid']); ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { $getParam="bid=".$_GET['bid']; ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                          </div>
                          <div class="form-group col-md-4 bgc-2">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.Name</b></p>

                              <select name="account" class="form-control m-b" id="activity_id3" onchange="location = this.value;"> 
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('EmployeeSavings/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeSavings/All") {?> selected <?php } ?> >All</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo base_url('EmployeeSavings/Activity?ename='.$name['emp_name']); ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { $getParam="ename=".$_GET['ename']; ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>
                           <!-- <div class="form-group col-md-2 bgc-2">
                              <p class="text-center">LINE OF ACTIVITY</p>
                              <select name="account" class="form-control m-b">
                                 <option> All</option>
                              </select>
                           </div> -->
                   
                           </div>
                        </form>
                        <div class="row input-padding">
                          <div class="form-group col-md-4">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Selection Criteria: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" readonly="" value="<?php 
                                 if(isset($_GET['type'])) { echo $_GET['type']; } 
                                 else if(isset($_GET['bid'])){ echo "EMP BID =".$_GET['bid']; } 
                                 else if(isset($_GET['ename'])){ echo "EMP ENAME =".$_GET['ename']; }
                                 else if($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeSavings/All"){ echo "All"; } ?>" placeholder="Designation Name" class="form-control p-0">
                              </div>
                           </div>
                           <div class="form-group col-md-4 p-0 pl-6">
                              <label class="col-lg-8 control-label p-0 text-right pt-9">TDS Per Year(Rs): </label>
                              <div class="col-lg-4 p-0">
                                 <input type="text" name="desig_id" id="tds_total_val" readonly="" value="" placeholder="ABBR" class="form-control p-0">
                              </div>
                           </div>
                        </div>
                            

                        <!-- START table-responsive-->
                     <div class="table-responsive fixtable">
                       <table class="table  table-bordered table-hover" id="fixTable">
                        <thead>
                          <tr>
                            <td>S.No</td>
                            <td>E ID</td>
                            <td>E BID</td>
                            <td>E Name</td>
                            <td>D.O.J</td>
                            <td>Designation</td>
                            <td>Department</td>
                            <td>CTC(Rs)</td>
                            <td>Taxable Amount(Rs)</td>
                            <td>80C Savings(Rs)</td>
                            <td>80CC Savings(Rs)</td>
                            <td>80D Savings(Rs)</td>
                            <td>80G Savings(Rs)</td>
                            <td>Taxable Amount After Savings(Rs)</td>
                            <td>TDS Per Year(Rs)</td>
                            <td>TDS Recovered(Rs)</td>
                            <td>TDS Balance(Rs)</td>
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                         
                         <?php if (isset($employees)) {
                           $i=1;
                           $tds_total=0;
                           foreach ($employees as $employee) { 
                               
                            if ($employee['tax_amt_aftr_savings']!=0) { 
                              $tds_total+=round($employee['tds_per_anm']);
                            }
                            ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $employee['temp_emp_id'] ?></td>
                           <td><?php echo $employee['emp_bid'] ?></td>
                           <td><?php echo $employee['emp_name'] ?></td>
                           <td><?php echo $employee['date_of_join'] ?></td>
                           <td><?php echo $employee['designation_name'] ?></td>
                           <td><?php echo $employee['dept_name'] ?></td>
                           <td><?php echo round($employee['CTC']) ?></td>
                           <td><?php echo round($employee['taxable_amount']) ?></td>
                           <td><?php echo $employee['sec_80c'] ?></td>
                            <td><?php echo $employee['sec_80cc'] ?></td>
                            <td><?php echo $employee['sec_80d'] ?></td>
                            <td>0</td>
                            <td><?php echo round($employee['tax_amt_aftr_savings']) ?></td>
                            <td><?php echo round($employee['tds_per_anm']) ?></td>
                            <td><?php echo round($employee['tds_recovered']) ?></td>
                            <td><?php echo round($employee['tds_balance']) ?></td>


                         </tr>
                        <?php } ?>

                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total :</td>
                            <td id="tds_total_id"><?php echo $tds_total ?></td>
                            <td></td>
                            <td></td>

                         </tr>
                     <?php } else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>No Records</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                         </tr>

                         <tr>
                          <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>

   <!-- END Scripts-->
   <script type="text/javascript" >
    $("#tds_total_val").val($("#tds_total_id").html());
      var base_url="<?php echo base_url(); ?>";
      function exportToExcel() {
        window.location.href=base_url+"Excel_export/EmployeeSavings?"+"<?php echo $getParam; ?>";
      }
      $("#reports_ul").addClass("nav collapse in");
      $("#esave_li").addClass("active");
   </script>
