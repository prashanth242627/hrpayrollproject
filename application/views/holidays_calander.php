<?php
 $holiday_name = ($get_holidays_list->holiday_name)?$get_holidays_list->holiday_name:'';
 $from_date = ($get_holidays_list->from_date)?$get_holidays_list->from_date:'';
 $to_date = ($get_holidays_list->to_date)?$get_holidays_list->to_date:'';
 $class = ($get_holidays_list->class)?$get_holidays_list->class:'';
 $holidays="";
 $days="";
 $i=1;
 $getParam="";
if (isset($_GET['year']) && !isset($_GET['month'])) {
   $getParam="year=".$_GET['year'];
}
else if (isset($_GET['year']) && isset($_GET['month'])) {
	if ($_GET['month']=="") {
		$getParam="year=".$_GET['year'];
	}
	else{
		$getParam="year=".$_GET['year']."&month=".$_GET['month'];
	}
   
}
if (!empty($get_holidays_list)) {
	foreach ($get_holidays_list as $row) {
	$to = new DateTime($row->to_date);
   $from = new DateTime($row->from_date);
   $days = $from->diff($to)->format("%a")+1;

	$holidays.="<tr><td>".$i++."</td><td>".$row->holiday_name."</td><td>".date("d-m-Y", strtotime($row->from_date))."</td><td>".date("d-m-Y", strtotime($row->to_date))."</td><td>".$days."</td><td>".$row->class."</td></tr>";
}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<section>
		<div class="main-content">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="panel panel-default">
							<div class="panel-heading form-heading">Holidays List</div>
							<div class="col-md-12">
							<div class="form-group col-md-6 bgc-3">
                              <p class="text-center"><b>Year</b></p>
                              <select name="account" class="form-control m-b" id="year_id">
                                 <option value="">Select</option>
                                 <option value="2019" <?php if (isset($_GET['year']) && $_GET['year']==2019) {  ?> selected <?php } ?>>2019</option>
                                 <option value="2020" <?php if (isset($_GET['year']) && $_GET['year']==2020) {  ?> selected <?php } ?>>2020</option>
                                 
                              </select>
                            </div>
                           <div class="form-group col-md-6 bgc-1">
                              <p class="text-center"><b>Month</b></p>

                              <select name="account" class="form-control m-b" id="month_id"> 
                                 <option value="">Select</option>
                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
                                 } ?>
                              </select>
                           </div>
							</div>
						</div>
							<div class="table-responsive">
								<table id="gradeX" class="table  table-bordered table-hover">
									<thead>
										<tr>
											<td>S.No</td>
											<td>Holiday Reason</td>
											<td>From Date</td>
											<td>To Date</td>
											<td>Days</td>
											<td>Class</td>
										</tr>
									</thead>
									<tbody>
										<?php echo $holidays; ?>
									</tbody>
									
								</table>
								<div class="col-md-12 btn-sec text-center">
								<ul class="btn-row">
				                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
				                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
				                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
				                  </ul>
				              </div>
							</div>
							
						</div>
				</div>	
			</div>
			
		</div>
	</section>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript">
		var base_url="<?php echo base_url(); ?>";
		$("#month_id").on("change",function(){
         var year= $("#year_id").val();
         var month= $("#month_id").val();
         if (year!="") {
            window.location.replace(base_url+"HolidaysList/activity?year="+year+"&month="+month);
         }
         else{
            window.location.href=base_url+"HolidaysList/activity?month="+month;
         }
         
      })
      $("#year_id").on("change",function(){
         var year= $("#year_id").val();
            window.location.href=base_url+"HolidaysList/activity?year="+year;
         
      })
      function exportToExcel() {
        window.location.href=base_url+"Excel_export/HolidaysList?"+"<?php echo $getParam; ?>";
      }
		$("#reports_ul").addClass("nv collapse in");
		$("#hcl_li").addClass("active");
	</script>
</body>
</html>