<?php
if (!isset($_GET['month']) && !isset($_GET['year'])) {
  $_GET['month']=date("F");
  $_GET['year']=date("Y");
}
if (isset($_GET['month']) && isset($_GET['year'])) {
  if (isset($_GET['ename'])) {
    $getParam="ename=".$_GET['ename']."&month=".$_GET['month']."&year=".$_GET['year'];
  }
  else if (isset($_GET['bid'])) {
    $getParam="bid=".$_GET['bid']."&month=".$_GET['month']."&year=".$_GET['year'];
  }
}

?>
<section>
   <!-- START Page content-->
   <div class="main-content">
      <!-- START row-->
      <div class="row">
         <div class="col-md-12 ">
            <form>
                          <div class="col-md-12">
                           
                          <div class="form-group col-md-3 bgc-4">
                          <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.B ID</b></p>
                              <select name="account" class="form-control m-b" id="emp_bid_id">
                                 <option value="">Select</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo $bid['emp_bid']; ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                            </div>
                            <div class="form-group col-md-3 bgc-5">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.Name</b></p>

                              <select name="account" class="form-control m-b" id="emp_name"> 
                                 <option value="">Select</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo $name['emp_name']; ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>
                           <div class="form-group col-md-3 bgc-3">
                              <div class="col-md-12 p-0">
                              <p class="text-center"><b>Year</b></p>
                              <select name="account" class="form-control m-b" id="year_id">
                                 <option value="">Select</option>
                                 <option value="2019" <?php if (isset($_GET['year']) && $_GET['year']==2019) {  ?> selected <?php } ?>>2019</option>
                                 <option value="2020" <?php if (isset($_GET['year']) && $_GET['year']==2020) {  ?> selected <?php } ?>>2020</option>
                                 
                              </select>
                              </div>
                            </div>
                           <div class="form-group col-md-3 bgc-1">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>Month</b></p>

                              <select name="account" class="form-control m-b" id="month_id"> 
                                 <option value="">Select</option>
                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
                                 } ?>
                              </select>
                              </div>
                           </div>
                   
                           </div>
                        </form>
                        <div class="row input-padding">
                          <div class="form-group col-md-4">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Selection Criteria: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" readonly="" value="<?php 
                                 if(isset($_GET['type'])) { echo $_GET['type']; } 
                                 else if(isset($_GET['bid'])){ echo "EMP BID =".$_GET['bid']; } 
                                 else if(isset($_GET['ename'])){ echo "EMP ENAME =".$_GET['ename']; } ?>" placeholder="Designation Name" class="form-control p-0">
                              </div>
                           </div>
                        </div>
               <!-- START panel-->
                <div class="panel panel-default">
               <div class="panel-heading form-heading">Employee PaySlip For The Month of <?php echo $_GET['month']." Year ".$_GET['year']; ?></div>
                <div class="panel-body">
                  <form action="<?php echo base_url(); ?>EmpPayScale/add"  novalidate=""  method="POST" name="payscaleform" class="form-horizontal">
                  <div class="col-md-12 form-sub input-padding" style="background: #f5f8fa">
                  <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Emp Id </label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['temp_emp_id'] ?>" name="Emp Id">
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Emp Name</label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['emp_name'] ?>"  name="Emp Name">
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">A/C No </label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['acc_no'] ?>" name="A/C No">
                        </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">PF.NO </label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['pf_no'] ?>" name="PF.NO">
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">TWD</label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['TWD'] ?>" name="TWD">
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">PAN NO </label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['pan'] ?>" name="A/C No">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">ESI NO </label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['esi_no'] ?>" name="ESI NO">
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">LOP Days</label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['lop_count'] ?>" name="TWD">
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">AAdhar No </label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['Aadhar'] ?>" name="A/C No">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Designation </label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['designation_name'] ?>" name="ESI NO">
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Pay Days</label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['PAY_DAYS'] ?>" name="TWD">
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">UAN No </label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="" class="form-control" readonly="" value="<?php echo $employee['uan'] ?>" name="A/C No">
                        </div>
                     </div>
                  </div>
                  </div>
                  </form>
    
               </div>
            </div>
               <!-- END panel-->
               <div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                     
                        <thead>

                           <tr>
                       <th>Gross(Rs)</th>
                       <th>Amount(Rs)</th>
                       <th>Gross Earnings(Rs)</th>
                       <th>Deductions(Rs)</th>
                       <th>Amount(Rs)</th>
                       <th>Net Pay(Rs)</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                          <tr>
                             <td>Basic</td>
                             <td><?php echo round($employee['basic_ctc']) ?></td>
                             <td><?php echo round($employee['basic_earnings']) ?></td>
                             <td>PF</td>
                             <td><?php echo round($employee['PF']) ?></td>
                             <td></td>
                          </tr>
                          <tr>
                             <td>DA</td>
                             <td><?php echo round($employee['da_ctc']) ?></td>
                             <td><?php echo round($employee['da_earnings']) ?></td>
                             <td>PT</td>
                             <td><?php echo round($employee['PT']) ?></td>
                             <td></td>
                          </tr>
                          <tr>
                             <td>Incr</td>
                             <td><?php echo round($employee['incr_ctc']) ?></td>
                             <td><?php echo round($employee['incr_earnings']) ?></td>
                             <td>ESI</td>
                             <td><?php echo round($employee['ESI']) ?></td>
                             <td></td>
                          </tr>
                          <tr>
                             <td>HRA</td>
                             <td><?php echo round($employee['hra_ctc']) ?></td>
                             <td><?php echo round($employee['hra_earnings']) ?></td>
                             <td>TDS</td>
                             <td><?php echo round($employee['TDS']) ?></td>
                             <td></td>
                          </tr>
                          <tr>
                             <td>CCA</td>
                             <td><?php echo round($employee['cca_ctc']) ?></td>
                             <td><?php echo round($employee['cca_earnings']) ?></td>
                             <td>10% SD</td>
                             <td><?php echo round($employee['SD']) ?></td>
                             <td></td>
                          </tr>
                          <tr>
                             <td>Other allowances</td>
                             <td><?php echo round($employee['other_allowance_ctc']) ?></td>
                             <td><?php echo round($employee['other_allowances_earnings']) ?></td>
                             <td>Others</td>
                             <td>0</td>
                             <td></td>
                          </tr>
                           <tr>
                             <td>HD,HOD,Coord allowances</td>
                             <td><?php echo round($employee['co_allowance_ctc']) ?></td>
                             <td><?php echo round($employee['co_allowance_earnings']) ?></td>
                             <td>Other Deductions</td>
                             <td><?php echo round($employee['management_deductions']) ?></td>
                             <td></td>
                          </tr>
                           <tr>
                             <td>Special allowances</td>
                             <td><?php echo round($employee['special_allowance_ctc']) ?></td>
                             <td><?php echo round($employee['special_allowance_earnings']) ?></td>
                             <td></td>
                             <td></td>
                             <td></td>
                          </tr>
                          <tr>
                             <td>Total</td>
                             <td><?php echo round($employee['FIXED_MONTHLY_CTC']) ?></td>
                             <td><?php echo round($employee['gross']) ?></td>
                             <td></td>
                             <td><?php echo round($employee['PF']+$employee['PT']+$employee['SD']+$employee['TDS']+$employee['ESI'])+round($employee['management_deductions']); ?></td>
                             <td><?php echo round($employee['net_income'])-round($employee['management_deductions']) ?></td>
                          </tr>
                          <tr>
                             <td colspan="6" class="text-center">In words: <?php echo $employee['total_in_rupees']." only" ?></td>
                          </tr>
                          <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td>Total</td>
                             <td>FIXED MONTHLY CTC</td>
                             <td><?php echo round($employee['FIXED_MONTHLY_CTC']) ?></td>
                          </tr>
                        </tbody>
                  </table>
            </div>
              <div class="table-responsive" >
                           <table id="gradeX" class="table  table-bordered table-hover">
                       <h4 class="text-center">TDS DETAILS</h4>
                        <thead>

                           <tr>
                       <th>Description</th>
                        <th>Gross(Rs)</th>
                       <th>Exempt(Rs)</th>
                       <th>Taxable(Rs)</th>
                       <th colspan="2">Income Tax Deduction(Rs)</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <tr>
                             <td>Basic Salary</td>
                             <td><?php echo round($tds_data['basic']) ?></td>
                             <td>0</td>
                             <td><?php echo round($tds_data['basic']) ?></td>
                             <td>Gross Salary</td>
                             <td><?php echo round($tds_data['taxable_amt']) ?></td>
                          </tr>
                          <tr>
                             <td>DA</td>
                             <td><?php echo round($tds_data['da']) ?></td>
                             <td>0</td>
                             <td><?php echo round($tds_data['da']) ?></td>
                             <td>PT</td>
                             <td><?php echo round($tds_data['PT_AMT']) ?></td>
                          </tr>
                          <tr>
                             <td>HRA</td>
                             <td><?php echo round($tds_data['hra']) ?></td>
                             <td><?php echo round($tds_data['hra_exmpt']) ?></td>
                             <td><?php echo round($tds_data['hra_tax_amt']) ?></td>
                             <td>80C</td>
                             <td><?php echo round($tds_data['80C_exmpt']) ?></td>
                          </tr>
                          <tr>
                             <td>Conveyance</td>
                             <td><?php echo round($tds_data['conveyance_allowance']) ?></td>
                             <td><?php echo round($tds_data['convy_exmpt']) ?></td>
                             <td><?php echo round($tds_data['convy_tax_amt']) ?></td>
                             <td>80D</td>
                             <td><?php echo round($tds_data['80D_exmpt']) ?></td>
                             
                          </tr>
                          <tr>
                             <td>Any Other allowances</td>
                             <td><?php echo round($tds_data['other_allowance']) ?></td>
                             <td>0</td>
                             <td><?php echo round($tds_data['other_allowance']) ?></td>
                             <td>Taxable Income</td>
                             <td><?php echo round($tds_data['taxable_income']) ?></td>
                          </tr>
                          <tr>
                             <td>Other cost components</td>
                             <td><?php echo round($tds_data['other_cost_cpmponents']) ?></td>
                             <td>0</td>
                             <td><?php echo round($tds_data['other_cost_cpmponents']) ?></td>
                             <td>Total Tax</td>
                             <td><?php echo round($tds_data['tax_amt']) ?></td>

                             
                          </tr>

                          <tr>
                            <td>Total</td>
                             <td><?php echo round($tds_data['gross']) ?></td>
                             <td><?php echo round($tds_data['convy_exmpt'])+round($tds_data['hra_exmpt']) ?> </td>
                             <td><?php echo round($tds_data['taxable_amt']) ?></td>
                             <td>Surcharges+EDCS</td>
                             <td><?php echo round($tds_data['surcharges']) ?></td>
                          </tr>

                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total Tax + Surcharge</td>
                            <td><?php echo round($tds_data['surcharges'])+round($tds_data['tax_amt']) ?></td>
                          </tr> 
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Tax Deducted Till Date</td>
                            <td><?php echo $tds_data['tds_deducted_till_date'] ?></td>
                          </tr>  
                          <tr>
                             <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Tax to be Deducted</td>
                            <td><?php echo round($tds_data['tds_to_be_deducted']) ?></td>
                          </tr>
                          <tr>
                             <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Projected Tax/Month</td>
                            <td><?php echo round($tds_data['projected_tax_per_month']) ?></td>
                          </tr>
                        </tbody>
                  </table>
            </div>
            <div class="table-responsive" >
              <table id="gradeX" class="table  table-bordered table-hover">
                <h4 class="text-center">Tax Paid</h4>
                <thead> 
                  <tr>
                    <th>Month</th>
                    <?php foreach ($taxs_paid as $tax_paid) { ?>
                      <th><?php echo date("F", mktime(0, 0, 0, $tax_paid['salary_month'], 10)); ?></th>
                    <?php } ?>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="mytable">
                  <tr>
                    <td>Amount(Rs)</td>
                    <?php foreach ($taxs_paid as $tax_paid) { ?>
                      <td><?php echo $tax_paid['tds_amt']; ?></td>
                    <?php } ?>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToPDF();">Download</button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>  
         </div>
      </div>
      <!-- END row-->
   </div>
   <!-- END Page content-->
   <!-- END Page Footer-->
<script type="text/javascript">
  var base_url="<?php echo base_url(); ?>";
  $("#emp_bid_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#emp_name").val("");
      });
      
      $("#emp_name").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
        $("#emp_bid_id").val("");
      });
      $("#year_id").on("change",function(){
        $("#month_id").val("");
      });
      $("#month_id").on("change",function(){
        var year=$("#year_id").val();
        var month=$("#month_id").val();
        var ename=$("#emp_name").val();
        var ebid=$("#emp_bid_id").val();

        if (year!="" && month!="") {
          if (ename!="") {
            window.location.replace(base_url+"EmpPaySlips/activity?ename="+ename+"&month="+month+"&year="+year);
          }
          else if (ebid!="") {
            window.location.replace(base_url+"EmpPaySlips/activity?bid="+ebid+"&month="+month+"&year="+year);
          }
        }
      });
      function exportToPDF() {
        
         window.location.href=base_url+"GeneratePdf/PaySlip?"+"<?php echo $getParam; ?>";
      }
  $("#reports_ul").addClass("nav collapse in");
      $("#eps_li").addClass("active");
</script>
