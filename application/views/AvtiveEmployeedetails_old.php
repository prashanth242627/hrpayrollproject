<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$this->load->view('theme/header');
$this->load->view('theme/sidebar');
?>
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->

                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Employee Details</div>
                     <div class="panel-body" style="overflow-x: scroll;">
                     <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-3 bgc-5">
                              <p class="text-center">LINE OF ACTIVITY</p>
                              <select name="account" class="form-control m-b" onchange="location = this.value;">
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('Employee_report/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/hrm/Employee_report/All") { ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php echo base_url('Employee_report/Activity?type='.$activity["dept_name"]); ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                           <div class="form-group col-md-2 bgc-1">
                              <p class="text-center">Satus</p>
                              <select name="account" class="form-control m-b p-0" onchange="location = this.value;">
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('Employee_report/Activity?status=Active'); ?>" <?php if (isset($_GET['status']) && $_GET['status']=='Active') { ?> selected <?php } ?>>Active</option>
                                 <option value="<?php echo base_url('Employee_report/Activity?status=Inactive'); ?>" <?php if (isset($_GET['status']) && $_GET['status']=='Inactive') { ?> selected <?php } ?>>Inactive</option>
                                 <option value="<?php echo base_url('Employee_report/Activity?status=New'); ?>" <?php if (isset($_GET['status']) && $_GET['status']=='New') { ?> selected <?php } ?>>New</option>

                              </select>
                           </div>
                            <div class="form-group col-md-3 bgc-4">
                              <p style="margin: 0px;">
                              </p><div class="col-md-6 p-0">
                              <p class="text-center">Search BY B ID</p>
                              <select name="account" class="form-control m-b" onchange="location = this.value;">
                                 <option value="">Select</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo base_url('Employee_report/Activity?bid='.$bid['emp_bid']); ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                               <div class="col-md-6 p-0">
                              <p class="text-center">BY E.Name</p>
                              <select name="account" class="form-control m-b" onchange="location = this.value;"> 
                                 <option value="<?php echo base_url('Employee_report/All'); ?>">Select</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo base_url('Employee_report/Activity?ename='.$name['emp_name']); ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>


                           <div class="form-group col-md-4 bgc-3">
                              <p style="margin: 0px;">
                              </p><div class="col-md-6 p-0">
                              <p class="text-center">Date From</p>
                              <input type="date" class="form-control" id="start_date" value="<?php if(isset($_GET['from'])){ echo $_GET['from']; } ?>">
                              </div>
                               <div class="col-md-6 p-0">
                              <p class="text-center">Date To</p>
                              <input type="date" class="form-control" id="end_date" value="<?php if(isset($_GET['to'])){ echo $_GET['to']; } ?>">
                              </div>
                              <p></p>
                           </div>
                           <!-- <div class="form-group col-md-2 bgc-2">
                              <p class="text-center">LINE OF ACTIVITY</p>
                              <select name="account" class="form-control m-b">
                                 <option> All</option>
                              </select>
                           </div> -->
                   
                           </div>
                        </form>

                        <!-- START table-responsive-->
                     <div class="table-responsive">
                       <table class="table  table-bordered table-hover">
                        <tbody class="mytable"> 
                         <tr>
                            <td>BID</td>
                            <td>E.Name</td>
                            <!-- <td>E BID</td> -->
                            <td>D.O.J</td>
                            <td>D.O.B</td>
                            <td>Address</td>
                            <td>Higher Education </td>
                            <!-- <td>Previous School </td> -->
                            <td>Experience</td>
                            <td>Mobile No</td>
                            <td>Designation</td>
                            <td>Department</td>
                            <td>Line of Activity</td>
                            <td>Last Working Day</td>
                            <td>Transportation</td>
                            <td>Status</td>
                         </tr>
                         <?php if (isset($employees)) {foreach ($employees as $employee) { 
                           ?>
                        <tr>
                           <td><?php echo $employee['emp_bid'] ?></td>
                           <td><?php echo $employee['emp_name'] ?></td>
                           <!-- <td></td> -->
                           <td><?php echo $employee['date_of_join'] ?></td>
                           <td><?php echo $employee['date_of_birth'] ?></td>
                           <td><?php echo $employee['address'] ?></td>
                           <td><?php echo $employee['education'] ?></td>
                           <!-- <td></td> -->
                           <td><?php echo $employee['experience'] ?></td>
                           <td><?php echo $employee['mobile'] ?></td>
                           <td><?php echo $employee['designation'] ?></td>
                           <td><?php echo $employee['dept_name'] ?></td>
                           <td><?php echo $employee['activity_id'] ?></td>
                           <td>last working date</td>
                           <td>transportation</td>
                           <?php if (isset($employee['flag'])) { ?>
                              <td>InActive</td>
                           <?php }else{ ?>
                           <td>Active</td> <?php } ?>
                         </tr>
                        <?php }} else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                           <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
         <!-- START Page footer-->
         <footer class="text-center">&copy; 2018 - HR Payroll</footer>
         <!-- END Page Footer-->
      </section>
      <!-- END Main section-->
   </div>
  
 <!-- START Page footer-->
<footer class="text-center">&copy; 2018 - HR Payroll</footer>
         <!-- END Page Footer-->
      </section>
      <!-- END Main section-->
   </div>
   <!-- END Main wrapper-->
   <!-- START Scripts-->
   <!-- Main vendor Scripts-->
   <script src="<?php echo base_url();?>vendor/jquery/jquery.min.js"></script>
   <script src="<?php echo base_url();?>vendor/bootstrap/js/bootstrap.min.js"></script>
   <!-- Plugins-->
   <script src="<?php echo base_url();?>vendor/chosen/chosen.jquery.min.js"></script>
   <script src="<?php echo base_url();?>vendor/slider/js/bootstrap-slider.js"></script>
   <script src="<?php echo base_url();?>vendor/filestyle/bootstrap-filestyle.min.js"></script>
   <!-- Animo-->
   <script src="<?php echo base_url();?>vendor/animo/animo.min.js"></script>
   <!-- Sparklines-->
   <script src="<?php echo base_url();?>vendor/sparklines/jquery.sparkline.min.js"></script>
   <!-- Slimscroll-->
   <script src="<?php echo base_url();?>vendor/slimscroll/jquery.slimscroll.min.js"></script>
   <!-- Store + JSON-->
   <script src="<?php echo base_url();?>vendor/store/store%2bjson2.min.js"></script>
   <!-- Classyloader-->
   <script src="<?php echo base_url();?>vendor/classyloader/js/jquery.classyloader.min.js"></script>
   <!-- START Page Custom Script-->
   <!-- Form Validation-->
   <script src="<?php echo base_url();?>vendor/parsley/parsley.min.js"></script>
   <!-- END Page Custom Script-->
   <!-- App Main-->
   <script src="<?php echo base_url();?>app/js/app.js"></script>
   <!-- END Scripts-->
   <script type="text/javascript" >
      var base_url="<?php echo base_url(); ?>";
      $("#end_date").on("change",function(){
         var start_date= $("#start_date").val();
         var end_date= $("#end_date").val();
         if (start_date!="") {
            window.location.replace(base_url+"Employee_report/activity?from="+start_date+"&to="+end_date);
         }
         else{
            window.location.href=base_url+"Employee_report/activity?to="+end_date;
         }
         
      })
      var employee_data=<?php if( isset($employees) && count($employees)){ echo json_encode($employees); } else{echo "{}"; } ?>;
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/action1";
         /*$.ajax({
         type: 'post',
           url: base_url+"Excel_export/action",
           data: {employees_data:employee_data},
           success: function (data) {
               document.location="data: application/vnd.ms-excel;base64,"+data;
              //var result = JSON.parse(data); 
            },
          error:function(error){
              console.log(error);
              $("#login_error_id").html(error);
          }
       });*/
      }
   </script>
</body>


</html>