<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (strpos($_SERVER['REQUEST_URI'], 'applications/hrpayroll/EmployeeLeaveStatus/All') !== false) {
  $getParam="type=All";
}


?>
      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
                        <div class="form-group col-md-6">
                              <label class="col-lg-1 control-label pt-9">Select</label>
                              <div class="col-lg-8">
                        <select name="account" class="form-control m-b col-md-6 input-imp txt-select" onchange="location = this.value;">
                        <option value="">Select</option>
                        <option selected value="<?php echo base_url('EmployeeLeaveStatus'); ?>">Employee Leaves Status</option>
                        <!-- <option  value="<?php echo base_url('ListOfAbsenties'); ?>">List Of Absentees</option> -->
                        <option  value="<?php echo base_url('EmployeeOnDuty'); ?>">List Of On Duty Employees</option>
                      </select>
                              </div>
                           </div>
                     </div>

            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                    
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading" >Employee Leaves Status</div>
                     <div class="panel-body" style="overflow-x: scroll;">
                     <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-4 bgc-5">
                              <p class="text-center"><b>Department</b></p>
                              <select name="account" class="form-control m-b" id="activity_id" onchange="location = this.value;">
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('EmployeeLeaveStatus/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeLeaveStatus/All") {?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php  echo base_url('EmployeeLeaveStatus/Activity?type='.$activity["dept_name"]); ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { $getParam="type=".$_GET['type']; ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                            <div class="form-group col-md-4 bgc-4">
                              <p style="margin: 0px;">
                              </p><div class="col-md-12 p-0">
                              <p class="text-center"><b>E.B ID</b></p>
                              <select name="account" class="form-control m-b" id="activity_id1" onchange="location = this.value;" >
                                 <option value="">Select</option>
                                 <option value="<?php echo  base_url('EmployeeLeaveStatus/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeLeaveStatus/All") {?> selected <?php } ?> >All</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo base_url('EmployeeLeaveStatus/Activity?bid='.$bid['emp_bid']); ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { $getParam="bid=".$_GET['bid']; ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                          </div>
                          <div class="form-group col-md-4 bgc-2">
                               <div class="col-md-12 p-0">
                              <p class="text-center"><b>E.Name</b></p>

                              <select name="account" class="form-control m-b" id="activity_id3" onchange="location = this.value;"> 
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('EmployeeLeaveStatus/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeLeaveStatus/All") {?> selected <?php } ?> >All</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo base_url('EmployeeLeaveStatus/Activity?ename='.$name['emp_name']); ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { $getParam="ename=".$_GET['ename']; ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>
                           <!-- <div class="form-group col-md-2 bgc-2">
                              <p class="text-center">LINE OF ACTIVITY</p>
                              <select name="account" class="form-control m-b">
                                 <option> All</option>
                              </select>
                           </div> -->
                   
                           </div>
                        </form>
                        <div class="row input-padding">
                          <div class="form-group col-md-4">
                              <label class="col-lg-5 control-label p-0 pt-9 text-right">Selection Criteria: </label>
                              <div class="col-lg-7 p-0">
                                 <input type="text" name="designation_name" readonly="" value="<?php 
                                 if(isset($_GET['type'])) { echo $_GET['type']; } 
                                 else if(isset($_GET['bid'])){ echo "EMP BID =".$_GET['bid']; } 
                                 else if(isset($_GET['ename'])){ echo "EMP ENAME =".$_GET['ename']; }
                                 else if($_SERVER['REQUEST_URI']=="/applications/hrpayroll/EmployeeLeaveStatus/All"){ echo "All"; } ?>" placeholder="Designation Name" class="form-control p-0">
                              </div>
                           </div>
                        </div>
                        <!-- START table-responsive-->
                     <div class="table-responsive fixtable">
                       <table class="table  table-bordered table-hover" id="fixTable">
                        <thead>
                          <tr >
                            <th rowspan="2">SL.NO</th>
                            <th rowspan="2">EID</th> 
                            <th rowspan="2">BID</th>
                            <th rowspan="2">E.Name</th>
                            <th rowspan="2">Date OF Join</th>
                            <th rowspan="2">Department</th>
                            <th rowspan="2" >Designation</th>
                            <th class="text-center" colspan="3">CL</th>
                            <th class="text-center" colspan="3">EL</th>
                            <th class="text-center" colspan="3">SL</th>
                         </tr>
                         <tr>
                            <th>OPB</th>
                            <th>Availed</th>
                            <th>Balance</th>
                            <th>OPB</th>
                            <th>Availed</th>
                            <th>Balance</th>
                            <th>OPB</th>
                            <th>Availed</th>
                            <th>Balance</th>
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                         
                         <?php if (isset($employees)) {
                           $i=1;
                           foreach ($employees as $leaves1) { 
                           ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $leaves1['temp_emp_id'] ?></td>
                           <td><?php echo $leaves1['emp_bid'] ?></td>
                           <td><?php echo $leaves1['emp_name'] ?></td>
                           <td><?php echo $leaves1['date_of_join'] ?></td>
                           <td><?php echo $leaves1['dept_name'] ?></td>
                           <td><?php echo $leaves1['designation_name'] ?></td>
                           <td><?php echo $leaves1['cl_opb']+$leaves1['cl_used'] ?></td>
                           <td><?php echo $leaves1['cl_used'] ?></td>
                           <td><?php echo $leaves1['cl_opb'] ?></td>
                           <td><?php echo $leaves1['el_opb']+$leaves1['el_used'] ?></td>
                           <td><?php echo $leaves1['el_used'] ?></td>
                           <td><?php echo $leaves1['el_opb'] ?></td>
                           <td><?php echo $leaves1['sl_opb']+$leaves1['sl_used'] ?></td>
                           <td><?php echo $leaves1['sl_used'] ?></td>
                           <td><?php echo $leaves1['sl_opb'] ?></td>
                           	
                         </tr>
                        <?php }} else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
   <script type="text/javascript" >
      var base_url="<?php echo base_url(); ?>";
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/EmployeeLeaveStatus?"+"<?php echo $getParam; ?>";
      }
   </script>
    <script>
      $("#reports_ul").addClass("nav collapse in");
      $("#els_li").addClass("active");
    </script> 
</body>


</html>