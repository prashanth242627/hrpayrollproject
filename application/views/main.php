<?php $colors=array("#123456","#8f99ca","#93d5ca","#86b5d5","#fb8679","#b7e071","#fdb86a","#29a2cc","#d31e1e","#7ca82b","#2f4f4f","#008080","#2e8b57","#3cb371","#90ee90","#2f4f4f","#008080","#2e8b57","#3cb371"); ?>



<script src="vendor/flot/jquery.flot.min.js"></script>
   <script src="vendor/flot/jquery.flot.tooltip.min.js"></script>
   <script src="vendor/flot/jquery.flot.resize.min.js"></script>
   <script src="vendor/flot/jquery.flot.pie.min.js"></script>
   <script src="vendor/flot/jquery.flot.time.min.js"></script>
   <script src="vendor/flot/jquery.flot.categories.min.js"></script> 
      <section>
         <!-- START Page content-->
         <div class="main-content" style="background-color:#f4f7f6">
           
            <div class="row">
               <!-- START dashboard main content-->
               <section class="col-md-12">
                   <div class="row">
                       <div class="col-lg-3 col-md-6">
                    <div class="card top_counter bg-inverse-clr">
                      <h3 class="text-center txt-name">Employee Details</h3>
                        <div class="body" style="background: #605ca8;color: #fff;">
                            <!--<div class="icon"><i class="fa fa-user"></i> </div>-->
                            <div class="content">
                                <div class="text">Total Employees</div>
                                <h5 class="number count d-i-block"><?php echo $employees_count[0]['emp_count'] ?></h5><a onClick="nt_pop_box('2')" href="javascript:void(0);"> <p class="r-arrow d-i-block"><i class="fa fa-long-arrow-right pl-20"></i></p></a>
                            </div>
                            <?php
                               foreach ($departments as $department) { ?>
                                <hr>
                            <div class="icon"><i class="fa fa-users"></i> </div>
                            <div class="content">
                                <div class="text"><?php echo $department['dept_name'] ?></div>
                                <h5 class="number count"><?php echo $department['dept_count'] ?></h5>
                            </div>
                              <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12">
                    <div class="card bg-blue-2">
                      <h3 class="txt-name">DepartmentWise Employees</h3>
                        <div class="body" style="background-color:#eee">
                            <ul class="bar-graph">
                              <?php
                               foreach ($designations as $designation) { 
                                $percentage=round(($designation['designation_count']/$employees_count[0]['emp_count'])*100);
                                $randomElement = $colors[array_rand($colors, 1)];
                                 ?>
                              <li class="bar success" data-toggle="tooltip" data-placement="top" title="<?php echo $designation['designation_name'] ?> - <?php echo $designation['designation_count'] ?> "   <?php echo "style='height:".($designation['designation_count']*2.5)."%;background-color:".$randomElement."'"; ?> title="">
                                <div class="percent"><?php echo $designation['designation_count'] ?></div>
                                <div class="description"><?php echo $designation['designation_name'] ?></div>
                              </li>
                              <?php } ?>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h3 style="padding: 0px 14px 0px;">Biometric Report</h3>
     
                <div class="col-lg-4 col-md-4" data-toggle="play-animation" data-play="zoomIn" data-offset="0" data-delay="10">
                    <div class="card text-center">
                        <div class="body" style="padding:0px">
                            <div id="piechart"></div>
                        </div>
                    </div>                    
                </div>
                 <div class="col-lg-4 col-md-4" data-toggle="play-animation" data-play="zoomIn" data-offset="0" data-delay="50">
                    <div class="card text-center">
                        <div class="body" style="padding:0px">
                            <div id="piechart2"></div>

                        </div>
                    </div>                    
                </div>
                <div class="col-lg-4 col-md-4" data-toggle="play-animation" data-play="zoomIn" data-offset="0" data-delay="50">
                    <div class="card text-center">
                        <div class="body" style="padding:0px">
                            
                            <div id="piechart3"></div>
                        </div>
                    </div>                    
                </div>
            </div>
            <?php 
            if ($this->session->userdata('role_id')==1) { ?>
              <div class="row" style="width:100%;">
                <h3 style="padding: 0px 14px 0px;">Monthly Reports  <?php
                $month=date('F',strtotime('01-'.$MonthWiseSalaries[0]['salary_month']."-".$MonthWiseSalaries[0]['salary_year']));
                 echo $month."/".$MonthWiseSalaries[0]['salary_year'] ?></h3>
                
                     <div style="width:20%;float:left" data-toggle="play-animation" data-play="zoomIn" data-offset="0" data-delay="0">
                         <p class="text-center" style="font-size: 16px;"><b>Monthly Salaries</b></p>
                    <div class="card top_counter">
                        <div class="body" style="background: #00c0ef;color: #fff;    margin: 3px;">
                            
                            <div class="content">
                                <div class="text">Fixed CTC</div>
                                <h5 class="number"><?php echo $MonthWiseSalaries[0]['fixed_monthly_ctc'] ?></h5>
                            </div>
                            <hr>
                            
                            <div class="content">
                                <div class="text">Gross Pay</div>
                                <h5 class="number"><?php echo $MonthWiseSalaries[0]['Gross_pay'] ?></h5>
                            </div>
                            <hr>
                             
                            <div class="content">
                                <div class="text">Net Pay</div>
                                <h5 class="number"><a onClick="nt_pop_box('1')" href="javascript:void(0);"><?php echo $MonthWiseSalaries[0]['NET_PAY'] ?> <i class="fa fa-long-arrow-right pl-20"></i></a></h5>
                            </div>
                        </div>
                    </div>
                </div>
                     <div style="width:20%;float:left" data-toggle="play-animation" data-play="zoomIn" data-offset="0" data-delay="20">
                         <p class="text-center" style="font-size: 16px;"><b>ESI</b></p>
                    <div class="card top_counter">
                        <div class="body" style="background: #00a65a;color: #fff;margin: 3px;">
                            
                            
                             
                            <div class="content">
                                <div class="text">Total</div>
                                <h5 class="number"><?php echo $SalariesDeductions['esi_count'] ?></h5>
                            </div>
                            <hr>
                          
                          
                            <div class="content">
                                <div class="text">Amount</div>
                                <h5 class="number"><?php echo $SalariesDeductions['total_esi_amt'] ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div style="width:20%;float:left" data-toggle="play-animation" data-play="zoomIn" data-offset="0" data-delay="40">
                         <p class="text-center" style="font-size: 16px;"><b>PF</b></p>
                    <div class="card top_counter">
                        <div class="body" style="background: #dd4b39;color: #fff;    margin: 3px;">
                            
                            
                              
                            <div class="content">
                                <div class="text">Total</div>
                                <h5 class="number"><?php echo $SalariesDeductions['pf_count'] ?></h5>
                            </div>
                            <hr>
                        
                          
                            <div class="content">
                                <div class="text">Amount</div>
                                <h5 class="number"><?php echo $SalariesDeductions['total_pf_amt'] ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                
                     <div style="width:20%;float:left" data-toggle="play-animation" data-play="zoomIn" data-offset="0" data-delay="60">
                         <p class="text-center" style="font-size: 16px;"><b>PT</b></p>
                    <div class="card top_counter">
                        <div class="body" style="background: #ff851b;color: #fff;    margin: 3px;">
                            
                            <div class="content">
                                <div class="text">Total</div>
                                <h5 class="number"><?php echo $SalariesDeductions['pt_count'] ?></h5>
                            </div>
                            <hr>
                          
                            <div class="content">
                                <div class="text">Total Amount</div>
                                <h5 class="number"><?php echo $SalariesDeductions['total_pt_amt'] ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                   <div style="width:20%;float:left" data-toggle="play-animation" data-play="zoomIn" data-offset="0" data-delay="60">
                         <p class="text-center" style="font-size: 16px;"><b>TDS</b></p>
                    <div class="card top_counter">
                        <div class="body" style="background: #605ca8;color: #fff;    margin: 3px;">
                            
                            <div class="content">
                                <div class="text">Total Employees</div>
                                <h5 class="number"><?php echo $SalariesDeductions['tds_count'] ?></h5>
                            </div>
                            <hr>
                          
                            <div class="content">
                                <div class="text">Total Amount</div>
                                <h5 class="number"><?php echo $SalariesDeductions['total_tds_amt'] ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
               </section>
               
               
               <section class="col-md-9">
                  <!-- START Secondary Widgets-->
                  <div class="row">
                     <div class="col-md-4" style="display: none;">
                        <!-- START widget-->
                        <div class="panel widget">
                           <div class="panel-body">
                              <div class="text-right text-muted">
                                 <em class="fa fa-bar-chart-o fa-2x"></em>
                              </div>
                              <?php
                               foreach ($designations as $designation) { ?>
                                 <p class="text-muted"><?php echo $designation['designation_name'] ?></p>
                              <h3 class="mt0"><?php echo $designation['designation_count'] ?></h3>
                              <?php } ?>
                              <div class="progress progress-striped progress-xs">
                                 <div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-info progress-40">
                                    <span class="sr-only">40% Complete</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- END widget-->
                     </div>
                    <!--  <div class="col-md-4">
                        <div class="panel widget">
                           <div class="panel-body">
                              <div class="text-right text-muted">
                                 <em class="fa fa-trophy fa-2x">TDS</em>
                              </div>
                              <?php
                               foreach ($tdsEmpCount as $tdsEmp) { ?>
                                 <p class="text-muted"><?php echo $tdsEmp['dept_name'] ?></p>
                              <h3 class="mt0"><?php echo $tdsEmp['TDS_count'] ?></h3>
                              <?php } ?>
                              <p class="text-muted">Amount</p>
                              <h3 class="mt0"><?php echo $SalariesDeductions['total_tds_amt'] ?></h3>
                              <div class="progress progress-striped progress-xs">
                                 <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-warning progress-60">
                                    <span class="sr-only">60% Complete</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div> -->
                  </div>
                  <!-- END Secondary Widgets-->
                  <!-- START chart-->
             
                  <!-- END chart-->
                  <!-- START messages and activity-->
               
                  <!-- END messages and activity-->
               </section>
               <!-- END dashboard main content-->
               <!-- START dashboard sidebar-->

               <!-- END dashboard sidebar-->
            </div>
         </div>
         <script>
          var designations=<?php echo json_encode($designations); ?>;
          var newobject=[];
          for (var i = 0; i < designations.length; i++) {
            var innerobject={};
            var object1 = designations[i];
            var innerobject={"label":object1["designation_name"],'y':object1["designation_count"]};
            newobject.push(innerobject);         
          }
         /* [{'y':designations[0]['designation_count'],"label":designations[0]['designation_name']},
        {'y':designations[1]['designation_count'],"label":designations[1]['designation_name']},
        {'y':designations[2]['designation_count'],"label":designations[2]['designation_name']},
        {'y':designations[3]['designation_count'],"label":designations[3]['designation_name']},
        {'y':designations[4]['designation_count'],"label":designations[4]['designation_name']},
        {'y':designations[5]['designation_count'],"label":designations[5]['designation_name']},
        {'y':designations[6]['designation_count'],"label":designations[6]['designation_name']},
        {'y':designations[7]['designation_count'],"label":designations[7]['designation_name']},
        {'y':designations[8]['designation_count'],"label":designations[8]['designation_name']},
        {'y':designations[9]['designation_count'],"label":designations[9]['designation_name']},
        {'y':designations[10]['designation_count'],"label":designations[10]['designation_name']},
        {'y':designations[11]['designation_count'],"label":designations[11]['designation_name']},
        {'y':designations[12]['designation_count'],"label":designations[12]['designation_name']},
        {'y':designations[13]['designation_count'],"label":designations[13]['designation_name']},
        {'y':designations[14]['designation_count'],"label":designations[14]['designation_name']},
        {'y':designations[15]['designation_count'],"label":designations[15]['designation_name']},
        {'y':designations[16]['designation_count'],"label":designations[16]['designation_name']},
        {'y':designations[17]['designation_count'],"label":designations[17]['designation_name']},
        {'y':designations[18]['designation_count'],"label":designations[18]['designation_name']},
        {'y':designations[19]['designation_count'],"label":designations[19]['designation_name']},
        {'y':designations[20]['designation_count'],"label":designations[20]['designation_name']},
        {'y':designations[21]['designation_count'],"label":designations[21]['designation_name']},
        {'y':designations[22]['designation_count'],"label":designations[22]['designation_name']}
      ]*/
window.onload = function () {

// Construct options first and then pass it as a parameter
var options1 = {
      title: {
        text: "DESIGNATION WISE EMPS"
      },
      axisY: {
        labelFontSize: 10,
        labelFontColor: "dimGrey"
      },
      data: [
      {
        type: "column",
        dataPoints: newobject
    }
    ]
    };
    console.log(options1);
$("#resizable").resizable({
	create: function (event, ui) {
		//Create chart.
		$("#chartContainer1").CanvasJSChart(options1);
	},
	resize: function (event, ui) {
		//Update chart size according to its container size.
		$("#chartContainer1").CanvasJSChart().render();
	}
});

}
</script>
<script src="vendor/script/jquery-ui.1.11.2.min.js"></script>
<script src="vendor/script/jquery.canvasjs.min.js"></script>

<!-- -------------------------Popup Start------------------------------>
                  <!--============Net Pay============-->
        <div class="nt_pop_box" id="nt_pop_box1">
        <div class="nt_pop_bx">
          <div class="nt_pop_x">x</div>
          <h2>Monthly Salaries</h2>
          <h3>Net Pay</h3>
          <p>
          <table>
            <thead>
              <tr>
              <th>Payment Type</th>
              <th>Employee Count</th>
              <th>Sum Amount(Rs)</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </p>
        </div>
    </div>
                 <!--============Total Employees============-->
    <div class="nt_pop_box" id="nt_pop_box2">
        <div class="nt_pop_bx total_emp">
          <div class="nt_pop_x">x</div>
          <h2>Employee Details</h2>
          <h3>Total Employees</h3>
          <p>
          <table>
            <thead>
              <tr>
              <th>Payment Type</th>
              <th>Employee Count</th>
              <th>Sum Amount(Rs)</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </p>
        </div>
    </div>
    <!---------------------------- Popup End--------------------->
    
    <script>
        $(document).ready(function(){ 
        
        $('.nt_pop_x').click(function(){
            $(".nt_pop_box").hide();
        });
        }); 
        function nt_pop_box(tid){
        //alert(tid);
            $("#nt_pop_box"+tid).show();
        }

    </script>


<!-- Piechart111111-->
<script type="text/javascript" src="vendor/script/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart1);

// Draw the chart and set the chart values
function drawChart1() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Present(<?php echo $bioCurrentDayCount[0]['p_cnt'] ?>)', <?php echo $bioCurrentDayCount[0]['p_cnt'] ?>],
  ['Absent(<?php echo $bioCurrentDayCount[0]['A_cnt'] ?>)', <?php echo $bioCurrentDayCount[0]['A_cnt'] ?>],
  ['Leaves(<?php echo $bioCurrentDayCount[0]['l_cnt'] ?>)', <?php echo $bioCurrentDayCount[0]['l_cnt'] ?>]
 
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Today BioMetric', 'width':450, 'height':170};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}
</script>

<!-- Piechart22222222222-->
<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],

  ['Present(<?php echo $DeptCurrentDayCount[0]['p_cnt'] ?>)', <?php echo $DeptCurrentDayCount[0]['p_cnt'] ?>],
  ['Absent(<?php echo $DeptCurrentDayCount[0]['A_cnt'] ?>)', <?php echo $DeptCurrentDayCount[0]['A_cnt'] ?>],
  ['Leave(<?php echo $DeptCurrentDayCount[0]['l_cnt'] ?>)', <?php echo $DeptCurrentDayCount[0]['l_cnt'] ?>]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':"<?php echo $DeptCurrentDayCount[0]['dept_name'] .' Attendace'; ?>", 'width':350, 'height':170};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
  chart.draw(data, options);
}
</script>
<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],

  ['Present(<?php echo $DeptCurrentDayCount[1]['p_cnt'] ?>)', <?php echo $DeptCurrentDayCount[1]['p_cnt'] ?>],
  ['Absent(<?php echo $DeptCurrentDayCount[1]['A_cnt'] ?>)', <?php echo $DeptCurrentDayCount[1]['A_cnt'] ?>],
  ['Leave(<?php echo $DeptCurrentDayCount[1]['l_cnt'] ?>)', <?php echo $DeptCurrentDayCount[1]['l_cnt'] ?>]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':"<?php echo $DeptCurrentDayCount[1]['dept_name'].' Attendace'; ?>", 'width':340, 'height':170};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart3'));
  chart.draw(data, options);
}

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 5000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
</script>
<style type="text/css">
  
.bar-graph {
  padding: 0;
  width: 100%;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-align-items: flex-end;
      -ms-flex-align: end;
          align-items: flex-end;
  height: 230px !important;
  margin: 0;
}

.bar-graph li {
  display: block;
  padding: 1.5625rem 0;
  position: relative;
  text-align: center;
  vertical-align: bottom;
  border-radius: 4px 4px 0 0;
  max-width: 4%;
  height: 100%;
  margin: 0 3px 0 0;
  -webkit-flex: 1 1 15%;
      -ms-flex: 1 1 15%;
          flex: 1 1 15%;
}

.bar-graph .bar-graph-axis {
  -webkit-flex: 1 1 8%;
      -ms-flex: 1 1 8%;
          flex: 1 1 8%;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-flex-direction: column;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-justify-content: space-between;
      -ms-flex-pack: justify;
          justify-content: space-between;
}

.bar-graph .bar-graph-label {
  margin: 0;
  background-color: none;
  color: #8a8a8a;
  position: relative;
}

@media print, screen and (min-width: 40em) {
  .bar-graph .bar-graph-label:before, .bar-graph .bar-graph-label:after {
    content: "";
    position: absolute;
    border-bottom: 1px dashed #8a8a8a;
    top: 0;
    left: 0;
    height: 50%;
    width: 20%;
  }
}

@media print, screen and (min-width: 40em) and (min-width: 64em) {
  .bar-graph .bar-graph-label:before, .bar-graph .bar-graph-label:after {
    width: 30%;
  }
}

@media print, screen and (min-width: 40em) {
  .bar-graph .bar-graph-label:after {
    left: auto;
    right: 0;
  }
}

.bar-graph .percent {
  color:#fff;
  width: 100%;
  font-size: 1.875rem;
  position: absolute;
 font-weight: 900;
  bottom: 0px;
}

@media print, screen and (min-width: 40em) {
  .bar-graph .percent {
        font-size: 10px;
  }
}

.bar-graph .description {
    color: #000;
  font-weight: 800;
  text-transform: lowercase;
  width: 100%;
  font-size: 14px;
  bottom: -20px;
  position: absolute;
  font-size: 1rem;
  overflow: hidden;
}


</style>
<style type="text/css">
  $bar-graph-height: 425px;

.bar-graph {
  padding: 0;
  width: 100%;
  display: flex;
  align-items: flex-end;
  height: $bar-graph-height;
  margin: 0;

  li {
    display: block;
    padding: rem-calc(25) 0;
    position: relative;
    text-align: center;
    vertical-align: bottom;
    border-radius: 4px 4px 0 0;
    max-width: 10%;
    height: 100%;
    margin: 0 1.8% 0 0;
    flex: 1 1 15%;
  }

  .bar-graph-axis {
    flex: 1 1 8%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }

  .bar-graph-label {
    margin: 0;
    background-color: none;
    color: $dark-gray;
    position: relative;

    @include breakpoint(medium) {
      &:before,
      &:after {
        content: "";
        position: absolute;
        border-bottom: 1px dashed $dark-gray;
        top: 0;
        left: 0;
        height: 50%;
        width: 20%;

        @include breakpoint(large) {
          width: 30%;
        }
      }

      &:after {
        left: auto;
        right: 0;
      }
    }
  }

  .percent {
    letter-spacing: -3px;
    opacity: 0.4;
    width: 100%;
    font-size: rem-calc(30);
    position: absolute;

    @include breakpoint(medium) {
      font-size: rem-calc(62);
    }

    span {
      font-size: rem-calc(30);
    }
  }

  .description {
    font-weight: 800;
    opacity: 0.5;
    text-transform: uppercase;
    width: 100%;
    font-size: 14px;
    bottom: 20px;
    position: absolute;
    font-size: rem-calc(16);
    overflow: hidden;
  }

 .bar {
    @each $name, $color in $foundation-palette {
      &.#{$name} {
        border: 1px solid $color;
        background: linear-gradient(lighten($color, 10%), $color 70%);
      }
    }
  }
}

.nt_pop_box {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 99999;
    background: rgba(0,0,0,0.6);
    overflow-y: auto;
    display: none;
}
.nt_pop_bx {
    position: fixed;
    left: 25%;
    bottom: 17%;
    display: table;
    margin: 60px auto;
    width: 50%;
    padding: 30px;
    background: #fff;
}
.nt_pop_x {
    position: absolute;
    top: 10px;
    right: 20px;
    font-size: 22px;
    cursor: pointer;
}
.nt_pop_bx h2 {
    display: block;
    font-size: 20px;
    color: #b90000;
    margin: 0 0 15px 0;
}
.nt_pop_bx h3 {
    display: block;
    font-size: 14px;
    color: #333;
    margin: 0 0 20px 0;
}
.overlay{
    position: absolute;
    width: 100%;
    bottom:0px;  
    background-color: #00000094; 
    /* background: linear-gradient(90deg,#0096e852 0,#0096e891 1%,#e031f77a);*/
     height: 100%;
}
.heading-1{
    color: #fff;
    font-size: 30px;
    font-weight: 600;
    font-style: italic;
}
.small-title{
    color: #fff;
}

.button1{
    padding: 10px 20px;
    background: #ff7a00;
    border-radius: 4px;
    cursor: pointer;
    top: 50%;
    right: 18%;
    position: absolute;
}
.getstarted{
    margin-top: 50px;
    background-image: url('../sliders/bg1.jpg');
    background-size: cover;
    background-position: center;
    position: relative;
}
.overlay-1{
    background: #00000082; 
    position: absolute;
    width: 100%;
    top:0px;
    height: 100%
}
.bg-inverse-clr{
    background-color: darkturquoise;
    color: #ffffff;
}
.txt-name{
    font-size: 20px;
    padding-top: 8px;
}
.bg-blue-2{
        background: steelblue;
    color: #fff;
    text-align: center;
}
.f-left {
    float:left;
}
.d-i-block{
        display:inline-block;
}
.pl-20{
    padding-left:8px;
}
.total_emp{
        top: 9%!important;
}
.bar-graph li{
  max-height:105%!important;  
}
</style>