<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Empsavingsmaster extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_empsavingsmaster = $this->Global_model->get_values_where($this->config->item('empsavingsmaster'),array('status'=>1));
		$get_empsavingsmaster_log = $this->Global_model->get_values_where($this->config->item('empsavingsmaster_log'), array('flag'=>2));
		$get_employees = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
		$data=array('current_page'=>'Savings Master','page_title'=>'Savings Master','parent_menu'=>'','template'=>'masters/empsavingsmaster/add','get_empsavingsmaster'=>$get_empsavingsmaster,'get_empsavingsmaster_log'=>$get_empsavingsmaster_log,'get_employees'=>$get_employees);
		$this->form_validation->set_rules('emp_id','emp_id','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_id');
			$sec_80c = $this->input->post('sec_80c');
			$sec_80d = $this->input->post('sec_80d');
			$sec_80cc = $this->input->post('sec_80cc');
			$sec_80g = $this->input->post('sec_80g');
			
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('emp_bid'=>$emp_bid,'sec_80c'=>$sec_80c,'sec_80g'=>$sec_80g, 'sec_80d'=>$sec_80d, 'sec_80cc'=>$sec_80cc, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Savings Master not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Savings Master inserted successfully'));
			}
			redirect('Empsavingsmaster');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('empsavingsmaster');
		$where = array('empsavings_id'=>base64_decode($id));
		$empsavings_id = base64_decode($id);
		$getSavingsMasterEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_empsavingsmaster = $this->Global_model->get_values_where($this->config->item('empsavingsmaster'),array('status'=>1));
		$get_empsavingsmaster_log = $this->Global_model->get_values_where($this->config->item('empsavingsmaster_log'), array('flag'=>2));
		$get_employees = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
			$data=array('current_page'=>'Savings Master','page_title'=>'Savings Master','parent_menu'=>'','template'=>'masters/empsavingsmaster/add','get_empsavingsmaster'=>$get_empsavingsmaster,'getSavingsMasterEdit'=>$getSavingsMasterEdit,'get_empsavingsmaster_log'=>$get_empsavingsmaster_log,'get_employees'=>$get_employees);
		$this->form_validation->set_rules('emp_id','emp_id','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_id');
			$sec_80c = $this->input->post('sec_80c');
			$sec_80d = $this->input->post('sec_80d');
			$sec_80cc = $this->input->post('sec_80cc');
			$sec_80g = $this->input->post('sec_80g');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('emp_bid'=>$emp_bid,'sec_80c'=>$sec_80c,'sec_80g'=>$sec_80g, 'sec_80d'=>$sec_80d, 'sec_80cc'=>$sec_80cc, 'status'=>$status,'empsavings_id'=>$empsavings_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Savings Master not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Savings Master updated successfully'));
			}
			redirect('Empsavingsmaster');
		}
		
	}
	function delete($id)
	{
			$empsavings_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('empsavings_id'=>$empsavings_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Savings Master not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Savings Master  Deleted successfully'));
		}
		redirect('Empsavingsmaster');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$empsavings_id = base64_decode($id);
				$undo_array = array('empsavings_id'=>$empsavings_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Savings Master not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Savings Master undo successfully'));
				}
		
		redirect('Empsavingsmaster');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$empsavings_id = $insert_array['empsavings_id'];
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$emp_bid = $insert_array['emp_bid'];
			$param['insertdata'] = array('empsavings_id'=>$this->Global_model->Max_count($this->config->item('empsavingsmaster'),'empsavings_id'),'emp_bid'=>$emp_bid,'sec_80c'=>$insert_array['sec_80c'], 'sec_80d'=>$insert_array['sec_80d'], 'sec_80cc'=>$insert_array['sec_80cc'],'sec_80g'=>$insert_array['sec_80g'],'status'=>1,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('empsavingsmaster');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			
			$empsavings_id = $insert_array['empsavings_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('empsavingsmaster'), array('empsavings_id'=>$empsavings_id));
				
				$param['insertdata'] = array('empsavings_id'=>$insertdata->empsavings_id,'emp_bid'=>$insertdata->emp_bid,'sec_80c'=>sec_80c, 'sec_80d'=>$insertdata->sec_80d,'sec_80g'=>$insert_array['sec_80g'], 'sec_80cc'=>$insertdata->sec_80cc,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('empsavingsmaster_log');
			/* Update log table end */
			$data1 = array('empsavings_id'=>$empsavings_id,'emp_bid'=>$insert_array['emp_bid'],'sec_80c'=>$insert_array['sec_80c'], 'sec_80d'=>$insert_array['sec_80d'],'sec_80g'=>$insert_array['sec_80g'], 'sec_80cc'=>$insert_array['sec_80cc'],'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('empsavings_id'=>$empsavings_id);
			$this->Global_model->update($this->config->item('empsavingsmaster'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$empsavings_id = $insert_array['empsavings_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('empsavingsmaster'), array('empsavings_id'=>$empsavings_id));
				
				$param['insertdata'] = array('empsavings_id'=>$insertdata->empsavings_id,'emp_bid'=>$insertdata->emp_bid,'sec_80c'=>$insertdata->sec_80c,'sec_80g'=>$insert_array['sec_80g'], 'sec_80d'=>$insertdata->sec_80d, 'sec_80cc'=>$insertdata->sec_80cc,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('empsavingsmaster_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('empsavingsmaster')." WHERE empsavings_id =".$empsavings_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$empsavings_id = $insert_array['empsavings_id'];
			$where = array('empsavings_id'=>$empsavings_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('empsavingsmaster_log')." WHERE empsavings_id=".$empsavings_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('empsavings_id'=>$empsavings_id,'sec_80c'=>$getPaymentTypes->sec_80c, 'sec_80d'=>$getPaymentTypes->sec_80d,'sec_80g'=>$insert_array['sec_80g'], 'sec_80cc'=>$getPaymentTypes->sec_80cc,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('empsavingsmaster'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Savings Master not Undo '));	
					redirect('Empsavingsmaster');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Empsavingsmaster');
		}	
	}
	function getEmployeeName()
	{
		$emp_bid = $this->input->post('emp_bid');
		$get_employees = $this->Global_model->get_values_where_single($this->config->item('employee'),array('emp_bid'=>$emp_bid));
		echo $get_employees->emp_name;
	}
}
