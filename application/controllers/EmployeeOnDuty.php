<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class EmployeeOnDuty extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('employee_leave_model');
	}
	public function index()
	{
		$data=array('current_page'=>'On Duty Employees','page_title'=>'On Duty Employees','parent_menu'=>'','template'=>'employee_on_duty');
		$activities=$this->employee_leave_model->activities();
		$employees_bid=$this->employee_leave_model->employeesBid();
		$employees_name=$this->employee_leave_model->employeesName();
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$this->load->view('theme/template',$data);
	}
	public function All()
	{
		$data=array('current_page'=>'On Duty Employees','page_title'=>'On Duty Employees','parent_menu'=>'','template'=>'employee_on_duty');
		$employees=array();
		$activities=$this->employee_leave_model->activities();
		$employees_bid=$this->employee_leave_model->employeesBid();
		$employees_name=$this->employee_leave_model->employeesName();
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		if ($month!=null && $year!=null) {
			$employees=$this->employee_leave_model->onDutyEmployeesWhere("MONTHNAME(am.leave_date)='".$month."' and YEAR(am.leave_date)='".$year."'");
		}
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['on_duty_employees']=$employees;
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$data=array('current_page'=>'On Duty Employees','page_title'=>'On Duty Employees','parent_menu'=>'','template'=>'employee_on_duty');
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');		
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$activities=$this->employee_leave_model->activities();
		$employees_bid=$this->employee_leave_model->employeesBid();
		$employees_name=$this->employee_leave_model->employeesName();
		$employees=array();
		if ($month!=null && $year!=null) {
			if ($activity_type!=null) {
				$employees=$this->employee_leave_model->onDutyEmployeesWhere("loa.dept_name='".$activity_type."' and MONTHNAME(am.leave_date)='".$month."' and YEAR(am.leave_date)='".$year."'");
			}
			else if ($bid!=null) {
				$employees=$this->employee_leave_model->onDutyEmployeesWhere("e.emp_bid=".$bid." and MONTHNAME(am.leave_date)='".$month."' and YEAR(am.leave_date)='".$year."'");
			}
			else if ($ename!=null) {
				$employees=$this->employee_leave_model->onDutyEmployeesWhere("e.emp_name='".$ename."' and MONTHNAME(am.leave_date)='".$month."' and YEAR(am.leave_date)='".$year."'");
			}
		}
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;

		$data['on_duty_employees'] = $employees;
		$this->load->view('theme/template',$data);
	}

}
