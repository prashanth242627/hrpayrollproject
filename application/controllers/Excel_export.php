<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Excel_export extends Global_System {
 public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
		$this->load->model('Salary_model');
		$this->load->model("PaymentMode_model");
		$this->load->model("attendance_reports_model");
		$this->load->model("monthwise_attendance_model");
		$this->load->model("Dept_Salaries_model");
		$this->load->model('Employee_pf_model');
		$this->load->model('Employee_pt_model');	
		$this->load->model('Employee_leave_model');
		$this->load->model("Savings_model");
		$this->load->model("Employee_esi_model");
	}
public function index()
 {
  $this->load->model("excel_export_model");
  $data["employee_data"] = $this->excel_export_model->fetch_data();
  $this->load->view("excel_export_view", $data);
 }
public function employeesData(){
 	$post_data=$this->input->get('data');
 	$activity_type=$this->input->get('type');
	$bid=$this->input->get('bid');
	$ename=$this->input->get('ename');
	$status=$this->input->get('status');
	$start_date=$this->input->get('from');
	$end_date=$this->input->get('to');
	$employees=array();
	if ($activity_type!=null) {
		if ($activity_type=="All") {
			$employees=$this->Employee_model->allEmployees();
		}
		else{
			$employees=$this->Employee_model->lineOfActivityEmployees($activity_type);
		}
	}
	else if ($bid!=null) {
		$employees=$this->Employee_model->employeeData("emp_bid=".$bid);
	}
	else if ($ename!=null) {
		$employees=$this->Employee_model->employeeData("emp_name='".$ename."'");
	}
	else if ($status!=null) {
		if ($status=="Active") {
			$employees=$this->Employee_model->activeEmployees();
		}
		else if ($status=="Inactive") {
			$employees=$this->Employee_model->inActiveEmployees();
		}
		else if ($status=="New") {
			$employees=$this->Employee_model->newEmployees();
		}
	}
	else if ($start_date!=null) {
		$employees=$this->Employee_model->getEmployeesData("date_of_join>='".$start_date."' and date_of_join <='".$end_date."'");
	}
	else if ($end_date!="" && $start_date==null) {
		$employees=$this->Employee_model->getEmployeesData("date_of_join <='".$end_date."'");
	}
 	if(count($employees)){
 		$this->load->model("excel_export_model");
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$table_columns = array("Sl.NO","Emp Id","BID","E.Name", "D.O.J", "D.O.B", "Address", "Higher Education","Experience","Mobile No","Department","Designation","Last Working Day","Transportation","Status");
		$column = 0;

		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		$column++;
		}
		$excel_row = 2;
		$i=1;
		foreach($employees as $row)
		{
			if (isset($row['flag'])) {
				$status="Inactive";
			}
			else{
				$status="Active";
			}
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row["temp_emp_id"]);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row["emp_bid"]);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['emp_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['date_of_join']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['date_of_birth']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['address']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['education']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['experience']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['mobile']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, "last working date");
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, "transportation");
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $status);
			$excel_row++;
		}
		
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Employees Data.xls"');
		ob_clean();
		$object_writer->save('php://output');
	}
	else{
		echo "NO-Data";
	}
 }
 public function MonthlySalaries()
 {
 	$activity_type=$this->input->get('type');
	$bid=$this->input->get('bid');
	$ename=$this->input->get('ename');
	$payment_mode=$this->input->get('payment_mode');
	$month=$this->input->get('month');
	$year=$this->input->get('year');
	$employees=array();
	if ($year!=null && $month!=null) {
		if ($bid!=null) {
			$employees=$this->Salary_model->CTCPerMonthByWhereYearMonth("e.emp_bid=".$bid,$year,$month);
		}
		else if ($ename!=null) {
			$employees=$this->Salary_model->CTCPerMonthByWhereYearMonth("e.emp_name='".ltrim($ename)."'",$year,$month);
		}
		else if ($activity_type=="All") {
				$employees=$this->Salary_model->allCTCPerMonth($month,$year);
			}
		else if ($payment_mode!=null ) {
			$employees=$this->Salary_model->CTCPerMonthByWhereYearMonth("pmttyp.payment_type='".$payment_mode."'",$year,$month);
		}
		else if ($activity_type!=null) {
			$employees=$this->Salary_model->CTCPerMonthByWhereYearMonth("l.dept_name='".$activity_type."'",$year,$month);
		}
		
	}
	if(count($employees)){
 		$this->load->model("excel_export_model");
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:Z1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:D2');
		$object->setActiveSheetIndex(0)->mergeCells('E2:G2');
		$object->setActiveSheetIndex(0)->mergeCells('H2:J2');
		$object->setActiveSheetIndex(0)->mergeCells('K2:M2');
		$object->getActiveSheet()->setCellValue('A1', 'Monthly Salaries');
		$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria: '.$activity_type);
		$object->getActiveSheet()->setCellValue('E2', 'Payment Mode: '.$payment_mode);

		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'BID:'.$bid);
		}
		if ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria:'.$ename);
		}

		$table_columns = array("Sl.No","E ID", "E BID", "E Name", "D.O.J", "Designation","Department","Bank A/c Number","Payment Mode","LOP Days","No Of Pay Days","Basic","INCR","D.A","H.R.A","C.C.A","Other allowances","Head/HOD/Coordinator allowances","Special Allowances","Gross Pay","PF Deducted","Professional Tax","ESI","TDS","Other Deductions","Remarks","Net Pay","Fixed Monthly CTS");                     
		$column = 0;

		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
		$column++;
		}
		$excel_row = 4;
		$i=1;
		$gross=0;
		$net=0;
		foreach($employees as $employee)
		{
			$gross+=$employee['Gross_pay'];
            $net+=$employee['Net_pay'];         
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['date_of_join']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['designation']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $employee['acc_no']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $employee['payment_mode']);
			if ($employee['Lop_days']==0.00) {
				$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, 0);
			}
			else{
				$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $employee['Lop_days']);
			}
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $employee['no_of_pay_days']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $employee['basic_earnings']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $employee['incr_earnings']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $employee['da_earnings']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $employee['hra_earnings']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $employee['cca_earnings']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $employee['other_allowance_earnings']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $employee['co_allowance_earnings']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $employee['special_allowance_earnings']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $employee['Gross_pay']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $employee['pf_deducted']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, $employee['professional_tax']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $employee['esi']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, $employee['TDS']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(24, $excel_row, $employee['management_deductions']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, $employee['remarks']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(26, $excel_row, $employee['Net_pay']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(27, $excel_row, $employee['fixed_monthly_ctc']);
			
			$excel_row++;
		}
		$object->getActiveSheet()->setCellValueByColumnAndRow(25,$excel_row,"Total");
		$object->getActiveSheet()->setCellValueByColumnAndRow(27,$excel_row,$gross);
		$object->getActiveSheet()->setCellValueByColumnAndRow(26,$excel_row,$net);
		$object->getActiveSheet()->setCellValue('H2', 'Gross :'.$gross);
		$object->getActiveSheet()->setCellValue('K2', 'Net :'.$net);

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Monthly salaries-'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');
	}
	else{
		echo "NO-Data";
	}
 }
 public function salaryDetails()
 {
 	$activity_type=$this->input->get('type');
	$bid=$this->input->get('bid');
	$ename=$this->input->get('ename');
	$payment_mode=$this->input->get('payment_mode');
	$employees=array();
	if ($activity_type!=null) {
		if ($activity_type=="All") {
			$employees=$this->Salary_model->allSalariesDetails();
		}
		else{
			$employees=$this->Salary_model->lineOfActivitySalaries($activity_type);
		}
	}
	else if ($bid!=null) {
		$employees=$this->Salary_model->employeeSalaryDataByBid($bid);
	}
	else if ($ename!=null) {
		$employees=$this->Salary_model->employeeSalaryDataByName($ename);
	}
	else if ($payment_mode!=null) {
		$employees=$this->Salary_model->paymentModeSalaries($payment_mode);
	}
	else if ($start_date!=null) {
		$employees=$this->Salary_model->getSalariesData("e.date_of_join>='".$start_date."' and e.date_of_join <='".$end_date."'");
	}
	else if ($end_date!="" && $start_date==null) {
		$employees=$this->Salary_model->getSalariesData("e.date_of_join <='".$end_date."'");
	}
	if(count($employees)){
 		$this->load->model("excel_export_model");
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$table_columns = array("Sl.No","E ID", "E BID", "E Name", "D.O.J", "Designation","Department","Bank A/c Number","Payment Mode","UAN","PF Number","ESI Number","Basic","INCR","D.A","H.R.A","C.C.A","Other allowances","Head/HOD/Coordinator allowances","Special Allowances","PF Deducted","Gross Pay","Net Pay");      
		$column = 0;

		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		$column++;
		}
		$excel_row = 2;
		$i=1;
		foreach($employees as $employee)
		{         
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['date_of_join']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $employee['acc_no']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $employee['payment_type']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $employee['uan']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $employee['pf_no']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $employee['esi_no']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $employee['basic']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $employee['incr']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $employee['da']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $employee['hra']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $employee['cca']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $employee['other_allowance']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $employee['co_allowance']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $employee['special_allowance']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $employee['empr_pf_contribution']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, $employee['gross']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $employee['CTC']);
			$excel_row++;
			
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="EmployeeCTSDetails.xls"');
		$object_writer->save('php://output');
	}
	else{
		echo "NO-Data";
	}
 }
 public function DepartmentSalaries()
 {
 	$activity_type=$this->input->get('type');
	$year=$this->input->get('year');
	$month=$this->input->get('month');
	if ($month!=null && $year!=null) {
		if ($activity_type!=null && $activity_type=="All") {
			$salaries=$this->Dept_Salaries_model->AlldeptSalaries($month,$year);
		}
		else if ($activity_type!=null) {
			$salaries=$this->Dept_Salaries_model->deptSalariesByWhere("la.dept_name='".$activity_type."'",$month,$year);
		}
	}
	if (count($salaries)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);  
		$object->setActiveSheetIndex(0)->mergeCells('A1:F1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:B2');
		$object->setActiveSheetIndex(0)->mergeCells('C2:D2');
		$object->setActiveSheetIndex(0)->mergeCells('E2:F2');
		$object->getActiveSheet()->setCellValue('A1', 'BioMetric Attendance Reports');
		$object->getActiveSheet()->setCellValue('A2', 'Department='.$activity_type);
		$object->getActiveSheet()->setCellValue('C2', 'Month='.$month);
		$object->getActiveSheet()->setCellValue('E2', 'Year='.$year);
		
		$table_columns = array("Sl.NO","Department","Employee Count","Gross Pay","Deductions","Net Pay");
		$column = 0;

		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
		$column++;
		}
		$excel_row = 4;
		$i=1;
		foreach($salaries as $employee)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_count'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['Gross_pay'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['deductions']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['Net_Pay']);
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="DepartmentSalaries'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
 public function EmployeeLeaveStatus()
 {
 		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$a_reports=array();
		if ($activity_type!=null) {
			if ($activity_type=="All") {
				$no_of_leaves=$this->Employee_leave_model->allEmployees();
			}
			else{
				$no_of_leaves=$this->Employee_leave_model->lineOfActivityEmployees($activity_type);
			}
			
		}
		else if ($bid!=null) {
			$no_of_leaves=$this->Employee_leave_model->noOfLeavesByWhere("e.emp_bid=".$bid);
		}
		else if ($ename!=null) {
			$no_of_leaves=$this->Employee_leave_model->noOfLeavesByWhere("e.emp_name='".$ename."'");
		}
		if (count($no_of_leaves)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:P1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:P2');
		$object->getActiveSheet()->setCellValue('A1', 'Employee Leaves Report');
		$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria: '.$activity_type);

		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria:'.$bid);
		}
		elseif ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria:'.$ename);
		}

        $object->setActiveSheetIndex(0)->mergeCells('A3:A4');
		$object->setActiveSheetIndex(0)->mergeCells('B3:B4');
		$object->setActiveSheetIndex(0)->mergeCells('C3:C4');
		$object->setActiveSheetIndex(0)->mergeCells('D3:D4');
		$object->setActiveSheetIndex(0)->mergeCells('E3:E4');
		$object->setActiveSheetIndex(0)->mergeCells('F3:F4');
		$object->setActiveSheetIndex(0)->mergeCells('G3:G4');

        $object->setActiveSheetIndex(0)->mergeCells('H3:J3');
        $object->setActiveSheetIndex(0)->mergeCells('K3:M3');
        $object->setActiveSheetIndex(0)->mergeCells('N3:P3');

		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Date Of Join","Department","Designation","CL","EL","SL");
		$column = 0;

		$object->getActiveSheet()->setCellValue('A3', 'Sl.NO');
		$object->getActiveSheet()->setCellValue('B3', 'E ID');
		$object->getActiveSheet()->setCellValue('C3', 'E BID');
		$object->getActiveSheet()->setCellValue('D3', 'E.Name');
		$object->getActiveSheet()->setCellValue('E3', 'Date Of Join');
		$object->getActiveSheet()->setCellValue('F3', 'Department');
		$object->getActiveSheet()->setCellValue('G3', 'Designation');

		$object->getActiveSheet()->setCellValue('H3', 'CL');
		$object->getActiveSheet()->setCellValue('K3', 'EL');
		$object->getActiveSheet()->setCellValue('N3', 'SL');

		$object->getActiveSheet()->setCellValue('H4', 'OPB');
		$object->getActiveSheet()->setCellValue('I4', 'USED');
		$object->getActiveSheet()->setCellValue('J4', 'Balance');

		$object->getActiveSheet()->setCellValue('K4', 'OPB');
		$object->getActiveSheet()->setCellValue('L4', 'USED');
		$object->getActiveSheet()->setCellValue('M4', 'Balance');

		$object->getActiveSheet()->setCellValue('N4', 'OPB');
		$object->getActiveSheet()->setCellValue('O4', 'USED');
		$object->getActiveSheet()->setCellValue('P4', 'Balance');


		$excel_row = 5;
		$i=1;
		foreach($no_of_leaves as $employee)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['date_of_join'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $employee['cl_opb']+$employee['cl_used']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $employee['cl_used']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $employee['cl_opb']);

			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $employee['el_opb']+$employee['el_used']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $employee['el_used']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $employee['el_opb']);

			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $employee['sl_opb']+$employee['sl_used']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $employee['sl_used']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $employee['sl_opb']);
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Employee Leave Status.xls"');
		ob_clean();
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
 public function EmpPFContribution()
 {

 		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$year=$this->input->get('year');
		$month=$this->input->get('month');
		$a_reports=array();
		$employees=array();
		if ($year!=null && $month!=null) {
			if ($activity_type!=null) {
				if ($activity_type=="All") {
					$employees=$this->Employee_pf_model->AllEmployeesPFData($year,$month);
				}
				else{
					$employees=$this->Employee_pf_model->EmployeePFDataWhere("la.dept_name='".$activity_type."'",$year,$month);
				}
				
			}
			else if ($ename!=null) {
				if ($ename=="All") {
					$employees=$this->Employee_pf_model->AllEmployeesPFData($year,$month);
				}
				else{
					$employees=$this->Employee_pf_model->EmployeePFDataWhere("e.emp_name='".$ename."'",$year,$month);
				}
				
			}
			else if ($bid!=null) {
				if ($bid=="All") {
					$employees=$this->Employee_pf_model->AllEmployeesPFData($year,$month);
				}
				else{
					$employees=$this->Employee_pf_model->EmployeePFDataWhere("e.emp_bid=".$bid,$year,$month);
				}
			}
		}
		if (count($employees)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:M1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$object->setActiveSheetIndex(0)->mergeCells('D2:E2');
		$object->setActiveSheetIndex(0)->mergeCells('F2:G2');
		$object->setActiveSheetIndex(0)->mergeCells('H2:J2');
		$object->setActiveSheetIndex(0)->mergeCells('K2:M2');
		$object->getActiveSheet()->setCellValue('A1', 'Employee PF Details');
		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Bid :'.$bid);
		}
		else if ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Emp Name :'.$ename);
		}
		else if ($activity_type!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Department :'.$activity_type);
		}
		$object->getActiveSheet()->setCellValue('D2', 'Month='.$month);
		$object->getActiveSheet()->setCellValue('F2', 'Year='.$year);
		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Designation","Department","PF A/C No","UAN","Gross Wages","PF Wages","PF","EPS Contribution","EPF Contribution","Admin Charges","INSP Charges","EDLI Charges","Total");
		$column = 0;

		foreach($table_columns as $field)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
			$column++;
		}
		$excel_row = 4;
		$i=1;
		$total_tds_year=0;
		$gross_wages=0;
		$Total=0;
		foreach($employees as $employee)
		{
			
			$gross_wages+=round($employee['gross_wages']);
           $pf_wages+=round($employee['pf_wages']);
           $PF+=round($employee['PF']);
           $EPS_Contribution+=round($employee['EPS_Contribution']);
           $EPF_Contribution+=round($employee['EPF_Contribution']);
           $admin_charges_Contribution+=round($employee['admin_charges_Contribution']);
           $insp_charges_Contribution+=round($employee['insp_charges_Contribution']);
           $EDLI_Charges+=round($employee['EDLI_Charges']);
           $Total+=round($employee['Total']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['pf_acc_no']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $employee['uan']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, round($employee['gross_wages']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, round($employee['pf_wages']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, round($employee['PF']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, round($employee['EPS_Contribution']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, round($employee['EPF_Contribution']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, round($employee['admin_charges_Contribution']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, round($employee['insp_charges_Contribution']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, round($employee['EDLI_Charges']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, round($employee['Total']));
			$excel_row++;
		}
		$object->getActiveSheet()->setCellValueByColumnAndRow(7,$excel_row,"Total");
		$object->getActiveSheet()->setCellValueByColumnAndRow(8,$excel_row,$gross_wages);
		$object->getActiveSheet()->setCellValueByColumnAndRow(9,$excel_row,$pf_wages);
		$object->getActiveSheet()->setCellValueByColumnAndRow(10,$excel_row,$PF);
		$object->getActiveSheet()->setCellValueByColumnAndRow(11,$excel_row,$EPS_Contribution);
		$object->getActiveSheet()->setCellValueByColumnAndRow(12,$excel_row,$EPF_Contribution);
		$object->getActiveSheet()->setCellValueByColumnAndRow(13,$excel_row,$admin_charges_Contribution);
		$object->getActiveSheet()->setCellValueByColumnAndRow(14,$excel_row,$insp_charges_Contribution);
		$object->getActiveSheet()->setCellValueByColumnAndRow(15,$excel_row,$EDLI_Charges);
		$object->getActiveSheet()->setCellValueByColumnAndRow(16,$excel_row,$Total);
		$object->getActiveSheet()->setCellValue('H2', 'Total Gross Wages :'.$gross_wages);
		$object->getActiveSheet()->setCellValue('K2', 'Total :'.$Total);
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$ename.'EmployeePFDetail'.$month.'-'.$year.'.xls"');
		
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
 public function ProfessionalTax()
 {

 		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$year=$this->input->get('year');
		$month=$this->input->get('month');
		$a_reports=array();
		$employees=array();
		if ($year!=null && $month!=null) {
			if ($activity_type!=null) {
				if ($activity_type=="All") {
					$employees=$this->Employee_pt_model->AllEmployeesPTData($year,$month);
				}
				else{
					$employees=$this->Employee_pt_model->EmployeePTDataWhere("la.dept_name='".$activity_type."'",$year,$month);
				}
				
			}
			else if ($ename!=null) {
				if ($ename=="All") {
					$employees=$this->Employee_pt_model->AllEmployeesPTData($year,$month);
				}
				else{
					$employees=$this->Employee_pt_model->EmployeePTDataWhere("e.emp_name='".$ename."'",$year,$month);
				}
				
			}
			else if ($bid!=null) {
				if ($bid=="All") {
					$employees=$this->Employee_pt_model->AllEmployeesPTData($year,$month);
				}
				else{
					$employees=$this->Employee_pt_model->EmployeePTDataWhere("e.emp_bid=".$bid,$year,$month);
				}
			}
		}
		if (count($employees)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:M1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$object->setActiveSheetIndex(0)->mergeCells('D2:E2');
		$object->setActiveSheetIndex(0)->mergeCells('F2:G2');
		$object->setActiveSheetIndex(0)->mergeCells('H2:J2');
		$object->getActiveSheet()->setCellValue('A1', 'Employee Professional Tax Details');
		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Bid :'.$bid);
		}
		else if ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Emp Name :'.$ename);
		}
		else if ($activity_type!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Department :'.$activity_type);
		}
		$object->getActiveSheet()->setCellValue('D2', 'Month='.$month);
		$object->getActiveSheet()->setCellValue('F2', 'Year='.$year);

		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Designation","Department","Professional Tax");
		$column = 0;

		foreach($table_columns as $field)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
			$column++;
		}
		$excel_row = 4;
		$i=1;
		$total_tds_year=0;
		$gross_wages=0;
		$Total=0;
		foreach($employees as $employee)
		{
           $Total+=round($employee['pt']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['pt']);
			$excel_row++;
		}
		$object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row,"Total");
		$object->getActiveSheet()->setCellValueByColumnAndRow(6,$excel_row,$Total);
		$object->getActiveSheet()->setCellValue('H2', 'Total :'.$Total);
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$ename.'EmployeePTDetail'.$month.'-'.$year.'.xls"');
		
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
 public function EmpESIreturns()
 {
 		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$year=$this->input->get('year');
		$month=$this->input->get('month');
		$a_reports=array();
		if ($year!=null && $month!=null) {
			if ($activity_type!=null) {
				if ($activity_type=="All") {
					$employees=$this->Employee_esi_model->AllEmployeesESIData($year,$month);
				}
				else{
					$employees=$this->Employee_esi_model->EmployeeESIDataWhere("la.dept_name='".$activity_type."'",$year,$month);
				}
			}
			else if ($ename!=null) {
				if ($ename=="All") {
					$employees=$this->Employee_esi_model->AllEmployeesESIData($year,$month);
				}
				else{
					$employees=$this->Employee_esi_model->EmployeeESIDataWhere("e.emp_name='".$ename."'",$year,$month);
				}
			}
			else if ($bid!=null) {
				if ($bid=="All") {
					$employees=$this->Employee_esi_model->AllEmployeesESIData($year,$month);
				}
				else{
					$employees=$this->Employee_esi_model->EmployeeESIDataWhere("e.emp_bid=".$bid,$year,$month);
				}
			}
		}
		if (count($employees)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:J1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:B2');
		$object->setActiveSheetIndex(0)->mergeCells('C2:D2');
		$object->setActiveSheetIndex(0)->mergeCells('E2:F2');
		$object->setActiveSheetIndex(0)->mergeCells('G2:H2');
		$object->setActiveSheetIndex(0)->mergeCells('I2:J2');
		$object->getActiveSheet()->setCellValue('A1', 'Employee PF Details');
		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Bid :'.$bid);
		}
		else if ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Emp Name :'.$ename);
		}
		else if ($activity_type!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Department :'.$activity_type);
		}
		$object->getActiveSheet()->setCellValue('C2', 'Month='.$month);
		$object->getActiveSheet()->setCellValue('E2', 'Year='.$year);
		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Designation","Department","Gross","ESI Consider","ESI Employee Contribution","ESI Employer Contribution");
		$column = 0;

		foreach($table_columns as $field)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
			$column++;
		}
		$excel_row = 4;
		$i=1;
		$total_tds_year=0;
		foreach($employees as $employee)
		{
			$esiec+=round($employee['esi_employee_contribution']);
            $esierc+=round($employee['esi_employr_contribution']);

			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, round($employee['gross']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, ($employee['esi_consider'])? "Yes":"No");
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, round($employee['esi_employee_contribution']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, round($employee['esi_employr_contribution']));
			$excel_row++;
		}
		$object->getActiveSheet()->setCellValueByColumnAndRow(7,$excel_row,"Total");
		$object->getActiveSheet()->setCellValueByColumnAndRow(8,$excel_row,$esiec);
		$object->getActiveSheet()->setCellValueByColumnAndRow(9,$excel_row,$esierc);

		$object->getActiveSheet()->setCellValue('G2', 'Total EMP Contribution :'.$esiec);
		$object->getActiveSheet()->setCellValue('I2', 'Total EMPER Contribution :'.$esierc);
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="EmployeeESIDetails'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
  public function EmployeeSavings()
 {
 		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$a_reports=array();
		if ($activity_type!=null) {
			if ($activity_type=="All") {
				$employees=$this->Savings_model->AllEmployeesSavingsData();
			}
			else{
				$employees=$this->Savings_model->EmployeeSavingsDataWhere("loa.dept_name='".$activity_type."'");
			}
			
		}
		else if ($bid!=null) {
			$employees=$this->Savings_model->EmployeeSavingsDataWhere("e.emp_bid=".$bid);
		}
		else if ($ename!=null) {
			$employees=$this->Savings_model->EmployeeSavingsDataWhere("e.emp_name='".$ename."'");
		}
		if (count($employees)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:P1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:H2');
		$object->setActiveSheetIndex(0)->mergeCells('I2:P2');
		$object->getActiveSheet()->setCellValue('A1', 'Employee TDS Details');
		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Bid :'.$bid);
		}
		else if ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Emp Name :'.$ename);
		}
		else if ($activity_type!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Department :'.$activity_type);
		}

		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Date of Join","Designation","Department","CTS","Taxable Amount","80C Savings","80CC Savings","80D Savings","Taxable Amount After Savings","TDS Per Year","TDS Recovered","TDS Balance");
		$column = 0;

		foreach($table_columns as $field)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
			$column++;
		}
		$excel_row = 4;
		$i=1;
		$total_tds_year=0;
		foreach($employees as $employee)
		{
			$total_tds_year+=round($employee['tds_year']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['date_of_join']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, round($employee['CTC']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, round($employee['Taxable_amt']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, round($employee['80c']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, round($employee['80cc']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, round($employee['80d']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, round($employee['After_savings']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, round($employee['tds_year']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, "");
			$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, "");
			$excel_row++;
		}
		$object->getActiveSheet()->setCellValueByColumnAndRow(12,$excel_row,"Total");
		$object->getActiveSheet()->setCellValueByColumnAndRow(13,$excel_row,$total_tds_year);
		$object->getActiveSheet()->setCellValue('I2', 'Total TDS Per Year :'.$total_tds_year);
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="EmployeeTDSDetails-'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }

 public function EmployeeMonthlyLeaveStatus()
 {
 		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$a_reports=array();
		if ($year!=null && $month!=null ) {
			if ($activity_type!=null) {
				
				if ($activity_type=="All") {
					$no_of_leaves=$this->Employee_leave_model->allEmployeesMonthlyLeaves($month,$year);
				}
				else{
					$no_of_leaves=$this->Employee_leave_model->noOfMonthlyLeavesByWhere("loa.dept_name='".$activity_type."'",$month,$year);
				}
			}
			else if ($bid!=null) {
				$no_of_leaves=$this->Employee_leave_model->noOfMonthlyLeavesByWhere("e.emp_bid=".$bid,$month,$year);
			}
			else if ($ename!=null) {
				$no_of_leaves=$this->Employee_leave_model->noOfMonthlyLeavesByWhere("e.emp_name='".$ename."'",$month,$year);
			}
		}
		
		if (count($no_of_leaves)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:H1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$object->setActiveSheetIndex(0)->mergeCells('D2:F2');
		$object->setActiveSheetIndex(0)->mergeCells('G2:H2');
		$object->getActiveSheet()->setCellValue('A1', 'Employee Monthly Leaves Status');
		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Bid='.$bid);
		}
		else if ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Emp Name='.$ename);
		}
		else if ($activity_type!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Department :'.$activity_type);
		}
		$object->getActiveSheet()->setCellValue('D2', 'Month='.$month);
		$object->getActiveSheet()->setCellValue('G2', 'Year='.$year);

		$object->setActiveSheetIndex(0)->mergeCells('A3:A4');
		$object->setActiveSheetIndex(0)->mergeCells('B3:B4');
		$object->setActiveSheetIndex(0)->mergeCells('C3:C4');
		$object->setActiveSheetIndex(0)->mergeCells('D3:D4');
		$object->setActiveSheetIndex(0)->mergeCells('E3:E4');
		$object->setActiveSheetIndex(0)->mergeCells('F3:F4');
		$object->setActiveSheetIndex(0)->mergeCells('G3:G4');
		$object->setActiveSheetIndex(0)->mergeCells('Q3:Q4');
		$object->setActiveSheetIndex(0)->mergeCells('R3:R4');
		$object->setActiveSheetIndex(0)->mergeCells('S3:S4');

        $object->setActiveSheetIndex(0)->mergeCells('H3:J3');
        $object->setActiveSheetIndex(0)->mergeCells('K3:M3');
        $object->setActiveSheetIndex(0)->mergeCells('N3:P3');

		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Date Of Join","Department","Designation","CL","EL","SL");
		$column = 0;

		$object->getActiveSheet()->setCellValue('A3', 'Sl.NO');
		$object->getActiveSheet()->setCellValue('B3', 'E ID');
		$object->getActiveSheet()->setCellValue('C3', 'E BID');
		$object->getActiveSheet()->setCellValue('D3', 'E.Name');
		$object->getActiveSheet()->setCellValue('E3', 'Date Of Join');
		$object->getActiveSheet()->setCellValue('F3', 'Department');
		$object->getActiveSheet()->setCellValue('G3', 'Designation');

		$object->getActiveSheet()->setCellValue('H3', 'CL');
		$object->getActiveSheet()->setCellValue('K3', 'EL');
		$object->getActiveSheet()->setCellValue('N3', 'SL');

		$object->getActiveSheet()->setCellValue('H4', 'OPB');
		$object->getActiveSheet()->setCellValue('I4', 'USED');
		$object->getActiveSheet()->setCellValue('J4', 'Balance');

		$object->getActiveSheet()->setCellValue('K4', 'OPB');
		$object->getActiveSheet()->setCellValue('L4', 'USED');
		$object->getActiveSheet()->setCellValue('M4', 'Balance');

		$object->getActiveSheet()->setCellValue('N4', 'OPB');
		$object->getActiveSheet()->setCellValue('O4', 'USED');
		$object->getActiveSheet()->setCellValue('P4', 'Balance');
		$object->getActiveSheet()->setCellValue('Q3', 'BIO LOP');
		$object->getActiveSheet()->setCellValue('R3', 'Total Lop Days');
		$object->getActiveSheet()->setCellValue('S3', 'No of days');
		$excel_row = 5;
		$i=1;
		foreach($no_of_leaves as $employee)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['date_of_join'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $employee['cl_open_balance']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $employee['cl_availed']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $employee['cl_balance']);

			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $employee['el_open_balance']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $employee['el_availed']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $employee['el_balance']);

			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $employee['sl_open_balance']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $employee['sl_availed']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $employee['sl_balance']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $employee['lop_count']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $employee['lop_availed']+$employee['lop_count']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $employee['no_of_days']);
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="EmployeeMonthlyLeaveStatus-'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
  public function EmployeeOnDuty()
 {
 		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$a_reports=array();
		if ($year!=null && $month!=null ) {
			if ($activity_type!=null) {
				
				if ($activity_type=="All") {
					$employees=$this->Employee_leave_model->onDutyEmployeesWhere("MONTHNAME(am.leave_date)='".$month."' and YEAR(am.leave_date)='".$year."'");
				}
				else{
					$employees=$this->Employee_leave_model->onDutyEmployeesWhere("loa.dept_name='".$activity_type."' and MONTHNAME(am.leave_date)='".$month."' and YEAR(am.leave_date)='".$year."'");
				}
			}
			else if ($bid!=null) {
				$employees=$this->Employee_leave_model->onDutyEmployeesWhere("e.emp_bid=".$bid." and MONTHNAME(am.leave_date)='".$month."' and YEAR(am.leave_date)='".$year."'");
			}
			else if ($ename!=null) {
				$employees=$this->Employee_leave_model->onDutyEmployeesWhere("e.emp_name='".$ename."' and MONTHNAME(am.leave_date)='".$month."' and YEAR(am.leave_date)='".$year."'");
			}
		}
		if (count($employees)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:H1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$object->setActiveSheetIndex(0)->mergeCells('D2:F2');
		$object->setActiveSheetIndex(0)->mergeCells('G2:H2');
		$object->getActiveSheet()->setCellValue('A1', 'On Duty Employees List');
		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria:'.$bid);
		}
		elseif ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria:'.$ename);
		}
		elseif($activity_type!=null){
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria: '.$activity_type);
		}
		else{
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria: ALL');
		}
		$object->getActiveSheet()->setCellValue('D2', 'Month='.$month);
		$object->getActiveSheet()->setCellValue('G2', 'Year='.$year);

		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Designation","Department","Leave Date","Reason");
		$column = 0;

		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
		$column++;
		}
		$excel_row = 4;
		$i=1;
		foreach($employees as $employee)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['leave_date']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, "OD");
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="OnDutyEmployeesList'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
 public function BankStatement()
 {
 		$activity_type=$this->input->get('type');
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$employees=array();
		if ($year!=null && $month!=null ) {
			$month_no=date('m',strtotime($month));
			$employees=$this->Salary_model->bankstatement("ems.salary_month=".$month_no." and ems.salary_year=".$year);
		}
		if (count($employees)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);

		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Account Number","Net Income");
		$column = 0;

		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		$column++;
		}
		$excel_row = 2;
		$i=1;
		foreach($employees as $employee)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, (string)$employee['acc_no']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, round($report['net_income']-$report['management_deductions']));
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="BankStatement-'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
 public function AttendanceReports()
 {
 		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$year=$this->input->get('year');
		$month=$this->input->get('month');
		$type=$this->input->get("type");
		$a_reports=array();
		if ($year!=null && $month!=null) {
			if ($bid!=null) {
				$a_reports=$this->attendance_reports_model->getAttendanceReportsByWhere("e.emp_bid=".$bid." AND YEAR(am.leave_date)=".$year." and MONTHNAME(am.leave_date)='".$month."'");
			}
			else if ($ename!=null) {
				$a_reports=$this->attendance_reports_model->getAttendanceReportsByWhere("e.emp_name=".$ename." AND YEAR(am.leave_date)=".$year." and MONTHNAME(am.leave_date)='".$month."'");
			}
			else if ($type!=null && $type=="All") {
				$a_reports=$this->attendance_reports_model->getAllReports($month,$year);
			}
			
		}
		if (count($a_reports)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:H1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$object->setActiveSheetIndex(0)->mergeCells('D2:F2');
		$object->setActiveSheetIndex(0)->mergeCells('G2:H2');
		$object->getActiveSheet()->setCellValue('A1', 'Attendance Reports');
		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Bid='.$bid);
		}
		if ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Emp Name='.$ename);
		}
		$object->getActiveSheet()->setCellValue('D2', 'Month='.$month);
		$object->getActiveSheet()->setCellValue('G2', 'Year='.$year);

		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Designation","Department","Leave Type","Leave Date");
		$column = 0;

		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
		$column++;
		}
		$excel_row = 4;
		$i=1;
		foreach($a_reports as $employee)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['leave_type']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $employee['leave_date']);
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="LeavesReport'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
 public function AttendanceSummary()
 {
 	$summary_date_from = $this->input->get('summary_date_from');
	$summary_date_to = $this->input->get('summary_date_to');		
	$a_reports=$this->Global_model->get_leave_emps_by_date_old($summary_date_from,$summary_date_to);
	$get_leave_types  = $this->Global_model->get_leave_types();
	$get_current_date_leaves_string="";
	$total_leaves=0;
	$a_reports=array();

		if (count($a_reports)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:H1');
		$object->getActiveSheet()->setCellValue('A1', 'Attendance Reports');
		$object->getActiveSheet()->setCellValue('A2', '');
		$object->getActiveSheet()->setCellValue('B2', '');
		$object->getActiveSheet()->setCellValue('C2', '');
		$object->getActiveSheet()->setCellValue('D2', '');
		$object->getActiveSheet()->setCellValue('E2', '');
		$object->getActiveSheet()->setCellValue('F2', '');
		$object->getActiveSheet()->setCellValue('G2', '');
		$object->getActiveSheet()->setCellValue('H2', '');

		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Teaching Subject","Department","Leave Type","Leave Date");
		$column = 0;
		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
		$column++;
		}
		$excel_row = 4;
		$i=1;
		foreach($a_reports as $employee)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['designation_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['leave_type']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $employee['leave_date']);
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="LeavesReport'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');
			
		}else{
			echo "NO-Data";
		}
 }
 public function paymentModes()
 {
 	$activity_type=$this->input->get('type');
	$bid=$this->input->get('bid');
	$ename=$this->input->get('ename');
	$payment_mode=$this->input->get('payment_mode');
	$start_date=$this->input->get('from');
	$end_date=$this->input->get('to');
	$employees=array();
	if ($bid!=null) {
		if ($bid=="All") {
			$employees=$this->PaymentMode_model->allSalariesDetails();
		}
		else{
			$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("e.emp_bid=".$bid);
		}
		
	}
	else if ($ename!=null) {
		if ($ename=="All") {
			$employees=$this->PaymentMode_model->allSalariesDetails();
		}
		else{
			$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("e.emp_name='".ltrim($ename)."'");
		}
		
	}
	else if ($payment_mode!=null && $activity_type!=null) {
		$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("pt.payment_type='".$payment_mode."' and la.dept_name='".$activity_type."'");
	}
	else if ($activity_type!=null) {
		if ($activity_type=="All") {
			$employees=$this->PaymentMode_model->allSalariesDetails();
		}
		else{
			$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("la.dept_name='".$activity_type."'");
		}
		
	}
	else if ($payment_mode!=null) {
		$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("pt.payment_type='".$payment_mode."'");
	}
	if(count($employees)){
 		$this->load->model("excel_export_model");
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->setActiveSheetIndex(0)->mergeCells('A1:J1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:D2');
		$object->setActiveSheetIndex(0)->mergeCells('E2:G2');
		
		$object->getActiveSheet()->setCellValue('A1', 'Payment Modes');
		

		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria:'.$bid);
		}
		elseif ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria:'.$ename);
		}
		elseif($activity_type!=null){
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria: '.$activity_type);
		}
		else{
			$object->getActiveSheet()->setCellValue('A2', 'Selection Criteria: ALL');
		}
		if ($payment_mode!=null) {
			$object->getActiveSheet()->setCellValue('E2', 'Payment Mode:'.$payment_mode);
		}
  
		$table_columns = array("Sl.NO","E ID","E BID","E.Name", "D.O.J","Designation","Department","Bank A/c Number","Payment Mode","Status");
		$column = 0;

		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);

		$column++;
		}
		$excel_row = 4;
		$i=1;
		foreach($employees as $employee)
		{
			if (isset($employee['flag'])) {
				$status="Inactive";
			}
			else{
				$status="Active";
			}
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['date_of_join'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['designation']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $employee['acc_no']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $employee['payment_type']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $status);
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="PaymentMOdeEmployees.xls"');
		ob_clean();
		$object_writer->save('php://output');
	}
	else{
		echo "NO-Data";
	}
 }
 public function HolidaysList()
 {
 	$this->load->model("Holidays_model");
 	$month=$this->input->get('month');
		$year=$this->input->get('year');
		$holidays=array();

		if ($year!=null && $month==null) {
			$holidays=$this->Holidays_model->get_holidays_list($this->config->item('holidays'),array('YEAR(from_date)'=>$year));
		}
		else if ($year!=null && $month!=null) {
			$holidays=$this->Holidays_model->get_holidays_list($this->config->item('holidays'),array('YEAR(from_date)'=>$year,'MONTHNAME(from_date)'=>$month));
		}
		if (count($holidays)) 
		{
			$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);  
		$table_columns = array("Sl.NO","Holiday Reason","From Date","To Date","No of Days","Class");
		$column = 0;
		$object->setActiveSheetIndex(0)->mergeCells('A1:F1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$object->setActiveSheetIndex(0)->mergeCells('D2:F2');
		
		
		$object->getActiveSheet()->setCellValue('A1', 'Holidays Calendar');
		$object->getActiveSheet()->setCellValue('A2', 'Year :'.$year);
		$object->getActiveSheet()->setCellValue('D2', 'Month :'.$month);
		
		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
		$column++;
		}
		$excel_row = 4;
		$i=1;
		foreach($holidays as $employee)
		{
			$to = new DateTime($employee->to_date);
		   $from = new DateTime($employee->from_date);
		   $days = $from->diff($to)->format("%a")+1;
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee->holiday_name);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, date("d-m-Y", strtotime($employee->from_date)) );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, date("d-m-Y", strtotime($employee->to_date)) );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $days);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee->class);
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="HolidaysList.xls"');
		ob_clean();
		$object_writer->save('php://output');	
		}
		else{
			echo "NO-Data";
		}
 }
 public function MonthlyReports()
 {

	$year=$this->input->get('year');
	$month=$this->input->get('month');
 	if ($year!=null && $month!=null) {
		$a_reports=$this->monthwise_attendance_model->getAttendanceReportsByWhere("YEAR(ba.log_date)=".$year." and MONTHNAME(ba.log_date)='".$month."'");
	}
	else if ($month!=null & $year==null) {
		$year=date('Y');
		$a_reports=$this->monthwise_attendance_model->getAttendanceReportsByWhere("MONTHNAME(ba.log_date)='".$month."' and YEAR(ba.log_date)='".date('Y')."'");
	}
	if (count($a_reports)>0) {
		
	$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0); 
		$object->setActiveSheetIndex(0)->mergeCells('A1:G1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:D2');
		$object->setActiveSheetIndex(0)->mergeCells('E2:G2');
		$object->getActiveSheet()->setCellValue('A1', 'Monthly Attendance Summary');
		$object->getActiveSheet()->setCellValue('A2', 'Month='.$month);
		$object->getActiveSheet()->setCellValue('E2', 'Year='.$year);

		$table_columns = array("Sl.NO","Date","Total Employees","Bio Metric(Count)","Leaves Applied","Difference","Emp Bio ids");
		$column = 0;
		
		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
		$column++;
		}
		$excel_row = 4;
		$i=1;
		foreach($a_reports as $employee)
		{
			$diff=array_diff(explode(",", $employee['total_employee_bids']), explode(",", $employee['emp_bids_diff']),explode(",", $employee['leave_bids']));
            $diff_string=implode(",", $diff);
            $nameOfDay = date('l', strtotime($employee['log_date']));
            if ($nameOfDay=="Sunday" || $employee['holiday_name']!=null) {
            	if ($employee['holiday_name']!=null) {
            		$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['holiday_name'] );
            		$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['holiday_name']);
					$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['holiday_name']);
					$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['holiday_name']);
            	}
            	else{
            		$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, "Sunday" );
            		$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, "Sunday" );
            		$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, "Sunday" );
            		$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, "Sunday"  );
            	}
			}
			else{
				$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['bio_count']);
				$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['leaves_applied']);
				$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['difference']);
				$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $diff_string );
			}
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['log_date']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['total_employees'] );

			
			
			$excel_row++;
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');

		header('Content-Disposition: attachment;filename="MonthlyAttendanceReports'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');	
		}
		else{
			echo "NO-Data";
		}
 }
 public function BioMetricReports()
 {
 		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$year=$this->input->get('year');
		$month=$this->input->get('month');
		$a_reports=array();
		if ($year!=null && $month!=null && $bid!=null) {
			$a_reports=$this->attendance_reports_model->getBioMetricReportsByWhere("e.emp_bid=".$bid." and YEAR(ba.log_date)=".$year." AND MONTHNAME(ba.log_date)='".$month."'");
		}
		else if ($year!=null && $month!=null && $ename!=null) {
			$a_reports=$this->attendance_reports_model->getBioMetricReportsByWhere("e.emp_name='".$ename."' and YEAR(ba.log_date)=".$year." AND MONTHNAME(ba.log_date)='".$month."'");
		}
		if (count($a_reports)) 
		{
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);  
		$object->setActiveSheetIndex(0)->mergeCells('A1:N1');
		$object->setActiveSheetIndex(0)->mergeCells('A2:E2');
		$object->setActiveSheetIndex(0)->mergeCells('F2:J2');
		$object->setActiveSheetIndex(0)->mergeCells('K2:N2');
		$object->getActiveSheet()->setCellValue('A1', 'BioMetric Attendance Reports');
		if ($bid!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Bid='.$bid);
		}
		if ($ename!=null) {
			$object->getActiveSheet()->setCellValue('A2', 'Emp Name='.$ename);
		}
		$object->getActiveSheet()->setCellValue('F2', 'Month='.$month);
		$object->getActiveSheet()->setCellValue('K2', 'Year='.$year);
		$table_columns = array("Sl.NO","E ID","E BID","E.Name","Designation","Department","Login Date","Schedule In","Bio In","Schedule Out","Bio Out","No Hours","Buffer Time","LOP(in days)");
		$column = 0;
		$object->setActiveSheetIndex(0)->mergeCells('A1:N1');
		$object->getActiveSheet()->setCellValue('A1', 'BioMetric Attendance Reports');
		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
		$column++;
		}
		$excel_row = 4;
		$i=1;
		foreach($a_reports as $employee)
		{
			
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $employee['temp_emp_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $employee['emp_bid'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $employee['emp_name'] );
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $employee['designation']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $employee['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $employee['log_date']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $employee['from_time']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $employee['in_time']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $employee['to_time']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $employee['out_time']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $employee['diffrence']);
			if (round($employee['buffer_time'])>0) {
            	$diff+=round($employee['buffer_time']); 
            	$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $diff);
				}
			else{
				$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, 0);
			}
			if ($diff>25) {
            $lop+=0.5;
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, "0.5 day");
             }
            else{
            	$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, "0 day");
            }
			$excel_row++;
		}
		$object->getActiveSheet()->setCellValueByColumnAndRow(12,$excel_row,"Total");
		$object->getActiveSheet()->setCellValueByColumnAndRow(13,$excel_row,$lop." days");
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$a_reports[0]['emp_name'].'-BioMetricAttendance-'.$month.'-'.$year.'.xls"');
		ob_clean();
		$object_writer->save('php://output');	
		}
		else{
			echo "NO-Data";
		}
 }
 function action1()
 {
  $this->load->model("excel_export_model");
  $this->load->library("excel");
  $object = new PHPExcel();

  $object->setActiveSheetIndex(0);

  $table_columns = array("BID","E.Name", "D.O.J", "D.O.B", "Address", "Higher Education","Experience","Mobile No","Designation","Department","Line of Activity","Last Working Day","Transportation","Status");

  $column = 0;

  foreach($table_columns as $field)
  {
   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
   $column++;
  }

  $employee_data = $this->excel_export_model->allEmployees();

  $excel_row = 2;
  $i=1;
		  foreach($employee_data as $row)
		  {
		  	if (isset($row['flag'])) {
				$status="Inactive";
			}
			else{
				$status="Active";
			}
   			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row["emp_bid"]);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['emp_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['date_of_join']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['date_of_birth']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['address']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['education']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['experience']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['mobile']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['designation']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['activity_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, "last working date");
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, "transportation");
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $status);
			$excel_row++;
  		}

  $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Employee Data.xls"');
  ob_clean();
  $object_writer->save('php://output');
 }

 
 
}
?>