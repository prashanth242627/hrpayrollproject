<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Empextrahoursmaster extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_empextrahoursmaster = $this->Global_model->get_values_where($this->config->item('empextrahoursmaster'),array('status'=>1));
		$get_empextrahoursmaster_log = $this->Global_model->get_values_where($this->config->item('empextrahoursmaster_log'), array('flag'=>2));
		$get_empextrahoursmasters = $this->Global_model->get_values_where($this->config->item('line_of_activity'),array('status'=>1));
		$data=array('current_page'=>'Extra  Hours Payment Master','page_title'=>'Extra  Hours Payment Master','parent_menu'=>'','template'=>'masters/empextrahoursmaster/add','get_empextrahoursmaster'=>$get_empextrahoursmaster,'get_empextrahoursmaster_log'=>$get_empextrahoursmaster_log,'get_empextrahoursmasters'=>$get_empextrahoursmasters);
		$this->form_validation->set_rules('per_hour_payment','per_hour_payment','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$activity_id = $this->input->post('activity_id');
			$per_hour_payment = $this->input->post('per_hour_payment');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('activity_id'=>$activity_id,'per_hour_payment'=>$per_hour_payment, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Extra  Hours Payment Master not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Extra  Hours Payment Master inserted successfully'));
			}
			redirect('Empextrahoursmaster');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('empextrahoursmaster');
		$where = array('empextrahours_id'=>base64_decode($id));
		$empextrahours_id = base64_decode($id);
		$gethoureMasterEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_empextrahoursmaster = $this->Global_model->get_values_where($this->config->item('empextrahoursmaster'),array('status'=>1));
		$get_empextrahoursmaster_log = $this->Global_model->get_values_where($this->config->item('empextrahoursmaster_log'), array('flag'=>2));
		$get_empextrahoursmasters = $this->Global_model->get_values_where($this->config->item('line_of_activity'),array('status'=>1));
			$data=array('current_page'=>'Extra  Hours Payment Master','page_title'=>'Extra  Hours Payment Master','parent_menu'=>'','template'=>'masters/empextrahoursmaster/add','get_empextrahoursmaster'=>$get_empextrahoursmaster,'gethoureMasterEdit'=>$gethoureMasterEdit,'get_empextrahoursmaster_log'=>$get_empextrahoursmaster_log,'get_empextrahoursmasters'=>$get_empextrahoursmasters);
		$this->form_validation->set_rules('per_hour_payment','per_hour_payment','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$activity_id = $this->input->post('activity_id');
			$per_hour_payment = $this->input->post('per_hour_payment');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('activity_id'=>$activity_id,'per_hour_payment'=>$per_hour_payment,'status'=>$status,'empextrahours_id'=>$empextrahours_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Extra  Hours Payment Master not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Extra  Hours Payment Master updated successfully'));
			}
			redirect('Empextrahoursmaster');
		}
		
	}
	function delete($id)
	{
			$empextrahours_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('empextrahours_id'=>$empextrahours_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Extra  Hours Payment Master not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Extra  Hours Payment Master  Deleted successfully'));
		}
		redirect('Empextrahoursmaster');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$empextrahours_id = base64_decode($id);
				$undo_array = array('empextrahours_id'=>$empextrahours_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Extra  Hours Payment Master not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Extra  Hours Payment Master undo successfully'));
				}
		
		redirect('Empextrahoursmaster');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$empextrahours_id = $insert_array['empextrahours_id'];
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$activity_id = $insert_array['activity_id'];
			$param['insertdata'] = array('empextrahours_id'=>$this->Global_model->Max_count($this->config->item('empextrahoursmaster'),'empextrahours_id'),'activity_id'=>$activity_id,'per_hour_payment'=>$insert_array['per_hour_payment'], 'status'=>1,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('empextrahoursmaster');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			
			$empextrahours_id = $insert_array['empextrahours_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('empextrahoursmaster'), array('empextrahours_id'=>$empextrahours_id));
				
				$param['insertdata'] = array('empextrahours_id'=>$insertdata->empextrahours_id,'activity_id'=>$insertdata->activity_id,'per_hour_payment'=>$insertdata->per_hour_payment,  'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('empextrahoursmaster_log');
			/* Update log table end */
			$data1 = array('empextrahours_id'=>$empextrahours_id,'activity_id'=>$insert_array['activity_id'],'per_hour_payment'=>$insert_array['per_hour_payment'], 'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('empextrahours_id'=>$empextrahours_id);
			$this->Global_model->update($this->config->item('empextrahoursmaster'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$empextrahours_id = $insert_array['empextrahours_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('empextrahoursmaster'), array('empextrahours_id'=>$empextrahours_id));
				
				$param['insertdata'] = array('empextrahours_id'=>$insertdata->empextrahours_id,'activity_id'=>$insertdata->activity_id,'per_hour_payment'=>$insertdata->per_hour_payment,  'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('empextrahoursmaster_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('empextrahoursmaster')." WHERE empextrahours_id =".$empextrahours_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$empextrahours_id = $insert_array['empextrahours_id'];
			$where = array('empextrahours_id'=>$empextrahours_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('empextrahoursmaster_log')." WHERE empextrahours_id=".$empextrahours_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('empextrahours_id'=>$empextrahours_id,'per_hour_payment'=>$getPaymentTypes->per_hour_payment,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('empextrahoursmaster'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Extra  Hours Payment Master not Undo '));	
					redirect('Empextrahoursmaster');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Empextrahoursmaster');
		}	
	}
	function getEmployeeName()
	{
		$activity_id = $this->input->post('activity_id');
		$get_empextrahoursmasters = $this->Global_model->get_values_where_single($this->config->item('empextrahoursmaster'),array('activity_id'=>$activity_id));
		echo $get_empextrahoursmasters->emp_name;
	}
}
