<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Modeofsalrypayment extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_payment_types = $this->Global_model->get_values_where($this->config->item('payment_types'),array('status'=>1));
		$get_payment_types_log = $this->Global_model->get_values_where($this->config->item('payment_types_log'), array('flag'=>2));
		$data=array('current_page'=>'Payment Types','page_title'=>'Payment Types','parent_menu'=>'','template'=>'masters/modeofsalrypayment/add','get_payment_types'=>$get_payment_types,'get_payment_types_log'=>$get_payment_types_log);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('payment_type','payment_type','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$payment_type = $this->input->post('payment_type');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('payment_type'=>$payment_type, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Payment Type not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Payment Types inserted successfully'));
			}
			redirect('Modeofsalrypayment');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('payment_types');
		$where = array('payment_type_id'=>base64_decode($id));
		$payment_type_id = base64_decode($id);
		$getPpaymentTypeEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_payment_types = $this->Global_model->get_values_where($this->config->item('payment_types'),array('status'=>1));
		$get_payment_types_log = $this->Global_model->get_values_where($this->config->item('payment_types_log'), array('flag'=>2));
			$data=array('current_page'=>'Payment Types','page_title'=>'Payment Types','parent_menu'=>'','template'=>'masters/modeofsalrypayment/add','get_payment_types'=>$get_payment_types,'getPpaymentTypeEdit'=>$getPpaymentTypeEdit,'get_payment_types_log'=>$get_payment_types_log);
		$this->form_validation->set_rules('payment_type','payment_type','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$payment_type = $this->input->post('payment_type');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('payment_type'=>$payment_type, 'status'=>$status,'payment_type_id'=>$payment_type_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Payment Type not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Payment Types updated successfully'));
			}
			redirect('Modeofsalrypayment');
		}
		
	}
	function delete($id)
	{
			$payment_type_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('payment_type_id'=>$payment_type_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Payment Type not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Payment Type  Deleted successfully'));
		}
		redirect('Modeofsalrypayment');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$payment_type_id = base64_decode($id);
				$undo_array = array('payment_type_id'=>$payment_type_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Payment Type not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Payment Type undo successfully'));
				}
		
		redirect('Modeofsalrypayment');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$payment_type = $insert_array['payment_type'];
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$param['insertdata'] = array('payment_type_id'=>$this->Global_model->Max_count($this->config->item('payment_types'),'payment_type_id'),'payment_type'=>$payment_type,'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('payment_types');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			
			$payment_type_id = $insert_array['payment_type_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('payment_types'), array('payment_type_id'=>$payment_type_id));
				
				$param['insertdata'] = array('payment_type_id'=>$insertdata->payment_type_id,'payment_type'=>$insertdata->payment_type,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('payment_types_log');
			/* Update log table end */
			$data1 = array('payment_type_id'=>$payment_type_id,'payment_type'=>$payment_type,'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('payment_type_id'=>$payment_type_id);
			$this->Global_model->update($this->config->item('payment_types'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$payment_type_id = $insert_array['payment_type_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('payment_types'), array('payment_type_id'=>$payment_type_id));
				
				$param['insertdata'] = array('payment_type_id'=>$insertdata->payment_type_id,'payment_type'=>$insertdata->payment_type,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('payment_types_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('payment_types')." WHERE payment_type_id =".$payment_type_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$payment_type_id = $insert_array['payment_type_id'];
			$where = array('payment_type_id'=>$payment_type_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('payment_types_log')." WHERE payment_type_id=".$payment_type_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('payment_type_id'=>$payment_type_id,'payment_type'=>$getPaymentTypes->payment_type,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('payment_types'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Payment Type not Undo '));	
					redirect('Modeofsalrypayment');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Modeofsalrypayment');
		}	
	}
}
