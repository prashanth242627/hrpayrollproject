<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';

class DepartmentSalaries extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_leave_model');
		$this->load->model('Dept_Salaries_model');
		$this->load->model("Myfunctions");
	}
	public function index()
	{
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		
		$data=array('current_page'=>'Department Salaries','page_title'=>'Department Salaries','parent_menu'=>'','template'=>'department_salaries');
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$this->load->view('theme/template',$data);
	}
	public function All()
	{
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$salaries=array();
		if ($month!=null && $year!=null) {
			$salaries=$this->Dept_Salaries_model->AlldeptSalaries($month,$year);
		}
		

		$data=array('current_page'=>'Department Salaries','page_title'=>'Department Salaries','parent_menu'=>'','template'=>'department_salaries');

		$activities=$this->Employee_leave_model->activities();
		$data['activities']=$activities;
		$data['salaries'] = $salaries;

		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$data['salaries']=array();
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		if ($month!=null && $year!=null) {
			 if ($activity_type!=null) {
				$salaries=$this->Dept_Salaries_model->deptSalariesByWhere("la.dept_name='".$activity_type."'",$month,$year);
			}
			else{
				$salaries=array();
			}
		}
		$data=array('current_page'=>'Department Salaries','page_title'=>'Department Salaries','parent_menu'=>'','template'=>'department_salaries');
		if (count($salaries)) 
		{
			$data['salaries'] = $salaries;
		}
		$data['activities']=$activities;
		$this->load->view('theme/template',$data);
	}

}
