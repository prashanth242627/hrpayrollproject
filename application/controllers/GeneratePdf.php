<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
require_once APPPATH.'libraries/tcpdf/examples/tcpdf_include.php';
class GeneratePdf extends Global_System {
public function __construct(){
	parent::__construct(); 
	$this->logincheck();
	$this->load->library('session');
	$this->load->helper('url');
  $this->load->model('Myfunctions');
}
function index()
{
	$this->load->model("excel_export_model");
  	$data["employee_data"] = $this->excel_export_model->fetch_data();
  	$this->load->view("excel_export_view", $data);
}
function LeaveFormPdf(){
  $emp_bid=$this->input->get('emp_bid');
  $emp_name=$this->input->get('emp_name');
   $leave_type_id=$this->input->get('leave_type_id');
  $leave_date_from=$this->input->get("leave_date_from");
  $leave_date_to=$this->input->get("leave_date_to");
  $no_leaves=$this->input->get("no_leaves");
  if ($emp_bid!="" && $emp_name!="") {
      $get_sl_data=$this->Myfunctions->getQueryData("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='SL' and lm.`emp_bid` = '".$emp_bid."'");
    $get_cl_data=$this->Myfunctions->getQueryData("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='CL' and lm.`emp_bid` = '".$emp_bid."'");
     $get_el_data=$this->Myfunctions->getQueryData("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='EL' and lm.`emp_bid` = '".$emp_bid."'");
     $get_leave_type=$this->Myfunctions->getQueryData("SELECT * FROM leave_types WHERE leave_type_id=$leave_type_id");
     $get_emp_data=$this->Myfunctions->getQueryData("Select e.emp_bid,e.emp_name,e.temp_emp_id,d.designation_name,loa.dept_name,e.mobile from employee e left JOIN line_of_activity loa on loa.activity_id=e.activity_id left JOIN designation d on d.designation_id=e.designation WHERE e.emp_bid=$emp_bid");
     $employee=$get_emp_data[0];
     if (!empty($get_cl_data)) {
      $data['cl']['opb']=$get_cl_data[0]['no_leaves'];
      $data['cl']['used']=$get_cl_data[0]['used'];
      $data['cl']['clb']=$get_cl_data[0]['no_leaves']-$get_cl_data[0]['used'];
      
    }
    else{
      $data['cl']['opb']=0;
      $data['cl']['used']=0;
      $data['cl']['clb']=0;
    }
    if (!empty($get_sl_data)) {
      $data['sl']['opb']=$get_sl_data[0]['no_leaves'];
      $data['sl']['used']=$get_sl_data[0]['used'];
      $data['sl']['clb']=$get_sl_data[0]['no_leaves']-$get_sl_data[0]['used'];
      
    }
    else{
      $data['sl']['opb']=0;
      $data['sl']['used']=0;
      $data['sl']['clb']=0;
    }
    if (!empty($get_el_data)) {
      $data['el']['opb']=$get_el_data[0]['no_leaves'];
      $data['el']['used']=$get_el_data[0]['used'];
      $data['el']['clb']=$get_el_data[0]['no_leaves']-$get_el_data[0]['used'];
      
    }
    else{
      $data['el']['opb']=0;
      $data['el']['used']=0;
      $data['el']['clb']=0;
    }
    $this->load->library('LeaveFormPdf');
  $pdf = new LeaveFormPdf('P', 'mm', 'A4', true, 'UTF-8', false);
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Nicola Asuni');
  $pdf->SetTitle('PaySlip');
  $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('courier', '', 10);
$pdf->SetMargins(10, 30, 10);
// add a page
$pdf->AddPage();
if (count($employee)>0) {
  $tbl='<table id="gradeX" style="border-collapse: collapse;width: 100%;">
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Emp Id: '.$employee['temp_emp_id'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Name: '.$employee['emp_name'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Emp Bid: '.$employee['emp_bid'].'</th>
      </tr>
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Departmanet: '.$employee['dept_name'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Designation: '.$employee['designation_name'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Phone NO: '.$employee['Phone'].'</th>
      </tr>
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">CLs OPB: '.($data['cl']['opb']-$data['cl']['used']).'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">ELs OPB: '.($data['el']['opb']-$data['el']['used']).'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">SLs OPB: '.($data['sl']['opb']-$data['sl']['used']).'</th>
      </tr>
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Leave Type: '.$get_leave_type[0]['leave_type'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">From Date: '.$leave_date_from.'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">To Date : '.$leave_date_to.'</th>
      </tr>
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">NO of Leaves(days): '.$no_leaves.'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;"></th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;"></th>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; color: #656565;" height="20" colspan="3">Resason For Leave :</th>
        </tr>
        <tr style="background-color: #f2f2f2;">
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; color: #656565;" height="20" colspan="3">Leave Address :</th>
        </tr>
        <tr style="background-color: #f2f2f2;">
        <th  style="padding-top: 12px; padding-bottom: 12px; text-align: left;color: #656565; ">Date :</th>
        <th  style="padding-top: 12px; padding-bottom: 12px; text-align: left; color: #656565; " colspan="2">
Signature : </th>

        </tr>
      </table><br><br>';

$tbl .= '<table id="gradeX" style="  border-collapse: collapse;width: 100%;">
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Approve</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Signature</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Role</th>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px; height="20" font-weight: 600;color: #656565;">Sanctioned/Rejected</td>
        <td style="border: 1px solid #ddd; padding: 8px; height="20" font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px; height="20" font-weight: 600;color: #656565;">HD</td>
      </tr>
      <tr>
        <td style="border: 1px solid #ddd; padding: 8px; height="20" font-weight: 600;color: #656565;">Sanctioned/Rejected</td>
        <td style="border: 1px solid #ddd; padding: 8px; height="20"  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px; height="20" font-weight: 600;color: #656565;">HM</td>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px; height="20" font-weight: 600;color: #656565;">Sanctioned/Rejected</td>
        <td style="border: 1px solid #ddd; padding: 8px; height="20" font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px; height="20" font-weight: 600;color: #656565;">Academic Dtrector/ Principal / Chairman </td>
      </tr>
      <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Medical Certicate</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Yes /No </td>
      </tr>
  </table>';

// output the HTML content
$pdf->writeHTML($tbl, true, false, false, false, '');
}

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('EmployeeLeaveForm.pdf', 'I');
  }
  else{
    echo "Please Select Emp ID or Emp Name";
  }

}
function PaySlip(){
  $this->load->model("payslip_model");
  $this->load->library('PayslipPdf');
	$bid=$this->input->get('bid');
	$ename=$this->input->get('ename');
	$month=$this->input->get('month');
	$year=$this->input->get('year');
	$pdf = new PayslipPdf('P', 'mm', 'A4', true, 'UTF-8', false);
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('PaySlip');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	if ($month!=null && $year!=null) {
		if ($bid!=null) {
			$payslip_data=$this->payslip_model->EmpPaySlipByWhere("e.emp_bid=".$bid,$month,$year);
      $tds_data=$this->payslip_model->PaySlipTDSDetails("p.emp_bid=".$bid,$month,$year);
      $taxs_paid=$this->payslip_model->taxPaid("emp_bid=".$bid,$month,$year);
      $amount_in_string=$payslip_data[0]['net_income']-$payslip_data[0]['management_deductions'];
			$in_words=$this->payslip_model->getIndianCurrency($amount_in_string);
		}
		else if ($ename!=null) {
			$payslip_data=$this->payslip_model->EmpPaySlipByWhere("e.emp_name='".$ename."'",$month,$year);
      $tds_data=$this->payslip_model->PaySlipTDSDetails("p.emp_name='".$ename."'",$month,$year);
      $taxs_paid=$this->payslip_model->taxPaid("emp_name=".$ename,$month,$year);
      $amount_in_string=$payslip_data[0]['net_income']-$payslip_data[0]['management_deductions'];
			$in_words=$this->payslip_model->getIndianCurrency($amount_in_string);
		}
	}
// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('courier', '', 10);
$pdf->SetMargins(10, 30, 10);
// add a page
$pdf->AddPage();
if (count($payslip_data)>0) {
	$employee=$payslip_data[0];
  $tds_data=$tds_data[0];
  $total_tax_sru=round($tds_data['surcharges']+$tds_data['tax_amt']);
	$tbl='<table id="gradeX" style="  border-collapse: collapse;width: 100%;">
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Emp Id: '.$employee['temp_emp_id'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Name: '.$employee['emp_name'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">A/C NO: '.$employee['acc_no'].'</th>
      </tr>
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Department: '.$employee['dept_name'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Designation: '.$employee['designation_name'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Date Of Join: '.$employee['date_of_join'].'</th>
      </tr>
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">PF NO: '.$employee['pf_no'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">TWD: '.$employee['TWD'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">PAN: '.$employee['pan'].'</th>
      </tr>
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">ESI NO: '.$employee['esi_no'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">LOP Days: '.$employee['lop_count'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Aadhar: '.$employee['Aadhar'].'</th>
      </tr>
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Pay Days: '.round($employee['PAY_DAYS']).'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">UAN NO: '.$employee['uan'].'</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Month/Year: '.$month.'/'.$year.'</th>
      </tr>
      </table><br><br>';


$tbl .= '<table id="gradeX" style="  border-collapse: collapse;width: 100%;">
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Gross(Rs)</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Amount(Rs)</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Gross Earnings(Rs)</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Deductions(Rs)</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Amount(Rs)</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Net Pay(Rs)</th>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Basic</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['basic_ctc']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['basic_earnings']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">PF</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['PF']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
      </tr>
      <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">DA</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['da_ctc']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['da_earnings']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">PT</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['PT']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">INCR</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['incr_ctc']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['incr_earnings']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">ESI</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['ESI']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
      </tr>
       <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">HRA</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['hra_ctc']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['hra_earnings']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">TDS</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['TDS']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
      </tr>
       <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">CCA</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['cca_ctc']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['cca_earnings']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">SD</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['SD']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
      </tr>
       <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Other allowances</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['other_allowance_ctc']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['other_allowances_earnings']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Other Deductions</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['management_deductions']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">HD,HOD,Coord allowances</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['co_allowance_ctc']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['co_allowance_earnings']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
      </tr>
        <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Special allowances</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['special_allowance_ctc']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['special_allowance_earnings']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
      </tr>
       <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Total</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['FIXED_MONTHLY_CTC']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['gross']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['PF']+$employee['PT']+$employee['SD']+$employee['TDS']+$employee['ESI']+$employee['management_deductions']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['net_income']-$employee['management_deductions']).'</td>
      </tr>
      <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;" colspan="6" class="text-center">In words: '.$in_words.' only. </td>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
       <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
       <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Total</td>
       <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;" colspan="2">FIXED MONTHLY CTC(Rs)</td>
       <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($employee['FIXED_MONTHLY_CTC']).'</td>
    </tr>
  </table>';
$tbl .= '<div style="align:center"><h4>TDS Details</h4></div> <table id="gradeX" style="  border-collapse: collapse;width: 100%;">
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Description</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Gross(Rs)</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Exempt(Rs)</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Taxable(Rs)</th>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;" colspan="2">Income Tax Deduction(Rs)</th>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Basic</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['basic']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">0</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['basic']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Gross Salary</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['taxable_amt']).'</td>
      </tr>
      <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">DA</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['da']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">0</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['da']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">PT</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['PT_AMT']).'</td>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">HRA</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['hra']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['hra_exmpt']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['hra_tax_amt']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">80C</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['80C_exmpt']).'</td>
      </tr>
       <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Conveyance</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['conveyance_allowance']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['convy_exmpt']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['convy_tax_amt']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">80D</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['80D_exmpt']).'</td>
      </tr>
       <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Any Other allowances</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['other_allowance']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">0</td>        
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['other_allowance']).'</td>

        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Taxable Income</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['taxable_income']).'</td>
      </tr>
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Other cost components</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['other_cost_cpmponents']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">0</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['other_cost_cpmponents']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Total Tax</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['tax_amt']).'</td>
      </tr>
        <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Total</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['gross']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['convy_exmpt']+$tds_data['hra_exmpt']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['taxable_amt']).'</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Surcharges+EDCS</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['surcharges']).'</td>
      </tr>
       <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Total Tax + Surcharge</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.$total_tax_sru.'</td>
      </tr> 
      <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Tax Deducted Till Date</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.$tds_data['tds_deducted_till_date'].'</td>
      </tr>  
      <tr style="background-color: #f2f2f2;">
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Tax to be Deducted</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['tds_to_be_deducted']).'</td>
      </tr>
      <tr>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;"></td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Projected Tax/Month</td>
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.round($tds_data['projected_tax_per_month']).'</td>
      </tr>
  </table>';
  $tbl.='<div style="align:center"><h4>TAX Paid</h4></div> <table id="gradeX" style="  border-collapse: collapse;width: 100%;">
      <tr>
        <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">Month</th>';
        foreach ($taxs_paid as $tax_paid) { 
          $month=date("F", mktime(0, 0, 0, $tax_paid['salary_month'], 10));
          $tbl.='<th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color:#8590e5; color: white;">'.$month.'</th>';
        }

     $tbl.='</tr>
      <tr >
        <td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">Amount(Rs)</td>';
        foreach ($taxs_paid as $tax_paid) { 
          $tbl.='<td style="border: 1px solid #ddd; padding: 8px;  font-weight: 600;color: #656565;">'.$tax_paid['tds_amt'].'</td>';
        }
      $tbl.='</tr>
      </table>';
// output the HTML content
$pdf->writeHTML($tbl, true, false, false, false, '');
}

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('PaySlip.pdf', 'I');

}
 
 
}
?>