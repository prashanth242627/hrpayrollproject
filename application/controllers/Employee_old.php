<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Employee extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_line_of_activity	= $this->Global_model->get_values('line_of_activity');
		$get_subject_details	= $this->Global_model->get_values('subject_details');
		$get_house_details		= $this->db->query("SELECT * FROM `transport_details` WHERE `residence_details_id` =1")->result();
		$get_transport_details		= $this->db->query("SELECT * FROM `transport_details` WHERE `residence_details_id` =2")->result();
		$get_reportingslots	= $this->Global_model->get_values('reportingslots');
		$get_role_details	= $this->Global_model->get_values('role');
		//$get_employee = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
		$get_employee = $this->Global_model->getEmployeewithActivity();
		$get_employee_log = $this->Global_model->get_values_where($this->config->item('employee_log'), array('flag'=>2));
		
		$data=array('current_page'=>'Employee ','page_title'=>'Employee ','parent_menu'=>'','template'=>'masters/employee/add','get_line_of_activity'=>$get_line_of_activity,'get_subject_details'=>$get_subject_details,'get_house_details'=>$get_house_details,'get_transport_details'=>$get_transport_details,'get_role_details'=>$get_role_details,'get_employee'=>$get_employee,'get_employee_log'=>$get_employee_log,'get_reportingslots'=>$get_reportingslots);
		//$this->load->view('theme/template',$data);
		
	
		
		$this->form_validation->set_rules('check','check','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			if($this->input->post('employeeadd') == "go")
			{
				$this->EmpBulkupload();
			}
			
			$emp_id  = $this->session->userdata('emp_id');
			$temp_emp_id = $this->input->post('emp_id');
			$dob = $this->input->post('dob');
			$emp_name = $this->input->post('emp_name');
			$education = $this->input->post('education');
			$emp_bid = $this->input->post('emp_bid');
			$experience = $this->input->post('experience');
			$doj = $this->input->post('doj');
			$mobile = $this->input->post('mobile');
			$activity_id = $this->input->post('line_of_activity');
			$subject_details_id = $this->input->post('subject_details');
			$residence_details_id = $this->input->post('house_details');
			$transport_details_id = $this->input->post('transport_details');
			$email = $this->input->post('email');
			$sex = $this->input->post('sex');
			$alternate_no = $this->input->post('alternate_no');
			$reportingslots_id = $this->input->post('reportingslots_id');
			$emergency_no = $this->input->post('emergency_no');
			$designation = $this->input->post('designation');
			$address = $this->input->post('address');
			$spouse_name = $this->input->post('spouse_name');
			$spouse_phoneno = $this->input->post('spouse_phoneno');
			$spouse_occupancy = $this->input->post('spouse_occupancy');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$param['insertdata'] = array('emp_id'=>$this->Global_model->Max_count($this->config->item('employee'),'emp_id'),'temp_emp_id' =>$temp_emp_id,'emp_name' =>$emp_name,'education' =>$education,'date_of_birth' =>$dob,'date_of_join' =>$doj,'emp_bid' =>$emp_bid,'experience' =>$experience,'mobile' =>$mobile,'activity_id' =>$activity_id,'subject_details_id' =>$subject_details_id,'residence_details_id' =>$residence_details_id,'transport_details_id' =>$transport_details_id,'date'=>$this->localdate("Y-m-d H:i:s"),'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'email'=>$email, 'sex'=>$sex, 'alternate_no'=>$alternate_no, 'reportingslots_id'=>$reportingslots_id, 'emergency_no'=>$emergency_no, 'designation'=>$designation, 'address'=>$address, 'spouse_name'=>$spouse_name, 'spouse_phoneno'=>$spouse_phoneno, 'spouse_occupancy'=>$spouse_occupancy);
			$param['table'] = $this->config->item('employee');
			$this->Global_model->insert_data($param);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Employee not inserted '));	
			}
			else
			{
				
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Employee inserted successfully'));
			}
			redirect('Employee');
		} 
	}
	function add() {
		$emp_id  = $this->session->userdata('emp_id');
			$temp_emp_id = $this->input->post('emp_id');
			$dob = $this->input->post('dob');
			$emp_name = $this->input->post('emp_name');
			$education = $this->input->post('education');
			$emp_bid = $this->input->post('emp_bid');
			$experience = $this->input->post('experience');
			$doj = $this->input->post('doj');
			$mobile = $this->input->post('mobile');
			$activity_id = $this->input->post('line_of_activity');
			$subject_details_id = $this->input->post('subject_details');
			$residence_details_id = $this->input->post('house_details');
			$transport_details_id = $this->input->post('transport_details');
			$email = $this->input->post('email');
			$sex = $this->input->post('sex');
			$alternate_no = $this->input->post('alternate_no');
			$reportingslots_id = $this->input->post('reportingslots_id');
			$emergency_no = $this->input->post('emergency_no');
			$designation = $this->input->post('designation');
			$address = $this->input->post('address');
			$spouse_name = $this->input->post('spouse_name');
			$spouse_phoneno = $this->input->post('spouse_phoneno');
			$spouse_occupancy = $this->input->post('spouse_occupancy');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$param['insertdata'] = array('emp_id'=>$this->Global_model->Max_count($this->config->item('employee'),'emp_id'),'temp_emp_id' =>$temp_emp_id,'emp_name' =>$emp_name,'education' =>$education,'date_of_birth' =>$dob,'date_of_join' =>$doj,'emp_bid' =>$emp_bid,'experience' =>$experience,'mobile' =>$mobile,'activity_id' =>$activity_id,'subject_details_id' =>$subject_details_id,'residence_details_id' =>$residence_details_id,'transport_details_id' =>$transport_details_id,'date'=>$this->localdate("Y-m-d H:i:s"),'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'email'=>$email, 'sex'=>$sex, 'alternate_no'=>$alternate_no, 'reportingslots_id'=>$reportingslots_id, 'emergency_no'=>$emergency_no, 'designation'=>$designation, 'address'=>$address, 'spouse_name'=>$spouse_name, 'spouse_phoneno'=>$spouse_phoneno, 'spouse_occupancy'=>$spouse_occupancy);
			$param['table'] = $this->config->item('employee');
			$this->Global_model->insert_data($param);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Employee not inserted '));	
			}
			else
			{
				
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Employee inserted successfully'));
			}
			redirect('Employee');
	}
	function edit($id)
	{
		$table = $this->config->item('employee');
		$where = array('emp_id'=>base64_decode($id));
		$o_emp_id = base64_decode($id);
		$getEmployees = $this->Global_model->get_values_where_single($table,$where);
		$get_line_of_activity	= $this->Global_model->get_values('line_of_activity');
		$get_subject_details	= $this->Global_model->get_values('subject_details');
		$get_house_details		= $this->db->query("SELECT * FROM `transport_details` WHERE `residence_details_id` =1")->result();
		$get_transport_details		= $this->db->query("SELECT * FROM `transport_details` WHERE `residence_details_id` =2")->result();
		$get_reportingslots	= $this->Global_model->get_values('reportingslots');
		$get_role_details	= $this->Global_model->get_values('role');
		$get_employee = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
		$get_employee_log = $this->Global_model->get_values_where($this->config->item('employee_log'), array('flag'=>2));
		$data=array('current_page'=>'Employee ','page_title'=>'Employee ','parent_menu'=>'','template'=>'masters/employee/add','get_employee'=>$get_employee,'get_subject_details'=>$get_subject_details,'get_house_details'=>$get_house_details,'get_transport_details'=>$get_transport_details,'get_role_details'=>$get_role_details,'getEmployees'=>$getEmployees,'get_employee_log'=>$get_employee_log,'get_line_of_activity'=>$get_line_of_activity,'get_reportingslots'=>$get_reportingslots);
		$this->form_validation->set_rules('emp_name','emp_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$temp_emp_id = $this->input->post('emp_id');
			$dob = $this->input->post('dob');
			$emp_name = $this->input->post('emp_name');
			$education = $this->input->post('education');
			$emp_bid = $this->input->post('emp_bid');
			$experience = $this->input->post('experience');
			$doj = $this->input->post('doj');
			//$date = $this->input->post('date');
			$mobile = $this->input->post('mobile');
			$activity_id = $this->input->post('line_of_activity');
			$subject_details_id = $this->input->post('subject_details');
			$residence_details_id = $this->input->post('house_details');
			$transport_details_id = $this->input->post('transport_details');
			$email = $this->input->post('email');
			$sex = $this->input->post('sex');
			$alternate_no = $this->input->post('alternate_no');
			$reportingslots_id = $this->input->post('reportingslots_id');
			$emergency_no = $this->input->post('emergency_no');
			$designation = $this->input->post('designation');
			$address = $this->input->post('address');
			$spouse_name = $this->input->post('spouse_name');
			$spouse_phoneno = $this->input->post('spouse_phoneno');
			$spouse_occupancy = $this->input->post('spouse_occupancy');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			/* Update log table start */
				$this->db->query("INSERT INTO `employee_log`(`emp_id`, `temp_emp_id`, `emp_name`, `education`, `date_of_birth`, `date_of_join`, `emp_bid`, `experience`, `sex`, `email`, `mobile`, `activity_id`, `subject_details_id`, `residence_details_id`, `transport_details_id`, `alternate_no`, `reportingslots_id`, `emergency_no`, `designation`, `address`, `spouse_name`, `spouse_phoneno`, `spouse_occupancy`, `status`,  `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (select emp_id, temp_emp_id, emp_name, education, date_of_birth, date_of_join, emp_bid, experience, sex, email, mobile, activity_id, subject_details_id, residence_details_id, transport_details_id, alternate_no, reportingslots_id, emergency_no, designation, address, spouse_name, spouse_phoneno, spouse_occupancy, status, '1', 'EDIT', $emp_id, now(), $emp_id, now() FROM employee WHERE emp_id= ".$o_emp_id.")");
			/* Update log table end */
			
			$data1 = array('emp_id'=>$o_emp_id,'temp_emp_id' =>$temp_emp_id,'emp_name' =>$emp_name,'education' =>$education,'date_of_birth' =>$dob,'date_of_join' =>$doj,'emp_bid' =>$emp_bid,'experience' =>$experience,'mobile' =>$mobile,'activity_id' =>$activity_id,'subject_details_id' =>$subject_details_id,'residence_details_id' =>$residence_details_id,'transport_details_id' =>$transport_details_id,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'status'=>$status,'email'=>$email, 'sex'=>$sex, 'alternate_no'=>$alternate_no, 'reportingslots_id'=>$reportingslots_id, 'emergency_no'=>$emergency_no, 'designation'=>$designation, 'address'=>$address, 'spouse_name'=>$spouse_name, 'spouse_phoneno'=>$spouse_phoneno, 'spouse_occupancy'=>$spouse_occupancy);
			$where = array('emp_id'=>$o_emp_id);
			$this->Global_model->update($this->config->item('employee'),$data1,$where);
			
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Employee not updated '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Employee updated successfully'));
			}
			redirect('Employee');
		}
		
	}
	function delete($id)
	{
		$o_emp_id = base64_decode($id);
		$emp_id  = $this->session->userdata('emp_id');
		$this->db->trans_begin();
			/* Update log table start */
				$this->db->query("INSERT INTO `employee_log`(`emp_id`, `temp_emp_id`, `emp_name`, `education`, `date_of_birth`, `date_of_join`, `emp_bid`, `experience`, `sex`, `email`, `mobile`, `activity_id`, `subject_details_id`, `residence_details_id`, `transport_details_id`, `alternate_no`, `reportingslots_id`, `emergency_no`, `designation`, `address`, `spouse_name`, `spouse_phoneno`, `spouse_occupancy`, `status`, `date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (select emp_id, temp_emp_id, emp_name, education, date_of_birth, date_of_join, emp_bid, experience, sex, email, mobile, activity_id, subject_details_id, residence_details_id, transport_details_id, alternate_no, reportingslots_id, emergency_no, designation, address, spouse_name, spouse_phoneno, spouse_occupancy, status, date, '1', 'EDIT', $emp_id, now(), $emp_id, now() FROM employee WHERE emp_id= ".$o_emp_id.")");
			/* Update log table end */
		$this->db->query("DELETE FROM ".$this->config->item('employee')." WHERE emp_id =".$o_emp_id);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Employee  not Deleted '));	
		}
		else
		{
			$this->Global_model->insert_data($param);
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Employee  Deleted successfully'));
		}
		redirect('Employee');
		
	}
	function undo($id)
	{
		$emp_id  = $this->session->userdata('emp_id');
		$this->db->trans_begin();
		$emp_id = base64_decode($id);
		$where = array('emp_id'=>base64_decode($id));
		$get_employee= $this->db->query("SELECT * FROM ".$this->config->item('employee_log')." WHERE emp_id=".base64_decode($id)." ORDER BY `Created_Datetime` DESC")->first_row();
		$param['insertdata'] = array('emp_id'=>$get_employee->emp_id,'temp_emp_id' =>$get_employee->temp_emp_id,'emp_name' =>$get_employee->emp_name,'education' =>$get_employee->education,'date_of_birth' =>$get_employee->dob,'date_of_join' =>$get_employee->doj,'emp_bid' =>$get_employee->emp_bid,'experience' =>$get_employee->experience,'mobile' =>$get_employee->mobile,'activity_id' =>$get_employee->activity_id,'subject_details_id' =>$get_employee->subject_details_id,'residence_details_id' =>$get_employee->residence_details_id,'transport_details_id' =>$get_employee->transport_details_id,'Updated_By'=>$get_employee->Updated_By,'Updated_Datetime'=>$get_employee->Updated_Datetime,'date'=>$get_employee->date,'status'=>$get_employee->status,'Created_By'=>$get_employee->Created_By,'Created_Datetime'=>$get_employee->Created_Datetime,'email'=>$get_employee->email, 'sex'=>$get_employee->sex, 'alternate_no'=>$get_employee->alternate_no, 'reportingslots_id'=>$get_employee->reportingslots_id, 'emergency_no'=>$get_employee->emergency_no, 'designation'=>$get_employee->designation, 'address'=>$get_employee->address, 'spouse_name'=>$get_employee->spouse_name, 'spouse_phoneno'=>$get_employee->spouse_phoneno, 'spouse_occupancy'=>$get_employee->spouse_occupancy);
		
		$this->Global_model->update($this->config->item('employee'),$param['insertdata'],$where);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Employee not Undo '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Employee undo successfully'));
		}
		redirect('Employee');
	}
	function errorpage()
	{
		$data=array('current_page'=>'Error ','page_title'=>'Error ','parent_menu'=>'','template'=>'masters/employee/errorpage');
		$this->load->view('theme/template',$data);
	}
	public function EmpBulkupload()
	{
				$file = $_FILES["emp_bluk"]["tmp_name"];
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);
				$get_sheetData = array();
				$get_sheets = $objPHPExcel->getAllSheets();
				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				foreach ($cell_collection as $cell) {
					$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
					$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
					$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
					if ($row == 1) {
						$header[$row][$column] = $data_value;
					} else {
						$arr_data[$row][$column] = $data_value;
						$arr_data1[$column][$row] = $data_value;
					}
				}
				$header = $header[1];
				$values = $arr_data;
				$this->db->trans_begin();
				$excelHeadingNames = array('S NO', 'EMP ID', 'BID', 'EMP NAME', 'REPT TIME FROM', 'RPT TIME TO', 'Sex', 'Date Of Joining', 'Date Of Birth', 'Mobile No', 'Alternate No', 'Emergency No', 'DESIG', 'DEPT', 'SUBJECT', 'EDUCATION', 'Experience', 'HOUSE', 'TRANSPORT', 'Email', 'Address', 'Spouse Name', 'Spouse PhoneNo', 'Spouse Ocupa');
				if(count(array_diff($header,$excelHeadingNames))==0)
				{
					$S_NO = array_search('S_NO', $header);
					$EMP_ID = array_search('EMP ID', $header);
					$BID = array_search('BID', $header);
					$EMP_NAME = array_search('EMP NAME', $header);
					$REPT_TIME_FROM = array_search('REPT TIME FROM', $header);
					$RPT_TIME_TO = array_search('RPT TIME TO', $header);
					$Sex = array_search('Sex', $header);
					$Date_Of_Joining = array_search('Date Of Joining', $header);
					$Date_Of_Birth = array_search('Date Of Birth', $header);
					$Mobile_No = array_search('Mobile No', $header);
					$Alternate_No = array_search('Alternate No', $header);
					$Emergency_No = array_search('Emergency No', $header);
					$DESIG = array_search('DESIG', $header);
					$DEPT = array_search('DEPT', $header);
					$SUBJECT = array_search('SUBJECT', $header);
					$EDUCATION = array_search('EDUCATION', $header);
					$Experience = array_search('Experience', $header);
					$HOUSE = array_search('HOUSE', $header);
					$TRANSPORT = array_search('TRANSPORT', $header);
					$Email = array_search('Email', $header);
					$Address = array_search('Address', $header);
					$Spouse_Name = array_search('Spouse Name', $header);
					$Spouse_PhoneNo = array_search('Spouse PhoneNo', $header);
					$Spouse_Ocupa = array_search('Spouse Ocupa', $header);
					foreach($arr_data1[$EMP_ID] as $key=>$value)
					{
						$S_NO1 =$arr_data1[$S_NO][$key];
						$EMP_ID1 =$arr_data1[$EMP_ID][$key];
						$BID1 =$arr_data1[$BID][$key];
						$EMP_NAME1 =$arr_data1[$EMP_NAME][$key];
						$REPT_TIME_FROM1 =$arr_data1[$REPT_TIME_FROM][$key];
						$RPT_TIME_TO1 =$arr_data1[$RPT_TIME_TO][$key];
						$Sex1 =$arr_data1[$Sex][$key];
						$Date_Of_Joining1 =date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($arr_data1[$Date_Of_Joining][$key]));
						$Date_Of_Birth1 = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($arr_data1[$Date_Of_Birth][$key]));
						$Mobile_No1 =$arr_data1[$Mobile_No][$key];
						$Alternate_No1 =$arr_data1[$Alternate_No][$key];
						$Emergency_No1 =$arr_data1[$Emergency_No][$key];
						$DESIG1 =$arr_data1[$DESIG][$key];
						$DEPT1 =$arr_data1[$DEPT][$key];
						$SUBJECT1 =$arr_data1[$SUBJECT][$key];
						$EDUCATION1 =$arr_data1[$EDUCATION][$key];
						$Experience1 =$arr_data1[$Experience][$key];
						$HOUSE1 =$arr_data1[$HOUSE][$key];
						$TRANSPORT1 =$arr_data1[$TRANSPORT][$key];
						$Email1 =$arr_data1[$Email][$key];
						$Address1 =$arr_data1[$Address][$key];
						$Spouse_Name1 =$arr_data1[$Spouse_Name][$key];
						$Spouse_PhoneNo1 =$arr_data1[$Spouse_PhoneNo][$key];
						$Spouse_Ocupa1 =$arr_data1[$Spouse_Ocupa][$key];
						
						
						 $getEmployee = $this->Global_model->get_values_where_single('employee', array('temp_emp_id'=>$EMP_ID1));
						if(COUNT($getEmployee) > 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'emp_id already exist please remove form excel and upload'));
							redirect('Employee');
						}
						else {
							
						} 
						$getEmployee1 = $this->Global_model->get_values_where_single('employee', array('emp_bid'=>$BID1));
						if(COUNT($getEmployee1) > 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'emp_bid already exist please remove form excel and upload'));
							redirect('Employee');
						}
						else {
							
						}
						$subjectDetails = $this->Global_model->get_values_where_single('subject_details', array('sub_name'=>$SUBJECT1));
						if($subjectDetails->sub_name == $SUBJECT1) {
							$subject_details_id = $subjectDetails->subject_details_id;
						}
						else {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'subject name not exist please remove form excel and upload'));
							redirect('Employee');
						}
						$getEmployeeEmail = $this->Global_model->get_values_where_single('employee', array('email'=>$Email1));
						if($getEmployeeEmail->email == $Email1) {
						$this->session->set_userdata(array('create_status'=>'failed'));
						$this->session->set_userdata(array('record_name'=>'email already please remove form excel and upload'));
						redirect('Employee');
						}
						
						$get_house_details	= $this->db->query("SELECT * FROM `transport_details` WHERE `residence_details_id` =1 and transport_name = '".$HOUSE1."'")->row();
						if(count($get_house_details) == 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'house name not exist please remove form excel and upload'));
							redirect('Employee');
						} else {
							$residence_details_id = $get_house_details->residence_details_id;
						}
						$get_transport_details		= $this->db->query("SELECT * FROM `transport_details` WHERE `residence_details_id` =2 and transport_name = '".$TRANSPORT1."'")->row();
						if(count($get_transport_details) == 0)
						{
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'Transport Details name not exist please remove form excel and upload'));
							redirect('Employee');
						} else  {
							$transport_details_id = $get_transport_details->residence_details_id;
						}
						
						/* $get_reportingslots	= $this->db->query("SELECT * FROM `reportingslots0` WHERE from_time='".$REPT_TIME_FROM1."' AND to_time= '".$RPT_TIME_TO1."'")->row();
						if(count($get_reportingslots) == 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'Reporting slots Details name not exist please remove form excel and upload'));
							redirect('Employee');
						} else { 
							$reportingslots_id = $get_reportingslots->reportingslots_id;
						} */
						$reportingslots_id =1;
						$get_line_of_activity	= $this->db->query("SELECT * FROM `line_of_activity` WHERE dept_name='".$DEPT1."'")->row();
						if(count($get_line_of_activity) == 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'Reporting slots Details name not exist please remove form excel and upload'));
							redirect('Employee');
						} else { 
							$activity_id = $get_line_of_activity->activity_id;
						}
						
						$emp_id = $this->Global_model->Max_count($this->config->item('employee'),'emp_id');
						$this->db->query("INSERT INTO `employee`(`emp_id`, `temp_emp_id`, `emp_name`, `education`, `date_of_birth`, `date_of_join`, `emp_bid`, `experience`, `sex`, `mobile`, `email`, `reportingslots_id`, `activity_id`, `subject_details_id`, `residence_details_id`, `transport_details_id`, `alternate_no`, `emergency_no`, `designation`, `address`, `spouse_name`, `spouse_phoneno`, `spouse_occupancy`, `status`, `date`) VALUES(($emp_id+1),'$EMP_ID1','$EMP_NAME1','$EDUCATION1','$Date_Of_Birth1','$Date_Of_Joining1','$BID1',$Experience1,'$Sex1','$Mobile_No1','$Email1',$reportingslots_id,$activity_id,$subject_details_id,$residence_details_id,$transport_details_id,'$Alternate_No1','$Emergency_No1','$DESIG1','$Address1','$Spouse_Name1','$Spouse_PhoneNo1','$Spouse_Ocupa1',1,now())");  
					}
					 if($this->db->trans_status()===FALSE)
					{
						$this->db->trans_rollback();
						$this->session->set_userdata(array('create_status'=>'failed'));
						$this->session->set_userdata(array('record_name'=>'Employee not inserted '));
					}
					else
					{
						$this->db->trans_commit();
						$this->session->set_userdata(array('create_status'=>'success'));
						$this->session->set_userdata(array('record_name'=>'Employee inserted successfully'));
					}
					redirect('Employee'); 
				}
				else {
					$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Please check excel heading is mismatching'));
					redirect('Employee');
				}
				
	}
	
}
