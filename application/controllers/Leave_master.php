<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Leave_master extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_leave_master = $this->Global_model->get_leave_master();
		$get_leave_master_log = $this->Global_model->get_leave_master_log();
		$get_employees = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
		$get_leave_types=$this->Global_model->get_leave_master_leave_types();
		//print_r($get_leave_types);exit();
		//$get_leave_types  = $this->Global_model->get_values_where($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"CL"));
		//$get_leave_types[]  = $this->Global_model->get_values_where($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"SL"));
		$data=array('current_page'=>'Leave Master','page_title'=>'Leave Master','parent_menu'=>'','template'=>'masters/leave_master/add','get_leave_master'=>$get_leave_master,'get_leave_master_log'=>$get_leave_master_log,'get_employees'=>$get_employees,'get_leave_types'=>$get_leave_types);
		$this->form_validation->set_rules('emp_name','emp_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_bid');
			$activity_id = $this->input->post('activity_id');
			$leave_type_id = $this->input->post('leave_type_id');
			$no_leaves = $this->input->post('no_leaves');
			$calendar_year = $this->input->post('calendar_year');
			$lemp_id = $this->input->post('emp_id');
			$this->db->trans_begin();
			$insert_array = array('activity_id'=>$activity_id,'emp_bid'=>$emp_bid, 'leave_type_id'=>$leave_type_id, 'no_leaves'=>$no_leaves,'emp_id'=>$lemp_id,'calendar_year'=>$calendar_year);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Leave Master not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Leave Master inserted successfully'));
			}
			redirect('leave_master');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('leave_master');
		$where = array('leave_master_id'=>base64_decode($id));
		$leave_master_id = base64_decode($id);
		$getLeaveMasterEdit = $this->Global_model->get_leave_master_single($leave_master_id);
		$get_leave_master = $this->Global_model->get_leave_master();
		$get_leave_master_log = $this->Global_model->get_leave_master_log();
		$get_employees = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
		$get_leave_types  = $this->Global_model->get_values_where($this->config->item('leave_types'),array('status'=>1));
		$data=array('current_page'=>'Leave Master','page_title'=>'Leave Master','parent_menu'=>'','template'=>'masters/leave_master/add','get_leave_master'=>$get_leave_master,'getLeaveMasterEdit'=>$getLeaveMasterEdit,'get_leave_master_log'=>$get_leave_master_log,'get_employees'=>$get_employees,'get_leave_types'=>$get_leave_types);
		$this->form_validation->set_rules('emp_bid','emp_bid','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_bid');
			$activity_id = $this->input->post('activity_id');
			$leave_type_id = $this->input->post('leave_type_id');
			$no_leaves = $this->input->post('no_leaves');
			$calendar_year = $this->input->post('calendar_year');
			$lemp_id = $this->input->post('emp_id');
			$this->db->trans_begin();
			$edit_array = array('activity_id'=>$activity_id,'emp_bid'=>$emp_bid, 'leave_type_id'=>$leave_type_id, 'no_leaves'=>$no_leaves,'emp_id'=>$lemp_id,'leave_master_id'=>$leave_master_id,'calendar_year'=>$calendar_year);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Leave Master not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Leave Master updated successfully'));
			}

			redirect('leave_master');
		}
		
	}
	function delete($id)
	{
			$leave_master_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('leave_master_id'=>$leave_master_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Leave Master not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Leave Master  Deleted successfully'));
		}
		redirect('leave_master');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$leave_master_id = base64_decode($id);
				$undo_array = array('leave_master_id'=>$leave_master_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Leave Master not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Leave Master undo successfully'));
				}
		
		redirect('leave_master');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$leave_master_id = $insert_array['leave_master_id'];
			$get_leaves = $this->db->query("SELECT * FROM leave_master WHERE `emp_bid` = '".$insert_array['emp_bid']."' and leave_type_id=".$insert_array['leave_type_id']." and calendar_year=".$insert_array['calendar_year'])->row();
			if ($get_leaves) {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Employee Have Already Leaves in Same Leave Type Please use Edit option to Change'));	
				redirect($_SERVER['HTTP_REFERER']);
				
			}
			$param['insertdata'] = array('leave_master_id'=>$this->Global_model->Max_count($this->config->item('leave_master'),'leave_master_id'),'emp_bid'=>$insert_array['emp_bid'],'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>$insert_array['no_leaves'],'calendar_year'=>$insert_array['calendar_year'],'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('leave_master');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
  		    $leave_master_id = $insert_array['leave_master_id'];
			$data1 = array('leave_master_id'=>$leave_master_id,'emp_bid'=>$insert_array['emp_bid'],'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>$insert_array['no_leaves'],'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('leave_master_id'=>$leave_master_id);
			$this->db->query("INSERT INTO `leave_master_log`(`leave_master_id`, `emp_bid`, `leave_type_id`, `no_leaves`, `calendar_year`,`date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (SELECT leave_master_id, emp_bid, leave_type_id, no_leaves, calendar_year, date, '1', 'EDIT', Created_By, Created_Datetime, Updated_By, Updated_Datetime FROM leave_master WHERE leave_master_id= ".$leave_master_id.")");
			$this->Global_model->update($this->config->item('leave_master'),$data1,$where);

		}
		else if($action == 'delete') {
			$leave_master_id = $insert_array['leave_master_id'];
			$this->db->query("INSERT INTO `leave_master_log`(`leave_master_id`, `emp_bid`, `leave_type_id`, `no_leaves`,  `calendar_year`,`date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (SELECT leave_master_id, emp_bid, leave_type_id, no_leaves,calendar_year, date, '2', 'DELETE', $emp_id, now(), $emp_id, now() FROM leave_master WHERE leave_master_id= ".$leave_master_id.")");
			$this->db->query("DELETE FROM ".$this->config->item('leave_master')." WHERE leave_master_id =".$leave_master_id);
		}
		else if($action == 'undo') {
			$leave_master_id = $insert_array['leave_master_id'];
			$where = array('leave_master_id'=>$leave_master_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('leave_master_log')." WHERE leave_master_id=".$leave_master_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('emp_bid'=>$getPaymentTypes->emp_bid,'leave_type_id'=>$getPaymentTypes->leave_type_id,'no_leaves'=>$getPaymentTypes->no_leaves,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('leave_master'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Leave Master not Undo '));	
					redirect('leave_master');
			}
			
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('leave_master');
		}	
	}
	function getEmployeeDetails()
	{
		$emp_bid = $this->input->post('emp_bid');
		$get_leave_masters = $this->db->query("SELECT * FROM `employee` e LEFT JOIN line_of_activity l ON e.`activity_id` = l.activity_id WHERE e.`emp_bid` = '".$emp_bid."'")->row();
		$data['emp_name'] = $get_leave_masters->emp_name;
		$data['dept_name'] = $get_leave_masters->dept_name;
		$data['emp_id'] = $get_leave_masters->emp_id;
		
		echo json_encode($data);
	}
}
