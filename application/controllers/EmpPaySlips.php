<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';

class EmpPaySlips extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_leave_model');
		$this->load->model('payslip_model');
	}
	public function index()
	{
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		
		$data=array('current_page'=>'EmpPaySlips','page_title'=>'EmpPaySlips','parent_menu'=>'','template'=>'EmpPayslipMaster');
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['taxs_paid']=array();
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$data['employees']=array();
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		if ($month!=null && $year!=null) {
			 if ($bid!=null) {
				$payslip_data=$this->payslip_model->EmpPaySlipByWhere("e.emp_bid=".$bid,$month,$year);
				$tds_data=$this->payslip_model->PaySlipTDSDetails("p.emp_bid=".$bid,$month,$year);
				$tax_paid=$this->payslip_model->taxPaid("emp_bid=".$bid,$month,$year);
			}
			else if ($ename!=null) {
				$payslip_data=$this->payslip_model->EmpPaySlipByWhere("e.emp_name='".$ename."'",$month,$year);
				$tds_data=$this->payslip_model->PaySlipTDSDetails("p.emp_name='".$ename."'",$month,$year);
				$tax_paid=$this->payslip_model->taxPaid("emp_name=".$ename,$month,$year);
			}
		}
		
		$data=array('current_page'=>'EmpPaySlips','page_title'=>'EmpPaySlips','parent_menu'=>'','template'=>'EmpPayslipMaster');
		if (count($payslip_data)) 
		{
			$data['employee'] = $payslip_data[0];
			$data['tds_data']=$tds_data[0];
			$data['taxs_paid']=$tax_paid;
			$data['employee']['total_in_rupees']=$this->payslip_model->getIndianCurrency($payslip_data[0]['net_income']-$payslip_data[0]['management_deductions']);
		}
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$this->load->view('theme/template',$data);
	}

}
