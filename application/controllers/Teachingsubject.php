<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Teachingsubject extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_subject_details = $this->Global_model->get_values_where($this->config->item('subject_details'),array('status'=>1));
		$get_subject_details_log = $this->Global_model->get_values_where($this->config->item('subject_details_log'), array('flag'=>2));
		$data=array('current_page'=>'Subject Details','page_title'=>'Subject Details','parent_menu'=>'','template'=>'masters/teachingsubject/add','get_subject_details'=>$get_subject_details,'get_subject_details_log'=>$get_subject_details_log);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('sub_name','sub_name','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$sub_name = $this->input->post('sub_name');
			$sub_id = $this->input->post('sub_id');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('sub_name'=>$sub_name,'sub_id'=>$sub_id, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Subject Details not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Subject Details inserted successfully'));
			}
			redirect('Teachingsubject');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('subject_details');
		$where = array('subject_details_id'=>base64_decode($id));
		$subject_details_id = base64_decode($id);
		$getsubject_detailsEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_subject_details = $this->Global_model->get_values_where($this->config->item('subject_details'),array('status'=>1));
		$get_subject_details_log = $this->Global_model->get_values_where($this->config->item('subject_details_log'), array('flag'=>2));
			$data=array('current_page'=>'Subject Details','page_title'=>'Subject Details','parent_menu'=>'','template'=>'masters/teachingsubject/add','get_subject_details'=>$get_subject_details,'getsubject_detailsEdit'=>$getsubject_detailsEdit,'get_subject_details_log'=>$get_subject_details_log);
		$this->form_validation->set_rules('sub_name','sub_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			/* echo "<pre>";
			print_r($this->input->post());
			exit(); */
			$emp_id  = $this->session->userdata('emp_id');
			$sub_name = $this->input->post('sub_name');
			$sub_id = $this->input->post('sub_id');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('sub_name'=>$sub_name,'sub_id'=>$sub_id, 'status'=>$status,'subject_details_id'=>$subject_details_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Subject Details not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Subject Details updated successfully'));
			}
			redirect('Teachingsubject');
		}
		
	}
	function delete($id)
	{
			$subject_details_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('subject_details_id'=>$subject_details_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Subject Details not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Subject Details  Deleted successfully'));
		}
		redirect('Teachingsubject');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$subject_details_id = base64_decode($id);
				$undo_array = array('subject_details_id'=>$subject_details_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Subject Details not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Subject Details undo successfully'));
				}
		
		redirect('Teachingsubject');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$sub_name = $insert_array['sub_name'];
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$sub_id = $insert_array['sub_id'];
			$param['insertdata'] = array('subject_details_id'=>$this->Global_model->Max_count($this->config->item('subject_details'),'subject_details_id'),'sub_name'=>$sub_name,'sub_id'=>$insert_array['sub_id'],'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('subject_details');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			$sub_id = $insert_array['sub_id'];
			$subject_details_id = $insert_array['subject_details_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('subject_details'), array('subject_details_id'=>$subject_details_id));
				
				$param['insertdata'] = array('subject_details_id'=>$insertdata->subject_details_id,'sub_name'=>$insertdata->sub_name,'sub_id'=>$insertdata->sub_id,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				
				$param['table'] = $this->config->item('subject_details_log');
			/* Update log table end */
			$data1 = array('subject_details_id'=>$subject_details_id,'sub_name'=>$sub_name,'sub_id'=>$sub_id,'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('subject_details_id'=>$subject_details_id);
			$this->Global_model->update($this->config->item('subject_details'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$subject_details_id = $insert_array['subject_details_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('subject_details'), array('subject_details_id'=>$subject_details_id));
				
				$param['insertdata'] = array('subject_details_id'=>$insertdata->subject_details_id,'sub_name'=>$insertdata->sub_name,'sub_id'=>$insertdata->sub_id,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('subject_details_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('subject_details')." WHERE subject_details_id =".$subject_details_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$subject_details_id = $insert_array['subject_details_id'];
			$where = array('subject_details_id'=>$subject_details_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('subject_details_log')." WHERE subject_details_id=".$subject_details_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('subject_details_id'=>$subject_details_id,'sub_name'=>$getPaymentTypes->sub_name,'sub_id'=>$getPaymentTypes->sub_id,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('subject_details'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Subject Details not Undo '));	
					redirect('Teachingsubject');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Teachingsubject');
		}	
	}
}
