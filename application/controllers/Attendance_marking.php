<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Attendance_marking extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		$this->load->model('Myfunctions');
	}
	public function index()
	{
		
		//$this->output->enable_profiler(TRUE);
		$get_attendance_marking = $this->Global_model->get_attendance_marking();

		$get_attendance_marking_log = $this->Global_model->get_attendance_marking_log();
		
		$get_employees = $this->Global_model->employeesBids();
		$get_holidays = $this->Global_model->get_values_where($this->config->item('holidays'),array('status'=>1));
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves = $this->Global_model->get_current_date_leaves_count();
		$get_current_date_leave_emps=$this->Global_model->get_current_date_leave_emps();
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves_string="";
			$total_leaves=0;
		   foreach ($get_leave_types as $get_leave_type) {
		   	$get_current_date_leaves = $this->Global_model->get_leaves_count_by_date($get_leave_type->leave_type_id,date('Y-m-d'),date('Y-m-d'));
		   	if (count($get_current_date_leaves)) {
			  			$total_leaves+=$get_current_date_leaves->leaves_count;
			  			$get_current_date_leaves_string.= "<td>".$get_current_date_leaves->leaves_count."</td>";

		  	}
		  	else{
		  		$get_current_date_leaves_string.= "<td>"."0"."</td>";
		  	}
		  } 
		//$get_emp_leavemaster  = $this->Global_model->get_emp_leavemaster($id);
		$data=array('current_page'=>'Attendance Marking','page_title'=>'Attendance Marking','parent_menu'=>'','template'=>'masters/attendance_marking/add','get_attendance_marking'=>$get_attendance_marking,'get_attendance_marking_log'=>$get_attendance_marking_log,'get_employees'=>$get_employees,'get_holidays'=>$get_holidays,"get_leave_types"=>$get_leave_types,"get_current_date_leaves"=>$get_current_date_leaves,"get_current_date_leave_emps"=>$get_current_date_leave_emps,"get_current_date_leaves_string"=>$get_current_date_leaves_string);
		$this->form_validation->set_rules('emp_name','emp_name','required');
		
		if($this->form_validation->run() === FALSE)
		{
			//print_r($data);
			$this->load->view('theme/template',$data);
		}
		else
		{	
			// $emp_id  = $this->session->userdata('emp_id');
			// $emp_bid = $this->input->post('emp_bid');
			// $leave_date = $this->input->post('leave_date');
			// //$leave_master_id = $this->input->post('leave_master_id');
			// $leave_type_id=$this->input->post('leave_type_id');
			// $no_leaves = $this->input->post('no_leaves');
			// $lemp_id = $this->input->post('emp_id');
			// $this->db->trans_begin();
			// $insert_array = array('emp_bid'=>$emp_bid, 'leave_date'=>$leave_date, 'leave_type_id'=>$leave_type_id,'no_leaves'=>1,'emp_id'=>$lemp_id);
			
			// $this->all_actions('insert',$insert_array);
			// if($this->db->trans_status()===FALSE)
			// {
			// 	$this->db->trans_rollback();
			// 	$this->session->set_userdata(array('create_status'=>'failed'));
			// 	$this->session->set_userdata(array('record_name'=>'Attendance Marking not inserted '));	
			// }
			// else
			// {
			// 	$this->db->trans_commit();
			// 	$this->session->set_userdata(array('create_status'=>'success'));
			// 	$this->session->set_userdata(array('record_name'=>'Attendance Marking inserted successfully'));
			// }
			// $get_leave_types  = $this->Global_model->get_leave_types();
			// $data['get_leave_types'] = $get_leave_types;
			redirect('attendance_marking');
			//print_r($insert_array);
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('attendance_marking');
		$where = array('leave_master_id'=>base64_decode($id));
		$leave_master_id = base64_decode($id);
		$getAttendanceMarkingEdit = $this->Global_model->get_leave_master_single($leave_master_id);
		$get_attendance_marking = $this->Global_model->get_attendance_marking();
		$get_attendance_marking_log = $this->Global_model->get_attendance_marking_log();
		$get_current_date_leave_emps=$this->Global_model->get_current_date_leave_emps();

		$get_employees = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
		$get_holidays = $this->Global_model->get_values_where($this->config->item('holidays'),array('status'=>1));
		$data=array('current_page'=>'Attendance Marking','page_title'=>'Attendance Marking','parent_menu'=>'','template'=>'masters/attendance_marking/add','get_attendance_marking'=>$get_attendance_marking,'getAttendanceMarkingEdit'=>$getAttendanceMarkingEdit,'get_attendance_marking_log'=>$get_attendance_marking_log,'get_employees'=>$get_employees,'get_holidays'=>$get_holidays);
		$this->form_validation->set_rules('emp_name','emp_name','required');
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves = $this->Global_model->get_current_date_leaves_count();
		$data['get_current_date_leave_emps']=$get_current_date_leave_emps;
		$data['get_leave_types'] = $get_leave_types;
		$data['get_current_date_leaves'] = $get_current_date_leaves;
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_bid');
			$activity_id = $this->input->post('activity_id');
			$leave_type_id = $this->input->post('leave_type_id');
			$no_leaves = $this->input->post('no_leaves');
			$calendar_year = $this->input->post('calendar_year');
			$lemp_id = $this->input->post('emp_id');
			$this->db->trans_begin();
			$edit_array = array('activity_id'=>$activity_id,'emp_bid'=>$emp_bid, 'leave_type_id'=>$leave_type_id, 'no_leaves'=>$no_leaves,'emp_id'=>$lemp_id,'attendance_id'=>$attendance_id,'calendar_year'=>$calendar_year);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking updated successfully'));
			}
			redirect('attendance_marking');
		}
		
	}
	function delete($id)
	{
			$attendance_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('attendance_id'=>$attendance_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking  Deleted successfully'));
		}
		redirect('attendance_marking');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$attendance_id = base64_decode($id);
				$undo_array = array('attendance_id'=>$attendance_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Attendance Marking not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Attendance Marking undo successfully'));
				}
		
		redirect('attendance_marking');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$emp_bid=$insert_array['emp_bid'];
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$get_employee_data = $this->Global_model->get_values_where_single($this->config->item('employee'),array('status'=>1,"emp_bid"=>$emp_bid));
			$emp_holiday_group_id=$get_employee_data->holiday_group_id;
			$get_lop_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"LOP"));
			$get_od_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"OD"));
			$get_h_lop_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"H.LOP"));
			$get_cl_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"CL"));
			$get_h_cl_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"H.CL"));
			$get_el_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"EL"));
			$get_h_el_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"H.EL"));
			$get_sl_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"SL"));
			$get_h_sl_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"H.SL"));
			$get_leave_date_data=$this->Global_model->get_values_where_single($this->config->item('attendance_marking'),array('emp_bid'=>$emp_bid,"leave_date"=>$insert_array['leave_date_from']));
			$get_holiday_data=$this->db->query("SELECT * FROM holidays WHERE from_date<='".$insert_array['leave_date_from']."' and to_date >= '".$insert_array['leave_date_from']."' and holiday_group_id=".$emp_holiday_group_id)->row();

			$day_name=date('l',strtotime($insert_array['leave_date_from']));
			/*if ($day_name=="Sunday" && false) {
				return json_encode(array("status"=>false,"msg"=>"Selected Date is Sunday"));
			}
			else if (!empty($get_holiday_data) && false) {
				return json_encode(array("status"=>false,"msg"=>"Selected Date is Holiday($get_holiday_data->holiday_name)"));
			}
			else*/ if (!empty($get_leave_date_data)) {
				$get_type_data=$this->Global_model->get_values_where_single($this->config->item('leave_types'),array('leave_type_id'=>$get_leave_date_data->leave_type_id));
				return json_encode(array("status"=>false,"msg"=>"Already $get_type_data->leave_type applied for this date"));
			}
			if ($get_lop_type->leave_type_id==$insert_array['leave_type_id'] || $get_h_lop_type->leave_type_id==$insert_array['leave_type_id']) {
				foreach ($insert_array['leave_dates'] as $leaves_date) {
					$param= array('attendance_id'=>$this->Global_model->Max_count($this->config->item('attendance_marking'),'attendance_id'),'emp_bid'=>$insert_array['emp_bid'],'emp_id'=>$insert_array['emp_id'],'leave_date'=>$leaves_date,'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>1,"status"=>1,"date"=>date('Y-m-d'),'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
						$this->Myfunctions->addRecord("attendance_marking",$param);
						$month=date('m',strtotime($leaves_date));
						$year=date("Y",strtotime($leaves_date));
						$lop_data=$this->Myfunctions->getData("emp_lop_calculation","emp_bid=".$insert_array['emp_bid']." and lop_month=".$month."  and lop_year=".$year);
						if (!empty($lop_data)) {
							if ($get_lop_type->leave_type_id==$insert_array['leave_type_id']) {
								$lop=$lop_data->lop_count+1;
							}
							else{
								$lop=$lop_data->lop_count+0.5;
							}
							$data1['lop_count']=$lop;
		            		$data1['updated_date']=date("Y-m-d H:i:s");
		            		$data1['updated_by']=$emp_id;
		            		//$this->Myfunctions->updateRecord("emp_lop_calculation",$data1,"emp_bid=".$insert_array['emp_bid']." and lop_month=".$month." and lop_year=".$year);
						}
						else{
							$data1['emp_bid']=$insert_array['emp_bid'];
							$data1['lop_month']=$month;
							$data1['lop_year']=$year;
							$data1['created_by']=$emp_id;
							if ($get_lop_type->leave_type_id==$insert_array['leave_type_id']) {
								$data1['lop_count']=1;
							}
							else{
								$data1['lop_count']=0.5;
							}
		            		$data1['updated_date']=date("Y-m-d H:i:s");
		            		$data1['updated_by']=$emp_id;
							//$this->Myfunctions->addRecord('emp_lop_calculation',$data1);
						}
				}
				
				return json_encode(array("status"=>true,"msg"=>"Success"));
			}
			else if ($get_cl_type->leave_type_id==$insert_array['leave_type_id'] || $get_h_cl_type->leave_type_id==$insert_array['leave_type_id']) {
				$cls_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='CL' and lm.`emp_bid` = '".$emp_bid."'")->row();
				if (!empty($cls_data) && $cls_data->no_leaves>0 && $cls_data->no_leaves>=$insert_array['no_leaves']) {
					foreach ($insert_array['leave_dates'] as $leaves_date) {
						$param= array('attendance_id'=>$this->Global_model->Max_count($this->config->item('attendance_marking'),'attendance_id'),'emp_bid'=>$insert_array['emp_bid'],'emp_id'=>$insert_array['emp_id'],'leave_date'=>$leaves_date,'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>1,"status"=>1,"date"=>date('Y-m-d'),'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
						$this->Myfunctions->addRecord("attendance_marking",$param);
						if ($get_cl_type->leave_type_id==$insert_array['leave_type_id']) {
							$used=$cls_data->used+count($insert_array['leave_dates']);
						}
						else{
							$used=$cls_data->used+0.5;
						}
						
						$data1['no_leaves']=$cls_data->no_leaves-$insert_array['no_leaves'];
						$data1['used']=$used;
	            		$data1['Updated_Datetime']=date("Y-m-d H:i:s");
	            		$data1['Updated_By']=$emp_id;
	            		$this->Myfunctions->updateRecord("leave_master",$data1,"emp_bid=".$insert_array['emp_bid']." and leave_type_id=".$get_cl_type->leave_type_id);
            		}
            		return json_encode(array("status"=>true,"msg"=>"Success"));
				}
				else{
					return json_encode(array("status"=>false,"msg"=>"Employee don't Have Enough CLs Data"));
				}
			}
			else if ($get_el_type->leave_type_id==$insert_array['leave_type_id'] || $get_h_el_type->leave_type_id==$insert_array['leave_type_id']) {
				$els_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='EL' and lm.`emp_bid` = '".$emp_bid."'")->row();
				if (!empty($els_data) && ($els_data->no_leaves)>0 && (($els_data->no_leaves)>=$insert_array['no_leaves'])) {
					foreach ($insert_array['leave_dates'] as $leaves_date) {
						$param= array('attendance_id'=>$this->Global_model->Max_count($this->config->item('attendance_marking'),'attendance_id'),'emp_bid'=>$insert_array['emp_bid'],'emp_id'=>$insert_array['emp_id'],'leave_date'=>$leaves_date,'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>1,"status"=>1,"date"=>date('Y-m-d'),'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
						$this->Myfunctions->addRecord("attendance_marking",$param);
						if ($get_el_type->leave_type_id==$insert_array['leave_type_id']) {
							$used=$els_data->used+count($insert_array['leave_dates']);
						}
						else{
							$used=$els_data->used+0.5;
						}
						$data1['no_leaves']=$els_data->no_leaves-$insert_array['no_leaves'];
						$data1['used']=$used;
	            		$data1['Updated_Datetime']=date("Y-m-d H:i:s");
	            		$data1['Updated_By']=$emp_id;
	            		$this->Myfunctions->updateRecord("leave_master",$data1,"emp_bid=".$insert_array['emp_bid']." and leave_type_id=".$get_el_type->leave_type_id);
	            	}
            		return json_encode(array("status"=>true,"msg"=>"Success"));
				}
				else{
					return json_encode(array("status"=>false,"msg"=>"Employee don't Have Enough ELs Data"));
				}
			}
			else if ($get_sl_type->leave_type_id==$insert_array['leave_type_id'] || $get_h_sl_type->leave_type_id==$insert_array['leave_type_id']) {

				$sls_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='SL' and lm.`emp_bid` = '".$emp_bid."'")->row();
				if (!empty($sls_data) && ($sls_data->no_leaves)>0 && (($sls_data->no_leaves)>=$insert_array['no_leaves'])) {
					foreach ($insert_array['leave_dates'] as $leaves_date) {
						$param= array('attendance_id'=>$this->Global_model->Max_count($this->config->item('attendance_marking'),'attendance_id'),'emp_bid'=>$insert_array['emp_bid'],'emp_id'=>$insert_array['emp_id'],'leave_date'=>$leaves_date,'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>1,"status"=>1,"date"=>date('Y-m-d'),'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
						$this->Myfunctions->addRecord("attendance_marking",$param);
						if ($get_sl_type->leave_type_id==$insert_array['leave_type_id']) {
							$used=$sls_data->used+count($insert_array['leave_dates']);
						}
						else{
							$used=$sls_data->used+0.5;
						}
						$data1['no_leaves']=$sls_data->no_leaves-$insert_array['no_leaves'];
						$data1['used']=$used;
	            		$data1['Updated_Datetime']=date("Y-m-d H:i:s");
	            		$data1['Updated_By']=$emp_id;
	            		$this->Myfunctions->updateRecord("leave_master",$data1,"emp_bid=".$insert_array['emp_bid']." and leave_type_id=".$get_sl_type->leave_type_id);
            		}
            		return json_encode(array("status"=>true,"msg"=>"Success"));
				}
				else{
					return json_encode(array("status"=>false,"msg"=>"Employee don't Have Enough SLs Data"));
				}
			}
			else if ($get_od_type->leave_type_id==$insert_array['leave_type_id']) {
				foreach ($insert_array['leave_dates'] as $leaves_date) {
					$param= array('attendance_id'=>$this->Global_model->Max_count($this->config->item('attendance_marking'),'attendance_id'),'emp_bid'=>$insert_array['emp_bid'],'emp_id'=>$insert_array['emp_id'],'leave_date'=>$leaves_date,'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>1,"status"=>1,"date"=>date('Y-m-d'),'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
					$this->Myfunctions->addRecord("attendance_marking",$param);
				}
            		return json_encode(array("status"=>true,"msg"=>"Success"));
				
			}
			else if ($insert_array['leave_type_id']==12) {
				foreach ($insert_array['leave_dates'] as $leaves_date) {
					$param= array('attendance_id'=>$this->Global_model->Max_count($this->config->item('attendance_marking'),'attendance_id'),'emp_bid'=>$insert_array['emp_bid'],'emp_id'=>$insert_array['emp_id'],'leave_date'=>$leaves_date,'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>1,"status"=>1,"date"=>date('Y-m-d'),'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
					$this->Myfunctions->addRecord("attendance_marking",$param);
				}
            		return json_encode(array("status"=>true,"msg"=>"Success"));
			}
			else {
				return json_encode(array("status"=>false,"msg"=>"Invalid Leave type"));
			}
			//$this->Global_model->insert_data($param);
			/*$this->Global_model->empleaveupdate($insert_array['emp_id'],$insert_array['leave_master_id'],$insert_array['no_leaves']);*/
		}
		else if($action == "edit")
		{
  		    $attendance_id = $insert_array['attendance_id'];
			$data1 = array('attendance_id'=>$attendance_id,'emp_id'=>$insert_array['emp_id'],'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>$insert_array['no_leaves'],'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('attendance_id'=>$attendance_id);
			$this->db->query("INSERT INTO `attendance_marking_log`(`attendance_id`, `emp_id`, `leave_type_id`, `no_leaves`, `calendar_year`,`date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (SELECT attendance_id, emp_id, leave_type_id, no_leaves, calendar_year, date, '1', 'EDIT', Created_By, Created_Datetime, Updated_By, Updated_Datetime FROM attendance_marking WHERE attendance_id= ".$attendance_id.")");
			$this->Global_model->update($this->config->item('attendance_marking'),$data1,$where);
		}
		else if($action == 'delete') {
			$attendance_id = $insert_array['attendance_id'];
			$this->db->query("INSERT INTO `attendance_marking_log`(`attendance_id`, `emp_id`, `leave_type_id`, `no_leaves`,  `calendar_year`,`date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (SELECT attendance_id, emp_id, leave_type_id, no_leaves,calendar_year, date, '2', 'DELETE', $emp_id, now(), $emp_id, now() FROM attendance_marking WHERE attendance_id= ".$attendance_id.")");
			$this->db->query("DELETE FROM ".$this->config->item('attendance_marking')." WHERE attendance_id =".$attendance_id);
		}
		else if($action == 'undo') {
			$attendance_id = $insert_array['attendance_id'];
			$where = array('attendance_id'=>$attendance_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('attendance_marking_log')." WHERE attendance_id=".$attendance_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('emp_id'=>$getPaymentTypes->emp_id,'leave_type_id'=>$getPaymentTypes->leave_type_id,'no_leaves'=>$getPaymentTypes->no_leaves,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('attendance_marking'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Attendance Marking not Undo '));	
					redirect('attendance_marking');
			}
			
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('attendance_marking');
		}	
	}

	function AddAttendaceMarking()
	{
		$emp_id  = $this->session->userdata('emp_id');
		$emp_bid = $this->input->post('emp_bid');
		$get_employee_data = $this->Global_model->get_values_where_single($this->config->item('employee'),array('status'=>1,"emp_bid"=>$emp_bid));
		$emp_holiday_group_id=$get_employee_data->holiday_group_id;

		//$leave_date = $this->input->post('leave_date');
		//$leave_master_id = $this->input->post('leave_master_id');
		$leave_type_id=$this->input->post('leave_type_id');
		$leave_date_from = date('Y-m-d',strtotime($this->input->post('leave_date_from')));
		$leave_date_to = date('Y-m-d',strtotime($this->input->post('leave_date_to')));

		$no_leaves = $this->input->post('no_leaves');
		$lemp_id = $this->input->post('emp_id');
		$this->db->trans_begin();
		$dates=array();
		//$insert_array = array('emp_bid'=>$emp_bid, 'leave_date'=>$leave_date, 'leave_type_id'=>$leave_type_id,'no_leaves'=>1,'emp_id'=>$lemp_id);
		$days_between = ceil(($leave_date_to - $leave_date_from) / 86400);
		
		if ($days_between>=0) {
			$period = new DatePeriod(
		     new DateTime($leave_date_from),
		     new DateInterval('P1D'),
		     new DateTime($leave_date_to)
			);
			$no_leaves_orignal=1;
			foreach ($period as $key => $value) {
			    $date_of_leave=$value->format('Y-m-d');  
			    $get_holiday_data=$this->db->query("SELECT * FROM holidays WHERE from_date<='".$date_of_leave."' and to_date >= '".$date_of_leave."' and holiday_group_id=".$emp_holiday_group_id)->row();

			    $day_name=date('l',strtotime($date_of_leave));
			    // if ($day_name=="Sunday") {
			    // 	$no_leaves_orignal=$no_leaves_orignal;
			    // }
			    // else if ($get_holiday_data) {
			    // 	$no_leaves_orignal=$no_leaves_orignal;
			    // } 
			    // else{
			    	$dates[]=$date_of_leave;
			    	$no_leaves_orignal+=1;
			    //}  
			}
			$get_holiday_data=$this->db->query("SELECT * FROM holidays WHERE from_date<='".$leave_date_to."' and to_date >= '".$leave_date_to."' and holiday_group_id=".$emp_holiday_group_id)->row();

/*			    $day_name=date('l',strtotime($leave_date_to));
			    if ($day_name=="Sunday") {
			    }
			    else if ($get_holiday_data) {
			    } 
			    else{*/
			    	array_push($dates,$leave_date_to);
			   // }
			
			$insert_array = array('emp_bid'=>$emp_bid,"leave_dates"=>$dates, 'leave_date_from'=>$leave_date_from,'leave_date_to'=>$leave_date_to, 'leave_type_id'=>$leave_type_id,'no_leaves'=>$no_leaves_orignal,'emp_id'=>$lemp_id);
			echo $this->all_actions('insert',$insert_array);
		}
		else{
			echo json_encode(array("msg"=>"Selected Dates are Invalied","status"=>false));
		}
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking not inserted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking inserted successfully'));
		}
	}
	function getEmployeeDetails()
	{
		$emp_bid = $this->input->post('emp_bid');
		$get_attendance_markings = $this->db->query("SELECT * FROM `employee` e LEFT JOIN line_of_activity l ON e.`activity_id` = l.activity_id LEFT JOIN subject_details s ON e.subject_details_id = s.subject_details_id WHERE e.`emp_bid` = '".$emp_bid."'")->row();
		$get_cl_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='CL' and lm.`emp_bid` = '".$emp_bid."'")->row();
		$get_el_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='EL' and lm.`emp_bid` = '".$emp_bid."'")->row();
		$get_sl_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='SL' and lm.`emp_bid` = '".$emp_bid."'")->row();

		$data['emp_name'] = $get_attendance_markings->emp_name;
		$data['dept_name'] = $get_attendance_markings->dept_name;
		$data['sub_name'] = $get_attendance_markings->sub_name;
		$data['emp_id'] = $get_attendance_markings->emp_id;
		if (!empty($get_cl_data)) {
			$data['cl']['opb']=$get_cl_data->no_leaves+$get_cl_data->used;
			$data['cl']['used']=$get_cl_data->used;
			$data['cl']['clb']=$get_cl_data->no_leaves;
			
		}
		else{
			$data['cl']['opb']=0;
			$data['cl']['used']=0;
			$data['cl']['clb']=0;
		}
		if (!empty($get_sl_data)) {
			$data['sl']['opb']=$get_sl_data->no_leaves+$get_sl_data->used;
			$data['sl']['used']=$get_sl_data->used;
			$data['sl']['clb']=$get_sl_data->no_leaves;
			
		}
		else{
			$data['sl']['opb']=0;
			$data['sl']['used']=0;
			$data['sl']['clb']=0;
		}
		if (!empty($get_el_data)) {
			$data['el']['opb']=$get_el_data->no_leaves+$get_el_data->used;
			$data['el']['used']=$get_el_data->used;
			$data['el']['clb']=$get_el_data->no_leaves;
			
		}
		else{
			$data['el']['opb']=0;
			$data['el']['used']=0;
			$data['el']['clb']=0;
		}
		$year=date("Y");
		$get_emp_leavemaster  = $this->Global_model->get_leave_types();
		$get_emp_leavemaster_data = "";
		foreach($get_emp_leavemaster as $row)
		{
			$no_leaves=$row->no_leaves;
			$used_count=$row->used_count;
			$remaining=$no_leaves-$used_count;
			$get_emp_leavemaster_data.= "<option value='".$row->leave_type_id."'>".$row->leave_type."</option>";
			//$get_emp_leavemaster_data .= "<option value='".$row->leave_type_id."'>".$row->leave_type."(".$no_leaves."/".$used_count."/".$remaining.")</option>";
		}
		$data['get_emp_leavemaster'] = $get_emp_leavemaster_data;
		
		$get_employees_leaves_count_types=$this->Global_model->get_employees_leaves_count_type($emp_bid,$year);
		$employee_leave_types_id="<th>Leave Type</th>";
		$total_leaves_id="<th>Total Leaves</th>";
		$remaining_leaves_id="<th>Used Leaves</th>";
		foreach ($get_employees_leaves_count_types as $get_employees_leaves_count_type) {
			$employee_leave_types_id.="<th>".$get_employees_leaves_count_type->leave_type."</th>";
			$total_leaves_id.="<th>".$get_employees_leaves_count_type->no_leaves."</th>";
			$remaining_leaves_id.="<th>".$get_employees_leaves_count_type->used_count."</th>";
		}
		$data['employee_leave_types_id'] = $employee_leave_types_id;
		$data['total_leaves_id'] = $total_leaves_id;
		$data['remaining_leaves_id'] = $remaining_leaves_id;
		echo json_encode($data);
	}
	function SummaryDetails()
	{
		$summary_date_from = $this->input->post('summary_date_from');
		$summary_date_to = $this->input->post('summary_date_to');
		//$summary_date=date("Y-m-d",$summary_date);
		
		$get_current_date_leave_emps=$this->Global_model->get_leave_emps_by_date_old($summary_date_from,$summary_date_to);
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves_string="";
			$total_leaves=0;
		   foreach ($get_leave_types as $get_leave_type) {
		   	$get_current_date_leaves = $this->Global_model->get_leaves_count_by_date($get_leave_type->leave_type_id,$summary_date_from,$summary_date_to);
		   	if (count($get_current_date_leaves)) {
			  			$total_leaves+=$get_current_date_leaves->leaves_count;
			  			$get_current_date_leaves_string.= "<td>".$get_current_date_leaves->leaves_count."</td>";

		  	}
		  	else{
		  		$get_current_date_leaves_string.= "<td>"."0"."</td>";
		  	}
		  } 
		$get_current_date_leave_emps_string="";
		if (count($get_current_date_leave_emps)>0) {
		  	$l=1;
		  	foreach ($get_current_date_leave_emps as $get_current_date_leave_emp) {
		  		$get_current_date_leave_emps_string.= "<tr><td>".$l++."</td><td>".$get_current_date_leave_emp->emp_bid."</td><td>".$get_current_date_leave_emp->emp_name."</td><td>".$get_current_date_leave_emp->emp_id."</td><td>".$get_current_date_leave_emp->dept_name."</td><td>".$get_current_date_leave_emp->sub_name."</td><td class='text-left'>".$get_current_date_leave_emp->leave_date."</td><td>".$get_current_date_leave_emp->leave_type."</td></tr>";
		  	}
		}
		else{
			$get_current_date_leave_emps_string.= "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
		}
		$data['get_current_date_leave_emps']=$get_current_date_leaves_string;
		$data['get_current_date_leaves'] = $get_current_date_leave_emps_string;
		echo json_encode($data);
	}
}
