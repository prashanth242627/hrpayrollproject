<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';

class ProbationPeriod extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_leave_model');
		$this->load->model('Dept_Salaries_model');
		$this->load->model("Myfunctions");
	}
	public function index()
	{
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		$end = date('Y-m-d', strtotime('-1 years'));
		$employees=$this->Myfunctions->getQueryDataList('SELECT e.*,loa.dept_name,d.designation_name FROM `employee`e left join line_of_activity loa on loa.activity_id=e.activity_id left join designation d on d.designation_id=e.designation WHERE e.status=1 and e.regularize_status=0 order by LENGTH(e.emp_bid),e.emp_bid asc');
		//$employees=$this->Myfunctions->getQueryDataList('SELECT e.*,loa.dept_name,d.designation_name FROM `employee`e left join line_of_activity loa on loa.activity_id=e.activity_id left join designation d on d.designation_id=e.designation WHERE e.status=1 and e.date_of_join>="'.$end.'" order by LENGTH(e.emp_bid),e.emp_bid asc');
		$data=array('current_page'=>'Probation Period','page_title'=>'Probation Period','parent_menu'=>'','template'=>'probationperiod');
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['employees']=$employees;
		$this->load->view('theme/template',$data);
	}
	public function Regularize($id)
	{
		$getCLsData=$this->Myfunctions->getData('leave_master',"emp_bid=$id and leave_type_id=5");
		if ($getCLsData) {
			$no_leaves=12-date('m');
			$data['no_leaves']=$getCLsData['no_leaves']+$no_leaves;
			$getCLsData=$this->Myfunctions->updateRecord('leave_master',$data,"emp_bid=$id and leave_type_id=5");
		}
		else{
			$no_leaves=12-date('m');
			$data['no_leaves']=$no_leaves;
			$data['leave_type_id']=5;
			$data['emp_bid']=$id;
			$data['used']=0;
			$data['calendar_year']=date('Y');
			$data['date']=date("Y-m-d");
			$data['Created_By']=1;
			$data['Created_Datetime']=date("Y-m-d H:i:s");
			$data['Updated_By']=1;
			$data['Updated_Datetime']=date("Y-m-d H:i:s");
			$getCLsData=$this->Myfunctions->addRecord('leave_master',$data);
		}
		$e_data['regularize_status']=1;
		$this->Myfunctions->updateRecord('employee',$e_data,'emp_bid='.$id);
		$this->session->set_userdata(array('create_status'=>'success'));
		$this->session->set_userdata(array('record_name'=>'Employee Regularized'));	
		redirect('ProbationPeriod');
	}

}
