<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Reportingslots extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_reportingslots = $this->Global_model->get_values_where($this->config->item('reportingslots'),array('status'=>1));
		$get_reportingslots_log = $this->Global_model->get_values_where($this->config->item('reportingslots_log'), array('flag'=>2));
		$data=array('current_page'=>'Reporting Slots','page_title'=>'Reporting Slots','parent_menu'=>'','template'=>'masters/reportingslots/add','get_reportingslots'=>$get_reportingslots,'get_reportingslots_log'=>$get_reportingslots_log);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('from_time','from_time','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$from_time = $this->input->post('from_time');
			$to_time = $this->input->post('to_time');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('from_time'=>$from_time,'to_time'=>$to_time, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Reporting Slots not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Reporting Slots inserted successfully'));
			}
			redirect('Reportingslots');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('reportingslots');
		$where = array('reportingslots_id'=>base64_decode($id));
		$reportingslots_id = base64_decode($id);
		$getPpaymentTypeEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_reportingslots = $this->Global_model->get_values_where($this->config->item('reportingslots'),array('status'=>1));
		$get_reportingslots_log = $this->Global_model->get_values_where($this->config->item('reportingslots_log'), array('flag'=>2));
			$data=array('current_page'=>'Reporting Slots','page_title'=>'Reporting Slots','parent_menu'=>'','template'=>'masters/reportingslots/add','get_reportingslots'=>$get_reportingslots,'getPpaymentTypeEdit'=>$getPpaymentTypeEdit,'get_reportingslots_log'=>$get_reportingslots_log);
		$this->form_validation->set_rules('from_time','from_time','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$from_time = $this->input->post('from_time');
			$to_time = $this->input->post('to_time');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('from_time'=>$from_time,'to_time'=>$to_time, 'status'=>$status,'reportingslots_id'=>$reportingslots_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Reporting Slots not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Reporting Slots updated successfully'));
			}
			redirect('Reportingslots');
		}
		
	}
	function delete($id)
	{
			$reportingslots_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('reportingslots_id'=>$reportingslots_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Reporting Slots not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Reporting Slots  Deleted successfully'));
		}
		redirect('Reportingslots');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$reportingslots_id = base64_decode($id);
				$undo_array = array('reportingslots_id'=>$reportingslots_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Reporting Slots not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Reporting Slots undo successfully'));
				}
		
		redirect('Reportingslots');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$from_time = $insert_array['from_time'];
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$to_time = $insert_array['to_time'];
			$param['insertdata'] = array('reportingslots_id'=>$this->Global_model->Max_count($this->config->item('reportingslots'),'reportingslots_id'),'from_time'=>$from_time,'to_time'=>$insert_array['to_time'],'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('reportingslots');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			$to_time = $insert_array['to_time'];
			$reportingslots_id = $insert_array['reportingslots_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('reportingslots'), array('reportingslots_id'=>$reportingslots_id));
				
				$param['insertdata'] = array('reportingslots_id'=>$insertdata->reportingslots_id,'from_time'=>$insertdata->from_time,'to_time'=>$insertdata->to_time,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('reportingslots_log');
			/* Update log table end */
			$data1 = array('reportingslots_id'=>$reportingslots_id,'from_time'=>$from_time,'to_time'=>$to_time,'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('reportingslots_id'=>$reportingslots_id);
			$this->Global_model->update($this->config->item('reportingslots'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$reportingslots_id = $insert_array['reportingslots_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('reportingslots'), array('reportingslots_id'=>$reportingslots_id));
				
				$param['insertdata'] = array('reportingslots_id'=>$insertdata->reportingslots_id,'from_time'=>$insertdata->from_time,'to_time'=>$insert_array['to_time'],'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('reportingslots_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('reportingslots')." WHERE reportingslots_id =".$reportingslots_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$reportingslots_id = $insert_array['reportingslots_id'];
			$where = array('reportingslots_id'=>$reportingslots_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('reportingslots_log')." WHERE reportingslots_id=".$reportingslots_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('reportingslots_id'=>$reportingslots_id,'from_time'=>$getPaymentTypes->from_time,'to_time'=>$getPaymentTypes->to_time,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('reportingslots'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Reporting Slots not Undo '));	
					redirect('Reportingslots');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Reportingslots');
		}	
	}
}
