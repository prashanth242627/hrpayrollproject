<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class SalariesCalculation extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		$this->load->model('Myfunctions');
	}
	public function index()
	{
		//$get_emp_leavemaster  = $this->Global_model->get_emp_leavemaster($id);
		$data=array('current_page'=>'EMP Salaries Calculation','page_title'=>'EMP Salaries Calculation','parent_menu'=>'','template'=>'masters/emp_monthly_salaries/add');
		$this->form_validation->set_rules('year', 'YEAR', 'required'); 
		$this->form_validation->set_rules('month', 'MONTH', 'required'); 
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$year = $this->input->post('year');
			$month = $this->input->post('month');
			$this->db->trans_begin();
			$insert_array = array('year'=>$year, 'month'=>$month,'emp_id'=>$emp_id);
			
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'failed to Calculate Salaries '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Success'));
			}
			redirect('SalariesCalculation');
			//print_r($insert_array);
		} 
		
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$month = $insert_array['month'];
			$year=$insert_array['year'];
			$month_no=date("m",strtotime($month));

      $last_record_of_month=$this->Myfunctions->getQueryData("SELECT * FROM `emp_lop_calculation` WHERE lop_month=".$month_no." and lop_year=".$year);
      if (count($last_record_of_month)) {
         $month_days=cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($month)),$year);
          if ($month_days>30) {
            $month_days=30;
          }
      $sql_query="select monsal.*, gross-(PF+ESI+PT+SD+tds) net_income 
from( select temp_emp_id, 
             emp_name, 
             sex Gender, 
             designation_name, 
             date_of_join, 
             acc_no, 
             pf_no, 
             esi_no, 
             uan, 
             Aadhar, 
             PAN, 
             ".$month_days." as TWD, 
             lop_count , 
             ".$month_days."-lop_count PAY_DAYS, 
             emp_bid, 
             basic_ctc, basic_earnings, 
             da_ctc, da_earnings, 
             incr_ctc,incr_earnings, 
             hra_ctc, hra_earnings, 
             cca_ctc, cca_earnings, 
             other_allowance_ctc, other_allowances_earnings, 
             co_allowance_ctc, co_allowance_earnings, 
             special_allowance_ctc, special_allowance_earnings, 
             m_gross FIXED_MONTHLY_CTC, 
             exmp_limit, 
             savings, 
             gross, 
             case when pf_consider = 1 and employment_type = 0 then case when (basic_earnings+da_earnings)* (select deduct_value from std_pf_deductions)/100 <=1800 
                  then round((basic_earnings+da_earnings)*(select deduct_value from std_pf_deductions)/100) 
                  else 1800 END else 0 end PF, 
             case when esi_consider = 1 and employment_type = 0 and m_gross<=21000 then round((m_gross)*esi.deduct_value/100) else 0 end ESI, 
             pt.deduct_value PT, 
             case when tax_gross < (exmp_limit+savings) then 0 else round(((tax_gross-(exmp_limit+savings))*tds.deduct_value/100)*12) END TDS, 
             case when (sdscope = 1 and DATE_ADD(NOW(), INTERVAL -10 MONTH) > date_of_join) then round(gross*10/100) else 0 end SD 
      from( select salary.*, 
                  (salary.basic_earnings+salary.other_allowances_earnings+salary.incr_earnings+salary.co_allowance_earnings+salary.da_earnings+ 
                   salary.special_allowance_earnings+salary.hra_earnings+salary.cca_earnings) gross 
            from ( select e.temp_emp_id, e.emp_name, e.sex, d.designation_name, e.emp_bid, e.date_of_join, pay.acc_no,pay.esi_no, pay.pf_no, pay.uan, pay.Aadhar, pay.PAN, 
                          pay.basic basic_ctc, pay.da da_ctc, pay.hra hra_ctc, pay.incr incr_ctc, 
                          pay.other_allowance other_allowance_ctc, pay.co_allowance co_allowance_ctc, 
                          pay.special_allowance special_allowance_ctc, pay.cca cca_ctc, 
                          (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca) m_gross, 
                          (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+(hra-(basic+da)*10/100)+
                          (case when cca> 0 then 19200-cca else 0 end)) tax_gross,
                         round(case when ifnull(lc.lop_count,0) <= 0 then (pay.basic) else (pay.basic)-((pay.basic/(".$month_days."))*lc.lop_count) end) basic_earnings, 
                         round(case when ifnull(lc.lop_count,0) <= 0 then (pay.other_allowance) else (pay.other_allowance)-((pay.other_allowance/(".$month_days."))*lc.lop_count) end) other_allowances_earnings, 
         round(case when ifnull(lc.lop_count,0) <= 0 then (pay.incr) else (pay.incr)-((pay.incr/(".$month_days."))*lc.lop_count) end) incr_earnings, 
        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.co_allowance) else (pay.co_allowance)-((pay.co_allowance/(".$month_days."))*lc.lop_count) end) co_allowance_earnings, 
        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.da) else (pay.da)-((pay.da/(".$month_days."))*lc.lop_count) end) da_earnings, 
        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.special_allowance) else (pay.special_allowance)-((pay.special_allowance/(".$month_days."))*lc.lop_count) end) special_allowance_earnings, 
        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.hra) else (pay.hra)-((pay.hra/(".$month_days."))*lc.lop_count) end) hra_earnings, 
        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.cca) else (pay.cca)-((pay.cca/( ".$month_days."))*lc.lop_count) end) cca_earnings, 
        esi_consider,is_pay_hold,pf_consider,sdscope,lc.lop_count, te.exmp_limit, employment_type,
        CASE when lc.lop_month between 4 and 12 then 175000 else (case when sm.sec_80c<150000 then sm.sec_80c else 150000 END +
                                                                  case when sm.sec_80d<25000 then sm.sec_80d else 25000 END ) end savings 
             from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid 
            LEFT JOIN designation d on e.designation = d.designation_id 
      LEFT JOIN std_tds_exemption te on case when e.sex='' then 'M' else e.sex END = te.sex 
      LEFT JOIN empsavingsmaster sm on e.emp_bid = sm.emp_bid 
      LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid and lc.lop_month=".date('m',strtotime($month))." and lc.lop_year = $year) salary 
    ) mon_sal 
    LEFT JOIN std_tds_deductions tds on (case when (m_gross-(exmp_limit+savings)) > 0 then (m_gross-(exmp_limit+savings)) else 0 end) between tds.min_limit and tds.max_limit 
    LEFT JOIN std_esi_deductions esi on round(m_gross) between esi.min_limit and esi.max_limit 
    LEFT JOIN std_pt_deductions pt on gross between pt.min_limit and pt.max_limit)monsal";

                $reports=$this->Myfunctions->getQueryDataList($sql_query);
                if (count($reports)>0) {
                foreach ($reports as $report) {
                  $get_management_deductions=array();
                  $get_management_deductions=$this->Myfunctions->getData("management_deductions","emp_bid=".$report['emp_bid']." and deduction_month=".$month_no."  and deduction_year=".$year);
                  $salary_data=$this->Myfunctions->getData("emp_monthly_salaries","emp_bid=".$report['emp_bid']." and salary_month=".$month_no."  and salary_year=".$year);
                  $data1['emp_bid']=$report['emp_bid'];
                  $data1['salary_month']=$month_no;
                  $data1['salary_year']=$year;
                  $data1['twd']=$report['TWD'];
                  $data1['lop_count']=$report['lop_count'];
                  $data1['pay_days']=$report['PAY_DAYS'];
                  $data1['basic_ctc']=$report['basic_ctc'];
                  $data1['basic_earnings']=$report['basic_earnings'];
                  $data1['da_ctc']=$report['da_ctc'];
                  $data1['da_earnings']=$report['da_earnings'];
                  $data1['incr_ctc']=$report['incr_ctc'];
                  $data1['incr_earnings']=$report['incr_earnings'];
                  $data1['hra_ctc']=$report['hra_ctc'];
                  $data1['hra_earnings']=$report['hra_earnings'];
                  $data1['cca_ctc']=$report['cca_ctc'];
                  $data1['cca_earnings']=$report['cca_earnings'];
                  $data1['other_allowance_ctc']=$report['other_allowance_ctc'];
                  $data1['other_allowance_earnings']=$report['other_allowances_earnings'];
                  $data1['co_allowance_ctc']=$report['co_allowance_ctc'];
                  $data1['co_allowance_earnings']=$report['co_allowance_earnings'];
                  $data1['special_allowance_ctc']=$report['special_allowance_ctc'];
                  $data1['special_allowance_earnings']=$report['special_allowance_earnings'];
                  $data1['fixed_monthly_ctc']=$report['FIXED_MONTHLY_CTC'];
                  $data1['exmp_limit']=$report['exmp_limit'];
                  $data1['savings']=$report['savings'];
                  $data1['gross']=$report['gross'];
                  $data1['pf']=$report['PF'];
                  $data1['esi']=$report['ESI'];
                  $data1['pt']=$report['PT'];
                  $data1['tds']=$report['TDS'];
                  if ($get_management_deductions) {
                    $data1['management_deductions']=$get_management_deductions['amount'];
                    $data1['remarks']=$get_management_deductions['remarks'];
                     
                  }
                  else{
                    $data1['management_deductions']=0;
                    $data1['remarks']=$get_management_deductions['remarks'];
                    
                  }
                  $data1['sd']=$report['SD'];
                  $data1['net_income']=$report['net_income'];
                  
                  if (!empty($salary_data)) {
                    $data1['updated_date']=date("Y-m-d h:i:s");
                    $this->Myfunctions->updateRecord("emp_monthly_salaries",$data1,"emp_bid=".$report['emp_bid']." and salary_month=".$month_no."  and salary_year=".$year);
                  }
            else{
              $data1['created_date']=date("Y-m-d h:i:s");
              $data1['updated_date']=date("Y-m-d h:i:s");
              $this->Myfunctions->addRecord('emp_monthly_salaries',$data1);
            }
                }
                
      }
    }
    else{
      $this->session->set_userdata(array('create_status'=>'failed'));
      $this->session->set_userdata(array('record_name'=>'Please Calculate Employee LOPS'));
      redirect('SalariesCalculation'); 
    }
			
    }
		
		else {
						$this->session->set_userdata(array('create_status'=>'failed'));
						$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
						redirect('SalariesCalculation');
				}	
	}
	public function activity()
	{
		$month=$this->input->get('month');
		$month_no=date("m",strtotime($month));
		$year=$this->input->get('year');
		$sql_query="select e.emp_bid,e.temp_emp_id,e.emp_name,d.designation_name,loa.dept_name,lop.lop_month,lop.lop_count,lop.lop_year from emp_emp_monthly_salaries lop left JOIN  employee e on e.emp_bid= lop.emp_bid left JOIN line_of_activity loa on e.activity_id=loa.activity_id left JOIN designation d on e.designation=d.designation_id where lop.lop_month=".$month_no." and lop.lop_year=".$year." and lop.lop_count<>0.0 order by emp_bid asc ";
		$lops_data=$this->Myfunctions->getQueryDataList($sql_query);
		$data=array('current_page'=>'EMP LOP Calculation','page_title'=>'EMP LOP Calculation','parent_menu'=>'','template'=>'masters/emp_monthly_salaries/add',"lops_data"=>$lops_data);
		$this->load->view('theme/template',$data);
	}
	
}
