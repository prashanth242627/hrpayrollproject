<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Global_System extends CI_Controller {
		
		//datetime functions
		public function __construct() {
			parent::__construct();
			date_default_timezone_set("Asia/Kolkata"); 
		}
		public function localdate($data)
		{ 
			
			if($data="Y-m-d H:i:s")
			{
				return date("Y-m-d H:i:s");
			}
			if($data="Y-m-d")
			{
				return date("Y-m-d");
			}
		}
		
		//login session check  method
        public function logincheck() 
        {
			if($this->session->userdata('emp_id')==TRUE)
			{
				return TRUE;
			}
			else
			{
				redirect('login');
			}
		}
		public function menu_data($org_id,$role_id) 
        {
			$user_type= $this->session->userdata('cozy_User_Type');
			if($user_type==1){
				$User_Type= "b.User_Type = $user_type";
			}
			elseif($user_type==2){
				$User_Type= "b.User_Type IN (1,2)";
			}
			elseif($user_type==3){
				$User_Type= "b.User_Type IN (1,2,3)";
			}
            if($user_type==1){ 
				$query="SELECT b.* 
				FROM  role_permissions a join menu_name b ON a.menu_code=b.menu_code
				WHERE a.Org_ID ='$org_id' AND a.role_id = '$role_id' AND ".$User_Type."  AND b.status='1' group by b.id ORDER BY b.sort_order";
			}
			else{
				$query="SELECT b.* 
				FROM  menu_name b 
				WHERE ".$user_type." AND b.status='1' group by b.id ORDER BY b.sort_order";
			}
			$res=$this->db->query($query);
			if($res ->num_rows()> 0)
			{
				return $res->result();
			}
			else
			{
				return array();
			}
			return array();
		}
		
		//logout method        
        public function logout()
		{
            $this->session->sess_destroy();
            redirect(base_url());
		}
		
		
	}	