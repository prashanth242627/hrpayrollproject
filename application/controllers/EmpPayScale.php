<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class EmpPayScale extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		$this->load->model('PayScale_model');
	}
	public function index()
	{	
		//$this->output->enable_profiler(TRUE);
		$get_employee= $this->Global_model->get_values($this->config->item('employee'));
		$payTypes = $this->Global_model->get_values($this->config->item('payment_types'));
		$data=array('current_page'=>'StanderdDeductions ','page_title'=>'Employee PayScale Master','parent_menu'=>'','template'=>'masters/payscale/EmpPayscaleMaster','get_employee'=>$get_employee,'payTypes'=>$payTypes);

		$this->load->view('theme/template',$data);
		
	}

	function  payscaleReadonly(){
		$get_employee= $this->Global_model->get_values($this->config->item('employee'));
		$payTypes = $this->Global_model->get_values($this->config->item('payment_types'));
		$data=array('current_page'=>'StanderdDeductions ','page_title'=>'Employee PayScale Master','parent_menu'=>'','template'=>'masters/payscale/EmpPayscaleMasterReadonly','get_employee'=>$get_employee,'payTypes'=>$payTypes);
		$this->load->view('theme/template',$data);
	}

	function add()
	{
		 
			if($this->input->post('employeeadd') == "go")
			{
				$this->EmppayscalBulkupload();
			}

		$emp_bid = $this->input->post('ebid');
		$pay_type = $this->input->post('pmt_type');
		$acc_no = $this->input->post('accno');
		$uan = $this->input->post('uan');
		$pf_no = $this->input->post('pf');
		$esi_no = $this->input->post('esi');
		$basic = $this->input->post('basic');
		$other_allowance = $this->input->post('oa');
		$incr = $this->input->post('incr');
		$co_allowance = $this->input->post('co_allow');
		$da = $this->input->post('da');
		$special_allowance = $this->input->post('spa');
		$hra = $this->input->post('hra');
		$bonus = $this->input->post('bonus');
		$cca = $this->input->post('cca');
		$standard_instalmemt = $this->input->post('standard_instalmemt');
		$inst_paid = $this->input->post('inst_paid');
		$esi_consider = ($this->input->post('esi_consider'))?1:0;
		$is_pay_hold = ($this->input->post('is_pay_hold'))?1:0;
		$sdscope = ($this->input->post('sdscope'))?1:0;
		$pf_consider = ($this->input->post('pf_consider'))?1:0;
		$param['table'] = $this->config->item('employee_pay_details');
		$whereCond = array('emp_bid'=>$emp_bid);
		$checkData = $this->Global_model->get_values_where_single($this->config->item('employee_pay_details'),$whereCond);
		if(empty($checkData)){
			$insertData = array('emp_bid' =>$emp_bid,
							'payment_type_id' =>$pay_type,
							'acc_no' =>$acc_no,
							'uan' =>$uan,
							'pf_no' =>$pf_no,
							'esi_no' =>$esi_no,
							'basic' =>$basic,
							'other_allowance' =>$other_allowance,
							'incr' =>$incr,
							'co_allowance' =>$co_allowance,
							'da' =>$da,
							'special_allowance' =>$special_allowance,
							'hra' =>$hra,
							'bonus' =>$bonus,
							'cca' =>$cca,
							'standard_instalmemt'=>$standard_instalmemt,
							'inst_paid'=>$inst_paid,
							'esi_consider'=>$esi_consider,
							'is_pay_hold'=>$is_pay_hold,
							'sdscope'=>$sdscope,
							'pf_consider'=>$pf_consider);
			$param['insertdata'] = $insertData;
			$this->Global_model->insert_data($param);
		}
		else
		{
			$insertData = array(
							'payment_type_id' =>$pay_type,
							'acc_no' =>$acc_no,
							'uan' =>$uan,
							'pf_no' =>$pf_no,
							'esi_no' =>$esi_no,
							'basic' =>$basic,
							'other_allowance' =>$other_allowance,
							'incr' =>$incr,
							'co_allowance' =>$co_allowance,
							'da' =>$da,
							'special_allowance' =>$special_allowance,
							'hra' =>$hra,
							'bonus' =>$bonus,
							'cca' =>$cca,
							'standard_instalmemt'=>$standard_instalmemt,
							'inst_paid'=>$inst_paid,
							'esi_consider'=>$esi_consider,
							'is_pay_hold'=>$is_pay_hold,
							'sdscope'=>$sdscope,
							'pf_consider'=>$pf_consider
						);
			$checkData1 = $this->Global_model->get_values_where_single($this->config->item('employee_pay_details_log'),$whereCond);
			if(!empty($checkData1)){
				$insertData1 = array(
							'payment_type_id' =>$checkData->payment_type_id,
							'acc_no' =>$checkData->acc_no,
							'uan' =>$checkData->uan,
							'pf_no' =>$checkData->pf_no,
							'esi_no' =>$checkData->esi_no,
							'basic' =>$checkData->basic,
							'other_allowance' =>$checkData->other_allowance,
							'incr' =>$checkData->incr,
							'co_allowance' =>$checkData->co_allowance,
							'da' =>$checkData->da,
							'special_allowance' =>$checkData->special_allowance,
							'hra' =>$checkData->hra,
							'bonus' =>$checkData->bonus,
							'cca' =>$checkData->cca,
							'standard_instalmemt'=>$standard_instalmemt,
							'inst_paid'=>$inst_paid,
							'esi_consider'=>$esi_consider,
							'is_pay_hold'=>$is_pay_hold,
							'sdscope'=>$sdscope,
							'pf_consider'=>$pf_consider
						);
				$this->Global_model->update($this->config->item('employee_pay_details_log'),$insertData1,$whereCond);

			}
			else
			{
				$insertData1 = array('emp_bid' =>$checkData->emp_bid,
							'payment_type_id' =>$checkData->payment_type_id,
							'acc_no' =>$checkData->acc_no,
							'uan' =>$checkData->uan,
							'pf_no' =>$checkData->pf_no,
							'esi_no' =>$checkData->esi_no,
							'basic' =>$checkData->basic,
							'other_allowance' =>$checkData->other_allowance,
							'incr' =>$checkData->incr,
							'co_allowance' =>$checkData->co_allowance,
							'da' =>$checkData->da,
							'special_allowance' =>$checkData->special_allowance,
							'hra' =>$checkData->hra,
							'bonus' =>$checkData->bonus,
							'cca' =>$checkData->cca,
							'standard_instalmemt'=>$standard_instalmemt,
							'inst_paid'=>$inst_paid,
							'esi_consider'=>$esi_consider,
							'is_pay_hold'=>$is_pay_hold,
							'sdscope'=>$sdscope,
							'pf_consider'=>$pf_consider
						);
				$param1['insertdata'] = $insertData1;
				$param1['table'] = $this->config->item('employee_pay_details_log');
				$this->Global_model->insert_data($param1);


			}
			$this->Global_model->update($this->config->item('employee_pay_details'),$insertData,$whereCond);
		}
		
		$this->session->set_userdata(array('create_status'=>'success'));
		$this->session->set_userdata(array('record_name'=>'Inserted successfully'));
		redirect('EmpPayScale');
		
	}
	function edit()
	{
		
		$this->session->set_userdata(array('create_status'=>'success'));
		$this->session->set_userdata(array('record_name'=>'Employee PayScale Masterupdated successfully'));
		redirect('EmpPayScale');
		
		
	}
	
	function undo()
	{
		
	}
	function errorpage()
	{
		
		$data=array('current_page'=>'Error ','page_title'=>'Error ','parent_menu'=>'','template'=>'masters/employee/errorpage');
		$this->load->view('theme/template',$data);
	}
	function getEmpData(){
		$emp_bid =$this->input->post('empbid');
		$where = array('emp_bid'=>$emp_bid);
		//$data = $this->Global_model->get_values_where_single($this->config->item('employee'),$where);
		$data = $this->PayScale_model->getEmpData($emp_bid);
		
		echo json_encode($data);

	}
	function EmppayscalBulkupload()
	{
		
				
				$file = $_FILES["emp_bluk"]["tmp_name"];
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);
				$get_sheetData = array();
				$get_sheets = $objPHPExcel->getAllSheets();
				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				foreach ($cell_collection as $cell) {
					$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
					$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
					$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
					if ($row == 1) {
						$header[$row][$column] = $data_value;
					} else {
						$arr_data[$row][$column] = $data_value;
						$arr_data1[$column][$row] = $data_value;
					}
				}
				$header = $header[1];
				$values = $arr_data;
				$excelHeadingNames = array('S.No','E.BID','Ename','Mode of payment','Bank A/c Number','UAN','PF Number','ESI Number','Basic','INCR','D.A','H.R.A','C.C.A','Other alowances','Head/HOD/Co ordinator alowances','Special alowances','Bonus','Any other alowances','ESI to concider','Is Payment to hold','10% s.d Scope','Standard Instalmemts','PF to consider');
				if(count(array_diff($header,$excelHeadingNames))==0)
				{
					$SNo =array_search('S.No',$header);
					$EBID =array_search('E.BID',$header);
					$Ename =array_search('Ename',$header);
					$Mode_of_payment =array_search('Mode of payment',$header);
					$Bank_Ac_Number =array_search('Bank A/c Number',$header);
					$UAN =array_search('UAN',$header);
					$PF_Number =array_search('PF Number',$header);
					$ESI_Number =array_search('ESI Number',$header);
					$Basic =array_search('Basic',$header);
					$INCR =array_search('INCR',$header);
					$DA =array_search('D.A',$header);
					$HRA =array_search('H.R.A',$header);
					$CCA =array_search('C.C.A',$header);
					$Other_alowances =array_search('Other alowances',$header);
					$HDD_ordinator_alowances =array_search('Head/HOD/Co ordinator alowances',$header);
					$Special_alowances =array_search('Special alowances',$header);
					$Bonus =array_search('Bonus',$header);
					$Any_other_alowances =array_search('Any other alowances',$header);
					$ESI_to_concider =array_search('ESI to concider',$header);
					$Is_Payment_to_hold =array_search('Is Payment to hold',$header);
					$sd_Scope =array_search('10% s.d Scope',$header);
					$Standard_Instalmemts =array_search('Standard Instalmemts',$header);
					$PF_to_consider =array_search('PF to consider',$header);
				
					foreach($arr_data1[$EBID] as $key=>$value)
					{
					$SNo1 = $arr_data1[$SNo][$key];
					$EBID1 = $arr_data1[$EBID][$key];
					$Ename1 = $arr_data1[$Ename][$key];
					$Mode_of_payment1 = $arr_data1[$Mode_of_payment][$key];
					$Bank_Ac_Number1 = $arr_data1[$Bank_Ac_Number][$key];
					$UAN1 = $arr_data1[$UAN][$key];
					$PF_Number1 = $arr_data1[$PF_Number][$key];
					$ESI_Number1 = $arr_data1[$ESI_Number][$key];
					$Basic1 = $arr_data1[$Basic][$key];
					$INCR1 = $arr_data1[$INCR][$key];
					$DA1 = $arr_data1[$DA][$key];
					$HRA1 = $arr_data1[$HRA][$key];
					$CCA1 = $arr_data1[$CCA][$key];
					$Other_alowances1 = $arr_data1[$Other_alowances][$key];
					$HDD_ordinator_alowances1 = $arr_data1[$HDD_ordinator_alowances][$key];
					$Special_alowances1 = $arr_data1[$Special_alowances][$key];
					$Bonus1 = $arr_data1[$Bonus][$key];
					$Any_other_alowances1 = $arr_data1[$Any_other_alowances][$key];
					$ESI_to_concider1 = ($arr_data1[$ESI_to_concider][$key]=="Y")?1:0;
					$Is_Payment_to_hold1 = ($arr_data1[$Is_Payment_to_hold][$key]=="Y")?1:0;
					$sd_Scope1 = ($arr_data1[$sd_Scope][$key]=="Y")?1:0;
					$Standard_Instalmemts1 = $arr_data1[$Standard_Instalmemts][$key];
					$PF_to_consider = ($arr_data1[$PF_to_consider][$key]=="Y")?1:0;
					
					 $getEmployee1 = $this->Global_model->get_values_where_single('employee_pay_details', array('emp_bid'=>$EBID1));
						if(COUNT($getEmployee1) > 0) {
							
						}
						else {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'emp_bid already exist please remove form excel and upload'));
							//redirect('EmpPayScale');
						}
						$payment_types = $this->Global_model->get_values_where_single('payment_types', array('payment_type'=>$Mode_of_payment1));

						if(COUNT($payment_types) > 0 || true) {
							$payment_type_id = 2;
						}
						else {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'Mode of salary payment type not exist please remove form excel or add payment type then upload'));
							redirect('EmpPayScale');
						}
						
						$this->db->query("INSERT IGNORE INTO `employee_pay_details`(`emp_bid`, `payment_type_id`, `acc_no`, `uan`, `pf_no`, `esi_no`, `basic`, `other_allowance`, `incr`, `da`, `special_allowance`, `hra`, `bonus`, `cca`, `standard_instalmemt`,  `esi_consider`, `is_pay_hold`, `sdscope`, `pf_consider`, `inserted_date`, `updated_date`) VALUES ('$EBID1',$payment_type_id,'$Bank_Ac_Number1','$UAN1','$PF_Number1','$ESI_Number1','$Basic1',$Other_alowances1,'$INCR1','$DA1','$Special_alowances1',$HRA1,$Bonus1,$CCA1,$Standard_Instalmemts1,$ESI_to_concider1,'$Is_Payment_to_hold1','$sd_Scope1','$PF_to_consider',now(),now())");  

					}
					if($this->db->trans_status()===FALSE)
					{
						$this->db->trans_rollback();
						$this->session->set_userdata(array('create_status'=>'failed'));
						$this->session->set_userdata(array('record_name'=>'employee_pay_details not inserted '));
					}
					else
					{
						$this->db->trans_commit();
						$this->session->set_userdata(array('create_status'=>'success'));
						$this->session->set_userdata(array('record_name'=>'employee_pay_details inserted successfully'));
					}
					redirect('EmpPayScale');
					//END Row
				}
				else {
					$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Please check excel heading is mismatching'));
					redirect('EmpPayScale');
				}
	}
}
