<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Designation extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$designations = $this->Global_model->get_values_where($this->config->item('designation'),array('status'=>1));
		$designations_log = $this->Global_model->get_values_where($this->config->item('designation_log'), array('flag'=>2));
		$data=array('current_page'=>'Designations','page_title'=>'Designations','parent_menu'=>'','template'=>'masters/designation/add','designations'=>$designations,'designations_log'=>$designations_log);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('designation_name','designation_name','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$designation_name = $this->input->post('designation_name');
			$desig_id = $this->input->post('desig_id');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('designation_name'=>$designation_name,'desig_id'=>$desig_id, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Designation not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Designation inserted successfully'));
			}
			redirect('Designation');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('designation');
		$where = array('designation_id'=>base64_decode($id));
		$designation_id = base64_decode($id);
		$getPpaymentTypeEdit = $this->Global_model->get_values_where_single($table,$where);
		$designations = $this->Global_model->get_values_where($this->config->item('designation'),array('status'=>1));
		$designations_log = $this->Global_model->get_values_where($this->config->item('designation_log'), array('flag'=>2));
			$data=array('current_page'=>'Designations','page_title'=>'Designations','parent_menu'=>'','template'=>'masters/Designation/add','designations'=>$designations,'getPpaymentTypeEdit'=>$getPpaymentTypeEdit,'designations_log'=>$designations_log);
		$this->form_validation->set_rules('designation_name','designation_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$designation_name = $this->input->post('designation_name');
			$desig_id = $this->input->post('desig_id');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('designation_name'=>$designation_name,'desig_id'=>$desig_id, 'status'=>$status,'designation_id'=>$designation_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Designations not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Designations updated successfully'));
			}
			redirect('designation');
		}
		
	}
	function delete($id)
	{
			$designation_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('designation_id'=>$designation_id);
			$this->all_actions('delete',$delete_array);
			echo $this->db->trans_status();
		if($this->db->trans_status()===FALSE)
		{
			//$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Designations not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Designations  Deleted successfully'));
		}
		
		redirect('designation');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$designation_id = base64_decode($id);
				$undo_array = array('designation_id'=>$designation_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Designations not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Designations undo successfully'));
				}
		
		redirect('designation');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$designation_name = $insert_array['designation_name'];
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$desig_id = $insert_array['desig_id'];
			$param['insertdata'] = array('designation_id'=>$this->Global_model->Max_count($this->config->item('designation'),'designation_id'),'designation_name'=>$designation_name,'desig_id'=>$insert_array['desig_id'],'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('designation');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			$desig_id = $insert_array['desig_id'];
			$designation_id = $insert_array['designation_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('designation'), array('designation_id'=>$designation_id));
				
				$param['insertdata'] = array('designation_id'=>$insertdata->designation_id,'designation_name'=>$insertdata->designation_name,'desig_id'=>$insertdata->desig_id,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('designation_log');
			/* Update log table end */
			$data1 = array('designation_id'=>$designation_id,'designation_name'=>$designation_name,'desig_id'=>$desig_id,'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('designation_id'=>$designation_id);
			$this->Global_model->update($this->config->item('designation'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$designation_id = $insert_array['designation_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('designation'), array('designation_id'=>$designation_id));
				
				$param['insertdata'] = array('designation_id'=>$insertdata->designation_id,'designation_name'=>$insertdata->designation_name,'desig_id'=>$insert_array['desig_id'],'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('designation_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('designation')." WHERE designation_id =".$designation_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$designation_id = $insert_array['designation_id'];
			$where = array('designation_id'=>$designation_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('designation_log')." WHERE designation_id=".$designation_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('designation_id'=>$designation_id,'designation_name'=>$getPaymentTypes->designation_name,'desig_id'=>$getPaymentTypes->desig_id,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('designation'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Designations not Undo '));	
					redirect('designation');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('designation');
		}	
	}
}
