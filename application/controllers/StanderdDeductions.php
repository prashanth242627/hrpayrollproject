<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class StanderdDeductions extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
	}
	public function index()
	{	
		//$this->output->enable_profiler(TRUE);
		$get_employee= $this->Global_model->get_values($this->config->item('employee'));

		$get_deductions= $this->Global_model->get_values($this->config->item('standerd_deductions'));

		if(!empty($get_deductions))
		{
			$table = $this->config->item('standerd_deductions');
			$where = array('deduct_id'=>1);
			//pf get pf data
			$pfData= $this->Global_model->get_values_where($table,$where);

			$where = array('deduct_id'=>2);
			// get special deductions data
			$spData= $this->Global_model->get_values_where($table,$where);
			$where = array('deduct_id'=>3);
			$ptData= $this->Global_model->get_values_where($table,$where);
			$where = array('deduct_id'=>4);
			$esiData= $this->Global_model->get_values_where($table,$where);
			$where = array('deduct_id'=>5);
			$tdsData= $this->Global_model->get_values_where($table,$where);
			$data=array('current_page'=>'StanderdDeductions ','page_title'=>'Standerd Deductions ','parent_menu'=>'','pfdata'=>$pfData,'spdata'=>$spData,'ptdata'=>$ptData,'esidata'=>$esiData,'tdsdata'=>$tdsData,'template'=>'masters/deductions/empDeductionReadonly','get_employee'=>$get_employee);

		}
		else
		{
			$data=array('current_page'=>'StanderdDeductions ','page_title'=>'Standerd Deductions ','parent_menu'=>'','template'=>'masters/deductions/empDeduction','get_employee'=>$get_employee);

		}
		$this->load->view('theme/template',$data);
		
	}
	function readonlyForm(){
		$get_employee= $this->Global_model->get_values($this->config->item('employee'));
		$data=array('current_page'=>'StanderdDeductions ','page_title'=>'Standerd Deductions ','parent_menu'=>'','template'=>'masters/deductions/empDeductionReadonly','get_employee'=>$get_employee);
		$this->load->view('theme/template',$data);

	}
	function addForm()
	{
		$get_employee= $this->Global_model->get_values($this->config->item('employee'));

		$table = $this->config->item('standerd_deductions');
		$where = array('deduct_id'=>1);
		//pf get pf data
		$pfData= $this->Global_model->get_values_where($table,$where);

		$where = array('deduct_id'=>2);
		// get special deductions data
		$spData= $this->Global_model->get_values_where($table,$where);
		$where = array('deduct_id'=>3);
		$ptData= $this->Global_model->get_values_where($table,$where);
		$where = array('deduct_id'=>4);
		$esiData= $this->Global_model->get_values_where($table,$where);
		$where = array('deduct_id'=>5);
		$tdsData= $this->Global_model->get_values_where($table,$where);
		
		$data=array('current_page'=>'StanderdDeductions ','page_title'=>'Standerd Deductions ','parent_menu'=>'','pfdata'=>$pfData,'spdata'=>$spData,'ptdata'=>$ptData,'esidata'=>$esiData,'tdsdata'=>$tdsData,'template'=>'masters/deductions/empDeductionEdit','get_employee'=>$get_employee);
		$this->load->view('theme/template',$data);

	}
	function add()
	{
		
		$pf = $this->input->post('pf');
		$deductid = 1;
		$form_values['deduct_id'] = $deductid;
		$form_values['deduction_type'] = 1;
		/*$form_values['min_limit'] = 0;
		$form_values['max_limit'] = 0;*/
		$form_values['deduct_value'] = 0;
		$param['table'] = $this->config->item('standerd_deductions');
		$param['insertdata'] = array('deduct_id' =>$deductid,'deduction_type' =>1,'deduct_value' =>$pf);
		$this->Global_model->insert_data($param);
		
		$sda = $this->input->post('sda');
		$sda = $this->input->post('sda');
		if(empty($sda)){
			$sda = $this->input->post('sdo');
		}

		$deductid = 2;
		$param['insertdata'] = array('deduct_id' =>$deductid,'deduction_type' =>1,'deduct_value' =>$sda);
		$this->Global_model->insert_data($param);
		//$sdo = $this->input->post('sdo');
		$deductid = 3;
		for($i=1;$i<=5;$i++){
			$ptlimit = $this->input->post('pt'.$i.'limit'); 
			$ptrate =  $this->input->post('pt'.$i.'rate');
			if($i==1)
			{
				$minLimit = 0;
			}
			else
			{
				$minLimit = $this->input->post('pt'.($i-1).'limit');
			}
			$param['insertdata'] = array('deduct_id' =>$deductid,'deduction_type' =>2,'min_limit' =>$minLimit,'max_limit' =>$ptlimit,'deduct_value' =>$ptrate);
			$this->Global_model->insert_data($param);
			
		}
		
		$deductid = 4;
		for($i=1;$i<=5;$i++){
			$esilimit = $this->input->post('esi'.$i.'limit'); 
			$esirate =  $this->input->post('esi'.$i.'rate');
			if($i==1)
			{
				$minLimit = 0;
			}
			else
			{
				$minLimit = $this->input->post('esi'.($i-1).'limit');
			}
			$param['insertdata'] = array('deduct_id' =>$deductid,'deduction_type' =>1,'min_limit' =>$minLimit,'max_limit' =>$esilimit,'deduct_value' =>$esirate);
			$this->Global_model->insert_data($param); 
		}

		$deductid = 5;
		for($i=1;$i<=9;$i++){
			$tdslimit = $this->input->post('tds'.$i.'limit'); 
			$tdsrate =  $this->input->post('tds'.$i.'rate');
			if($i==1)
			{
				$minLimit = 0;
			}
			else
			{
				$minLimit = $this->input->post('tds'.($i-1).'limit');
			}
			$param['insertdata'] = array('deduct_id' =>$deductid,'deduction_type' =>1,'min_limit' =>$minLimit,'max_limit' =>$tdslimit,'deduct_value' =>$tdsrate);
			$this->Global_model->insert_data($param); 

		}
		/*if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Standerd Deductions not updated '));	
		}
		else
		{
			$this->Global_model->insert_data($param);
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Standered Deductions updated successfully'));
		}*/
		$this->session->set_userdata(array('record_name'=>'Standered Deductions updated successfully'));
		redirect('StanderdDeductions');
		
	}
	function edit()
	{
		
		$pf = $this->input->post('pf');
		$pf_id = $this->input->post('pf1id');
		$deductid = 1;
		$form_values['deduct_id'] = $deductid;
		$form_values['deduction_type'] = 1;
		/*$form_values['min_limit'] = 0;
		$form_values['max_limit'] = 0;*/
		$form_values['deduct_value'] = 0;
		$param['table'] = $this->config->item('standerd_deductions');
		$param['updatedata'] = array('deduct_id' =>$deductid,'deduction_type' =>1,'deduct_value' =>$pf);
		$where = array('std_deduct_id'=>$pf_id);

		$table = $this->config->item('standerd_deductions_log');
		$where1 = array('std_deduct_id'=>$pf_id);
		//pf get pf data
		$pfData1= $this->Global_model->get_values_where($table,$where);
		if(!empty($pfData1)){
			//update code
			$pfDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);
			$param1['updatedata'] = array('deduct_id' =>$pfDataOld->deduct_id,'deduction_type' =>1,'deduct_value' =>$pfDataOld->deduct_value);
			$logWhere = array('std_deduct_id'=>$pfDataOld->std_deduct_id);
			$this->Global_model->update($this->config->item('standerd_deductions_log'),$param1['updatedata'],$logWhere);
			
		}
		else{
			//insert code
			$pfDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);
			$param1['table'] = $this->config->item('standerd_deductions_log');
			$param1['insertdata'] = array('std_deduct_id'=>$pfDataOld->std_deduct_id,'deduct_id' =>$pfDataOld->deduct_id,'deduction_type' =>1,'deduct_value' =>$pfDataOld->deduct_value);
			$this->Global_model->insert_data($param1);
		}
		
		$this->Global_model->update($this->config->item('standerd_deductions'),$param['updatedata'],$where);
		
		
		$sda = $this->input->post('sda');
		if(empty($sda)){
			$sda = $this->input->post('sdo');
		}
		$sd_id = $this->input->post('sd1id');

		$deductid = 2;
		
		$param['updatedata'] = array('deduct_id' =>$deductid,'deduction_type' =>1,'deduct_value' =>$sda);
		$where = array('std_deduct_id'=>$sd_id);

		$where1 = array('std_deduct_id'=>$sd_id);
		//pf get pf data
		$sdData1= $this->Global_model->get_values_where($table,$where);
		if(!empty($sdData1)){
			//update code
			$sdDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);
			$param1['updatedata'] = array('deduct_id' =>$sdDataOld->deduct_id,'deduction_type' =>1,'deduct_value' =>$sdDataOld->deduct_value);
			$logWhere = array('std_deduct_id'=>$sdDataOld->std_deduct_id);
			$this->Global_model->update($this->config->item('standerd_deductions_log'),$param1['updatedata'],$logWhere);
			
		}
		else{
			//insert code
			$sdDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);
			$param1['table'] = $this->config->item('standerd_deductions_log');
			$param1['insertdata'] = array('std_deduct_id'=>$sdDataOld->std_deduct_id,'deduct_id' =>$sdDataOld->deduct_id,'deduction_type' =>1,'deduct_value' =>$sdDataOld->deduct_value);
			$this->Global_model->insert_data($param1);
		}
		
		$this->Global_model->update($this->config->item('standerd_deductions'),$param['updatedata'],$where);

		
		$deductid = 3;
		for($i=1;$i<=5;$i++){
			$pt_id = $this->input->post('pt'.$i.'id');
			$ptlimit = $this->input->post('pt'.$i.'limit'); 
			$ptrate =  $this->input->post('pt'.$i.'rate');
			if($i==1)
			{
				$minLimit = 0;
			}
			else
			{
				$minLimit = $this->input->post('pt'.($i-1).'limit');
			}
			$param['updatedata'] = array('deduct_id' =>$deductid,'deduction_type' =>2,'min_limit' =>$minLimit,'max_limit' =>$ptlimit,'deduct_value' =>$ptrate);
			$where = array('std_deduct_id'=>$pt_id);

			$ptData1= $this->Global_model->get_values_where($table,$where);
			$where1 = array('std_deduct_id'=>$pt_id);
			if(!empty($ptData1)){
				//update code
				$ptDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);
				$param1['updatedata'] = array('deduct_id' =>$ptDataOld->deduct_id,'deduction_type' =>2,'min_limit' =>$ptDataOld->min_limit,'max_limit' =>$ptDataOld->max_limit,'deduct_value' =>$ptDataOld->deduct_value);
				$logWhere = array('std_deduct_id'=>$ptDataOld->std_deduct_id);
				$this->Global_model->update($this->config->item('standerd_deductions_log'),$param1['updatedata'],$logWhere);
				
			}
			else{
				//insert code
				$ptDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);

				$param1['table'] = $this->config->item('standerd_deductions_log');
				$param1['insertdata'] = array('std_deduct_id'=>$ptDataOld->std_deduct_id,'deduct_id' =>$ptDataOld->deduct_id,'deduction_type' =>2,'min_limit' =>$ptDataOld->min_limit,'max_limit' =>$ptDataOld->max_limit,'deduct_value' =>$ptDataOld->deduct_value);
				$this->Global_model->insert_data($param1);
				
			}

			$this->Global_model->update($this->config->item('standerd_deductions'),$param['updatedata'],$where);
			
		}
		
		$deductid = 4;
		for($i=1;$i<=5;$i++){
			$esi_id = $this->input->post('esi'.$i.'id');
			$esilimit = $this->input->post('esi'.$i.'limit'); 
			$esirate =  $this->input->post('esi'.$i.'rate');
			if($i==1)
			{
				$minLimit = 0;
			}
			else
			{
				$minLimit = $this->input->post('esi'.($i-1).'limit');
			}
			$param['updatedata'] = array('deduct_id' =>$deductid,'deduction_type' =>1,'min_limit' =>$minLimit,'max_limit' =>$esilimit,'deduct_value' =>$esirate);
			$where = array('std_deduct_id'=>$esi_id);

			$esiData1= $this->Global_model->get_values_where($table,$where);
			$where1 = array('std_deduct_id'=>$esi_id);
			if(!empty($esiData1)){
				//update code
				$esiDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);
				$param1['updatedata'] = array('deduct_id' =>$esiDataOld->deduct_id,'deduction_type' =>1,'min_limit' =>$esiDataOld->min_limit,'max_limit' =>$esiDataOld->max_limit,'deduct_value' =>$esiDataOld->deduct_value);
				$logWhere = array('std_deduct_id'=>$esiDataOld->std_deduct_id);
				$this->Global_model->update($this->config->item('standerd_deductions_log'),$param1['updatedata'],$logWhere);
				
			}
			else{
				//insert code
				$esiDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);

				$param1['table'] = $this->config->item('standerd_deductions_log');
				$param1['insertdata'] = array('std_deduct_id'=>$esiDataOld->std_deduct_id,'deduct_id' =>$esiDataOld->deduct_id,'deduction_type' =>1,'min_limit' =>$esiDataOld->min_limit,'max_limit' =>$esiDataOld->max_limit,'deduct_value' =>$esiDataOld->deduct_value);
				$this->Global_model->insert_data($param1);
				
			}

			$this->Global_model->update($this->config->item('standerd_deductions'),$param['updatedata'],$where); 
		}
		
		$deductid = 5;
		for($i=1;$i<=9;$i++){
			$tds_id = $this->input->post('tds'.$i.'id');
			$tdslimit = $this->input->post('tds'.$i.'limit'); 
			$tdsrate =  $this->input->post('tds'.$i.'rate');
			if($i==1)
			{
				$minLimit = 0;
			}
			else
			{
				$minLimit = $this->input->post('tds'.($i-1).'limit');
			}
			$param['updatedata'] = array('deduct_id' =>$deductid,'deduction_type' =>1,'min_limit' =>$minLimit,'max_limit' =>$tdslimit,'deduct_value' =>$tdsrate);
			$where = array('std_deduct_id'=>$tds_id);

			$tdsData1= $this->Global_model->get_values_where($table,$where);
			$where1 = array('std_deduct_id'=>$tds_id);
			if(!empty($tdsData1)){
				//update code
				$tdsDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);
				$param1['updatedata'] = array('deduct_id' =>$tdsDataOld->deduct_id,'deduction_type' =>1,'min_limit' =>$tdsDataOld->min_limit,'max_limit' =>$tdsDataOld->max_limit,'deduct_value' =>$tdsDataOld->deduct_value);
				$logWhere = array('std_deduct_id'=>$tdsDataOld->std_deduct_id);
				$this->Global_model->update($this->config->item('standerd_deductions_log'),$param1['updatedata'],$logWhere);
				
			}
			else{
				//insert code
				$tdsDataOld = $this->Global_model->get_values_where_single($param['table'],$where1);

				$param1['table'] = $this->config->item('standerd_deductions_log');
				$param1['insertdata'] = array('std_deduct_id'=>$tdsDataOld->std_deduct_id,'deduct_id' =>$tdsDataOld->deduct_id,'deduction_type' =>1,'min_limit' =>$tdsDataOld->min_limit,'max_limit' =>$tdsDataOld->max_limit,'deduct_value' =>$tdsDataOld->deduct_value);
				$this->Global_model->insert_data($param1);
				
			}


			$this->Global_model->update($this->config->item('standerd_deductions'),$param['updatedata'],$where);
		}
		
		$this->session->set_userdata(array('create_status'=>'success'));
		$this->session->set_userdata(array('record_name'=>'Standerd Deductions updated successfully'));
		redirect('StanderdDeductions');
		
		
	}
	
	function undo()
	{
		$logData = $this->Global_model->get_values($this->config->item('standerd_deductions_log'));
		if(!empty($logData)){
			foreach ($logData as  $value) {
				$updatedata = array('deduct_id' =>$value->deduct_id,'deduction_type' =>1,'min_limit' =>$value->min_limit,'max_limit' =>$value->max_limit,'deduct_value' =>$value->deduct_value);
				$logWhere = array('std_deduct_id'=>$value->std_deduct_id);
				$this->Global_model->update($this->config->item('standerd_deductions'),$updatedata,$logWhere);
			}
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Undo completed successfully'));
		}
		else
		{
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'No records to undo'));
		}
		
		redirect('StanderdDeductions');
	}
	function errorpage()
	{
		
		$data=array('current_page'=>'Error ','page_title'=>'Error ','parent_menu'=>'','template'=>'masters/employee/errorpage');
		$this->load->view('theme/template',$data);
	}
}
