<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Roles extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
		
	}


	public function index()
	{

	    $CI =& get_instance();
        $CI->load->model('Myfunctions');
		$users=$CI->Myfunctions->getQueryDataList("SELECT * FROM `user` WHERE `status`=1");
		$data=array('current_page'=>'Roles','page_title'=>'Roles','parent_menu'=>'','template'=>'masters/roles/add','role'=>$users);
		$this->form_validation->set_rules('username','username','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Dont have option to create New Roles'));
			redirect('Roles');
		} 
		
	}
	function edit($id)
	{
		//$user_id = base64_decode($id);
		//$getPpaymentTypeEdit = $this->Global_model->get_values_where_single($table,$where);
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
		$getPpaymentTypeEdit=$CI->Myfunctions->getData('user','role_id='.base64_decode($id));
		$users = $CI->Myfunctions->getQueryDataList("SELECT * FROM `user` WHERE `status`=1");
	    $data=array('current_page'=>'Roles','page_title'=>'Roles','parent_menu'=>'','template'=>'masters/roles/add','role'=>$users,'getPpaymentTypeEdit'=>$getPpaymentTypeEdit);
		$this->form_validation->set_rules('username','username','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			//$emp_id  = $this->session->userdata('emp_id');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			//$role_id = $this->input->post('role_id');
			$status = ($this->input->post('status'))?1:0;
			//$this->db->trans_begin();
			$edit_array = array('username'=>$username,'password'=>$password, 'status'=>$status,'role_id'=>base64_decode($id));
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Role not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Role updated successfully'));
			}
			redirect('Roles');
		}
		
	}
	function delete($id)
	{
			$role_id = base64_decode($id);
			//$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('role_id'=>$role_id);
			$this->all_actions('delete',$delete_array);
			//echo $this->db->trans_status();
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Roles not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Roles  Deleted successfully'));
		}
		
		redirect('Roles');
		
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$username = $insert_array['username'];
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
            log_message('debug', 'insert action');
			/*$desig_id = $insert_array['desig_id'];
			$param['insertdata'] = array('designation_id'=>$this->Global_model->Max_count($this->config->item('designation'),'designation_id'),'designation_name'=>$designation_name,'desig_id'=>$insert_array['desig_id'],'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('designation');
			$this->Global_model->insert_data($param);*/
			$table_name='user';
			$data['username']=$this->input->post('username');
			$data['password']=$this->input->post('password');
		}
		else if($action == "edit")
		{
		    $CI =& get_instance();
            $CI->load->model('Myfunctions');
            $id=$this->session->userdata('emp_id');
			$role_id = $insert_array['role_id'];
			$data1 = array('password'=>$insert_array['password'],'username'=>$insert_array['username'],'Updated_By'=>$id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$update=$CI->Myfunctions->updateRecord('user',$data1,'role_id='.$role_id);
			//$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
		    $CI =& get_instance();
            $CI->load->model('Myfunctions');
			$role_id = $insert_array['role_id'];
			$delete=$CI->Myfunctions->deleteData('user', "role_id=".$role_id);
			//$this->db->query("DELETE FROM ".$this->config->item('user')." WHERE role_id =".$designation_id);
			//$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$designation_id = $insert_array['designation_id'];
			$where = array('designation_id'=>$designation_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('designation_log')." WHERE designation_id=".$designation_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('designation_id'=>$designation_id,'designation_name'=>$getPaymentTypes->designation_name,'desig_id'=>$getPaymentTypes->desig_id,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('designation'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Designations not Undo '));	
					redirect('designation');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('designation');
		}	
	}
}
