<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class EmployeeMonthlyLeaveStatus extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_leave_model');
	}
	public function index()
	{
		$month=date("F",strtotime("-1 month"));
		$year=date("Y",strtotime("-1 month"));
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		$data=array('current_page'=>'Dashboard','page_title'=>'Dashboard','parent_menu'=>'','template'=>'emp_monthly_leave_status');
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['uploadstatus']=$this->Employee_leave_model->checkLastUpdatedStatus($month,$year);
		$this->load->view('theme/template',$data);
	}
	public function All()
	{
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		if ($month!=null && $year!=null) {
			$employees=$this->Employee_leave_model->allEmployeesMonthlyLeaves($month,$year);
		}
		$data=array('current_page'=>'Dashboard','page_title'=>'Dashboard','parent_menu'=>'','template'=>'emp_monthly_leave_status');
		$data['employees']=array();
		if ($employees) 
		{
			$data['employees'] = $employees;
		}
		
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['uploadstatus']=$this->Employee_leave_model->checkLastUpdatedStatus($month,$year);
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$data['employees']=array();
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		if ($month!=null && $year!=null) {
			if ($activity_type!=null) {
				$no_of_leaves=$this->Employee_leave_model->noOfMonthlyLeavesByWhere("loa.dept_name='".$activity_type."'",$month,$year);
			}
			else if ($bid!=null) {
				$no_of_leaves=$this->Employee_leave_model->noOfMonthlyLeavesByWhere("e.emp_bid=".$bid,$month,$year);
			}
			else if ($ename!=null) {
				$no_of_leaves=$this->Employee_leave_model->noOfMonthlyLeavesByWhere("e.emp_name='".$ename."'",$month,$year);
			}
		}
		$data=array('current_page'=>'Dashboard','page_title'=>'Dashboard','parent_menu'=>'','template'=>'emp_monthly_leave_status');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['uploadstatus']=$this->Employee_leave_model->checkLastUpdatedStatus($month,$year);
		if (count($no_of_leaves)) 
		{
			$data['employees'] = $no_of_leaves;
			
		}
		$this->load->view('theme/template',$data);
	}

}
