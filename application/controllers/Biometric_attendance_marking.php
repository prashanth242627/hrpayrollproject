<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Biometric_attendance_marking extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		$this->load->model('Myfunctions');
		$this->load->model("Employee_model");
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_attendance_marking = $this->Global_model->get_attendance_marking();

		$get_attendance_marking_log = $this->Global_model->get_attendance_marking_log();
		
		$get_employees = $this->Global_model->employeesBids();
		$get_holidays = $this->Global_model->get_values_where($this->config->item('holidays'),array('status'=>1));
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves = $this->Global_model->get_current_date_leaves_count();
		$get_current_date_leave_emps=$this->Global_model->get_current_date_leave_emps();
		//$get_emp_leavemaster  = $this->Global_model->get_emp_leavemaster($id);
		$data=array('current_page'=>'Attendance Marking','page_title'=>'Attendance Marking','parent_menu'=>'','template'=>'masters/biometric_marking/add','get_attendance_marking'=>$get_attendance_marking,'get_attendance_marking_log'=>$get_attendance_marking_log,'get_employees'=>$get_employees,'get_holidays'=>$get_holidays,"get_leave_types"=>$get_leave_types,"get_current_date_leaves"=>$get_current_date_leaves,"get_current_date_leave_emps"=>$get_current_date_leave_emps);
		$this->form_validation->set_rules('emp_name','emp_name','required');
		
		if($this->form_validation->run() === FALSE)
		{
			//print_r($data);
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			
			$this->db->trans_begin();
			$insert_array = $this->input->post();
			
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking inserted successfully'));
			}
			$get_leave_types  = $this->Global_model->get_leave_types();
			$data['get_leave_types'] = $get_leave_types;
			redirect('Biometric_attendance_marking');
			//print_r($insert_array);
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('attendance_marking');
		$where = array('leave_master_id'=>base64_decode($id));
		$leave_master_id = base64_decode($id);
		$getAttendanceMarkingEdit = $this->Global_model->get_leave_master_single($leave_master_id);
		$get_attendance_marking = $this->Global_model->get_attendance_marking();
		$get_attendance_marking_log = $this->Global_model->get_attendance_marking_log();
		$get_current_date_leave_emps=$this->Global_model->get_current_date_leave_emps();

		$get_employees = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
		$get_holidays = $this->Global_model->get_values_where($this->config->item('holidays'),array('status'=>1));
		$data=array('current_page'=>'Attendance Marking','page_title'=>'Attendance Marking','parent_menu'=>'','template'=>'masters/biometric_marking/add','get_attendance_marking'=>$get_attendance_marking,'getAttendanceMarkingEdit'=>$getAttendanceMarkingEdit,'get_attendance_marking_log'=>$get_attendance_marking_log,'get_employees'=>$get_employees,'get_holidays'=>$get_holidays);
		$this->form_validation->set_rules('emp_name','emp_name','required');
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves = $this->Global_model->get_current_date_leaves_count();
		$data['get_current_date_leave_emps']=$get_current_date_leave_emps;
		$data['get_leave_types'] = $get_leave_types;
		$data['get_current_date_leaves'] = $get_current_date_leaves;
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_bid');
			$activity_id = $this->input->post('activity_id');
			$leave_type_id = $this->input->post('leave_type_id');
			$no_leaves = $this->input->post('no_leaves');
			$calendar_year = $this->input->post('calendar_year');
			$lemp_id = $this->input->post('emp_id');
			$this->db->trans_begin();
			$edit_array = array('activity_id'=>$activity_id,'emp_bid'=>$emp_bid, 'leave_type_id'=>$leave_type_id, 'no_leaves'=>$no_leaves,'emp_id'=>$lemp_id,'attendance_id'=>$attendance_id,'calendar_year'=>$calendar_year);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking updated successfully'));
			}
			redirect('Biometric_attendance_marking');
		}
		
	}
	function delete($id)
	{
			$attendance_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('attendance_id'=>$attendance_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking  Deleted successfully'));
		}
		redirect('Biometric_attendance_marking');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$attendance_id = base64_decode($id);
				$undo_array = array('attendance_id'=>$attendance_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Attendance Marking not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Attendance Marking undo successfully'));
				}
		
		redirect('Biometric_attendance_marking');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$get_attendance_markings = $this->db->query("SELECT * FROM `employee` e LEFT JOIN line_of_activity l ON e.`activity_id` = l.activity_id LEFT JOIN subject_details s ON e.subject_details_id = s.subject_details_id WHERE e.`emp_bid` = '".$insert_array['emp_bid']."'")->row();
			//print_r($get_attendance_markings);
			$date=date("Y-m-d",strtotime($insert_array['attendance_date']));
			$from_time=date("h:m:ss",strtotime($get_attendance_markings->from_time));
			$to_time=date("h:m:ss",strtotime($get_attendance_markings->to_time));
			$param= array('emp_bid'=>$insert_array['emp_bid'],'log_date'=>$date,'log_time'=>$from_time,"status"=>1);

			$param1= array('emp_bid'=>$insert_array['emp_bid'],'log_date'=>$date,'log_time'=>$to_time,"status"=>1);
			$this->Myfunctions->addRecord("biometirc_attendance",$param1);
			$this->Myfunctions->addRecord("biometirc_attendance",$param);
		}
		else if($action == "edit")
		{
  		    $attendance_id = $insert_array['attendance_id'];
			$data1 = array('attendance_id'=>$attendance_id,'emp_id'=>$insert_array['emp_id'],'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>$insert_array['no_leaves'],'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('attendance_id'=>$attendance_id);
			$this->db->query("INSERT INTO `attendance_marking_log`(`attendance_id`, `emp_id`, `leave_type_id`, `no_leaves`, `calendar_year`,`date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (SELECT attendance_id, emp_id, leave_type_id, no_leaves, calendar_year, date, '1', 'EDIT', Created_By, Created_Datetime, Updated_By, Updated_Datetime FROM attendance_marking WHERE attendance_id= ".$attendance_id.")");
			$this->Global_model->update($this->config->item('attendance_marking'),$data1,$where);
		}
		else if($action == 'delete') {
			$attendance_id = $insert_array['attendance_id'];
			$this->db->query("INSERT INTO `attendance_marking_log`(`attendance_id`, `emp_id`, `leave_type_id`, `no_leaves`,  `calendar_year`,`date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (SELECT attendance_id, emp_id, leave_type_id, no_leaves,calendar_year, date, '2', 'DELETE', $emp_id, now(), $emp_id, now() FROM attendance_marking WHERE attendance_id= ".$attendance_id.")");
			$this->db->query("DELETE FROM ".$this->config->item('attendance_marking')." WHERE attendance_id =".$attendance_id);
		}
		else if($action == 'undo') {
			$attendance_id = $insert_array['attendance_id'];
			$where = array('attendance_id'=>$attendance_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('attendance_marking_log')." WHERE attendance_id=".$attendance_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('emp_id'=>$getPaymentTypes->emp_id,'leave_type_id'=>$getPaymentTypes->leave_type_id,'no_leaves'=>$getPaymentTypes->no_leaves,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('attendance_marking'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Attendance Marking not Undo '));	
					redirect('Biometric_attendance_marking');
			}
			
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Biometric_attendance_marking');
		}	
	}
	function getEmployeeDetails()
	{
		$emp_bid = $this->input->post('emp_bid');
		$get_attendance_markings = $this->db->query("SELECT * FROM `employee` e LEFT JOIN line_of_activity l ON e.`activity_id` = l.activity_id LEFT JOIN subject_details s ON e.subject_details_id = s.subject_details_id WHERE e.`emp_bid` = '".$emp_bid."'")->row();
		$data['emp_name'] = $get_attendance_markings->emp_name;
		$data['dept_name'] = $get_attendance_markings->dept_name;
		$data['sub_name'] = $get_attendance_markings->sub_name;
		$data['emp_id'] = $get_attendance_markings->emp_id;
		$year=date("Y");
		$get_emp_leavemaster  = $this->Global_model->get_employees_leaves_count_type($data['emp_id'],$year);
		$get_emp_leavemaster_data = "";
		foreach($get_emp_leavemaster as $row)
		{
			$no_leaves=$row->no_leaves;
			$used_count=$row->used_count;
			$remaining=$no_leaves-$used_count;
			$get_emp_leavemaster_data .= "<option value='".$row->leave_master_id."'>".$row->leave_type."(".$no_leaves."/".$used_count."/".$remaining.")</option>";
		}
		$data['get_emp_leavemaster'] = $get_emp_leavemaster_data;
		
		$get_employees_leaves_count_types=$this->Global_model->get_employees_leaves_count_type($data['emp_id'],$year);
		$employee_leave_types_id="<th>Leave Type</th>";
		$total_leaves_id="<th>Total Leaves</th>";
		$remaining_leaves_id="<th>Used Leaves</th>";
		foreach ($get_employees_leaves_count_types as $get_employees_leaves_count_type) {
			$employee_leave_types_id.="<th>".$get_employees_leaves_count_type->leave_type."</th>";
			$total_leaves_id.="<th>".$get_employees_leaves_count_type->no_leaves."</th>";
			$remaining_leaves_id.="<th>".$get_employees_leaves_count_type->used_count."</th>";
		}
		$data['employee_leave_types_id'] = $employee_leave_types_id;
		$data['total_leaves_id'] = $total_leaves_id;
		$data['remaining_leaves_id'] = $remaining_leaves_id;
		echo json_encode($data);
	}
	function SummaryDetails()
	{
		$summary_date = $this->input->post('summary_date');
		//$summary_date=date("Y-m-d",$summary_date);
		$get_current_date_leaves = $this->Global_model->get_leaves_count_by_date($summary_date);
		$get_current_date_leave_emps=$this->Global_model->get_leave_emps_by_date($summary_date);
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves_string="";
			$total_leaves=0;
		   foreach ($get_leave_types as $get_leave_type) {
		   	if (count($get_current_date_leaves)) {
			  	foreach ($get_current_date_leaves as $get_current_date_leave) {
			  		if ($get_leave_type->leave_type===$get_current_date_leave->leave_type) {
			  			$total_leaves+=$get_current_date_leave->leaves_count;
			  			$get_current_date_leaves_string.= "<td>".$get_current_date_leave->leaves_count."</td>";
			  		}
			  		else{
			  			$get_current_date_leaves_string.= "<td>"."0"."</td>";
			  		}
			  	}
		  	}
		  	else{
		  		$get_current_date_leaves_string.= "<td>"."0"."</td>";
		  	}
		  } 
		  $get_current_date_leaves_string.= "<th>".$total_leaves."</th>";
		$get_current_date_leave_emps_string="";
		if (count($get_current_date_leave_emps)>0) {
		  	$l=1;
		  	foreach ($get_current_date_leave_emps as $get_current_date_leave_emp) {
		  		$get_current_date_leave_emps_string.= "<tr><td>".$l++."</td><td>".$get_current_date_leave_emp->emp_bid."</td><td>".$get_current_date_leave_emp->emp_name."</td><td>".$get_current_date_leave_emp->emp_id."</td><td>".$get_current_date_leave_emp->leave_date."</td><td>".$get_current_date_leave_emp->dept_name."</td><td>".$get_current_date_leave_emp->sub_name."</td><td>".$get_current_date_leave_emp->leave_type."</td></tr>";
		  	}
		}
		else{
			$get_current_date_leave_emps_string.= "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
		}
		$data['get_current_date_leave_emps']=$get_current_date_leaves_string;
		$data['get_current_date_leaves'] = $get_current_date_leave_emps_string;
		echo json_encode($data);
	}
}
