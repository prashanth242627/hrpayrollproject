<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class MonthlySalaries extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
		$this->load->model('Salary_model');
	}
	public function index()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Salary_model->paymentModes();
		$data=array('current_page'=>'Monthly Salary Details','page_title'=>'Monthly Salary Details','parent_menu'=>'','template'=>'MonthlySalaryDetails');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		$data['message']="";
		$employees=array();
		$this->load->view('theme/template',$data);
	}
	public function All()
	{
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Salary_model->paymentModes();
		if ($year!=null && $month!=null) {

			$month_no=date("m",strtotime($month));
			$last_record_of_month=$this->Myfunctions->getQueryData("SELECT * FROM `emp_monthly_salaries` WHERE salary_month=".$month_no." and salary_year=".$year);
			if (count($last_record_of_month)) {
				$employees=$this->Salary_model->allCTCPerMonth($month,$year);
				$message="";
			}
			else{
				$employees=array();
				$message="Please Calculate Employee Monthly Salaries for Selected Month";
			}
			
		}
		$data=array('current_page'=>'Monthly Salary Details','page_title'=>'Monthly Salary Details','parent_menu'=>'','template'=>'MonthlySalaryDetails');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		$data['message']=$message;
		if ($employees) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$ename=$this->input->get('ename');
		$payment_mode=$this->input->get('payment_mode');
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Salary_model->paymentModes();
		$employees=array();

		if ($year!=null && $month!=null) {
			if ($bid!=null) {
				$employees=$this->Salary_model->CTCPerMonthByWhereYearMonth("e.emp_bid=".$bid,$year,$month);
			}
			else if ($ename!=null) {
				$employees=$this->Salary_model->CTCPerMonthByWhereYearMonth("e.emp_name='".ltrim($ename)."'",$year,$month);
			}
			else if ($activity_type!=null && $payment_mode!=null) {
				$employees=$this->Salary_model->CTCPerMonthByWhereYearMonth("l.dept_name='".$activity_type."' and pmttyp.payment_type='".$payment_mode."'",$year,$month);
			}
			else if ($payment_mode!=null) {
				$employees=$this->Salary_model->CTCPerMonthByWhereYearMonth("pmttyp.payment_type='".$payment_mode."'",$year,$month);
			}
			else if ($activity_type!=null) {
				$employees=$this->Salary_model->CTCPerMonthByWhereYearMonth("l.dept_name='".$activity_type."'",$year,$month);
			}
			$month_no=date("m",strtotime($month));
			$last_record_of_month=$this->Myfunctions->getQueryData("SELECT * FROM `emp_monthly_salaries` WHERE salary_month=".$month_no." and salary_year=".$year);
			if (count($last_record_of_month)) {
				$message="";
			}
			else{
				$employees=array();
				$message="Please Calculate Employee Monthly Salaries for Selected Month";
			}
		}
		$data=array('current_page'=>'Monthly Salary Details','page_title'=>'Monthly Salary Details','parent_menu'=>'','template'=>'MonthlySalaryDetails');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		$data['message']=$message;
		if (count($employees)) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('theme/template',$data);
	}

}
