<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class BankStatement extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
		$this->load->model("Salary_model");
	}
	public function index()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$get_leave_types  = $this->Global_model->get_leave_types();
		$data=array('current_page'=>'MonthWise Attendance Comparisions','page_title'=>'MonthWise Attendance Comparisions','parent_menu'=>'','template'=>'bank_statement');
		$data['activities']=$activities;
		$data['leave_types']=$get_leave_types;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$year=$this->input->get('year');
		$month=$this->input->get('month');		
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$get_leave_types  = $this->Global_model->get_leave_types();

		$a_reports=array();

		if ($year!=null && $month!=null) {
			$month_no=date('m',strtotime($month));
			$a_reports=$this->Salary_model->bankstatement("ems.salary_month=".$month_no." and ems.salary_year=".$year);
		}
		// else if ($month!=null & $year==null) {
		// 	$a_reports=$this->monthwise_attendance_model->getAttendanceReportsByWhere("MONTHNAME(ba.log_date)='".$month."' and YEAR(ba.log_date)='".date('Y')."'");
		// }
		$data=array('current_page'=>'MonthWise Attendance Comparisions','page_title'=>'MonthWise Attendance Comparisions','parent_menu'=>'','template'=>'bank_statement');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['leave_types']=$get_leave_types;
		$data['holidays']=$holidays;
		if (count($a_reports)) 
		{
			$data['reports'] = $a_reports;
			
		}
		$this->load->view('theme/template',$data);
	}

}
