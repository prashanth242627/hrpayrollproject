<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class PaymentModes extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
		$this->load->model('PaymentMode_model');
	}
	public function index()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->PaymentMode_model->paymentModes();
		$data=array('current_page'=>'Salary Payment Methods','page_title'=>'Salary Payment Methods','parent_menu'=>'','template'=>'SalaryPaymentMethods');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		$employees=array();
		$this->load->view('theme/template',$data);
	}
	public function All()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->PaymentMode_model->paymentModes();
		$employees=$this->PaymentMode_model->allSalariesDetails();
		$data=array('current_page'=>'Salary Payment Methods','page_title'=>'Salary Payment Methods','parent_menu'=>'','template'=>'SalaryPaymentMethods');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		if ($employees) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$payment_mode=$this->input->get('payment_mode');
		$start_date=$this->input->get('from');
		$end_date=$this->input->get('to');
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->PaymentMode_model->paymentModes();
		$employees=array();
		
		if ($bid!=null) {
			if ($bid=="All") {
				$employees=$this->PaymentMode_model->allSalariesDetails();
			}
			else{
				$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("e.emp_bid=".$bid);
			}
			
		}
		else if ($ename!=null) {
			if ($ename=="All") {
				$employees=$this->PaymentMode_model->allSalariesDetails();
			}
			else{
				$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("e.emp_name='".ltrim($ename)."'");
			}
			
		}
		else if ($payment_mode!=null && $activity_type!=null) {
			$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("pt.payment_type='".$payment_mode."' and la.dept_name='".$activity_type."'");
		}
		else if ($activity_type!=null) {
			$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("la.dept_name='".$activity_type."'");
		}
		else if ($payment_mode!=null) {
			$employees=$this->PaymentMode_model->employeeSalaryDataByWhere("pt.payment_type='".$payment_mode."'");
		}
		$data=array('current_page'=>'Salary Payment Methods','page_title'=>'Salary Payment Methods','parent_menu'=>'','template'=>'SalaryPaymentMethods');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		if (count($employees)) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('theme/template',$data);
	}

}
