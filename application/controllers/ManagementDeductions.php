<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class ManagementDeductions extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		$this->load->model('Employee_leave_model');
		$this->load->model('Global_model');
		$this->load->model('Myfunctions');
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);

		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		$get_line_of_activity_log = $this->Global_model->get_values_where($this->config->item('line_of_activity_log'), array('flag'=>2));
		$data=array('current_page'=>'Management Deductions','page_title'=>'Management Deductions','parent_menu'=>'','template'=>'masters/management_deductions/add','bids'=>$employees_bid,'get_line_of_activity_log'=>$get_line_of_activity_log);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('emp_bid','emp_bid','required');
		$this->form_validation->set_rules('month','month','required');
		$this->form_validation->set_rules('year','year','required');
		$this->form_validation->set_rules('amount','amount','required');
		
		 if($this->form_validation->run() === FALSE)
		{
			//print_r($this->form_validation->run());exit();
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$designation_name = $this->input->post('designation_id');
			$emp_bid = $this->input->post('emp_bid');
			$month = $this->input->post('month');
			$year = $this->input->post('year');
			$amount=$this->input->post('amount');
			$remarks=$this->input->post('remarks');
			$this->db->trans_begin();
			$insert_array = array('emp_bid'=>$emp_bid,'remarks'=>$remarks,'amount'=>$amount,'month'=>$month,'year'=>$year);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Management Deductions not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Deducted successfully'));
			}
			redirect('ManagementDeductions');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('line_of_activity');
		$where = array('activity_id'=>base64_decode($id));
		$activity_id = base64_decode($id);
		$getPpaymentTypeEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_line_of_activity = $this->Global_model->get_values_where($this->config->item('line_of_activity'),array('status'=>1));
		$get_line_of_activity_log = $this->Global_model->get_values_where($this->config->item('line_of_activity_log'), array('flag'=>2));
			$data=array('current_page'=>'Line of Activity','page_title'=>'Line of Activity','parent_menu'=>'','template'=>'masters/ManagementDeductions/add','get_line_of_activity'=>$get_line_of_activity,'getPpaymentTypeEdit'=>$getPpaymentTypeEdit,'get_line_of_activity_log'=>$get_line_of_activity_log);
		$this->form_validation->set_rules('dept_name','dept_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$dept_name = $this->input->post('dept_name');
			$dept_id = $this->input->post('dept_id');
			$from_time = $this->input->post('from_time');
			$to_time = $this->input->post('to_time');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('dept_name'=>$dept_name,'dept_id'=>$dept_id,'from_time'=>$from_time,'to_time'=>$to_time, 'status'=>$status,'activity_id'=>$activity_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Line of Activity not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Line of Activity updated successfully'));
			}
			redirect('ManagementDeductions');
		}
		
	}
	function delete($id)
	{
			$activity_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('activity_id'=>$activity_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Line of Activity not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Line of Activity  Deleted successfully'));
		}
		redirect('ManagementDeductions');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$activity_id = base64_decode($id);
				$undo_array = array('activity_id'=>$activity_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Line of Activity not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Line of Activity undo successfully'));
				}
		
		redirect('ManagementDeductions');
	}
	function all_actions($action,$insert_array)
	{		
		$emp_id  = $this->session->userdata('emp_id');
		$amount = $insert_array['amount'];
		if($action == "insert")
		{
			$getData=$this->Myfunctions->getData('management_deductions','emp_bid='.$insert_array['emp_bid'].' and deduction_month='.date('m',strtotime($insert_array['month'])).' and deduction_year='.$insert_array['year']);
			if ($getData) {
				$data1 = array('amount'=>$insert_array['amount'],'remarks'=>$insert_array['remarks'],'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$where = array('emp_bid'=>$insert_array['emp_bid'],'deduction_month'=>date('m',strtotime($insert_array['month'])),'deduction_year'=>$insert_array['year']);
				$this->Global_model->update('management_deductions',$data1,$where);
			}
			else{
				$param= array('emp_bid'=>$insert_array['emp_bid'],'amount'=>$insert_array['amount'],'deduction_month'=>date('m',strtotime($insert_array['month'])),'deduction_year'=>$insert_array['year'],'remarks'=>$insert_array['remarks'],'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Myfunctions->addRecord("management_deductions",$param);
			}
			
		}
		else if($action == "edit")
		{
			$dept_id = $insert_array['dept_id'];
			$activity_id = $insert_array['activity_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('line_of_activity'), array('activity_id'=>$activity_id));
				
				$param['insertdata'] = array('activity_id'=>$insertdata->activity_id,'dept_name'=>$insertdata->dept_name,'dept_id'=>$insertdata->dept_id,'from_time'=>$insertdata->from_time,'to_time'=>$insertdata->to_time,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('line_of_activity_log');
			/* Update log table end */
			$data1 = array('activity_id'=>$activity_id,'dept_name'=>$dept_name,'dept_id'=>$dept_id,'status'=>$status,'from_time'=>$from_time,'to_time'=>$to_time,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('activity_id'=>$activity_id);
			$this->Global_model->update($this->config->item('line_of_activity'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$activity_id = $insert_array['activity_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('line_of_activity'), array('activity_id'=>$activity_id));
				
				$param['insertdata'] = array('activity_id'=>$activity_id,'dept_name'=>$insertdata->dept_name,'dept_id'=>$insertdata->dept_id,'status'=>$insertdata->status,'from_time'=>$insertdata->from_time,'to_time'=>$insertdata->to_time,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('line_of_activity_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('line_of_activity')." WHERE activity_id =".$activity_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$activity_id = $insert_array['activity_id'];
			$where = array('activity_id'=>$activity_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('line_of_activity_log')." WHERE activity_id=".$activity_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('activity_id'=>$activity_id,'dept_name'=>$getPaymentTypes->dept_name,'dept_id'=>$getPaymentTypes->dept_id,'from_time'=>$getPaymentTypes->from_time,'to_time'=>$getPaymentTypes->to_time,'status'=>$getPaymentTypes->status,'from_time'=>$getPaymentTypes->from_time,'to_time'=>$getPaymentTypes->to_time,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('line_of_activity'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Line of Activity not Undo '));	
					redirect('ManagementDeductions');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('ManagementDeductions');
		}	
	}
	function getEmployeeDetails()
	{
		$emp_bid = $this->input->post('emp_bid');
		$get_employee_details = $this->db->query("SELECT * FROM `employee` e LEFT JOIN line_of_activity l ON e.`activity_id` = l.activity_id LEFT JOIN  designation d on d.designation_id=e.designation WHERE e.`emp_bid` = '".$emp_bid."'")->row();
		$data['emp_name'] = $get_employee_details->emp_name;
		$data['dept_name'] = $get_employee_details->dept_name;
		$data['designation_name'] = $get_employee_details->designation_name;
		$data['emp_id'] = $get_employee_details->emp_id;
		echo json_encode($data);
	}
}
