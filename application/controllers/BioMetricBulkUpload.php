<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'controllers/Global_System.php';
class BioMetricBulkUpload extends Global_System {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('students_tbl_model');
    }

    public function index()
    {
      //  $data['students'] = $this->students_tbl_model->getAll();
        $this->load->view('student_details',$data);
    }
    public function import()
    {
        if(!$this->input->is_ajax_request()){
            show_404();
        }
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        $this->form_validation->set_rules('file','File','callback_notEmpty');
        
        $response= false;
        if(!$this->form_validation->run()){
            $response['status']    = 'form-incomplete';
            $response['errors']    =    array(
                array(
                    'field'    => 'input[name="file"]',
                    'error'    => form_error('file')
                )
            );
        }
        else{
            try{
                
                $filename = $_FILES["file"]["tmp_name"];
                if($_FILES['file']['size'] > 0)
                {
                    $file = fopen($filename,"r");
                    $is_header_removed = TRUE;
                    while(($importdata = fgetcsv($file, 10000, ",")) !== FALSE)
                    {
                        if(!$is_header_removed){
                            $is_header_removed = TRUE;
                            continue;
                        }
                        $row = array(
                            'emp_bid'     =>  !empty($importdata[1])?$importdata[1]:'',
                            'log_date'         =>  !empty($importdata[2])?date("Y-m-d",strtotime($importdata[2])):'',
                            'log_time'        =>  !empty($importdata[3])? date("h:m a",strtotime(str_replace("30-12-1899 ", "", $importdata[3]))):'');
                        echo json_encode($row);exit();
                        $this->db->trans_begin();
                        $this->students_tbl_model->add($row);
                        if(!$this->db->trans_status()){
                            $this->db->trans_rollback();
                            $response['status']='error';
                            $response['message']='Something went wrong while saving your data';
                            break;
                        }else{
                            $this->db->trans_commit();
                            $response['status']='success';
                            $response['message']='Successfully added new record.';
                        }
                    }
                    fclose($file);
                }
               
            }
            catch(Exception $e){
                $this->db->trans_rollback();
                $response['status']='error';
                $response['message']='Something went wrong while trying to communicate with the server.';
            }
        }
        echo json_encode($response);
    }
    public function notEmpty(){
        if(!empty($_FILES['file']['name'])){
            return TRUE;
        }
        else{
            $this->form_validation->set_message('notEmpty','The {field} field can not be empty.');
            return FALSE;
        }
    }
}