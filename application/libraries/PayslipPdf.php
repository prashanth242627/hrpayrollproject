<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class PayslipPdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }
    public function Header() {
			// Logo
			$image_file = 'http://rdenterprises.info/applications/hrpayroll/images/635008738265488791_JUBILEE.png';
			// Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
			$this->Rect(0, 0, 1000, 20,'F',array(),array(133, 144, 229));
			$this->Image($image_file, 5, 0, 20, '', 'PNG', '', 'T', false, 25, '', false, false, 0, false, false, false);
			$this->SetTextColor(255,255,255);
			// Set font
			$this->SetFont('helvetica', 'B', 13);
			// Title
			// Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M') 
			$this->Cell(0, 5, "", 0, false, 'R', 0, '', 0, false, 'M', 'M');
			$this->Ln();
			$this->Ln();

			$this->Cell(0, 0, "Employee PaySlip ", 0, false, 'C', 0, '', 0, false, 'M', 'M');
			
			
		}

		// Page footer
		public function Footer() {
			// Position at 15 mm from bottom
			$this->SetY(-15);
			// Set font
			$this->SetFont('helvetica', 'I', 8);
			// Page number
			$this->Cell(0, 10, "This is a Computer Generated PaySlip Doesn't required any Signature..", 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
}
