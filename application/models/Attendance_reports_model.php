<?php
class Attendance_reports_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
                $this->load->model('Myfunctions');
	}
    //using
	public function getAllReports($month,$year)
        {
                $sql_query="select e.temp_emp_id,e.emp_name,e.emp_bid,am.leave_date,lt.leave_type,loa.dept_name,d.designation_name,lt.leave_type from  line_of_activity loa, designation d, employee e RIGHT JOIN attendance_marking am on am.emp_id=e.emp_id LEFT JOIN leave_master lm on lm.leave_master_id=am.leave_master_id LEFT JOIN leave_types lt ON lt.leave_type_id=lm.leave_type_id where e.activity_id=loa.activity_id and d.designation_id=e.designation and YEAR(am.leave_date)=".$year." and MONTHNAME(am.leave_date)='".$month."' order by length(e.emp_bid),e.emp_bid";
                $reports=$this->Myfunctions->getQueryDataList($sql_query);
                return $reports;
        }
	public function getAttendanceReportsByDept($where)
	{
        $sql_query="select e.temp_emp_id,e.emp_name,e.emp_bid,am.leave_date,lt.leave_type,loa.dept_name,d.designation_name,lt.leave_type from  line_of_activity loa, designation d, employee e RIGHT JOIN attendance_marking am on am.emp_bid=e.emp_bid LEFT JOIN leave_master lm on lm.leave_master_id=am.leave_master_id LEFT JOIN leave_types lt ON lt.leave_type_id=lm.leave_type_id where e.activity_id=loa.activity_id and d.designation_id=e.designation and loa.dept_name='".$where."'";

        $reports=$this->Myfunctions->getQueryDataList($sql_query);
        return $reports;
	}
    // using
        public function getAttendanceReportsByWhere($where=' status=1')
        {
                $sql_query="select e.temp_emp_id,e.emp_name,e.emp_bid,am.leave_date,lt.leave_type,loa.dept_name,d.designation_name from  line_of_activity loa, designation d, employee e RIGHT JOIN attendance_marking am on am.emp_bid=e.emp_bid LEFT JOIN leave_types lt ON lt.leave_type_id=am.leave_type_id where e.activity_id=loa.activity_id and d.designation_id=e.designation and ".$where;
                $reports=$this->Myfunctions->getQueryDataList($sql_query);
                return $reports;
        }
        public function getBioMetricReportsOfCurrentDate()
        {
                $sql_query="SELECT e.temp_emp_id,e.emp_name,ba.emp_bid,loa.dept_name,loa.from_time,loa.to_time,d.designation_name,ba.log_date,MIN(ba.log_time) as in_time, MAX(ba.log_time) as out_time,TIMEDIFF(MAX(ba.log_time),MIN(ba.log_time)) as diffrence,ba.log_date FROM line_of_activity loa, designation d,  biometirc_attendance ba left JOIN employee e on e.emp_bid=ba.emp_bid WHERE e.activity_id=loa.activity_id and d.designation_id=e.designation and ba.log_date=CURRENT_DATE GROUP BY ba.log_date, ba.emp_bid";
                $reports=$this->Myfunctions->getQueryDataList($sql_query);
                return $reports;
        }
        public function getAllBioMetricReports()
        {
                $sql_query="SELECT e.temp_emp_id,e.emp_name,ba.emp_bid,loa.dept_name,loa.from_time,loa.to_time,ba.log_date,d.designation_name,ba.log_date, MIN(ba.log_time) as in_time, MAX(ba.log_time) as out_time,TIMEDIFF(MAX(ba.log_time),MIN(ba.log_time)) as diffrence FROM line_of_activity loa, designation d,  biometirc_attendance ba left JOIN employee e on e.emp_bid=ba.emp_bid WHERE e.activity_id=loa.activity_id and d.designation_id=e.designation GROUP BY ba.log_date, ba.emp_bid";
                $reports=$this->Myfunctions->getQueryDataList($sql_query);
                //print_r($sql_query  );exit();
               return $reports;
        }
        public function getBioMetricReportsByDept($where)
        {
                $sql_query="SELECT e.temp_emp_id,e.emp_name,ba.emp_bid,loa.dept_name,loa.from_time,loa.to_time,ba.log_date,d.designation_name,ba.log_date, MIN(ba.log_time) as in_time, MAX(ba.log_time) as out_time,TIMEDIFF(MAX(ba.log_time),MIN(ba.log_time)) as diffrence FROM line_of_activity loa, designation d,  biometirc_attendance ba left JOIN employee e on e.emp_bid=ba.emp_bid WHERE e.activity_id=loa.activity_id and d.designation_id=e.designation  and loa.dept_name='".$where."' GROUP BY ba.log_date, ba.emp_bid";
                $reports=$this->Myfunctions->getQueryDataList($sql_query);

                return $reports;
        }
        public function getBioMetricReportsByWhere($where)
        {
                 $sql_query="SELECT e.temp_emp_id,e.emp_name,ba.emp_bid,loa.dept_name,ba.log_date,d.designation_name,ba.log_date, MIN(ba.log_time) as in_time, MAX(ba.log_time) as out_time, TIMEDIFF(MAX(ba.log_time),MIN(ba.log_time)) as diffrence, case when ba.log_date>='2019-04-12' and ba.log_date<='2019-05-31' then '09:30:00' else loa.from_time end as from_time,loa.to_time,case when ba.log_date>='2019-04-12' and ba.log_date<='2019-05-31' then (TIME_TO_SEC(MIN(ba.log_time))-TIME_TO_SEC('09:30:00'))/60 else (TIME_TO_SEC(MIN(ba.log_time))-TIME_TO_SEC(loa.from_time))/60 end as buffer_time FROM line_of_activity loa, designation d,  biometirc_attendance ba left JOIN employee e on e.emp_bid=ba.emp_bid WHERE e.activity_id=loa.activity_id and d.designation_id=e.designation  and ".$where." GROUP BY ba.log_date, ba.emp_bid order by buffer_time";
                 
                $reports=$this->Myfunctions->getQueryDataList($sql_query);
                return $reports;
        }
        public function getManulaAndBioMetricReportsOfCurrentMonthByBid($bid)
        {
            $first_day_this_month = date('Y-m-01');
            $last_day_this_month  = date('Y-m-d');
          //  echo $first_day_this_month." ".$last_day_this_month;exit;
            $sql_query="select  d.temp_emp_id, d.emp_bid,d.emp_name,d.log_date,d.dept_name,d.designation_name, max(ba_status), max(am_status), max(status),
case when max(ba_status) ='P' and max(am_status) is not null then 'Manul Check required' 
     when max(ba_status) is null and max(am_status) is null then 'Check'
else null END  cp
from 
(
SELECT e.temp_emp_id as temp_emp_id, e.emp_bid as emp_bid, e.emp_name as emp_name,loa.dept_name,desi.designation_name, b.log_date as log_date, 'P' as ba_status, null as am_status, b.status 
FROM line_of_activity loa, designation desi,employee e LEFT JOIN biometirc_attendance b ON e.emp_bid = b.emp_bid 
WHERE b.log_date >= '".$first_day_this_month."' AND b.log_date <= '".$last_day_this_month."' AND
loa.activity_id=e.activity_id AND desi.designation_id=e.designation 
AND e.emp_bid=".$bid."  
AND b.status = 1 
UNION 
SELECT e1.temp_emp_id, e1.emp_bid, e1.emp_name,loa1.dept_name,desi1.designation_name, a.leave_date, null as ba_status, lt.leave_type as am_status, a.status 
FROM leave_types lt,line_of_activity loa1, designation desi1,leave_master lm, employee e1 LEFT JOIN attendance_marking a  ON e1.emp_id = a.emp_id 
WHERE a.leave_date >= '".$first_day_this_month."' AND a.leave_date <= '".$last_day_this_month."' 
AND a.leave_master_id=lm.leave_master_id 
AND loa1.activity_id=e1.activity_id
AND desi1.designation_id=e1.designation
AND lm.leave_type_id=lt.leave_type_id 
AND e1.emp_bid=".$bid." 
AND a.status = 1
union 
(select emp.temp_emp_id,  emp.emp_bid,emp.emp_name ,loa2.dept_name,desi2.designation_name, v.selected_date, null as ba_status, null as am_status, null as status
 from 
(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v,
 employee emp LEFT JOIN line_of_activity loa2 on loa2.activity_id=emp.activity_id LEFT JOIN designation desi2 on desi2.designation_id= emp.designation
where selected_date between '".$first_day_this_month."' and '".$last_day_this_month."'
and emp.emp_bid = ".$bid.") 
) d
group by d.log_date, d.temp_emp_id, d.emp_bid,d.emp_name,d.log_date
ORDER BY d.log_date ASC";
            $reports=$this->Myfunctions->getQueryDataList($sql_query);
            return $reports;
        }
         public function getManulaAndBioMetricReportsOfCurrentMonthByName($name)
        {
            $first_day_this_month = date('Y-m-01');
            $last_day_this_month  = date('Y-m-d');
          //  echo $first_day_this_month." ".$last_day_this_month;exit;
            $sql_query="select  d.temp_emp_id, d.emp_bid,d.emp_name,d.log_date,d.dept_name,d.designation_name, max(ba_status), max(am_status), max(status),
case when max(ba_status) ='P' and max(am_status) is not null then 'Manul Check required' 
     when max(ba_status) is null and max(am_status) is null then 'Check'
else null END  cp
from 
(
SELECT e.temp_emp_id as temp_emp_id, e.emp_bid as emp_bid, e.emp_name as emp_name,loa.dept_name,desi.designation_name, b.log_date as log_date, 'P' as ba_status, null as am_status, b.status 
FROM line_of_activity loa, designation desi,employee e LEFT JOIN biometirc_attendance b ON e.emp_bid = b.emp_bid 
WHERE b.log_date >= '".$first_day_this_month."' AND b.log_date <= '".$last_day_this_month."' AND
loa.activity_id=e.activity_id AND desi.designation_id=e.designation 
AND e.emp_name='".$name."'  
AND b.status = 1 
UNION 
SELECT e1.temp_emp_id, e1.emp_bid, e1.emp_name,loa1.dept_name,desi1.designation_name, a.leave_date, null as ba_status, lt.leave_type as am_status, a.status 
FROM leave_types lt,line_of_activity loa1, designation desi1,leave_master lm, employee e1 LEFT JOIN attendance_marking a  ON e1.emp_id = a.emp_id 
WHERE a.leave_date >= '".$first_day_this_month."' AND a.leave_date <= '".$last_day_this_month."' 
AND a.leave_master_id=lm.leave_master_id 
AND loa1.activity_id=e1.activity_id
AND desi1.designation_id=e1.designation
AND lm.leave_type_id=lt.leave_type_id 
AND e1.emp_name='".$name."' 
AND a.status = 1
union 
(select emp.temp_emp_id,  emp.emp_bid,emp.emp_name ,loa2.dept_name,desi2.designation_name, v.selected_date, null as ba_status, null as am_status, null as status
 from 
(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v,
 employee emp LEFT JOIN line_of_activity loa2 on loa2.activity_id=emp.activity_id LEFT JOIN designation desi2 on desi2.designation_id= emp.designation
where selected_date between '".$first_day_this_month."' and '".$last_day_this_month."'
and emp.emp_name = '".$name."') 
) d
group by d.log_date, d.temp_emp_id, d.emp_bid,d.emp_name,d.log_date
ORDER BY d.log_date ASC";
            $reports=$this->Myfunctions->getQueryDataList($sql_query);
            return $reports;
        }
         public function getManulaAndBioMetricReportsByBidMonth($bid,$month,$year)
        {
            $first_day_this_month = date('Y-m-01',strtotime($month." ".$year));
            
            $last_day_this_month  = date('Y-m-t',strtotime($month." ".$year));
            $sql_query="select  d.temp_emp_id, d.emp_bid,d.emp_name,d.log_date,d.dept_name,d.designation_name, max(ba_status), max(am_status), max(status),
case when max(ba_status) ='P' and max(am_status) is not null then 'Manul Check required' 
     when max(ba_status) is null and max(am_status) is null then 'Check'
else null END  cp
from 
(
SELECT e.temp_emp_id as temp_emp_id, e.emp_bid as emp_bid, e.emp_name as emp_name,loa.dept_name,desi.designation_name, b.log_date as log_date, 'P' as ba_status, null as am_status, b.status 
FROM line_of_activity loa, designation desi,employee e LEFT JOIN biometirc_attendance b ON e.emp_bid = b.emp_bid 
WHERE b.log_date >= '".$first_day_this_month."' AND b.log_date <= '".$last_day_this_month."' AND
loa.activity_id=e.activity_id AND desi.designation_id=e.designation 
AND e.emp_bid=".$bid."  
AND b.status = 1 
UNION 
SELECT e1.temp_emp_id, e1.emp_bid, e1.emp_name,loa1.dept_name,desi1.designation_name, a.leave_date, null as ba_status, lt.leave_type as am_status, a.status 
FROM leave_types lt,line_of_activity loa1, designation desi1,leave_master lm, employee e1 LEFT JOIN attendance_marking a  ON e1.emp_id = a.emp_id 
WHERE a.leave_date >= '".$first_day_this_month."' AND a.leave_date <= '".$last_day_this_month."' 
AND a.leave_master_id=lm.leave_master_id 
AND loa1.activity_id=e1.activity_id
AND desi1.designation_id=e1.designation
AND lm.leave_type_id=lt.leave_type_id 
AND e1.emp_bid=".$bid." 
AND a.status = 1
union 
(select emp.temp_emp_id,  emp.emp_bid,emp.emp_name ,loa2.dept_name,desi2.designation_name, v.selected_date, null as ba_status, null as am_status, null as status
 from 
(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v,
 employee emp LEFT JOIN line_of_activity loa2 on loa2.activity_id=emp.activity_id LEFT JOIN designation desi2 on desi2.designation_id= emp.designation
where selected_date between '".$first_day_this_month."' and '".$last_day_this_month."'
and emp.emp_bid = ".$bid.") 
) d
group by d.log_date, d.temp_emp_id, d.emp_bid,d.emp_name,d.log_date
ORDER BY d.log_date ASC";
            $reports=$this->Myfunctions->getQueryDataList($sql_query);
            return $reports;
        }
        public function getManulaAndBioMetricReportsByNameMonth($name,$month,$year)
        {
            $first_day_this_month = date('Y-m-01',strtotime($month." ".$year));
            $last_day_this_month  = date('Y-m-t',strtotime($month." ".$year));
          //  echo $first_day_this_month." ".$last_day_this_month;exit;
            $sql_query="select  d.temp_emp_id, d.emp_bid,d.emp_name,d.log_date,d.dept_name,d.designation_name, max(ba_status), max(am_status), max(status),
case when max(ba_status) ='P' and max(am_status) is not null then 'Manul Check required' 
     when max(ba_status) is null and max(am_status) is null then 'Check'
else null END  cp
from 
(
SELECT e.temp_emp_id as temp_emp_id, e.emp_bid as emp_bid, e.emp_name as emp_name,loa.dept_name,desi.designation_name, b.log_date as log_date, 'P' as ba_status, null as am_status, b.status 
FROM line_of_activity loa, designation desi,employee e LEFT JOIN biometirc_attendance b ON e.emp_bid = b.emp_bid 
WHERE b.log_date >= '".$first_day_this_month."' AND b.log_date <= '".$last_day_this_month."' AND
loa.activity_id=e.activity_id AND desi.designation_id=e.designation 
AND e.emp_name='".$name."'  
AND b.status = 1 
UNION 
SELECT e1.temp_emp_id, e1.emp_bid, e1.emp_name,loa1.dept_name,desi1.designation_name, a.leave_date, null as ba_status, lt.leave_type as am_status, a.status 
FROM leave_types lt,line_of_activity loa1, designation desi1,leave_master lm, employee e1 LEFT JOIN attendance_marking a  ON e1.emp_id = a.emp_id 
WHERE a.leave_date >= '".$first_day_this_month."' AND a.leave_date <= '".$last_day_this_month."' 
AND a.leave_master_id=lm.leave_master_id 
AND loa1.activity_id=e1.activity_id
AND desi1.designation_id=e1.designation
AND lm.leave_type_id=lt.leave_type_id 
AND e1.emp_name='".$name."' 
AND a.status = 1
union 
(select emp.temp_emp_id,  emp.emp_bid,emp.emp_name ,loa2.dept_name,desi2.designation_name, v.selected_date, null as ba_status, null as am_status, null as status
 from 
(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v,
 employee emp LEFT JOIN line_of_activity loa2 on loa2.activity_id=emp.activity_id LEFT JOIN designation desi2 on desi2.designation_id= emp.designation
where selected_date between '".$first_day_this_month."' and '".$last_day_this_month."'
and emp.emp_name = '".$name."') 
) d
group by d.log_date, d.temp_emp_id, d.emp_bid,d.emp_name,d.log_date
ORDER BY d.log_date ASC";
            $reports=$this->Myfunctions->getQueryDataList($sql_query);
            return $reports;
        }
}
?>