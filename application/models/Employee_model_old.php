<?php
class Employee_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}
	public function allEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $sql_query1="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        $employees_log=$CI->Myfunctions->getQueryDataList($sql_query1);
        foreach ($employees_log as $employee_log) {
        	array_push($employees, $employee_log);
        }
        return $employees;
	}
	public function activities()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $employees=$CI->Myfunctions->getDataList("line_of_activity","");
        return $employees;
	}
	public function employeesBid()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select emp_bid from employee order by emp_bid asc";
        $employees_bid=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees_bid;
	}
	public function employeesName()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select emp_name from employee order by emp_name asc";
        $employees_name=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees_name;
	}
	public function employeeData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function lineOfActivityEmployees($type)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where loa.dept_name='".$type."' and e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function activeEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function inActiveEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function newEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $d2 = date('y-m-d', strtotime('-30 days'));
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and date>".$d2;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function getEmployeesData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e, line_of_activity loa where e.activity_id=loa.activity_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
}
?>