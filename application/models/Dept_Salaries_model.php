<?php
class Dept_Salaries_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct(); 
    }
    public function AlldeptSalaries($month,$year)
    {
      $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $month_days=cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($month)),$year);
        if ($month_days>30) {
          $month_days=30;
        }
        $sql_query="select dept_name, count(*) emp_count,sum(gross) Gross_pay,sum(pf+esi+PT+sd+tds) deductions, sum(gross-(pf+esi+PT+sd+tds)) Net_Pay
        from(

        select temp_emp_id, emp_name, emp_bid, date_of_join, m_gross master_gross,

        basic, hra, da, other_allowances, incr, co_allowance, special_allowance,cca,

        gross, lop_count,exmp_limit, savings, dept_name,

        case when pf_consider = 1 then

            case when (basic+da)* (select deduct_value from std_pf_deductions)/100 <=1800

                  then round((basic+da)*(select deduct_value from std_pf_deductions)/100) else 1800 END

                 else 0

        end pf,

        case when esi_consider = 1 then round((gross)*esi.deduct_value/100)

             else 0

        end esi,

        pt.deduct_value PT,

        case when m_gross < (exmp_limit+savings) then 0

             else round(((m_gross-(exmp_limit+savings))*tds.deduct_value/100))

        END TDS,

        case when (sdscope = 1 and DATE_ADD(NOW(), INTERVAL -10 MONTH) > date_of_join) then round(gross*10/100)

             else 0

        end sd

        from(

        select salary.*,

        (salary.basic+salary.other_allowances+salary.incr+salary.co_allowance+salary.da+salary.special_allowance+salary.hra+salary.cca) gross

        from (

        select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join,

            (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca) m_gross,   

        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.basic)

             else (pay.basic)-((pay.basic/(".$month_days."))*lc.lop_count)

        end) basic,    

        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.other_allowance)

             else (pay.other_allowance)-((pay.other_allowance/(".$month_days."))*lc.lop_count)

        end) other_allowances,    

        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.incr)

             else (pay.incr)-((pay.incr/(".$month_days."))*lc.lop_count)

        end) incr,

        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.co_allowance)

             else (pay.co_allowance)-((pay.co_allowance/(".$month_days."))*lc.lop_count)

        end) co_allowance,

        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.da)

             else (pay.da)-((pay.da/(".$month_days."))*lc.lop_count)

        end) da,

        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.special_allowance)

             else (pay.special_allowance)-((pay.special_allowance/(".$month_days."))*lc.lop_count)

        end) special_allowance,

        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.hra)

             else (pay.hra)-((pay.hra/(".$month_days."))*lc.lop_count)

        end) hra,

        round(case when ifnull(lc.lop_count,0) <= 0 then (pay.cca)

             else (pay.cca)-((pay.cca/(".$month_days."))*lc.lop_count)

        end) cca,

        esi_consider,is_pay_hold,pf_consider,sdscope,lc.lop_count, se.exmp_limit,

        CASE when lc.lop_month between 4 and 12 then 180000

                 else (case when sm.sec_80c<150000 then sm.sec_80c else 150000 END

                      +case when sm.sec_80d<25000 then sm.sec_80d else 25000 END

                      +case when sm.sec_80cc<25000 then sm.sec_80cc else 25000 END)

            end savings, la.dept_name

        from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid

        LEFT JOIN std_tds_exemption se on case when e.sex ='' then 'M' else e.sex end = se.sex   

        LEFT JOIN empsavingsmaster sm on e.emp_bid = sm.emp_bid   

        LEFT JOIN line_of_activity la on e.activity_id = la.activity_id   

        LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid

                                  and lc.lop_month=".date('m',strtotime($month))."

                                  and lc.lop_year =".$year." ) salary

        ) mon_sal

        LEFT JOIN std_tds_deductions tds on

           (case when (m_gross-(exmp_limit+savings)) > 0 then (m_gross-(exmp_limit+savings))

                else 0 end)  between tds.min_limit and tds.max_limit

        LEFT JOIN std_esi_deductions esi on round(m_gross) between esi.min_limit and esi.max_limit

        LEFT JOIN std_pt_deductions pt on  gross between pt.min_limit and pt.max_limit

        ) tot

        group by dept_name";
        $employees=$CI->Myfunctions->getQueryData($sql_query);
        return $employees;
    }
    public function deptSalariesByWhere($where,$month,$year)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $month_days=cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($month)),$year);
        if ($month_days>30) {
          $month_days=30;
        }
        $sql_query="select dept_name, count(*) emp_count,sum(gross) Gross_pay,sum(pf+esi+PT+sd+tds) deductions, sum(gross-(pf+esi+PT+sd+tds)) Net_Pay

from(

select temp_emp_id, emp_name, emp_bid, date_of_join, m_gross master_gross,

basic, hra, da, other_allowances, incr, co_allowance, special_allowance,cca,

gross, lop_count,exmp_limit, savings, dept_name,

case when pf_consider = 1 then

    case when (basic+da)* (select deduct_value from std_pf_deductions)/100 <=1800

          then round((basic+da)*(select deduct_value from std_pf_deductions)/100) else 1800 END

         else 0

end pf,

case when esi_consider = 1 then round((gross)*esi.deduct_value/100)

     else 0

end esi,

pt.deduct_value PT,

case when m_gross < (exmp_limit+savings) then 0

     else round(((m_gross-(exmp_limit+savings))*tds.deduct_value/100))

END TDS,

case when (sdscope = 1 and DATE_ADD(NOW(), INTERVAL -10 MONTH) > date_of_join) then round(gross*10/100)

     else 0

end sd

from(

select salary.*,

(salary.basic+salary.other_allowances+salary.incr+salary.co_allowance+salary.da+salary.special_allowance+salary.hra+salary.cca) gross

from (

select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join,

    (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca) m_gross,   

round(case when ifnull(lc.lop_count,0) <= 0 then (pay.basic)

     else (pay.basic)-((pay.basic/(".$month_days."))*lc.lop_count)

end) basic,    

round(case when ifnull(lc.lop_count,0) <= 0 then (pay.other_allowance)

     else (pay.other_allowance)-((pay.other_allowance/(".$month_days."))*lc.lop_count)

end) other_allowances,    

round(case when ifnull(lc.lop_count,0) <= 0 then (pay.incr)

     else (pay.incr)-((pay.incr/(".$month_days."))*lc.lop_count)

end) incr,

round(case when ifnull(lc.lop_count,0) <= 0 then (pay.co_allowance)

     else (pay.co_allowance)-((pay.co_allowance/(".$month_days."))*lc.lop_count)

end) co_allowance,

round(case when ifnull(lc.lop_count,0) <= 0 then (pay.da)

     else (pay.da)-((pay.da/(".$month_days."))*lc.lop_count)

end) da,

round(case when ifnull(lc.lop_count,0) <= 0 then (pay.special_allowance)

     else (pay.special_allowance)-((pay.special_allowance/(".$month_days."))*lc.lop_count)

end) special_allowance,

round(case when ifnull(lc.lop_count,0) <= 0 then (pay.hra)

     else (pay.hra)-((pay.hra/(".$month_days."))*lc.lop_count)

end) hra,

round(case when ifnull(lc.lop_count,0) <= 0 then (pay.cca)

     else (pay.cca)-((pay.cca/(".$month_days."))*lc.lop_count)

end) cca,

esi_consider,is_pay_hold,pf_consider,sdscope,lc.lop_count, se.exmp_limit,

CASE when lc.lop_month between 4 and 12 then 180000

         else (case when sm.sec_80c<150000 then sm.sec_80c else 150000 END

              +case when sm.sec_80d<25000 then sm.sec_80d else 25000 END

              +case when sm.sec_80cc<25000 then sm.sec_80cc else 25000 END)

    end savings, la.dept_name

from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid

LEFT JOIN std_tds_exemption se on case when e.sex = '' then 'M' else e.sex end = se.sex   

LEFT JOIN empsavingsmaster sm on e.emp_bid = sm.emp_bid   

LEFT JOIN line_of_activity la on e.activity_id = la.activity_id   

LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid

                          and lc.lop_month=".date('m',strtotime($month))."

                                  and lc.lop_year =".$year." where $where ) salary

) mon_sal

LEFT JOIN std_tds_deductions tds on

   (case when (m_gross-(exmp_limit+savings)) > 0 then (m_gross-(exmp_limit+savings))

        else 0 end)  between tds.min_limit and tds.max_limit

LEFT JOIN std_esi_deductions esi on round(m_gross) between esi.min_limit and esi.max_limit

LEFT JOIN std_pt_deductions pt on  gross between pt.min_limit and pt.max_limit

) tot

group by dept_name";
        $employees=$CI->Myfunctions->getQueryData($sql_query);
        return $employees;
    }
}
?>