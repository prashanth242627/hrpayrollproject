<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Employee_leave_model extends CI_Model
{
        public function __construct()
	{
		parent::__construct(); 
	}
    public function checkLastUpdatedStatus($month,$year)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $month=date('m',strtotime($month));
        $sql_query="SELECT lop_month,lop_year FROM `emp_lop_calculation` where lop_month=$month and lop_year=$year ORDER by date(updated_date) desc LIMIT 1"; 
        $employees=$CI->Myfunctions->getQueryData($sql_query);
        if ($employees) {
            return false;
        }
        else{
            return true;
        }
        
    }
	public function allEmployees()
	{
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        
        $sql_query="select 
                    e.emp_bid,e.emp_name,e.temp_emp_id,e.date_of_join,d.designation_name,loa.dept_name,
                    MAX(CASE WHEN l.leave_type_id=3 THEN l.no_leaves else 0 end) el_opb, 
                    MAX(CASE WHEN l.leave_type_id=3 THEN l.used else 0.0 end) el_used,
                    MAX(CASE WHEN l.leave_type_id=4 THEN l.no_leaves else 0 end) sl_opb, 
                    MAX(CASE WHEN l.leave_type_id=4 THEN l.used else 0.0 end) sl_used,
                    MAX(CASE WHEN l.leave_type_id=5 THEN l.no_leaves else 0 end) cl_opb, 
                    MAX(CASE WHEN l.leave_type_id=5 THEN used else 0.0 end) cl_used
                    FROM employee e LEFT JOIN leave_master l on e.emp_bid=l.emp_bid LEFT JOIN designation d on d.designation_id=e.designation LEFT JOIN line_of_activity loa on loa.activity_id=e.activity_id
                    GROUP by emp_bid ORDER by LENGTH(e.emp_bid), e.emp_bid asc";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function allEmployeesMonthlyLeaves($month,$year){
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year); 
        if ($prev_month_days>30) {
            $prev_month_days=30;
        }
        $sql_query="select e.temp_emp_id, e.emp_bid, e.emp_name,loa.dept_name,d.designation_name, e.date_of_join, lop.lop_count,lop.lop_month, ".$prev_month_days." as no_of_days,
(case when el_assigned is NULL then 0 else el_assigned end) as el_assigned, 
(case when el_assigned is NULL then 0 else el_assigned end)-(case when prev_el_availed is NULL then 0 else prev_el_availed END) as el_open_balance, 
(case when el_availed is NULL then 0 else el_availed end) el_availed,
(case when el_assigned is NULL then 0 else el_assigned end)-((case when prev_el_availed is NULL then 0 else prev_el_availed END)+(case when el_availed IS NULL then 0 else el_availed END)) el_balance,
(case when sl_assigned is NULL then 0 else sl_assigned end) sl_assigned , 
(case when sl_assigned is NULL then 0 else sl_assigned end)-(case when prev_sl_availed is NULL then 0 else prev_sl_availed END) sl_open_balance,
(case when sl_availed is NULL then 0 else sl_availed end) sl_availed, 
(case when sl_assigned is NULL then 0 else sl_assigned end)-((case when prev_sl_availed IS NULL then 0 else prev_sl_availed end)+(case when sl_availed IS NULL then 0 else sl_availed END)) sl_balance,
(case when cl_assigned is NULL then 0 else cl_assigned end) cl_assigned, 
(case when cl_assigned is NULL then 0 else cl_assigned end)-(case when prev_cl_availed is NULL then 0 else prev_cl_availed END) cl_open_balance,
(case when cl_availed is NULL then 0 else cl_availed END) as cl_availed, 
(case when cl_assigned is NULL then 0 else cl_assigned END)-((case when prev_cl_availed is NULL then 0 else prev_cl_availed END)+(case when cl_availed is NULL then 0 else cl_availed END)) cl_balance,
case when lop_availed is NULL then 0 else lop_availed end as lop_availed,
am.cl_leave_dates, am.lop_leave_dates
from employee e LEFT JOIN (select emp_bid,
                            sum(case when leave_type_id=3 then no_leaves else 0 end) el_assigned,
                            sum(case when leave_type_id=4 then no_leaves else 0 end) sl_assigned,
                            sum(case when leave_type_id=5 then no_leaves else 0 end) cl_assigned
                            from leave_master
                            group by emp_bid) l on e.emp_bid=l.emp_bid 
LEFT JOIN (select emp_bid, 
            GROUP_CONCAT(DISTINCT day(case when leave_type_id=5 then leave_date else '' end)) cl_leave_dates,
           GROUP_CONCAT(DISTINCT day(case when leave_type_id=9 then leave_date else '' end)) lop_leave_dates,
           SUM(case when leave_type_id =3 then 1 else 0 END) el_availed,
           SUM(case when leave_type_id =4 then 1 else 0 END) sl_availed,
           SUM(case when leave_type_id =5 then 1 else 0 END) cl_availed,
           SUM(case when leave_type_id =9 then 1 else 0 END) lop_availed
           from attendance_marking
           where month(leave_date) = ".date('m',strtotime($month))." and year(leave_date)=".$year."
           group by emp_bid) am on e.emp_bid = am.emp_bid          
LEFT JOIN (select emp_bid, 
           SUM(case when leave_type_id =3 then 1 else 0 END) prev_el_availed,
           SUM(case when leave_type_id =4 then 1 else 0 END) prev_sl_availed,
           SUM(case when leave_type_id =5 then 1 else 0 END) prev_cl_availed
           from attendance_marking
           where month(leave_date)< ".date('m',strtotime($month))." and year(leave_date)=".$year."
           group by emp_bid) prev_am on e.emp_bid = prev_am.emp_bid  
LEFT JOIN emp_lop_calculation lop on lop.emp_bid=e.emp_bid and lop.lop_month=".date('m',strtotime($month))." and lop.lop_year=".$year."
LEFT JOIN designation d on d.designation_id=e.designation
LEFT JOIN line_of_activity loa on loa.activity_id=e.activity_id
ORDER BY LENGTH(e.emp_bid),e.emp_bid asc";
            //LEFT JOIN (SELECT emp_bid,lop_count from emp_lop_calculation WHERE lop_month=12 and lop_year=2018 GROUP by emp_bid) lops on e.emp_bid=lops.emp_bid
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        
        return $employees;
	}
	public function noOfMonthlyLeavesByWhere($where,$month,$year)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year); 
        if ($prev_month_days>30) {
            $prev_month_days=30;
        }
        $sql_query="select e.temp_emp_id, e.emp_bid, e.emp_name, e.date_of_join,loa.dept_name,d.designation_name,lop.lop_count,lop.lop_month, ".$prev_month_days." as no_of_days,
(case when el_assigned is NULL then 0 else el_assigned end) as el_assigned, 
(case when el_assigned is NULL then 0 else el_assigned end)-(case when prev_el_availed is NULL then 0 else prev_el_availed END) as el_open_balance, 
(case when el_availed is NULL then 0 else el_availed end) el_availed,
(case when el_assigned is NULL then 0 else el_assigned end)-((case when prev_el_availed is NULL then 0 else prev_el_availed END)+(case when el_availed IS NULL then 0 else el_availed END)) el_balance,
(case when sl_assigned is NULL then 0 else sl_assigned end) sl_assigned , 
(case when sl_assigned is NULL then 0 else sl_assigned end)-(case when prev_sl_availed is NULL then 0 else prev_sl_availed END) sl_open_balance,
(case when sl_availed is NULL then 0 else sl_availed end) sl_availed, 
(case when sl_assigned is NULL then 0 else sl_assigned end)-((case when prev_sl_availed IS NULL then 0 else prev_sl_availed end)+(case when sl_availed IS NULL then 0 else sl_availed END)) sl_balance,
(case when cl_assigned is NULL then 0 else cl_assigned end) cl_assigned, 
(case when cl_assigned is NULL then 0 else cl_assigned end)-(case when prev_cl_availed is NULL then 0 else prev_cl_availed END) cl_open_balance,
(case when cl_availed is NULL then 0 else cl_availed END) as cl_availed, 
(case when cl_assigned is NULL then 0 else cl_assigned END)-((case when prev_cl_availed is NULL then 0 else prev_cl_availed END)+(case when cl_availed is NULL then 0 else cl_availed END)) cl_balance,
case when lop_availed is NULL then 0 else lop_availed end as lop_availed,
am.cl_leave_dates, am.lop_leave_dates
from employee e LEFT JOIN (select emp_bid,
                            sum(case when leave_type_id=3 then no_leaves else 0 end) el_assigned,
                            sum(case when leave_type_id=4 then no_leaves else 0 end) sl_assigned,
                            sum(case when leave_type_id=5 then no_leaves else 0 end) cl_assigned
                            from leave_master
                            group by emp_bid) l on e.emp_bid=l.emp_bid 
LEFT JOIN (select emp_bid,  
            GROUP_CONCAT(DISTINCT day(case when leave_type_id=5 then leave_date else '' end)) cl_leave_dates,
           GROUP_CONCAT(DISTINCT day(case when leave_type_id=9 then leave_date else '' end)) lop_leave_dates,
           SUM(case when leave_type_id =3 then 1 else 0 END) el_availed,
           SUM(case when leave_type_id =4 then 1 else 0 END) sl_availed,
           SUM(case when leave_type_id =5 then 1 else 0 END) cl_availed,
           SUM(case when leave_type_id =9 then 1 else 0 END) lop_availed
           from attendance_marking
           where month(leave_date) = ".date('m',strtotime($month))." and year(leave_date)=".$year."
           group by emp_bid) am on e.emp_bid = am.emp_bid          
LEFT JOIN (select emp_bid, 
           SUM(case when leave_type_id =3 then 1 else 0 END) prev_el_availed,
           SUM(case when leave_type_id =4 then 1 else 0 END) prev_sl_availed,
           SUM(case when leave_type_id =5 then 1 else 0 END) prev_cl_availed
           from attendance_marking
           where month(leave_date)< ".date('m',strtotime($month))." and year(leave_date)=".$year."
           group by emp_bid) prev_am on e.emp_bid = prev_am.emp_bid  
LEFT JOIN emp_lop_calculation lop on lop.emp_bid=e.emp_bid and lop.lop_month=".date('m',strtotime($month))." and lop.lop_year=".$year."
LEFT JOIN designation d on d.designation_id=e.designation
LEFT JOIN line_of_activity loa on loa.activity_id=e.activity_id
WHERE $where
ORDER BY  LENGTH(e.emp_bid),e.emp_bid asc";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);

        return $employees;
	}
	public function activities()
	{
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $employees=$CI->Myfunctions->getDataList("line_of_activity","");
        return $employees;
    }
    public function employeesBid()
    {
    	$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select emp_bid from employee order by LENGTH(emp_bid),emp_bid asc";
        $employees_bid=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees_bid;
	}
	public function employeesName()
	{
                $CI =& get_instance();
        $CI->load->model('Myfunctions');
                $sql_query="select emp_name from employee order by emp_name asc";
                $employees_name=$CI->Myfunctions->getQueryDataList($sql_query);
                return $employees_name;
	}
	public function employeeData($where)
	{
	$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function lineOfActivityEmployees($type)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select 
                            e.emp_bid,e.emp_name,e.temp_emp_id,e.date_of_join,d.designation_name,loa.dept_name, 
                            MAX(CASE WHEN l.leave_type_id=3 THEN l.no_leaves else 0 end) el_opb, 
                            MAX(CASE WHEN l.leave_type_id=3 THEN l.used else 0.0 end) el_used,
                            MAX(CASE WHEN l.leave_type_id=4 THEN l.no_leaves else 0 end) sl_opb, 
                            MAX(CASE WHEN l.leave_type_id=4 THEN l.used else 0.0 end) sl_used,
                            MAX(CASE WHEN l.leave_type_id=5 THEN l.no_leaves else 0 end) cl_opb, 
                            MAX(CASE WHEN l.leave_type_id=5 THEN used else 0.0 end) cl_used
                            FROM employee e LEFT JOIN leave_master l on e.emp_bid=l.emp_bid LEFT JOIN designation d on d.designation_id=e.designation LEFT JOIN line_of_activity loa on loa.activity_id=e.activity_id
                            WHERE loa.dept_name='".$type."'
                            GROUP by emp_bid ORDER by emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function activeEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function inActiveEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function newEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $d2 = date('Y-m-d', strtotime('-30 days'));
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and e.date>".$d2;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function getEmployeesData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e, line_of_activity loa where e.activity_id=loa.activity_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function leaveType()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select * from leave_types order by leave_type";
        $leave_types=$CI->Myfunctions->getQueryDataList($sql_query);
        return $leave_types;
	}
    // not using    
        public function onDutyEmployees()
        {
                $CI =& get_instance();
                $CI->load->model('Myfunctions');
                $sql_query="SELECT e.temp_emp_id,e.emp_bid,e.emp_name,d.designation_name,am.leave_date,loa.dept_name FROM line_of_activity loa,designation d, employee e,leave_types lt LEFT JOIN attendance_marking am on am.leave_type_id=lt.leave_type_id WHERE lt.leave_type='OD' and e.emp_bid= am.emp_bid and e.designation=d.designation_id and e.activity_id=loa.activity_id GROUP BY am.emp_bid";
                //echo $sql_query; exit;
                $onDutyEmployees=$CI->Myfunctions->getQueryDataList($sql_query);
                return $onDutyEmployees;
        }
        public function onDutyEmployeesWhere($where)
        {
                $CI =& get_instance();
                $CI->load->model('Myfunctions');
                $sql_query="SELECT e.temp_emp_id,e.emp_bid,e.emp_name,d.designation_name,am.leave_date,loa.dept_name FROM line_of_activity loa,designation d, employee e,leave_types lt LEFT JOIN attendance_marking am on am.leave_type_id=lt.leave_type_id WHERE lt.leave_type='OD' and $where and e.emp_bid= am.emp_bid and e.designation=d.designation_id and e.activity_id=loa.activity_id order by length(e.emp_bid),e.emp_bid";
        
                $onDutyEmployees=$CI->Myfunctions->getQueryDataList($sql_query);
                return $onDutyEmployees;
        }
        public function noOfLeavesByWhere($where)
        {
                $CI =& get_instance();
                $CI->load->model('Myfunctions');
                $sql_query="select 
                            e.emp_bid,e.emp_name,e.temp_emp_id,e.date_of_join,d.designation_name,loa.dept_name, 
                            MAX(CASE WHEN l.leave_type_id=3 THEN l.no_leaves else 0 end) el_opb, 
                            MAX(CASE WHEN l.leave_type_id=3 THEN l.used else 0.0 end) el_used,
                            MAX(CASE WHEN l.leave_type_id=4 THEN l.no_leaves else 0 end) sl_opb, 
                            MAX(CASE WHEN l.leave_type_id=4 THEN l.used else 0.0 end) sl_used,
                            MAX(CASE WHEN l.leave_type_id=5 THEN l.no_leaves else 0 end) cl_opb, 
                            MAX(CASE WHEN l.leave_type_id=5 THEN used else 0.0 end) cl_used
                            FROM employee e LEFT JOIN leave_master l on e.emp_bid=l.emp_bid LEFT JOIN designation d on d.designation_id=e.designation LEFT JOIN line_of_activity loa on loa.activity_id=e.activity_id where $where
                            GROUP by emp_bid ORDER by length(e.emp_bid),e.emp_bid";
                $noOfLeavesByWhere=$CI->Myfunctions->getQueryDataList($sql_query);
               // print_r($sql_query); exit();
                return $noOfLeavesByWhere;
                //echo $sql_query;
        }
}
?>