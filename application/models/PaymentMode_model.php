<?php
class PaymentMode_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}
	public function allSalariesDetails()
	{
	$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select epd.*,e.education,e.designation,e.emp_id,e.date_of_join,e.emp_name,pt.payment_type, la.dept_name from employee_pay_details epd,employee e,payment_types pt, line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=epd.emp_bid and epd.payment_type_id=pt.payment_type_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        /*$sql_query1="select epd.*,e.education,e.flag, e.designation,e.emp_id,e.date_of_join,e.emp_name, pt.payment_type, la.dept_name from employee_pay_details epd,employee_log e,payment_types pt, line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=epd.emp_bid and epd.payment_type_id=pt.payment_type_id";
        $employees_log=$CI->Myfunctions->getQueryDataList($sql_query1);
        foreach ($employees_log as $employee_log) {
        	array_push($employees, $employee_log);
        }*/
        return $employees;
	}
        public function employeeSalaryDataByWhere($where)
        {
                $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select epd.*,e.education,e.designation,e.emp_id,e.date_of_join,e.emp_name,pt.payment_type, la.dept_name from employee_pay_details epd,employee e,payment_types pt, line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=epd.emp_bid and epd.payment_type_id=pt.payment_type_id and $where";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
        }
	
	public function paymentModes()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select payment_type from payment_types order by payment_type";
        $data=$CI->Myfunctions->getQueryDataList($sql_query);
        return $data;
	}
}
?>