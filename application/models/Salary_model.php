<?php
class Salary_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}
    
	public function allSalariesDetails()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,

            pay.basic,

            pay.other_allowance,

            pay.incr,

            pay.co_allowance,

            pay.da,

            pay.special_allowance,

            pay.hra,

            pay.cca,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,

            case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end as empr_pf_contribution,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+

            (case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end) as CTC

from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid

where pt.payment_type_id=pay.payment_type_id

and loa.activity_id=e.activity_id

and e.designation=d.designation_id

order by e.emp_bid asc";
        /*$sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,
            pay.basic,
            pay.other_allowance,
            pay.incr,
            pay.co_allowance,
            pay.da,
            pay.special_allowance,
            pay.hra,
            pay.cca,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,
            ((pay.basic+pay.da)*12/100) as empr_pf_contribution,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+((pay.basic+pay.da)*12/100) as CTC
            from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid where pt.payment_type_id=pay.payment_type_id and loa.activity_id=e.activity_id and e.designation=d.designation_id order by e.emp_bid asc";*/
        
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        
        return $employees;
	}
	public function lineOfActivitySalaries($type)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,

            pay.basic,

            pay.other_allowance,

            pay.incr,

            pay.co_allowance,

            pay.da,

            pay.special_allowance,

            pay.hra,

            pay.cca,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,

            case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end as empr_pf_contribution,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+

            (case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end) as CTC

from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid

where pt.payment_type_id=pay.payment_type_id

and loa.dept_name='".$type."'

and e.designation=d.designation_id

order by e.emp_bid asc";
        /*$sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,
            pay.basic,
            pay.other_allowance,
            pay.incr,
            pay.co_allowance,
            pay.da,
            pay.special_allowance,
            pay.hra,
            pay.cca,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,
            ((pay.basic+pay.da)*12/100) as empr_pf_contribution,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+((pay.basic+pay.da)*12/100) as CTC
            from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid where pt.payment_type_id=pay.payment_type_id and loa.activity_id=e.activity_id and e.designation=d.designation_id and loa.dept_name='".$type."' order by e.emp_bid asc";*/
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function paymentModeSalaries($payment_type)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,

            pay.basic,

            pay.other_allowance,

            pay.incr,

            pay.co_allowance,

            pay.da,

            pay.special_allowance,

            pay.hra,

            pay.cca,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,

            case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end as empr_pf_contribution,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+

            (case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end) as CTC

from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid

where loa.activity_id=e.activity_id

and pt.payment_type='".$payment_type."'

and e.designation=d.designation_id

order by e.emp_bid asc";
        /*$sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,
            pay.basic,
            pay.other_allowance,
            pay.incr,
            pay.co_allowance,
            pay.da,
            pay.special_allowance,
            pay.hra,
            pay.cca,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,
            ((pay.basic+pay.da)*12/100) as empr_pf_contribution,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+((pay.basic+pay.da)*12/100) as CTC
            from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid where pt.payment_type_id=pay.payment_type_id and loa.activity_id=e.activity_id and e.designation=d.designation_id and pt.payment_type='".$payment_type."' order by e.emp_bid asc";*/
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function employeeSalaryDataByBid($bid)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,

            pay.basic,

            pay.other_allowance,

            pay.incr,

            pay.co_allowance,

            pay.da,

            pay.special_allowance,

            pay.hra,

            pay.cca,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,

            case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end as empr_pf_contribution,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+

            (case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end) as CTC

from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid

where loa.activity_id=e.activity_id

and pt.payment_type_id=pay.payment_type_id

and e.designation=d.designation_id

and e.emp_bid='".$bid."'

order by e.emp_bid asc";
        /*$sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,
            pay.basic,
            pay.other_allowance,
            pay.incr,
            pay.co_allowance,
            pay.da,
            pay.special_allowance,
            pay.hra,
            pay.cca,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,
            ((pay.basic+pay.da)*12/100) as empr_pf_contribution,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+((pay.basic+pay.da)*12/100) as CTC
            from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid where pt.payment_type_id=pay.payment_type_id and loa.activity_id=e.activity_id and e.designation=d.designation_id and e.emp_bid='".$bid."' order by e.emp_bid asc";*/
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function employeeSalaryDataByName($ename)
	{
	$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,

            pay.basic,

            pay.other_allowance,

            pay.incr,

            pay.co_allowance,

            pay.da,

            pay.special_allowance,

            pay.hra,

            pay.cca,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,

            case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end as empr_pf_contribution,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+

            (case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end) as CTC

from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid

where loa.activity_id=e.activity_id

and pt.payment_type_id=pay.payment_type_id

and e.designation=d.designation_id

and e.emp_name='".$ename."'

order by e.emp_bid asc";
        /*$sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,
            pay.basic,
            pay.other_allowance,
            pay.incr,
            pay.co_allowance,
            pay.da,
            pay.special_allowance,
            pay.hra,
            pay.cca,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,
            ((pay.basic+pay.da)*12/100) as empr_pf_contribution,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+((pay.basic+pay.da)*12/100) as CTC
            from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid where pt.payment_type_id=pay.payment_type_id and loa.activity_id=e.activity_id and e.designation=d.designation_id and e.emp_name='".$ename."' order by e.emp_bid asc";*/
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
        public function paymentModeAndDeptSalaries($dept_name,$payment_type)
        {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,

            pay.basic,

            pay.other_allowance,

            pay.incr,

            pay.co_allowance,

            pay.da,

            pay.special_allowance,

            pay.hra,

            pay.cca,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,

            case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end as empr_pf_contribution,

            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+

            (case when (pay.basic+pay.da) >= 180000 then 180000*12/100 else ((pay.basic+pay.da)*12/100) end) as CTC

from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid

where loa.dept_name='".$dept_name."' and pt.payment_type='".$payment_type."'

and e.designation=d.designation_id

order by e.emp_bid asc";
        /*$sql_query="select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join, d.designation_name, loa.dept_name, pt.payment_type, pay.acc_no, pay.uan,pay.pf_no, pay.esi_no,
            pay.basic,
            pay.other_allowance,
            pay.incr,
            pay.co_allowance,
            pay.da,
            pay.special_allowance,
            pay.hra,
            pay.cca,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca) gross,
            ((pay.basic+pay.da)*12/100) as empr_pf_contribution,
            (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+((pay.basic+pay.da)*12/100) as CTC
            from payment_types pt, line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid where pt.payment_type_id=pay.payment_type_id and loa.activity_id=e.activity_id and e.designation=d.designation_id and loa.dept_name='".$dept_name."' and pt.payment_type='".$payment_type."' order by e.emp_bid asc";*/
        
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;   
        }
        // all employees CTC per month with default month and present year
        public function allCTCPerMonth($month,$year)
        {
                $CI=& get_instance();
                $CI->load->model('Myfunctions');

                $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);
                if ($prev_month_days>=30) {
                	$prev_month_days=30;
                }
                $sql_query="select e.temp_emp_id emp_id, e.emp_bid, e.emp_name, e.date_of_join, d.designation_name, l.dept_name,pay.acc_no, pmttyp.payment_type payment_mode,pay.uan, pay.pf_no, pay.esi_no,
       slry.basic_earnings, slry.incr_earnings, slry.da_earnings, slry.hra_earnings,
       slry.cca_earnings, slry.other_allowance_earnings, slry.co_allowance_earnings,
       slry.special_allowance_earnings, slry.pf as pf_deducted,slry.pt professional_tax,
       slry.esi, slry.tds,slry.gross Gross_pay, slry.net_income as Net_pay, slry.fixed_monthly_ctc,
       slry.lop_count Lop_days, slry.pay_days as no_of_pay_days,slry.management_deductions,slry.remarks
from employee e LEFT JOIN designation d on (e.designation = d.designation_id)
     LEFT JOIN line_of_activity l on (e.activity_id = l.activity_id)
     LEFT JOIN employee_pay_details pay on (e.emp_bid = pay.emp_bid)
     LEFT JOIN payment_types pmttyp on pay.payment_type_id = pmttyp.payment_type_id
     LEFT JOIN (select * 
                from emp_monthly_salaries
                where salary_month = ".date('m',strtotime($month))."
                and salary_year = ".$year.") slry  on (e.emp_bid = slry.emp_bid) order by length(e.emp_bid),e.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
        }
        public function CTCPerMonthByWhereYearMonth($where,$year,$month)
        {

                $CI=& get_instance();
                $CI->load->model('Myfunctions');
                $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);  
                $sql_query="select e.temp_emp_id emp_id, e.emp_bid, e.emp_name, e.date_of_join, d.designation_name, l.dept_name,
       pay.acc_no, pmttyp.payment_type payment_mode,pay.uan, pay.pf_no, pay.esi_no,
       slry.basic_earnings, slry.incr_earnings, slry.da_earnings, slry.hra_earnings,
       slry.cca_earnings, slry.other_allowance_earnings, slry.co_allowance_earnings,
       slry.special_allowance_earnings, slry.pf as pf_deducted,slry.pt professional_tax,
       slry.esi, slry.tds,slry.gross Gross_pay, slry.net_income as Net_pay, slry.fixed_monthly_ctc,
       slry.lop_count Lop_days, slry.pay_days as no_of_pay_days,slry.management_deductions,slry.remarks
from employee e LEFT JOIN designation d on (e.designation = d.designation_id)
     LEFT JOIN line_of_activity l on (e.activity_id = l.activity_id)
     LEFT JOIN employee_pay_details pay on (e.emp_bid = pay.emp_bid)
     LEFT JOIN payment_types pmttyp on pay.payment_type_id = pmttyp.payment_type_id
     LEFT JOIN (select * 
                from emp_monthly_salaries
                where salary_month = ".date('m',strtotime($month))."
                and salary_year = ".$year.") slry  on (e.emp_bid = slry.emp_bid) where $where order by length(e.emp_bid),e.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
        }
	public function getSalariesData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select epd.*,e.education,e.designation,e.emp_id,e.date_of_join,e.emp_name,pt.payment_type, la.dept_name from employee_pay_details epd,employee e,payment_types pt, line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=epd.emp_bid and epd.payment_type_id=pt.payment_type_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function activeEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function inActiveEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function newEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $d2 = date('Y-m-d', strtotime('-30 days'));
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and e.date>".$d2;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function paymentModes()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select payment_type from payment_types order by payment_type";
        $data=$CI->Myfunctions->getQueryDataList($sql_query);
        return $data;
	}
    public function bankstatement($where)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.emp_bid,e.temp_emp_id,e.emp_name,epd.acc_no,ems.net_income,ems.management_deductions,slry.remarks from emp_monthly_salaries ems LEFT JOIN employee e on ems.emp_bid=e.emp_bid LEFT JOIN employee_pay_details epd on epd.emp_bid=ems.emp_bid where epd.payment_type_id=2 and $where order by length(e.emp_bid),e.emp_bid";
        $data=$CI->Myfunctions->getQueryDataList($sql_query);
        return $data;
    }
}
?>