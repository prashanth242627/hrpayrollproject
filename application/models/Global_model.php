<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Global_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
       
    }            
   
public function insert_data($param)
{
return $this->db->insert($param['table'],$param['insertdata']);

}
public function get_values($table) 
{
	$res=$this->db->get($table);
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
	// Produces: SELECT * FROM mytable
}
public function employeesBids()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select emp_bid from employee order by LENGTH(emp_bid),emp_bid asc";
        $employees_bid=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees_bid;
	}
public function get_values_limit($table,$limit) 
{
	$this->db->limit($limit);
	$res=$this->db->get($table);
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
	// Produces: SELECT * FROM mytable LIMIT 20, 10 
}

public function get_values_where($table, $where) 
{
	$res=$this->db->get_where($table, $where);
	//print_r($this->db->error());
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function get_values_where_single($table, $where) 
{
	$res=$this->db->get_where($table, $where);
	if($res->num_rows()> 0)
	{
		return $res->row();
	}
	else
	{
		return array();
	}
}

public function get_values_select($colmns,$table,$where) 
{
	
	$this->db->distinct();			
	$this->db->select($colmns);			
	$res=$this->db->get_where($table, $where);
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}

public function get_values_sum($column,$table,$where) 
{
	$this->db->select_sum($column);
	$this->db->select($colmns);			
	$res=$this->db->get_where($table, $where);
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function rows_count($table)
{
	$res=$this->db->query("SELECT (COUNT(*)+1) as rcount FROM ".$table." WHERE 1")->row();
	return $res->rcount;
}
public function Max_count($table,$max_id)
{
	$res=$this->db->query("SELECT (IFNULL(MAX($max_id),1)+1) as max_value FROM ".$table)->row();
	return $res->max_value;
}

public function update($table,$data,$where) 
{
	return $this->db->update($table, $data, $where);
	
}     
public function updateRecord($table,$data,$where)
	{
	$set='';

	foreach($data as $k=>$v){
					
		$set.=$k."='".$v."',";		
	}
		
	$set=rtrim($set,',');
	
	$sql="update ".$table." set ".$set." where ".$where;	
	$query_result=$this->db->query($sql);
	if ($query_result) {
		return 1;
	}
	else{
		$msg = $this->db->error();
		return $msg;
	}

	}   
public function get_leave_master() 
{
	
	$res=$this->db->query("SELECT e.emp_bid, e.emp_name, m.`no_leaves`, l.leave_type, a.dept_name, m.leave_master_id FROM `leave_master` m LEFT JOIN leave_types l ON m.`leave_type_id` = l.leave_type_id AND l.status = 1 LEFT JOIN ( employee e LEFT JOIN line_of_activity a ON e.activity_id = a.activity_id AND a.status = 1 ) ON m.emp_bid = e.emp_bid WHERE 1");

	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function get_employees_data() 
{
	$res=$this->db->query("SELECT e.emp_bid, e.emp_name,e.temp_emp_id,e.holiday_group_id from employee e  WHERE 1 order by emp_bid,LENGTH(e.emp_bid) asc");
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function get_leave_master_leave_types(){
	$res=$this->db->query("SELECT * FROM `leave_types` where leave_type in ('CL','EL','SL') order by leave_type");
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function get_leave_types() 
{
	
	$res=$this->db->query("SELECT * FROM `leave_types` order by leave_type");
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function get_current_date_leaves_count(){
	$res=$this->db->query("SELECT lt.leave_type,COUNT(am.leave_type_id) as leaves_count FROM attendance_marking am   LEFT JOIN leave_types lt on am.leave_type_id=lt.leave_type_id   WHERE  am.leave_date=CURRENT_DATE GROUP BY  lt.leave_type ORDER by lt.leave_type");
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function get_current_date_leave_emps(){
	$res=$this->db->query("SELECT e.emp_name,e.emp_id,e.emp_bid,loa.dept_name,sd.sub_name,lt.leave_type,am.leave_date FROM attendance_marking am LEFT JOIN  leave_types lt on am.leave_type_id=lt.leave_type_id left join employee e on e.emp_bid=am.emp_bid left join subject_details sd on e.subject_details_id=sd.subject_details_id left join line_of_activity loa on loa.activity_id=e.activity_id  WHERE  am.leave_date=CURRENT_DATE ORDER by e.emp_name");
	//SELECT e.emp_name,e.emp_id,e.emp_bid,loa.dept_name,sd.sub_name,lt.leave_type FROM employee e JOIN attendance_marking am on e.emp_id=am.emp_id JOIN leave_master lm ON lm.leave_master_id=am.leave_master_id JOIN leave_types lt on lt.leave_type_id=lm.leave_type_id JOIN subject_details sd on sd.subject_details_id=e.subject_details_id WHERE am.leave_date=CURRENT_DATE
	if ($res->num_rows()>0) {
		return $res->result();
	}
	else{
		return array();
	}
}
public function get_employees_leaves_count_type($emp_id,$year){
	//$res=$this->db->query("SELECT lm.no_leaves, lt.leave_type FROM `leave_master` lm JOIN leave_types lt on lm.leave_type_id =lt.leave_type_id WHERE lm.emp_id=".$emp_id." and lm.calendar_year='".$year."'");
	$res=$this->db->query("SELECT lm.no_leaves,lm.leave_master_id, lt.leave_type, COUNT(am.attendance_id) as used_count FROM `leave_master` lm JOIN leave_types lt on lm.leave_type_id =lt.leave_type_id LEFT JOIN attendance_marking am on am.leave_master_id=lm.leave_master_id WHERE lm.emp_bid='".$emp_id."' and lm.calendar_year=".$year."  GROUP BY am.emp_id");

	if ($res->num_rows()>0) {
		return $res->result();
	}
	else{
		return array();
	}
	
}
//with individual data with emp_bids and dates
public function get_leave_emps_by_date_old($fromdate,$todate){
	$res=$this->db->query("SELECT e.emp_name,e.emp_id,e.emp_bid,loa.dept_name,sd.sub_name,lt.leave_type,am.leave_date FROM attendance_marking am LEFT JOIN leave_types lt on am.leave_type_id=lt.leave_type_id left join employee e on e.emp_bid=am.emp_bid left join subject_details sd on e.subject_details_id=sd.subject_details_id left join line_of_activity loa on loa.activity_id=e.activity_id  WHERE  am.leave_date>='".$fromdate."' and am.leave_date<='".$todate."' ORDER by am.leave_date, LENGTH(e.emp_bid),e.emp_bid");
	if ($res->num_rows()>0) {
		return $res->result();
	}
	else{
		return array();
	}
}
//with group_concact data with emp_bids and dates

public function get_leave_emps_by_date($fromdate,$todate){
	$res=$this->db->query("SELECT e.emp_name,e.emp_id,e.emp_bid,loa.dept_name,sd.sub_name,group_concat(concat(lt.leave_type,': ',am.leave_date) separator ', ') as leave_dates FROM attendance_marking am LEFT JOIN leave_types lt on am.leave_type_id=lt.leave_type_id left join employee e on e.emp_bid=am.emp_bid left join subject_details sd on e.subject_details_id=sd.subject_details_id left join line_of_activity loa on loa.activity_id=e.activity_id WHERE am.leave_date>='".$fromdate."' and am.leave_date<='".$todate."' GROUP by e.emp_bid ORDER by LENGTH(e.emp_bid),e.emp_bid,am.leave_date");
	if ($res->num_rows()>0) {
		return $res->result();
	}
	else{
		return array();
	}
}
public function get_leaves_count_by_date($leave_type_id,$fromdate,$todate){
	$res=$this->db->query("SELECT lt.leave_type,COUNT(am.leave_type_id) as leaves_count FROM attendance_marking am  LEFT JOIN leave_types lt on am.leave_type_id=lt.leave_type_id   WHERE  am.leave_date>='".$fromdate."' and am.leave_date<='".$todate."' and am.leave_type_id=".$leave_type_id." GROUP BY  lt.leave_type ORDER by lt.leave_type,LENGTH(am.emp_bid),am.emp_bid");
	if($res->num_rows()> 0)
	{
		return $res->row();
	}
	else
	{
		return array();
	}
}
public function get_leave_master_single($id) 
{
	
	$res=$this->db->query("SELECT e.emp_bid, e.emp_name,e.emp_bid, m.`no_leaves`, l.leave_type,l.leave_type_id, a.dept_name, m.leave_master_id FROM `leave_master` m LEFT JOIN leave_types l ON m.`leave_type_id` = l.leave_type_id AND l.status = 1 LEFT JOIN ( employee e LEFT JOIN line_of_activity a ON e.activity_id = a.activity_id AND a.status = 1 ) ON m.emp_bid = e.emp_bid WHERE m.leave_master_id =".$id);
	if($res->num_rows()> 0)
	{
		return $res->row();
	}
	else
	{
		return array();
	}
}	
public function get_leave_master_log() 
{
	
	$res=$this->db->query("SELECT e.emp_bid, e.emp_name, m.`no_leaves`, l.leave_type, a.dept_name, m.leave_master_id FROM `leave_master_log` m LEFT JOIN leave_types l ON m.`leave_type_id` = l.leave_type_id AND l.status = 1 LEFT JOIN ( employee e LEFT JOIN line_of_activity a ON e.activity_id = a.activity_id AND a.status = 1 ) ON m.emp_bid = e.emp_bid WHERE m.flag=2");
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function getEmployeewithActivity() {
	$res = $this->db->query("SELECT e.*, l.dept_name FROM `employee` e LEFT JOIN line_of_activity l ON e.activity_id = l.activity_id WHERE 1");
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function get_attendance_marking() 
{
	
	$res=$this->db->query("SELECT e.emp_bid, e.emp_name, m.`no_leaves`, l.leave_type, a.dept_name, m.leave_master_id FROM `leave_master` m LEFT JOIN leave_types l ON m.`leave_type_id` = l.leave_type_id AND l.status = 1 LEFT JOIN ( employee e LEFT JOIN line_of_activity a ON e.activity_id = a.activity_id AND a.status = 1 ) ON m.emp_bid = e.emp_bid WHERE 1");
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
public function get_attendance_marking_log() 
{
	
	$res=$this->db->query("SELECT e.emp_bid, e.emp_name, m.`no_leaves`, l.leave_type, a.dept_name, m.leave_master_id FROM `leave_master_log` m LEFT JOIN leave_types l ON m.`leave_type_id` = l.leave_type_id AND l.status = 1 LEFT JOIN ( employee e LEFT JOIN line_of_activity a ON e.activity_id = a.activity_id AND a.status = 1 ) ON m.emp_bid = e.emp_bid WHERE m.flag=2");
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}	
public function get_emp_leavemaster($id)
{
	$res=$this->db->query("SELECT * FROM `leave_master` l LEFT JOIN leave_types t ON l.leave_type_id = t.leave_type_id WHERE `emp_id` = ".$id."");
	if($res->num_rows()> 0)
	{
		return $res->result();
	}
	else
	{
		return array();
	}
}
/*public function empleaveupdate($emp_id,$leave_master_id,$leaves)
{
	$this->db->query("UPDATE leave_master SET no_leaves = (no_leaves-".$leaves.") WHERE leave_master_id = ".$leave_master_id." and `emp_id` = ".$emp_id."");
	
}*/

}
