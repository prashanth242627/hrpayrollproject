<?php
class Dashboard_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}
	public function allEmployeesCount()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select count(distinct emp_bid) as emp_count from employee";
        $employees=$CI->Myfunctions->getQueryData($sql_query);
        return $employees;
	}
	public function deptCount()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select la.dept_name, count(0) as dept_count
                    from employee e, line_of_activity la
                    where e.activity_id = la.activity_id
                    group by dept_name";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function designationCount()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select la.dept_name, d.designation_name, count(0) as designation_count
                    from employee e, line_of_activity la, designation d
                    where e.activity_id = la.activity_id
                    and e.designation = d.designation_id
                    group by la.dept_name, d.designation_name
                    ";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function esiEmpCount()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select dept_name, count(*) esi_count
                    from employee e, employee_pay_details pay, line_of_activity la
                    where e.emp_bid = pay.emp_bid
                    and e.activity_id = la.activity_id
                    AND (basic+da+hra+cca+special_allowance+other_allowance+co_allowance) < 21000 
                    and esi_consider = 1
                    group by dept_name";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function pfEmpCount()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select dept_name, count(*) pf_count
            from employee e, employee_pay_details pay, line_of_activity la
            where e.emp_bid = pay.emp_bid
            and e.activity_id = la.activity_id
            and pf_consider = 0
            group by dept_name";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function tdsEmpCount()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select dept_name, count(*) TDS_count
                from employee e, employee_pay_details pay, line_of_activity la
                where e.emp_bid = pay.emp_bid
                and e.activity_id = la.activity_id
                AND case when e.sex = 'M' then (basic+da+hra+cca+special_allowance+other_allowance+co_allowance)+50000
                         else (basic+da+hra+cca+special_allowance+other_allowance+co_allowance) end > 250000 
                group by dept_name";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function bioCurrentDayCount()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select sum(case when ba.emp_bid is not NULL then 1 else 0 end) p_cnt, 
                   sum(case when ba.emp_bid is NULL and leave_type_id is NULL then 1 else 0 end) A_cnt,
                   sum(case when ba.emp_bid is NULL and leave_type_id is not NULL then 1 else 0 end) l_cnt
                    from  employee e  LEFT JOIN (select distinct emp_bid 
                                                 from biometirc_attendance 
                                                 where log_date = '".date("Y-m-d")."'
                                                ) ba on e.emp_bid = ba.emp_bid
                           LEFT JOIN (select emp_bid, leave_type_id from attendance_marking where leave_date = '".date("Y-m-d")."') am
                                 on e.emp_bid = am.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function DeptCurrentDayCount()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select dept_name,
       sum(case when ba.emp_bid is not NULL then 1 else 0 end) p_cnt, 
       sum(case when ba.emp_bid is NULL and leave_type_id is NULL then 1 else 0 end) A_cnt,
       sum(case when ba.emp_bid is NULL and leave_type_id is not NULL then 1 else 0 end) l_cnt
from  employee e  LEFT JOIN (select distinct emp_bid 
                             from biometirc_attendance 
                             where log_date = '".date("Y-m-d")."'
                            ) ba on e.emp_bid = ba.emp_bid
       LEFT JOIN (select emp_bid, leave_type_id from attendance_marking where leave_date = '".date("Y-m-d")."') am   on e.emp_bid = am.emp_bid
       LEFT JOIN line_of_activity la on e.activity_id = la.activity_id
group by dept_name";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function MonthWiseSalaries()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="SELECT salary_month, salary_year, 
               count(emp_bid) emp_count,
               sum(fixed_monthly_ctc) fixed_monthly_ctc, 
               sum(gross) Gross_pay, 
               sum(net_income) NET_PAY
                FROM emp_monthly_salaries where salary_month=".date('m', strtotime('first day of previous month'))." and salary_year=".date('Y', strtotime('first day of previous month'))." group by salary_month, salary_year";
                
       $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function PaymentModeSalaries()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="SELECT salary_month, salary_year, 
               count(emp_bid) emp_count,
               sum(fixed_monthly_ctc) fixed_monthly_ctc, 
               sum(gross) Gross_pay, 
               sum(net_income) NET_PAY
                FROM emp_monthly_salaries where salary_month=".date('m', strtotime('first day of previous month'))." and salary_year=".date('Y', strtotime('first day of previous month'))." group by salary_month, salary_year";
       $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function SalariesDeductions()
    {
        //date('Y', strtotime('first day of previous month'));
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $lastcalmonthandyear=$CI->Myfunctions->getQueryData("SELECT salary_month,salary_year from emp_monthly_salaries WHERE salary_year=(SELECT MAX(salary_year) from emp_monthly_salaries) GROUP by salary_month ORDER by salary_month desc LIMIT 1");
        $month=$lastcalmonthandyear[0]['salary_month'];
        $year=$lastcalmonthandyear[0]['salary_year'];
        if ($month==0) {
            $month=date('m');
            $year=date('y');
        }
        $sql_query="SELECT salary_month, salary_year, 
               count(emp_bid) emp_count,
               sum(pf) total_pf_amt, 
               sum(case when pf>0 then 1 else 0 end) pf_count,
               sum(pt) total_pt_amt, 
               sum(case when pt>0 then 1 else 0 end) pt_count,
               sum(esi) total_esi_amt,
               sum(case when esi>0 then 1 else 0 end) esi_count,
               sum(tds) total_tds_amt,
               sum(case when tds>0 then 1 else 0 end) tds_count
        FROM emp_monthly_salaries where salary_month=".$month." and salary_year=".$year."
        group by salary_month, salary_year";
       $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
}
?>