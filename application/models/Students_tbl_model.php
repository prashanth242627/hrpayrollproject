<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Students_tbl_model extends CI_Model {

    public $variable;

    public function __construct()
    {
        parent::__construct();
    }
    public function add($row, $dateCreated = FALSE) {
        $this->db->insert('biometirc_attendance', $row);
        return $this->db->insert_id();
    }
    public function getAll() {
        if (isset($this->orderby) && $this->orderby != '') {
            $this->db->order_by($this->orderby);
        }
        $query = $this->db->get('biometirc_attendance');
        return $query->result_array();
    }
}

?>