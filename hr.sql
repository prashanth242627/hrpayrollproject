-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 17, 2018 at 05:18 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr`
--

-- --------------------------------------------------------

--
-- Table structure for table `empextrahoursmaster`
--

CREATE TABLE `empextrahoursmaster` (
  `empextrahours_id` int(8) NOT NULL,
  `activity_id` int(8) NOT NULL,
  `per_hour_payment` varchar(128) NOT NULL,
  `status` int(2) NOT NULL,
  `date` date NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empextrahoursmaster`
--

INSERT INTO `empextrahoursmaster` (`empextrahours_id`, `activity_id`, `per_hour_payment`, `status`, `date`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, 1, '25', 1, '2018-11-17', 1, '2018-11-17 09:21:06', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `empextrahoursmaster_log`
--

CREATE TABLE `empextrahoursmaster_log` (
  `empextrahours_id` int(8) NOT NULL,
  `activity_id` int(8) NOT NULL,
  `per_hour_payment` varchar(128) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `status` int(2) NOT NULL,
  `date` date NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empextrahoursmaster_log`
--

INSERT INTO `empextrahoursmaster_log` (`empextrahours_id`, `activity_id`, `per_hour_payment`, `flag`, `description`, `status`, `date`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(3, 2, 'per_hour_payment', 1, 'EDIT', 1, '2018-11-17', 1, '2018-11-17 09:28:51', 1, '2018-11-17 09:28:51'),
(3, 2, '13', 2, 'DELETE', 1, '2018-11-17', 1, '2018-11-17 09:28:56', 1, '2018-11-17 09:28:56');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(4) NOT NULL,
  `temp_emp_id` varchar(16) NOT NULL,
  `emp_name` varchar(128) NOT NULL,
  `education` varchar(128) NOT NULL,
  `date_of_birth` date NOT NULL,
  `date_of_join` date NOT NULL,
  `emp_bid` varchar(128) NOT NULL,
  `experience` int(4) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `activity_id` int(4) NOT NULL,
  `subject_details_id` int(4) NOT NULL,
  `residence_details_id` int(4) NOT NULL,
  `transport_details_id` int(4) NOT NULL,
  `status` int(2) NOT NULL,
  `Created_By` int(4) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(4) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `temp_emp_id`, `emp_name`, `education`, `date_of_birth`, `date_of_join`, `emp_bid`, `experience`, `mobile`, `activity_id`, `subject_details_id`, `residence_details_id`, `transport_details_id`, `status`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, 'RAJ1', 'Sreevidhya', 'MCA', '2018-11-17', '2018-11-17', 'AAA', 3, '9177161316', 1, 2, 1, 3, 1, 1, '2018-11-17 06:57:14', 0, '0000-00-00 00:00:00'),
(4, 'RAJ1', 'Rajender', 'MCA', '2018-11-13', '2018-11-13', 'BBB', 3, '9177161316', 2, 3, 1, 3, 1, 1, '2018-11-13 22:29:54', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `employee_log`
--

CREATE TABLE `employee_log` (
  `emp_id` int(4) NOT NULL,
  `temp_emp_id` varchar(16) NOT NULL,
  `emp_name` varchar(128) NOT NULL,
  `education` varchar(128) NOT NULL,
  `date_of_birth` date NOT NULL,
  `date_of_join` date NOT NULL,
  `emp_bid` varchar(128) NOT NULL,
  `experience` int(4) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `activity_id` int(4) NOT NULL,
  `subject_details_id` int(4) NOT NULL,
  `residence_details_id` int(4) NOT NULL,
  `transport_details_id` int(4) NOT NULL,
  `status` int(2) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `Created_By` int(4) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(4) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_log`
--

INSERT INTO `employee_log` (`emp_id`, `temp_emp_id`, `emp_name`, `education`, `date_of_birth`, `date_of_join`, `emp_bid`, `experience`, `mobile`, `activity_id`, `subject_details_id`, `residence_details_id`, `transport_details_id`, `status`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(1, 'RAJ1', 'Sreevidhya1', 'MCA', '0000-00-00', '0000-00-00', '0', 3, '9177161316', 2, 3, 1, 3, 0, 1, 'EDIT', 1, '2018-11-13 22:30:06', 0, '0000-00-00 00:00:00'),
(1, 'RAJ1', 'Sreevidhya1', 'MCA', '0000-00-00', '0000-00-00', '0', 3, '9177161316', 2, 3, 1, 3, 0, 1, 'EDIT', 1, '2018-11-13 22:32:41', 0, '0000-00-00 00:00:00'),
(1, 'RAJ1', 'Sreevidhya', 'MCA', '0000-00-00', '0000-00-00', 'AAA01', 3, '9177161316', 2, 3, 1, 3, 0, 1, 'EDIT', 1, '2018-11-17 07:00:23', 0, '0000-00-00 00:00:00'),
(1, 'RAJ1', 'Rajender', 'MCA', '0000-00-00', '0000-00-00', '1', 3, '9177161316', 2, 3, 1, 3, 0, 1, 'EDIT', 1, '2018-11-17 07:01:36', 0, '0000-00-00 00:00:00'),
(1, 'RAJ1', 'Sreevidhya', 'MCA', '0000-00-00', '0000-00-00', 'AAA01', 3, '9177161316', 1, 2, 1, 3, 0, 1, 'EDIT', 1, '2018-11-17 07:01:45', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `empsavingsmaster`
--

CREATE TABLE `empsavingsmaster` (
  `empsavings_id` int(8) NOT NULL,
  `emp_bid` varchar(128) NOT NULL,
  `sec_80c` int(8) NOT NULL,
  `sec_80d` int(8) NOT NULL,
  `sec_80cc` int(8) NOT NULL,
  `status` int(2) NOT NULL,
  `date` date NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empsavingsmaster`
--

INSERT INTO `empsavingsmaster` (`empsavings_id`, `emp_bid`, `sec_80c`, `sec_80d`, `sec_80cc`, `status`, `date`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, 'AAA', 8021, 8121, 8221, 1, '2018-11-17', 1, '2018-11-17 08:17:55', 1, '2018-11-17 08:18:01'),
(3, 'BBB', 801, 811, 821, 1, '2018-11-17', 1, '2018-11-17 08:03:30', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `empsavingsmaster_log`
--

CREATE TABLE `empsavingsmaster_log` (
  `empsavings_id` int(8) NOT NULL,
  `emp_bid` varchar(128) NOT NULL,
  `sec_80c` int(8) NOT NULL,
  `sec_80d` int(8) NOT NULL,
  `sec_80cc` int(8) NOT NULL,
  `status` int(2) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `date` date NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empsavingsmaster_log`
--

INSERT INTO `empsavingsmaster_log` (`empsavings_id`, `emp_bid`, `sec_80c`, `sec_80d`, `sec_80cc`, `status`, `flag`, `description`, `date`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, 'AAA', 0, 81, 82, 1, 1, 'EDIT', '2018-11-17', 1, '2018-11-17 08:03:38', 1, '2018-11-17 08:03:38'),
(2, 'AAA', 0, 812, 822, 1, 1, 'EDIT', '2018-11-17', 1, '2018-11-17 08:08:48', 1, '2018-11-17 08:08:48'),
(2, 'AAA', 0, 8121, 8221, 1, 1, 'EDIT', '2018-11-17', 1, '2018-11-17 08:13:23', 1, '2018-11-17 08:13:23'),
(2, 'AAA', 0, 8121, 8221, 1, 1, 'EDIT', '2018-11-17', 1, '2018-11-17 08:17:51', 1, '2018-11-17 08:17:51'),
(2, 'AAA', 0, 8121, 8221, 1, 1, 'EDIT', '2018-11-17', 1, '2018-11-17 08:18:01', 1, '2018-11-17 08:18:01');

-- --------------------------------------------------------

--
-- Table structure for table `leave_types`
--

CREATE TABLE `leave_types` (
  `leave_type_id` int(8) NOT NULL,
  `leave_type` varchar(128) NOT NULL,
  `status` int(2) NOT NULL,
  `date` date NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_types`
--

INSERT INTO `leave_types` (`leave_type_id`, `leave_type`, `status`, `date`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, 'CL', 1, '2018-11-17', 1, '2018-11-17 09:45:23', 1, '2018-11-17 09:45:23');

-- --------------------------------------------------------

--
-- Table structure for table `leave_types_log`
--

CREATE TABLE `leave_types_log` (
  `leave_type_id` int(8) NOT NULL,
  `leave_type` varchar(128) NOT NULL,
  `status` int(2) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `date` date NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_types_log`
--

INSERT INTO `leave_types_log` (`leave_type_id`, `leave_type`, `status`, `flag`, `description`, `date`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, 'CL', 1, 1, 'EDIT', '2018-11-17', 1, '2018-11-17 09:45:20', 1, '2018-11-17 09:45:20');

-- --------------------------------------------------------

--
-- Table structure for table `line_of_activity`
--

CREATE TABLE `line_of_activity` (
  `activity_id` int(8) NOT NULL,
  `dept_name` varchar(128) NOT NULL,
  `dept_id` varchar(16) NOT NULL,
  `status` int(2) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_of_activity`
--

INSERT INTO `line_of_activity` (`activity_id`, `dept_name`, `dept_id`, `status`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(1, 'ACCADEMIC', 'ACD', 1, 1, '2018-11-16 05:11:19', 1, '2018-11-16 05:11:19'),
(2, 'SUB STAFF', 'SUB STAFF', 1, 1, '2018-11-12 15:18:24', 1, '2018-11-12 15:18:24'),
(3, 'DRIVERS', 'drv', 1, 1, '2018-11-12 14:30:15', 0, '0000-00-00 00:00:00'),
(4, 'SECURITY', 'SECURITY', 1, 1, '2018-11-12 15:18:27', 1, '2018-11-12 15:18:27');

-- --------------------------------------------------------

--
-- Table structure for table `line_of_activity_log`
--

CREATE TABLE `line_of_activity_log` (
  `activity_id` int(8) NOT NULL,
  `dept_name` varchar(128) NOT NULL,
  `dept_id` varchar(16) NOT NULL,
  `status` int(2) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_of_activity_log`
--

INSERT INTO `line_of_activity_log` (`activity_id`, `dept_name`, `dept_id`, `status`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(4, 'SECURITY', 'SECURITY', 1, 2, 'DELETE', 1, '2018-11-12 14:30:30', 1, '2018-11-12 14:30:30'),
(2, 'SUB STAFF', 'SUB STAFF', 1, 1, 'EDIT', 1, '2018-11-12 14:30:47', 1, '2018-11-12 14:30:47'),
(4, 'SECURITY', 'SECURITY', 1, 1, 'EDIT', 1, '2018-11-12 15:03:01', 1, '2018-11-12 15:03:01'),
(1, 'ACCADEMIC', 'ACD', 1, 1, 'EDIT', 1, '2018-11-12 15:09:54', 1, '2018-11-12 15:09:54'),
(1, 'ACCADEMIC', 'ACD', 1, 1, 'EDIT', 1, '2018-11-12 15:15:37', 1, '2018-11-12 15:15:37');

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `payment_type_id` int(8) NOT NULL,
  `payment_type` varchar(128) NOT NULL,
  `status` int(2) NOT NULL,
  `date` date NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`payment_type_id`, `payment_type`, `status`, `date`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(3, 'AS', 0, '2018-11-16', 1, '2018-11-16 22:55:19', 0, '0000-00-00 00:00:00'),
(2, 'CASH2', 1, '2018-11-16', 1, '2018-11-16 22:39:56', 1, '2018-11-16 22:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `payment_types_log`
--

CREATE TABLE `payment_types_log` (
  `payment_type_id` int(8) NOT NULL,
  `payment_type` varchar(128) NOT NULL,
  `status` int(2) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `date` date NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_types_log`
--

INSERT INTO `payment_types_log` (`payment_type_id`, `payment_type`, `status`, `flag`, `description`, `date`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(3, 'Oucher', 1, 2, 'DELETE', '2018-11-16', 1, '2018-11-16 22:54:49', 1, '2018-11-16 22:54:49'),
(2, 'CASH', 1, 1, 'EDIT', '2018-11-16', 1, '2018-11-16 22:57:56', 1, '2018-11-16 22:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `reportingslots`
--

CREATE TABLE `reportingslots` (
  `reportingslots_id` int(8) NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `status` int(2) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reportingslots`
--

INSERT INTO `reportingslots` (`reportingslots_id`, `from_time`, `to_time`, `status`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, '01:00:00', '22:59:00', 1, 1, '2018-11-12 21:16:20', 1, '2018-11-12 21:16:35'),
(1, '01:59:00', '20:59:00', 1, 1, '2018-11-12 21:09:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `reportingslots_log`
--

CREATE TABLE `reportingslots_log` (
  `reportingslots_id` int(8) NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `status` int(2) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reportingslots_log`
--

INSERT INTO `reportingslots_log` (`reportingslots_id`, `from_time`, `to_time`, `status`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, '12:00:00', '03:59:00', 1, 2, 'DELETE', 1, '2018-11-12 21:15:36', 1, '2018-11-12 21:15:36'),
(2, '01:00:00', '12:59:00', 1, 1, 'EDIT', 1, '2018-11-12 21:16:35', 1, '2018-11-12 21:16:35');

-- --------------------------------------------------------

--
-- Table structure for table `residence_details`
--

CREATE TABLE `residence_details` (
  `residence_details_id` int(8) NOT NULL,
  `residence_name` varchar(128) NOT NULL,
  `residence_id` varchar(16) NOT NULL,
  `status` int(2) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `residence_details`
--

INSERT INTO `residence_details` (`residence_details_id`, `residence_name`, `residence_id`, `status`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(1, 'House', 'HID', 1, 1, '2018-11-12 17:39:44', 1, '2018-11-12 18:47:44'),
(2, 'Transport', 'trasID', 1, 1, '2018-11-12 17:40:11', 1, '2018-11-12 18:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `residence_details_log`
--

CREATE TABLE `residence_details_log` (
  `residence_details_id` int(8) NOT NULL,
  `residence_name` varchar(128) NOT NULL,
  `residence_id` varchar(16) NOT NULL,
  `status` int(2) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `residence_details_log`
--

INSERT INTO `residence_details_log` (`residence_details_id`, `residence_name`, `residence_id`, `status`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(1, 'House', 'HID', 1, 1, 'EDIT', 1, '2018-11-12 17:33:04', 1, '2018-11-12 17:33:04'),
(1, 'House1', 'HID1', 1, 1, 'EDIT', 1, '2018-11-12 17:33:28', 1, '2018-11-12 17:33:28'),
(2, 'House2', 'HID2', 1, 2, 'DELETE', 1, '2018-11-12 17:36:08', 1, '2018-11-12 17:36:08'),
(2, 'House2', 'HID2', 1, 2, 'DELETE', 1, '2018-11-12 17:39:53', 1, '2018-11-12 17:39:53'),
(2, 'House3', 'hid3', 1, 1, 'EDIT', 1, '2018-11-12 18:29:40', 1, '2018-11-12 18:29:40'),
(2, 'House3', 'HID3', 1, 1, 'EDIT', 1, '2018-11-12 18:30:19', 1, '2018-11-12 18:30:19'),
(1, 'House1', 'HID1', 1, 1, 'EDIT', 1, '2018-11-12 18:47:44', 1, '2018-11-12 18:47:44');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(8) NOT NULL,
  `role_name` varchar(128) NOT NULL,
  `status` int(2) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`, `status`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(1, 'admin', 1, 1, '2018-11-13 00:00:00', 1, '2018-11-13 00:00:00'),
(2, 'staff', 1, 1, '2018-11-13 00:00:00', 1, '2018-11-13 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `standerd_deductions`
--

CREATE TABLE `standerd_deductions` (
  `std_deduct_id` int(11) NOT NULL,
  `deduct_id` int(11) NOT NULL,
  `deduction_type` enum('1','2') NOT NULL COMMENT '1-percentage,2-rupees',
  `min_limit` int(11) NOT NULL,
  `max_limit` int(11) NOT NULL,
  `deduct_value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject_details`
--

CREATE TABLE `subject_details` (
  `subject_details_id` int(8) NOT NULL,
  `sub_name` varchar(128) NOT NULL,
  `sub_id` varchar(16) NOT NULL,
  `status` int(2) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_details`
--

INSERT INTO `subject_details` (`subject_details_id`, `sub_name`, `sub_id`, `status`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, 'English', 'eng', 1, 1, '2018-10-24 00:00:00', 1, '2018-10-24 00:00:00'),
(3, 'HIndi', 'hindi', 1, 1, '2018-11-12 16:12:01', 1, '2018-11-12 16:12:01'),
(1, 'Telugu', 'tel', 1, 1, '2018-10-24 00:00:00', 1, '2018-10-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subject_details_log`
--

CREATE TABLE `subject_details_log` (
  `subject_details_id` int(8) NOT NULL,
  `sub_name` varchar(128) NOT NULL,
  `sub_id` varchar(16) NOT NULL,
  `status` int(2) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_details_log`
--

INSERT INTO `subject_details_log` (`subject_details_id`, `sub_name`, `sub_id`, `status`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(3, 'HIndi', 'hindi', 1, 1, 'EDIT', 1, '2018-11-12 16:11:48', 1, '2018-11-12 16:11:48');

-- --------------------------------------------------------

--
-- Table structure for table `transport_details`
--

CREATE TABLE `transport_details` (
  `transport_details_id` int(8) NOT NULL,
  `transport_name` varchar(128) NOT NULL,
  `residence_details_id` int(8) NOT NULL,
  `status` int(2) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transport_details`
--

INSERT INTO `transport_details` (`transport_details_id`, `transport_name`, `residence_details_id`, `status`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(1, 'SCHOOL', 1, 1, 1, '2018-11-12 22:39:29', 0, '0000-00-00 00:00:00'),
(2, 'OWN', 1, 1, 1, '2018-11-12 22:39:33', 0, '0000-00-00 00:00:00'),
(3, 'OWN', 2, 1, 1, '2018-11-12 22:39:37', 0, '0000-00-00 00:00:00'),
(4, 'SCHOOL', 2, 1, 1, '2018-11-12 22:39:41', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `transport_details_log`
--

CREATE TABLE `transport_details_log` (
  `transport_details_id` int(8) NOT NULL,
  `transport_name` varchar(128) NOT NULL,
  `residence_details_id` int(8) NOT NULL,
  `status` int(2) NOT NULL,
  `flag` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `Created_By` int(8) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(8) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transport_details_log`
--

INSERT INTO `transport_details_log` (`transport_details_id`, `transport_name`, `residence_details_id`, `status`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(2, 'OWN', 2, 1, 1, 'EDIT', 1, '2018-11-12 19:45:44', 1, '2018-11-12 19:45:44'),
(2, 'OWN', 1, 1, 2, 'DELETE', 1, '2018-11-12 19:47:33', 1, '2018-11-12 19:47:33'),
(3, 'OWN', 2, 1, 2, 'DELETE', 1, '2018-11-12 19:49:37', 1, '2018-11-12 19:49:37'),
(4, 'OWN', 2, 1, 2, 'DELETE', 1, '2018-11-12 19:49:56', 1, '2018-11-12 19:49:56'),
(3, 'OWN1', 2, 1, 2, 'DELETE', 1, '2018-11-12 19:55:02', 1, '2018-11-12 19:55:02'),
(4, 'OWN', 1, 1, 2, 'DELETE', 1, '2018-11-12 20:13:35', 1, '2018-11-12 20:13:35'),
(3, 'OWN1', 1, 1, 2, 'DELETE', 1, '2018-11-12 20:13:56', 1, '2018-11-12 20:13:56'),
(4, 'OWN', 2, 1, 2, 'DELETE', 1, '2018-11-12 22:39:21', 1, '2018-11-12 22:39:21'),
(3, 'OWN', 1, 1, 2, 'DELETE', 1, '2018-11-12 22:39:22', 1, '2018-11-12 22:39:22'),
(3, 'OWN', 1, 1, 2, 'DELETE', 1, '2018-11-12 22:39:23', 1, '2018-11-12 22:39:23'),
(1, 'SCHOOL', 1, 1, 2, 'DELETE', 1, '2018-11-12 22:39:24', 1, '2018-11-12 22:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `emp_id` int(11) NOT NULL,
  `role_id` int(4) NOT NULL,
  `username` varchar(11) NOT NULL,
  `password` varchar(11) NOT NULL,
  `status` int(2) NOT NULL,
  `login_status` int(4) NOT NULL,
  `login_time` datetime NOT NULL,
  `Created_By` int(4) NOT NULL,
  `Created_Datetime` datetime NOT NULL,
  `Updated_By` int(4) NOT NULL,
  `Updated_Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`emp_id`, `role_id`, `username`, `password`, `status`, `login_status`, `login_time`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) VALUES
(1, 1, 'admin', 'admin', 1, 0, '2018-11-07 00:00:00', 1, '2018-11-07 00:00:00', 1, '2018-11-07 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `empextrahoursmaster`
--
ALTER TABLE `empextrahoursmaster`
  ADD PRIMARY KEY (`empextrahours_id`);

--
-- Indexes for table `empextrahoursmaster_log`
--
ALTER TABLE `empextrahoursmaster_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `employee_log`
--
ALTER TABLE `employee_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `empsavingsmaster`
--
ALTER TABLE `empsavingsmaster`
  ADD PRIMARY KEY (`empsavings_id`),
  ADD UNIQUE KEY `emp_bid` (`emp_bid`);

--
-- Indexes for table `empsavingsmaster_log`
--
ALTER TABLE `empsavingsmaster_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `leave_types`
--
ALTER TABLE `leave_types`
  ADD PRIMARY KEY (`leave_type_id`);

--
-- Indexes for table `leave_types_log`
--
ALTER TABLE `leave_types_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `line_of_activity`
--
ALTER TABLE `line_of_activity`
  ADD PRIMARY KEY (`activity_id`,`dept_name`,`dept_id`);

--
-- Indexes for table `line_of_activity_log`
--
ALTER TABLE `line_of_activity_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`payment_type`);

--
-- Indexes for table `payment_types_log`
--
ALTER TABLE `payment_types_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `reportingslots`
--
ALTER TABLE `reportingslots`
  ADD PRIMARY KEY (`from_time`,`to_time`);

--
-- Indexes for table `reportingslots_log`
--
ALTER TABLE `reportingslots_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `residence_details`
--
ALTER TABLE `residence_details`
  ADD PRIMARY KEY (`residence_name`,`residence_id`);

--
-- Indexes for table `residence_details_log`
--
ALTER TABLE `residence_details_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_name`);

--
-- Indexes for table `standerd_deductions`
--
ALTER TABLE `standerd_deductions`
  ADD PRIMARY KEY (`std_deduct_id`);

--
-- Indexes for table `subject_details`
--
ALTER TABLE `subject_details`
  ADD PRIMARY KEY (`sub_name`,`sub_id`);

--
-- Indexes for table `subject_details_log`
--
ALTER TABLE `subject_details_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `transport_details`
--
ALTER TABLE `transport_details`
  ADD PRIMARY KEY (`transport_details_id`,`transport_name`,`residence_details_id`);

--
-- Indexes for table `transport_details_log`
--
ALTER TABLE `transport_details_log`
  ADD PRIMARY KEY (`Created_Datetime`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`emp_id`,`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `standerd_deductions`
--
ALTER TABLE `standerd_deductions`
  MODIFY `std_deduct_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
